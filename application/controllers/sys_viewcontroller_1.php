<?php

class Sys_viewcontroller extends CI_Controller {
    
    function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Visualizzazione prima schermata
     * @author Alessandro Galli
     */
    public function index() 
    {
        //$this->load->view('homegenerale');
        $this->view_home();
    }
    
    
    /**
     * Visualizza homepage
     * 
     * @param type $interface interfaccia utente
     * @author Alessandro Galli 
     */
    public function view_home($interface='desktop')
    {
        $data=array();
            if($this->logged())
            {
                $data['data']['content']=  $this->load_content_home($interface);
                $this->load->view("sys/$interface/base",$data);
            }
            else
            {
                $this->view_login($interface);
            }
    }
    
    /** 
     * Visualizza login
     * 
     * @param type $interface interfaccia utente
     * @author Alessandro Galli
     */
    public function view_login($interface = 'desktop') 
    {
        //se già loggato richiamo la funzione che visualizza la home
        if(logged($this))
        {
            $this->view_home('desktop');
        }
        else
        {
            //carico la schermata di login
            $this->load->view('sys/'.$interface.'/login');
            
        }
    }
    
     /**
     * esecuzione login
     * 
     * @param type $interface interfaccia utente
     * @author Alessandro Galli  
     */
    public function login($interface='desktop'){
        //controllo se è stato inserito uno username
        if($this->input->post('username')!=false)
        {
            $user_data=$this->Sys_model->get_user_login($this->input->post('username'));
            
            //controllo siano stati recuperati dati dal database per quello username
            if(sizeof($user_data)==1)
            {
                //controllo che la password sia corretta
                if($user_data[0]['password']==$this->input->post('password'))
                {
                    //$this->session->set_userdata('email',$user_data[0]['email']);//LUCA: Mi serve per riutilizzare la mail nel minicrm
                    $this->session->set_userdata('username', $this->input->post('username'));
                    $this->session->set_userdata('idutente',$user_data[0]['id']);
                    $this->session->set_userdata('userid',$user_data[0]['id']);
                    $this->session->set_userdata('CodiceDocente',$user_data[0]['CodiceDocente']);
                    $this->view_home('desktop');
                }
                else
                {
                    //se la password non è corretta ripropone il login
                    $this->view_login($interface);
                }
            }
            else
            {
                //se l'utente non esiste ripropone il login
                $this->view_login($interface);
            }
        }
        else
        {
            //se non è stato messo username ripropone il login
            $this->view_login($interface);
        }
      
    }
    
    /**
     * Esecuzione logout
     * 
     * @param type $interface interfaccia utente
     * @author Alessandro Galli
     */
    public function logout($interface = 'desktop') {

            
        //$this->session->unset_userdata('username');
        $this->session->sess_destroy();
        $this->view_login($interface);
        
    }
    
    /*
     * Controlla se si è loggati
     * 
     * @return boolean true se loggati
     * @author Alessandro Galli
     */
     public function logged()
    {
            if ($this->session->userdata('username'))
                return true;
            else
                return false;     
    }
    
    
    /*
     * Caricamento Ajax del contenuto della Home
     * @author Alessandro Galli
     */
    public function ajax_load_content_home($interface='desktop')
    {
        $block=$this->load_content_home($interface);
        echo $block;
    }
    
    
    /*
     * Carica il contenuto della home
     * 
     * @param type $interface
     * @return content/home.php
     * @author Alessandro Galli
     */
    public function load_content_home($interface='desktop')
    {      
        $data['data']['block']=array();
        return $this->load->view("sys/$interface/content/home",$data,true);
    }
    
    /**
     *  
     * @param type $annoaccademico
     * @author Alessandro Galli
     */
    public function ajax_load_block_checklist_classi($annoaccademico)
    {
        $block=$this->load_block_checklist_classi($annoaccademico);
        echo $block;
    }
    
    /**
     * Carica il blocco con la checklist delle classi di un certo anno accademico
     * 
     * @param type $annoaccademico
     * @author Alessandro Galli
     */
    public function load_block_checklist_classi($annoaccademico)
    {
        $classi=$this->Sys_model->get_classi($annoaccademico);
        $data['data']['classi']=$classi;
        return $this->load->view("sys/desktop/block/checklist_classi",$data,true);
    }
    
    /**
     * 
     * @param type $docente
     * @param type $materia_selected
     * 
     * @author Alessandro Galli
     */
    public function ajax_load_block_select_materie($docente=null,$materia_selected=null)
    {
        $block=$this->load_block_select_materie($docente);
        echo $block;
    }
    
    /**
     * 
     * @param type $docente docente di cui visualizzare le materie che insegna
     * @param type $materia_selected materia preselezionata
     * @return type
     * @author Alessandro Galli
     * 
     * Ritorna il blocco con la select delle materie
     */
    public function load_block_select_materie($docente=null,$materia_selected=null)
    {
        $materie=$this->Sys_model->get_materie($docente);
        $data['data']['materie']=$materie;
        $data['data']['materia_selected']=$materia_selected;
        return $this->load->view("sys/desktop/block/select_materie",$data,true);
    }
    
    /**
     * 
     * @param type $materia
     * @param type $docente_selected
     * @author Alessandro Galli
     */
    public function ajax_load_block_select_docenti($materia=null,$docente_selected=null)
    {
        $block=$this->load_block_select_docenti($materia);
        echo $block;
    }
    
    /**
     * 
     * @param type $materia materia di cui visualizzare i docenti
     * @param type $docente_selected docente preselezionato
     * @return type
     * @author Alessandro Galli
     * 
     * Ritorna il blocco html con la select dei docenti
     */
    public function load_block_select_docenti($materia=null,$docente_selected=null)
    {
        $docenti=$this->Sys_model->get_docenti($materia);
        $data['data']['docenti']=$docenti;
        $data['data']['docente_selected']=$docente_selected;
        return $this->load->view("sys/desktop/block/select_docenti",$data,true);
    }
    
    
    public function ajax_load_content_presenze_lezione()
    {
        $block=$this->load_content_presenze_lezione();
        echo $block;
    }
    
    /**
     * 
     * @return string
     * @author Luca Giordano
     */
    public function load_content_presenze_lezione()
    {
        $eseguireLogout=false;
        date_default_timezone_set('Europe/Rome');
        $giorno=date('Y-m-d');
        $OrarioAttuale=date("H:i:s");
        //$OrarioAttuale = date("H:i:s",strtotime("-30minutes",strtotime($giorno.' '.$OrarioAttuale)));
        $CodiceDocente=$this->session->userdata('CodiceDocente');
        $margine=30;//margine espresso in secondi (mezz'ora)   
        
        //prendo il registro presente in quanto mi servirà per risalire al numero dell'aula ed all'elenco dei partecipanti
        $data['RegistroPresenze']=$this->Sys_model->get_registro_presenze($giorno,$OrarioAttuale,$CodiceDocente);
        //prendo la lista degli studenti che stanno nel registro presenze prelevato con la riga sopra
        $data['LezionePresente']=false;
        if(count($data['RegistroPresenze'])>0)
        {
            $data['facolta']=$this->Sys_model->get_facolta_registropresenza($data['RegistroPresenze'][0]['codice']);
            $data['corso'] = $this->Sys_model->get_corso_registropresenza($data['RegistroPresenze'][0]['codice']);
            $data['classe']=$this->Sys_model->get_classe_registropresenza($data['RegistroPresenze'][0]['codice']);
            $data['materia']=$this->Sys_model->get_materia_registropresenza($data['RegistroPresenze'][0]['codice']);
            $data['LezionePresente']=true;
            //prelevo la lista degli studenti che partecipano ad un determinato RegistroPresenze
            $data['ListaStudenti']=$this->Sys_model->get_lista_studenti($data['RegistroPresenze'][0]['codice']);

            //recupero le timbrature in base al numero di terminale e al giorno in formato ggmmaa
            $timbrature=$this->Sys_model->getTimbrature($data['RegistroPresenze'][0]['Terminale'],date('dmy'));

            $oraPrevistaInizioLezione=substr($data['RegistroPresenze'][0]['OraPrevistaInizioLezione1'], 11, 8);
            $oraPrevistaFineLezione = substr($data['RegistroPresenze'][0]['OraPrevistaFineLezione1'], 11, 8);
            $OrarioInizioTimbratureAmmmesse=date('H:i:s',strtotime("-30minutes",strtotime(date("Y-m-d").' '.$oraPrevistaInizioLezione)));
            
            //preparo le variabili per il conteggio del numero di timbrate del professore
            $OrarioInizioTimbratureAmmessePerConteggio=date('Hi',strtotime("-30minutes",strtotime(date("Y-m-d").' '.$oraPrevistaInizioLezione)));
            $oraPrevistaInizioLezionePerConteggio=  str_replace(":", "", $oraPrevistaInizioLezione);//elimino i 2 punti
            $oraPrevistaInizioLezionePerConteggio = substr($oraPrevistaInizioLezionePerConteggio,0,4);
            $oraPrevistaFineLezionePerConteggio=  str_replace(":", "", $oraPrevistaFineLezione);//elimino i 2 punti
            $oraPrevistaFineLezionePerConteggio = substr($oraPrevistaFineLezionePerConteggio,0,4);
            
            //recupero il numero di badge del docente
            $rows=$this->Sys_model->get_badge_docente($CodiceDocente); //quindi per prima cosa prelevo il badge associato al docente
            $BadgeDocente = $rows[0]['NumeroMatricola'];//abbiamo assunto che nel campo NumeroMatricola ci sarà il numero del badge docente
            $NumTimbratureDocente = $this->Sys_model->ContaTimbrature($data['RegistroPresenze'][0]['Terminale'],date('dmy'),$BadgeDocente,$OrarioInizioTimbratureAmmessePerConteggio,$oraPrevistaFineLezionePerConteggio);
            
            if($NumTimbratureDocente==1)
            {
                //verifico se il docente ha timbrato          
                $rows=$this->Sys_model->getTimbrature($data['RegistroPresenze'][0]['Terminale'],date("dmy"),$BadgeDocente);
                
                $data['ArrivatoDocente']=false;//per sicurezza dico che il docente non è arrivato
                foreach($rows as $timbratura)
                {
                    $oraBadgeDocente= substr($timbratura['HHMM'], 0,2).':'.substr($timbratura['HHMM'], 2,2).':00';
                    if(($oraBadgeDocente>=$OrarioInizioTimbratureAmmmesse)&&($oraBadgeDocente<=$oraPrevistaFineLezione))
                    {
                        $data['ArrivatoDocente']=true;
                        if(($oraBadgeDocente>$oraPrevistaInizioLezione)&&($oraBadgeDocente<=$oraPrevistaFineLezione))
                            $oraPrevistaInizioLezione=$oraBadgeDocente; //allora vuol dire che per tutti gli studenti che l'ora di inizio prevista è quella del docente
                    }
                }

                //ora che ho le timbrature posso fare un controllo incrociato per capire quali studenti della lista
                //raccolta dal db si sono presentati o meno nel range di tempo (mezzora prima) impostato

                $i=0;
                $data['StudentiEffettivi']=0;
                foreach($data['ListaStudenti'] as $studente)
                {
                    $data['ListaStudenti'][$i]['presente']=false;
                    $fotoStudente=$this->Sys_model->get_foto_studente($studente['codicestudente']);
                    
                    if(count($fotoStudente)>0)
                    {
                        if($fotoStudente[0]['NomeFile']==null)
                            $data['ListaStudenti'][$i]['Path_Foto']= domain_url().'/jdocserver/jdocuniludes/foto/man-icon.png';
                        else
                        {
                            $path_url= $fotoStudente[0]['NomeFile'].".".$fotoStudente[0]['Estensione'];
                            $data['ListaStudenti'][$i]['Path_Foto']= domain_url().'/jdocserver/jdocuniludes/foto/'.$path_url;
                        }
                    }
                    else
                        $data['ListaStudenti'][$i]['Path_Foto']= domain_url().'/jdocserver/jdocuniludes/foto/man-icon.png';
                    
                    foreach($timbrature as $timbratura)
                    {
                        $matricola=$studente['Matricola'];
                        $rows = $this->Sys_model->get_badge_from_matricola($matricola);
                        if(count($rows)!=0)
                        {
                            $BadgeMatch = $rows[0]['badge']; 
                            if($BadgeMatch==$timbratura['NumeroBadge'])
                            {
                                //costruisco la stringa data/ora della timbratura formattata in un modo che mi serve per convertirla
                                //in timestamp che è poi il punto di partenza per sottrarre mezz'ora (che serve come range)
                                $giorno = substr($timbratura['GGMMAA'],0, 2);
                                $mese = substr($timbratura['GGMMAA'], 2, 2);
                                $anno = substr($timbratura['GGMMAA'], 4, 2);

                                //adesso costruisco l'orario formattato con lo stesso metodo precedente
                                $ora = substr($timbratura['HHMM'],0,2);
                                $minuti = substr($timbratura['HHMM'],2,2);
                                $secondi = '00';
                                $oraBadge = $ora.':'.$minuti.':'.$secondi;

                                //costruisco la data dalla quale sono ammesse le timbrature
                                //$OrarioInizioTimbratureAmmmesse=date('H:i:s',strtotime("-30minutes",strtotime(date("Y-m-d").' '.$oraPrevistaInizioLezione)));

                                if(($oraBadge>=$OrarioInizioTimbratureAmmmesse)&&($oraBadge<=$oraPrevistaInizioLezione))
                                {    
                                    $data['ListaStudenti'][$i]['presente']=true;
                                    $data['ListaStudenti'][$i]['ritardo']=false;
                                    $data['StudentiEffettivi']++;
                                }
                                else if(($oraBadge>=$oraPrevistaInizioLezione) && ($oraBadge<=$oraPrevistaFineLezione))
                                {
                                    $data['ListaStudenti'][$i]['presente']=true;
                                    $data['ListaStudenti'][$i]['ritardo']=true;
                                    $data['StudentiEffettivi']++;
                                }
                                
                                else if(($oraBadge<$OrarioInizioTimbratureAmmmesse)||($oraBadge>$oraPrevistaFineLezione))
                                    $data['ListaStudenti'][$i]['presente']=false;
                            }
                        }
                    }
                    $i++;
                }
            }
            if($NumTimbratureDocente==0)
                $data['ArrivatoDocente']=false;
            if($NumTimbratureDocente>1)
                $eseguireLogout=true;
        }
       if($eseguireLogout==false)
            return $this->load->view("sys/desktop/content/presenze_lezione",$data,true);
        else
        {
            $this->session->sess_destroy();
            return 'logout';
        }
    }
    
    
    
    /**
     * @author Alessandro Galli
     */
    public function ajax_load_content_docenti_calendario()
    {
        $block=$this->load_content_docenti_calendario();
        echo $block;
    }
    
    /**
     * 
     * @return html
     * @author Alessandro Galli
     * 
     * Ritorna il content con il calendario del docente loggato
     */
    public function load_content_docenti_calendario()
    {
        $data=array();
        return $this->load->view("sys/desktop/content/docenti_calendario",$data,true);
    }
    
    
    
    
    /**
     * @author Alessandro Galli
     */
    public function ajax_load_content_docenti_presenze()
    {
        $block=$this->load_content_docenti_presenze();
        echo $block;
    }
    
    /**
     * 
     * @return html
     * @author Alessandro Galli
     * 
     * Ritorna il content con la gestione delle presenze da parte del docente loggato
     */
    public function load_content_docenti_presenze()
    {
        $data=array();
        return $this->load->view("sys/desktop/content/docenti_presenze",$data,true);
    }
    
    
    /**
     * @author Alessandro Galli
     */
    public function ajax_load_content_docenti_didattica()
    {
        $block=$this->load_content_docenti_didattica();
        echo $block;
    }
    
    /**
     * 
     * @return html
     * @author Alessandro Galli
     * 
     * Ritorna il content per la gestione della didattica da parte del docente loggato
     */
    public function load_content_docenti_didattica()
    {
        //elenco progetti del docente
        $recordSource=$this->Sys_model->get_record_source_docenti_didattica($this->session->userdata("CodiceDocente"));   
        $data['data']['recordSource']=$recordSource;
        if(count($recordSource)>0)
        {
            //se c'è almeno un progetto registrato per il docente, ne mostro di default il primo
            $block_progetto_docente=$this->load_block_progetto_docente();
        }
        else
        {
            $block_progetto_docente="";
        }
        $data['data']['block']['progetto_docente']=  $block_progetto_docente;
        return $this->load->view("sys/desktop/content/docenti_didattica",$data,true);
    }
    
    /**
     * @author Alessandro Galli
     */
    public function ajax_load_block_progetto_docente()
    {
        $block=$this->load_block_progetto_docente();
        echo $block;
    }
    
    /**
     * 
     * @return html
     * @author Alessandro Galli
     * 
     * Ritorna il blocco html di un singolo progetto del docente
     */
    public function load_block_progetto_docente()
    {
        $data=array();
        return $this->load->view("sys/desktop/block/progetto_docente",$data,true);
    }
    
    /**
     * @author Alessandro Galli
     */
    public function ajax_load_content_docenti_domandeaperte()
    {
        $block=$this->load_content_docenti_domandeaperte();
        echo $block;
    }
    
    /**
     * 
     * @return html
     * @author Alessandro Galli
     * 
     * Ritorna il content per la gestione delle domande aperte da parte del docente loggato
     */
    public function load_content_docenti_domandeaperte()
    {
        $data=array();
        return $this->load->view("sys/desktop/content/docenti_domandeaperte",$data,true);
    }
    
    
    /**
     * @author Alessandro Galli
     */
    public function ajax_load_content_docenti_multiplechoice()
    {
        $block=$this->load_content_docenti_multiplechoice();
        echo $block;
    }
    
    /**
     * 
     * @return html
     * @author Alessandro Galli
     * 
     * Ritorna il content per la gestione delle multiplechoice da parte del docente loggato
     */
    public function load_content_docenti_multiplechoice()
    {
        $data=array();
        return $this->load->view("sys/desktop/content/docenti_multiplechoice",$data,true);
    }
    
    
    /**
     * @author Alessandro Galli
     */
    public function ajax_load_content_docenti_esami_orali()
    {
        $block=$this->load_content_docenti_esami_orali();
        echo $block;
    }
    
    /**
     * 
     * @return html
     * @author Alessandro Galli
     * 
     * Ritorna il content per la getione degli esami orali
     */
    public function load_content_docenti_esami_orali()
    {
        $data=array();
        return $this->load->view("sys/desktop/content/docenti_esami_orali",$data,true);
    }
    
   
    /**
     * @author Luca Giordano
     */
    public function ajax_load_progetto_didattica()
    {
        //recupero le informazioni del docente
        $data['recordSource']=$this->Sys_model->get_record_source_docenti_didattica($this->session->userdata("CodiceDocente"));
        echo json_encode($data);
    }
    
    /**
     * 
     * @param type $CodiceProgettoDidattica
     * @author Luca Giordano
     */
    public function ajax_load_progetto_didattica_parte1($CodiceProgettoDidattica)
    {
        $data['Facolta']=$this->Sys_model->get_facolta_progetto_didattica($this->session->userdata('CodiceDocente'));
        $data['AnniAccademici']=$this->Sys_model->get_anni_accademici();
        $data['AnniDiCorso']=$this->Sys_model->get_anni_di_corso();
        $data['IntestazioniProgettoDidattica']['ElencoDocenti']=$this->Sys_model->get_docente($this->session->userdata('CodiceDocente'));
        $data['IntestazioniProgettoDidattica']['CodiceProgettoDidattica']=$CodiceProgettoDidattica;
        //var_dump($data);
        echo json_encode($data);
    }
    
    /**
     * 
     * @param type $CodiceCorso
     * @author Luca Giordano
     */
    public function ajax_get_materie_intestazioni($CodiceCorso)
    {
        $data=$this->Sys_model->get_materia_intestazioni($CodiceCorso,$this->session->userdata('CodiceDocente'));
        echo json_encode($data);
    }
    
    
    /**
     * Funzione ajax che ritorna l'elenco dei corsi di una determinata materia in base al docente loggato ed al codice facoltà selezionato
     * @param type $CodiceFacolta
     * @author Luca Giordano
     */
    public function ajax_load_block_corso_facolta($CodiceFacolta)
    {
        $data=$this->Sys_model->load_corso_facolta($CodiceFacolta,$this->session->userdata("CodiceDocente"));
        echo json_encode($data);
    }
    
    /**
     * 
     * @param type $CodiceCorso
     * @author Luca Giordano
     */
    public function ajax_load_block_materie_corso($CodiceCorso)
    {
        $data=$this->Sys_model->load_materie_corso($CodiceCorso,$this->session->userdata("CodiceDocente"));
        echo json_encode($data);
    }
    
    /**
     * @author Alessandro Galli
     */
    public function ajax_load_content_festivita_tirocini()
    {
        $block=$this->load_content_festivita_tirocini();
        echo $block;
    }
    
    /**
     * 
     * @return html
     * @author Alessandro Galli
     * 
     * Ritorna il content relativo alla pianificazione dei periodi
     */
    public function load_content_festivita_tirocini()
    {
        $data=array();
        $data['data']['classi']=  $this->Sys_model->get_classi(6);
        return $this->load->view("sys/desktop/content/festivita_tirocini",$data,true);
    }
    
    /**
     * @author Alessandro Galli
     */
    public function ajax_load_content_disponibilita_docenti()
    {
        $block=$this->load_content_disponibilita_docenti();
        echo $block;
    }
    
    /**
     * 
     * @return html
     * @author Alessandro Galli
     * 
     * Ritorna il content pre la pianificazione dei periodi di disponibilità dei docenti 
     */
    public function load_content_disponibilita_docenti()
    {
        $data=array();
        $data['data']['block']['select_docenti']=  $this->load_block_select_docenti();
        return $this->load->view("sys/desktop/content/disponibilita_docenti",$data,true);
    }
    
    /**
     * @author Alessandro Galli
     */
    public function ajax_load_content_calendari()
    {
        $block=$this->load_content_calendari();
        echo $block;
    }
    
    /**
     * 
     * @param type $interface
     * @return content/home.php
     * @author Alessandro Galli
     * 
     * Ritorna il content per la gestione dei calendari e delle lezioni
     */
    public function load_content_calendari($codice_annoaccademico="")
    {      
         $data['data']['anniaccademici']=  $this->Sys_model->get_anniaccademici();
         $data['data']['block']['select_docenti']=$this->load_block_select_docenti();
         $data['data']['block']['select_materie']=$this->load_block_select_materie();
         $data['data']['block']['calendari']="";
        return $this->load->view("sys/desktop/content/calendari",$data,true);
    }
    
    /**
     * @author Alessandro Galli
     * 
     */
    public function ajax_load_block_calendari()
    {
        $block=  $this->load_block_calendari();
        echo $block;
    }
    
    /**
     * 
     * @return html
     * @author Alessandro Galli
     * 
     * Blocco con tutti i calendari risultati da una ricerca partita dal content dei calendari
     */
    public function load_block_calendari()
    {
        $data['data']['calendari']=array();
        $vistacalendari=array();
        $post=$_POST;
        if(array_key_exists('vistacalendari', $post))
        {
            $vistacalendari=$post['vistacalendari'];
        }
        $data['data']['block']=array();
        $classi_selezionate=array();
        if(array_key_exists('classe', $post))
        {
            foreach ($post['classe'] as $key => $value) {
                $classi_selezionate[]=$value;
            }
        }
        $anniaccademici=  $this->Sys_model->get_anniaccademici();
        $codice_annoaccademico=6;
        if(array_key_exists('annoaccademico', $post))
        {
            if($post['annoaccademico']!='')
            {
                $codice_annoaccademico=$post['annoaccademico'];
            }
        }
        $classi=$this->Sys_model->get_classi($codice_annoaccademico);
        foreach ($classi as $key => $classe) {
            $codice_classe=$classe['Codice'];
            if(count($classi_selezionate)>0)
            {
                if(in_array($codice_classe, $classi_selezionate))
                {
                    $data['data']['calendari'][$codice_classe]['descrizione']=$classe['Descrizione'];
                    $parametri['codice_classe']=$codice_classe;
                    $parametri['vistacalendari']=$vistacalendari;
                    //$data['data']['calendari'][$codice_classe]['block']=$this->load_block_calendario($parametri);
                }
            }
            else
            {
                $data['data']['calendari'][$codice_classe]['descrizione']=$classe['Descrizione'];
                $parametri['codice_classe']=$codice_classe;
                $parametri['vistacalendari']=$vistacalendari;
                //$data['data']['calendari'][$codice_classe]['block']=$this->load_block_calendario($parametri);
            }
            
        }
        return $this->load->view("sys/desktop/block/calendari",$data,true);
    }
    
    
    /**
     * 
     * @param type $codice_classe
     * @author Alessandro Galli
     */
    public function ajax_load_block_calendario($codice_classe)
    {
        $block=  $this->load_block_calendario($codice_classe);
        echo $block;
    }
    
    /**
     * 
     * @param int $codice_classe codice della classe di cui si vuole vedere il calendario
     * @return html
     * @author Alessandro Galli
     * 
     * Ritorna il blocco di un singolo calendario(nella visualizzazione settimanel, o mensile, o entrambe) di una classe
     */
    public function load_block_calendario($codice_classe)
    {
        $data=array();
        $parametri=$_POST;
        $vistacalendari=$parametri['vistacalendari'];
        $data['data']['parametri']=$parametri;
        $data['data']['codice_classe']=$codice_classe;
        $data['data']['vistacalendari']=$vistacalendari;
        $mese=$parametri['mese'];
        $codiceannoaccademico=$parametri['annoaccademico'];
        //$data['data']['anno']=  $this->Sys_model->get_anno($codiceannoaccademico,$mese);
        $data['data']['block']['select_docenti']=$this->load_block_select_docenti(null, $parametri['docente']);
        $data['data']['block']['select_materie']=$this->load_block_select_materie(null, $parametri['materia']);
        $eventi_calendario_generale=  $this->Sys_model->get_calendario_generale($codice_classe,$parametri);
        $calendari=array();
        foreach ($eventi_calendario_generale as $key => $evento_calendario_generale) {
            $evento=array();
            $evento['codice']=$evento_calendario_generale['Codice'];
            $evento['titolo']=$evento_calendario_generale['ArgomentoDescrizione'];
            $giorno=  date('Y-m-d', strtotime($evento_calendario_generale['Giorno']));
            $evento['data']=$giorno;
            //$orainizio=  str_replace('1899-12-30 ', '', $evento_calendario_generale['OraInizio']);
            $orainizio= date('H:i', strtotime($evento_calendario_generale['OraInizio']));
            $evento['orainizio']=$orainizio;
            //$orafine=  str_replace('1899-12-30 ', '', $evento_calendario_generale['OraFine']);
            $orafine=  date('H:i', strtotime($evento_calendario_generale['OraFine']));
            $evento['orafine']=$orafine;
            $CodiceTipoCalendario=$evento_calendario_generale['CodiceTipoCalendario'];
            
            $calendari[$CodiceTipoCalendario]['nomecalendario']='';
            $calendari[$CodiceTipoCalendario]['tiporender']='evento';
            $calendari[$CodiceTipoCalendario]['coloreEventi']='#000000';
                
            if($CodiceTipoCalendario==1)
            {
                $calendari[$CodiceTipoCalendario]['nomecalendario']='Lezione';
                $calendari[$CodiceTipoCalendario]['tiporender']='evento';
                $calendari[$CodiceTipoCalendario]['coloreEventi']='#0A539D';
            }
            if($CodiceTipoCalendario==9)
            {
                $calendari[$CodiceTipoCalendario]['nomecalendario']='Vacanza';
                $calendari[$CodiceTipoCalendario]['tiporender']='background';
                $calendari[$CodiceTipoCalendario]['coloreEventi']='lightblue';
            }

            
            $calendari[$CodiceTipoCalendario]['eventi'][]=$evento;
        }
        
        // INDISPONIBILITA' DOCENTE

            $eventi_calendario_generale=  $this->Sys_model->get_indisponibilita_docente($parametri);
            foreach ($eventi_calendario_generale as $key => $evento_calendario_generale) {
                $evento=array();
                $evento['codice']=$evento_calendario_generale['Codice'];
                $evento['titolo']=$evento_calendario_generale['ArgomentoDescrizione'];
                $giorno=  date('Y-m-d', strtotime($evento_calendario_generale['Giorno']));
                $evento['data']=$giorno;
                $orainizio= date('H:i', strtotime($evento_calendario_generale['OraInizio']));
                $evento['orainizio']=$orainizio;
                $orafine= date('H:i', strtotime($evento_calendario_generale['OraFine']));
                $evento['orafine']=$orafine;
                $CodiceTipoCalendario=$evento_calendario_generale['CodiceTipoCalendario'];

                $calendari['indisponibilitadocente']['nomecalendario']='Indisponibilità';
                $calendari['indisponibilitadocente']['tiporender']='background';
                $calendari['indisponibilitadocente']['coloreEventi']='red';
                $calendari['indisponibilitadocente']['eventi'][]=$evento;
            }

            // IMPEGNI DOCENTE
            $eventi_calendario_generale=  $this->Sys_model->get_impegni_docente($codice_classe,$parametri);
            foreach ($eventi_calendario_generale as $key => $evento_calendario_generale) {
                $evento=array();
                $evento['codice']=$evento_calendario_generale['Codice'];
                $evento['titolo']=$evento_calendario_generale['ArgomentoDescrizione'];
                $giorno=  date('Y-m-d', strtotime($evento_calendario_generale['Giorno']));
                $evento['data']=$giorno;
                $orainizio= date('H:i', strtotime($evento_calendario_generale['OraInizio']));
                $evento['orainizio']=$orainizio;
                $orafine= date('H:i', strtotime($evento_calendario_generale['OraFine']));
                $evento['orafine']=$orafine;
                $CodiceTipoCalendario=$evento_calendario_generale['CodiceTipoCalendario'];

                $calendari['impegnidocente']['nomecalendario']='lezioni';
                $calendari['impegnidocente']['tiporender']='evento';
                $calendari['impegnidocente']['coloreEventi']='#bcbcbc';
                $calendari['impegnidocente']['eventi'][]=$evento;
            }
        $data['data']['calendari']=$calendari;
        return $this->load->view("sys/desktop/block/calendario",$data,true);
    }
    
    /**
     * @author Alessandro Galli
     * 
     * Imposta una lezione nel calendario di una classe. parametri nel post
     */
    public function ajax_set_lezione()
    {
        $post=$_POST;
        $parametri=array();
        $parametri['fields']['Giorno']=date('Y-m-d', strtotime($post['start']));
        $parametri['fields']['OraInizio']='1899-12-30 '.date('H:i', strtotime($post['start']));
        $parametri['fields']['OraFine']='1899-12-30 '.date('H:i', strtotime($post['end']));;
        $parametri['fields']['CodiceTipoCalendario']=1;
        $parametri['fields']['CodiceAnnoAccademico']=6;
        $parametri['fields']['CodiceClasse']=$post['classe'];
        $parametri['fields']['CodiceMateria']=$post['materia'];
        $parametri['fields']['CodiceDocente1']=$post['docente'];
        $this->Sys_model->set_evento($parametri);
    }
    
    /**
     * @author Alessandro Galli
     * 
     * Associa un tipo di periodo(vacanze,tirocini,esami ecc) a uno specifico giorno. parametri nel post
     */
    public function ajax_set_periodo()
    {
        $post=$_POST;
        $parametri=array();
        $parametri['fields']['Giorno']=$post['data_evento'];
        $parametri['fields']['OraInizio']='1899-12-30 08:00';
        $parametri['fields']['OraFine']='1899-12-30 20:00';
        $parametri['fields']['CodiceTipoCalendario']=$post['codice_tipocalendario'];
        $parametri['fields']['CodiceAnnoAccademico']=6;
        $parametri['fields']['CodiceClasse']=$post['codice_classe'];
        $this->Sys_model->set_evento($parametri);
    }
    
    /**
     * @author Alessandro Galli
     * 
     * Associa l'indisponibilità di un docente a uno specifico giorno. Parametri nel post
     */
    public function ajax_set_indisponibilita()
    {
        $post=$_POST;
        $parametri=array();
        $parametri['fields']['Giorno']=$post['data_evento'];
        if($post['indisponibilita']=='intera')
        {
            $parametri['fields']['OraInizio']='1899-12-30 08:00';
        }
        if($post['indisponibilita']=='mezza')
        {
            $parametri['fields']['OraInizio']='1899-12-30 13:00';
        }
        $parametri['fields']['OraFine']='1899-12-30 20:00';
        $parametri['fields']['CodiceTipoCalendario']=13;
        $parametri['fields']['CodiceAnnoAccademico']=6;
        $parametri['fields']['CodiceDocente1']=$post['docente'];
        $this->Sys_model->set_evento($parametri);
    }

    
    /**
     * Caricamento ajax della sezione di gestione code
     * 
     * @param type $interface
     * @author Alessandro Galli
     */
    public function ajax_load_content_gestione_code($interface='desktop')
    {
        $block=  $this->load_content_gestione_code();
        echo $block;
    }
    
    
    
    /**
     * Caricamento della sezione di gestione code
     * 
     * @param type $interface
     * @return view della gestione code 
     * @author Alessandro Galli
     */
    public function load_content_gestione_code($interface='desktop')
    {
        $data['data']['block']['block_code']=  $this->load_block_code('gestione_code');
        $data['data']['block']['block_autobatch']=  $this->load_block_autobatch('gestione_code');
        $data['data']['block']['block_visualizzatore']=  $this->load_block_visualizzatore("", "", "", 'desktop');
        $data['data']['settings']=$this->Sys_model->get_settings();
        return $this->load->view("sys/$interface/content/gestione_code", $data, true);
    }
    
    
    public function ajax_load_content_calendario($interface='desktop')
    {
        $block=  $this->load_content_calendario();
        echo $block;
    }
    
    /*
    public function load_content_calendario($interface='desktop')
    {
        $data=array();
        return $this->load->view("sys/$interface/content/calendario", $data, true);
    }*/
    
    /**
     * Funzione che aggiorna i valori di un evento del calendario
     * @author Luca Giordano <l.giordano@about-x.com>
     */
    public function modifica_evento_calendario()
    {
        $post=$_POST;
        $tableid=$post['tableid'];
        $recordid=$post['recordid'];
        $fieldid_data=$post['fieldid_data'];
        $nuova_data=$post['nuova_data'];
        $fieldid_orainizio=$post['fieldid_orainizio'];
        $nuova_orainizio=$post['nuova_orainizio'];
        $fieldid_orafine=$post['fieldid_orafine'];
        $nuova_orafine=$post['nuova_orafine'];
        
        $sql="UPDATE user_".$tableid." SET ".$fieldid_data."='$nuova_data',".$fieldid_orainizio."='$nuova_orainizio',".$fieldid_orafine."='$nuova_orafine' WHERE recordid_ LIKE '$recordid'";
        $this->Sys_model->execute_query($sql);
    }
    
    /*
    public function ajax_load_content_report($interface='desktop')
    {
        $block=  $this->load_content_report();
        echo $block;
    }*/
    
    /*
    public function load_content_report($interface='desktop')
    {
        $data=array();
        return $this->load->view("sys/$interface/content/report", $data, true);
    }*/
    
    /**
     * Caricamento ajax del blocco code
     * 
     * @param String $funzione tipo di funzione(inserimento-modifica-ricerca) da cui si sta richiamando il blocco code
     * @param String $coda_precaricataid id della coda precaricata
     * @author Alessandro Galli
     */
    public function ajax_load_block_code($funzione='',$coda_precaricataid=''){
        $block=  $this->load_block_code($funzione, $coda_precaricataid);
        echo $block;
    }
    
    
    /**
     * Caricamento del blocco code
     * 
     * @param String $funzione tipo di funzione(inserimento-modifica-ricerca) da cui si sta richiamando il blocco code
     * @param String $coda_precaricataid id della coda precaricata
     * @return view del blocco code
     * @author Alessandro Galli
     */
    public function load_block_code($funzione='',$coda_precaricataid=''){
        $lista_code= $this->Sys_model->get_lista_code();
        $data['data']['lista_code']=$lista_code;
        if($funzione=='gestione_code')
        {
            $data['data']['block_upload_files']=$this->load_block_upload_files($funzione);
        }
        $data['data']['funzione']=$funzione;
        $data['data']['coda_precaricataid']=$coda_precaricataid;
        return $this->load->view('sys/desktop/block/code',$data, TRUE);
        
    }
    
    public function load_block_autobatch($funzione='',$coda_precaricataid=''){
        $lista_code= $this->Sys_model->get_lista_autobatch();
        $data['data']['lista_code']=$lista_code;
        $data['data']['funzione']=$funzione;
        $data['data']['coda_precaricataid']=$coda_precaricataid;
        return $this->load->view('sys/desktop/block/autobatch',$data, TRUE);
    }
    
    
    public function ajax_load_block_visualizzatore($cartella,$nomefile,$extension,$recordid="",$interface='desktop'){
        $block=  $this->load_block_visualizzatore($cartella, $nomefile, $extension,$recordid, $interface);
        echo $block;
    }
    
    
    public function load_block_visualizzatore($cartella,$nomefile,$extension,$recordid="",$interface='desktop'){
        //$host_url=  str_ireplace("jdocweb/", "", base_url());
        $host_url=domain_url();
        //$host_url=  domain_url();
        $cartella= str_replace("-", "/", $cartella);
        $pos=  strpos($cartella, 'Appl');
        if($pos === false) {
            $link_originale= $host_url."JDocServer/".$cartella.$nomefile;
            $path_originale= "../JDocServer/".$cartella.$nomefile;
        }
        else
        {
            $link_originale= $host_url."Neuteck/docusys/".$cartella.$nomefile;
            $path_originale= "../Neuteck/docusys//".$cartella.$nomefile;
        }
        $extension=strtolower($extension);
        if(($extension=='pdf')||($extension=='jpg')||($extension=='png')||($extension=='mp4'))
        {
            if(file_exists($path_originale))
            {
              $link_visualizzatore=$link_originale;  
            }
            else
            {
                $link_visualizzatore='error';
            }
        }
        else
        {
            $path_test=str_replace(".".$extension, "_preview.pdf", $path_originale);
            if(file_exists($path_test))
            {
                $link_visualizzatore=$host_url.str_replace("../", "", $path_test);
            }
            else
            {
                $path_test=str_replace(".".$extension, ".pdf", $path_originale);
                if(file_exists($path_test))
                {
                    $link_visualizzatore=$host_url.str_replace("../", "", $path_test);
                }
                else
                {
                    $link_visualizzatore='null';
                }
            }
        }

        $data['data']['link_originale']=$link_originale;
        $data['data']['link_visualizzatore']=$link_visualizzatore;
        $data['data']['extension']=  $extension;
        $data['data']['cartella']=$cartella;
        $data['data']['nomefile']=$nomefile;
        $data['data']['recordid']=$recordid;
        $data['data']['settings']=  $this->Sys_model->get_settings();
        return $this->load->view('sys/desktop/block/visualizzatore',$data, TRUE);
    }
    
    
    public function ajax_crea_coda($nomecoda="null"){
        $userid=$this->session->userdata('userid');
        $rows=$this->Sys_model->select("SELECT firstname,folder_serverside FROM sys_user WHERE id=$userid");
        if($nomecoda=='null')
        {
        $firstname=$rows[0]['firstname'];
        $nomecoda=$firstname.'_'.date('d_m_Y_H_s');
        }
        $this->Sys_model->crea_coda($nomecoda);
        echo $nomecoda;
        
    }
    
    
    public function ajax_load_block_lista_files($funzione,$tipo,$idcoda='sys_batch_temp'){
        echo $this->load_block_lista_files($funzione,$tipo,$idcoda);
    }
    
    
    /**
     *  
     * @param type $funzione
     * @param type $originefiles
     * @param type $idcoda
     * @param type $tableid
     * @param type $recordid
     * @param type $interface
     * @return type
     */
    public function load_block_lista_files($funzione,$originefiles='allegati',$idcoda=null,$tableid=null,$recordid=null,$interface='desktop'){
        if($funzione=='scheda')
        {
            $funzione='modifica';
        }
        if($originefiles=='coda')
        {
            if($idcoda!=null)
            {
                $files_coda=  $this->Sys_model->get_files_coda($idcoda);
            }
            else
            {
              $files_coda=array();  
            }
            $data['data']['files']=$files_coda;
        }
        if($originefiles=='autobatch')
        {
            if($idcoda!=null)
            {
                $files_coda=  $this->Sys_model->get_files_autobatch($idcoda);
            }
            else
            {
              $files_coda=array();  
            }
            $data['data']['files']=$files_coda;
        }
        if($originefiles=='allegati')
        {
            if((($tableid!=null)&&($recordid!=null))&&($recordid!='null'))
            {
            $rows= $this->Sys_model->get_allegati($tableid, $recordid);
            }
            else
            {
                $rows=array();
            }
             $data['data']['files']=$rows; 
        }
        $data['data']['originefiles']=$originefiles;
        $data['data']['funzione']=$funzione;
        $data['data']['idcoda']=$idcoda;
        $data['data']['tableid']=$tableid;
        $data['data']['recordid']=$recordid;

        return $this->load->view('sys/desktop/block/lista_files',$data, TRUE);
    }
    
    
    public function load_block_upload_files($funzione='',$popuplvl_new=0){
        //raccolgo tutti i record della sys_batch_temp di un determinato idutente
        $userid=$this->session->userdata('userid');
        $batchid="sys_batch_temp_$userid"."_"."$popuplvl_new";
        $results=$this->Sys_model->select("SELECT filename,fileext FROM sys_batch_file WHERE batchid='$batchid' AND creatorid=$userid");
        //per ogni record estratto elimino dal file_system e dal db
        foreach($results as $result)
        {
            $filename=$result['filename'];
            $fileext=$result['fileext'];
            $nomefile=$result['filename'].'.'.$result['fileext'];//compongo il nome del file da eliminare
            $pathcompleta="../JDocServer/batch/$batchid/$nomefile";
            if(($batchid!='')&&($nomefile!='')&&(file_exists($pathcompleta)))
            {
                unlink($pathcompleta);
            }
            //die("ERRORE ELIMINAZIONE FILE: ".$nomefile);//se non riesce ad eliminare il file allora blocca tutta la webapp
            $this->Sys_model->execute_query("DELETE FROM sys_batch_file WHERE batchid='$batchid' AND filename='$filename' AND fileext='$fileext' AND creatorid=$userid");
        }
        $data=null;
        $data['data']['funzione']=$funzione;
        $data['data']['popuplvl']=$popuplvl_new;
        return $this->load->view('sys/desktop/block/upload_files',$data, TRUE);
    }
    
    
    public function uploadfile($popuplvl=0)
    {
        
        $response="NO";
        $userid=$this->session->userdata('idutente');
        $coda="sys_batch_temp_$userid"."_"."$popuplvl";
        
        
        /*if($coda=='sys_batch_temp') //se la coda è quella temporanea
        {
            $upload_dir.='/'.$this->session->userdata('userid').'/'.$popuplvl.'/'; //allora concatena l'id dell'utente
        $this->Sys_model->execute_query("UPDATE sys_batch_file SET crypted='Y' WHERE batchid='sys_batch_temp'");
            
        }*/
        if (!is_dir("../JDocServer/batch")) 
        {
            mkdir("../JDocServer/batch");
        }
        if (!is_dir("../JDocServer/batch/$coda")) 
        {
            mkdir("../JDocServer/batch/$coda");
        }
        
        $upload_dir="../JDocServer/batch/$coda";

        $ext='';
        $files=$_FILES;
        $post=$_POST;
        if(isset($_FILES['allegati'])) //controllo se esiste l'array dei file
        {
            $files = $_FILES['allegati'];
            $i=0;
            //var_dump($files);
            while($i < count($files['tmp_name'])){
                //echo "<br>LA I VALE: ".$i.'<br>';
                
                    
                $ext = pathinfo($files['name'][$i], PATHINFO_EXTENSION); //estraggo l'estensione dal file
                //RECUPERO IL MAX FILE
                $data=$this->Sys_model->select("SELECT MAX(filename) FROM sys_batch_file WHERE batchid='$coda'");
                $maxfile=$data[0]['max'];
                if($maxfile==null)
                    $maxfile=0;
                $maxfile++;
                $veromaxfile=''; //dichiaro una variabile che sarà quella che realmente contiene il vero nome max file
                $numerozeri=8-strlen($maxfile); //ottengo il numeri di zeri da inserire davanti alla stringa
             
                for($k=0;$k<$numerozeri;$k++)
                    $veromaxfile.='0'; //aggiungo N zeri tanti quanti quelli calcolati
                
                 $veromaxfile.=$maxfile; //infine concateno la stringa con numeri di zeri giusti con il numero massimo di file
                 $veromaxfile=$this->Sys_model->generate_filename_coda($coda);
                 $description=$files['name'][$i]; //la descrition che finirà nel database è il nome vero del file compreso di estensione
                 
                 $now=date("Y-m-d H:i:s");  
                 $this->Sys_model->execute_query("INSERT INTO sys_batch_file(batchid,filename,description,creatorid,fileext,creationdate) VALUES('$coda','$veromaxfile','$description','$userid','$ext','$now')");
                 //sleep(1);
                 if(is_uploaded_file($files['tmp_name'][$i])){
                    $path_completa=$upload_dir.'/'.$veromaxfile.'.'.$ext;
                    move_uploaded_file($files['tmp_name'][$i], $path_completa);
                }
                
                 if(!file_exists($upload_dir.$files['name'][$i]))
                    echo "<br>IL FILE NON E' ANCORA STATO SPOSTATO";
                 else{
                    rename($path_completa,$upload_dir.$veromaxfile.'.'.$ext); //rinomino il file con l'id che è inserito nel db
                    echo "<br>CARICAMENTO E RINOMINAZIONE EFFETTUATA CON SUCCESSO";
                 }
                 $i++;
            }
        }
        //$block=  $this->load_block_sys_batch_temp($popuplvl);
    }
    
    public function ajax_load_block_sys_batch_temp($popuplvl=0){
        $block=  $this->load_block_sys_batch_temp($popuplvl);
        echo $block;
    }
    
    
    public function load_block_sys_batch_temp($popuplvl=0){
        $userid=$this->session->userdata('userid');
        $batchid="sys_batch_temp_$userid"."_"."$popuplvl";
        $files_coda=  $this->Sys_model->get_files_coda($batchid,$popuplvl);
        $data['data']['files']=$files_coda;
        $data['data']['originefiles']="coda";
        $data['data']['funzione']='inserimento';
        $data['data']['block_upload_files']="";
        $data['data']['idcoda']='sys_batch_temp';
        $data['data']['tableid']=null;
        $data['data']['recordid']=null;
        return $this->load->view('sys/desktop/block/lista_files',$data, TRUE);
    }
    
    public function ajax_salva_modifiche_allegati($tableid,$recordid)
    {
        $post=$_POST;
        $this->Sys_model->modifica_allegati($post, $tableid,$recordid);
    }
    
    public function ajax_salva_modifiche_coda($codaid)
    {
        $post=$_POST;
        $this->Sys_model->salva_modifiche_coda($codaid, $post);
    }
    
    
    public function ajax_load_content_inserimento($tableid,$interface='desktop')
    {
        $block=$this->load_content_inserimento($tableid, $interface);
        echo $block;
    }
    public function load_content_inserimento($tableid,$interface='desktop')
    {
            $data['data']['schede']['scheda_dati_inserimento']=$this->load_scheda_dati_inserimento($tableid);
            $data['data']['tableid']=$tableid;
            //$data['data']['block']['block_lista_files']=  $this->load_block_lista_files('inserimento', 'allegati');
            $data['data']['block']['block_allegati']=$this->load_block_allegati($tableid, 'null','inserimento',$interface);
            $data['data']['block']['block_code']=$this->load_block_code('inserimento');
            $data['data']['block']['block_autobatch']=  $this->load_block_autobatch('inserimento');
            $data['data']['settings']=$this->Sys_model->get_settings();
            return $this->load->view("sys/$interface/content/inserimento", $data,true);
    }
    
    public function ajax_load_scheda_dati_inserimento($tableid,$interface='desktop')
    {
        $scheda=  $this->load_scheda_dati_inserimento($tableid,$interface);
        echo $scheda;
    }
    
    public function load_scheda_dati_inserimento($tableid,$interface='desktop')
    {
        $data['data']['tableid']=$tableid;
        $data['data']['recordid']='null';
        $data['data']['funzione']="inserimento";
        $data['data']['scheda_container']='null';
        $data['data']['block']['block_dati_labels']=  $this->load_block_dati_labels($tableid, 'null', 'inserimento', 'scheda_dati_inserimento', 'null');
        return $this->load->view("sys/$interface/schede/scheda_dati_inserimento", $data,true);
    }
    
    public function ajax_load_block_dati_labels($tableid,$recordid,$funzione,$scheda_container,$block_container){
        $block=  $this->load_block_dati_labels($tableid,$recordid,$funzione,$scheda_container,$block_container);
        echo $block;
    }
    
    /**
     * Carica il blocco dei campi
     * 
     * @param String $tableid 
     * @param String $funzione
     * @param String $recordid
     * @return blocco html
     * @author Alessandro Galli
     */
    public function load_block_dati_labels($tableid,$recordid='null',$funzione,$scheda_container,$block_container){
        $fields=  $this->Sys_model->get_labels_table($tableid,$funzione,$recordid);
        $data['data']['fields']=$fields;
        $data['data']['tableid']=$tableid;
        $data['data']['funzione']=$funzione;
        $data['data']['recordid']=$recordid;
        $data['data']['scheda_container']=$scheda_container;
        $data['data']['block_container']=$block_container;
        $data['data']['settings']=  $this->Sys_model->get_settings();
        //$data['data']['query']=  $this->Sys_model->get_search_query($tableid, array());
        $block_dati_labels = $this->load->view('sys/desktop/block/block_dati_labels', $data, TRUE);
        return $block_dati_labels;
    }
    
    public function load_block_dati_labels_invio_mail_modulo($tableid,$recordid='null',$funzione,$scheda_container,$block_container){
        $fields=  $this->Sys_model->get_labels_table_invio_mail_modulo($tableid,$funzione,$recordid);
        $data['data']['fields']=$fields;
        $data['data']['tableid']=$tableid;
        $data['data']['funzione']=$funzione;
        $data['data']['recordid']=$recordid;
        $data['data']['scheda_container']=$scheda_container;
        $data['data']['block_container']=$block_container;
        $data['data']['settings']=  $this->Sys_model->get_settings();
        //$data['data']['query']=  $this->Sys_model->get_search_query($tableid, array());
        $block_dati_labels = $this->load->view('sys/desktop/custom/ww/block_dati_labels_invio_mail_modulo', $data, TRUE);
        return $block_dati_labels;
    }
    
    
    public function ajax_load_block_tables_labelcontainer($tableid,$label,$table_index,$table_param,$type,$mastertableid,$funzione,$recordid,$scheda_container,$interface='desktop')
    {
        $blocco=  $this->load_block_tables_labelcontainer($tableid, $label, $table_index, $table_param, $type, $mastertableid, $funzione, $recordid,$scheda_container, $interface);
        echo $blocco;
    }
    
    public function load_block_tables_labelcontainer($tableid,$label,$table_index,$table_param,$type,$mastertableid,$funzione,$recordid,$scheda_container,$interface='desktop'){
        //caricamento records linked
        if($funzione=='inserimento')
        {
           if($type=='master')
           {
               if(($scheda_container=='scheda_dati_inserimento')&&($recordid!='null'))
                {
                    $funzione='modifica';
                }
                $data['data']['table']=  $this->load_block_table($tableid, $label, $table_index, $table_param, $type, $mastertableid, $funzione, $recordid,$scheda_container, $interface);
           }
           if($type=='linked')
           {
               $records=$data['data']['records']=  $this->Sys_model->get_records_linkedtable($tableid, $mastertableid, $recordid);
               $data['data']['records']= $this->load_block_records_linkedtable($tableid,$mastertableid,$recordid,'modifica');
               $recordid='null';
               if(count($records)==0)
               {
                    $data['data']['table']=  $this->load_block_table($tableid, $label, $table_index, $table_param, $type, $mastertableid, $funzione, $recordid,$scheda_container, $interface);
               }
           }
           if($type=='linkedmaster')
           {
               if(($scheda_container=='scheda_dati_inserimento')&&($recordid!='null'))
                {
                    $funzione='modifica';
                }
               $data['data']['records_linkedmaster']= $this->load_block_records_linkedmaster($tableid,$recordid,$mastertableid,$funzione);
           }
        }
        if($funzione=='ricerca')
        {
           if($type=='master')
           {
              $data['data']['table']=  $this->load_block_table($tableid, $label, $table_index, $table_param, $type, $mastertableid, $funzione, $recordid,$scheda_container, $interface);  
           }
           if($type=='linked')
           {
               $data['data']['table']=  $this->load_block_table($tableid, $label, $table_index, $table_param, $type, $mastertableid, $funzione, $recordid,$scheda_container, $interface); 
           }
           if($type=='linkedmaster')
           {
                $label='null';
                $data['data']['table']=  $this->load_block_table($tableid, $label, $table_index, $table_param, $type, $mastertableid, $funzione, $recordid,$scheda_container, $interface);
           }
           if($type=='ocr')
           {
                $label='null';
                $data['data']['table']=  $this->load_block_table($tableid, $label, $table_index, $table_param, $type, $mastertableid, $funzione, $recordid,$scheda_container, $interface);
           }
           if($type=='tutti')
           {
                $label='null';
                $data['data']['table']=  $this->load_block_table($tableid, $label, $table_index, $table_param, $type, $mastertableid, $funzione, $recordid,$scheda_container, $interface);
           }
        }
        if($funzione=='modifica')
        {
           if($type=='master')
           {
                $data['data']['table']=  $this->load_block_table($tableid, $label, $table_index, $table_param, $type, $mastertableid, $funzione, $recordid,$scheda_container, $interface);
           }
           if($type=='linked')
           {
               $records=  $this->Sys_model->get_records_linkedtable($tableid, $mastertableid, $recordid);
               $data['data']['records']= $this->load_block_records_linkedtable($tableid,$mastertableid,$recordid,'modifica');
               $recordid='null';
               if(count($records)==0)
               {
                    $data['data']['table']=  $this->load_block_table($tableid, $label, $table_index, $table_param, $type, $mastertableid, 'inserimento', $recordid,$scheda_container, $interface);
               }
           }
           if($type=='linkedmaster')
           {
                $recordid= $this->Sys_model->get_linkedmaster_recordid($mastertableid,$recordid,$tableid);
                $label='null';
               $data['data']['records_linkedmaster']= $this->load_block_records_linkedmaster($tableid,$recordid,$mastertableid,$funzione);
           }
        }
        if($funzione=='scheda')
        {
            if($type=='master')
           {
               $data['data']['table']=  $this->load_block_table($tableid, $label, $table_index, $table_param, $type, $mastertableid, $funzione, $recordid,$scheda_container, $interface);  
           }
           if($type=='linked')
           {
               $records=  $this->Sys_model->get_records_linkedtable($tableid, $mastertableid, $recordid);
               $data['data']['records']= $this->load_block_records_linkedtable($tableid,$mastertableid,$recordid,'modifica');
               $recordid='null';
               if(count($records)==0)
               {
                    //$data['data']['table']=  $this->load_block_table($tableid, $label, $table_index, $table_param, $type, $mastertableid, 'inserimento', $recordid,$scheda_container, $interface);
               }
           }
           if($type=='linkedmaster')
           {
                $recordid= $this->Sys_model->get_linkedmaster_recordid($mastertableid,$recordid,$tableid);
                $label='null';
                $data['data']['records_linkedmaster']= $this->load_block_records_linkedmaster($tableid,$recordid,$mastertableid,$funzione); 
                
           }
        }

        //caricamento records linkedmaster
        
        /*if(($type=='master')||(($type=='linked')&&(($funzione=='ricerca')||($funzione=='inserimento'))))
        {
            if(($type=='master')&&($scheda_container=='scheda_dati_inserimento')&&($recordid!='null'))
            {
                $funzione='modifica';
            }
           $data['data']['table']=  $this->load_block_table($tableid, $label, $table_index, $table_param, $type, $mastertableid, $funzione, $recordid,$scheda_container, $interface); 
        }*/
        
        $data['data']['tableid']=$tableid;
        $data['data']['recordid']=$recordid;
        $data['data']['funzione']=$funzione;
        $data['data']['type']=$type;
        $data['data']['table_index']=$table_index;
        $data['data']['table_param']=$table_param;
        $data['data']['label']='';
        $data['data']['settings']=  $this->Sys_model->get_settings();
        return $this->load->view('sys/'.$interface.'/block/tables_labelcontainer',$data, TRUE);
    }
    
   
    
    public function load_block_records_linkedtable($linkedtableid,$mastertableid,$masterrecordid,$funzione,$interface='desktop')
    {
        $data['data']['records']=  $this->Sys_model->get_records_linkedtable($linkedtableid, $mastertableid, $masterrecordid);
        $columns=$this->Sys_model->get_colums($linkedtableid, 1);
        $linkedtable='user_'.strtolower($linkedtableid);
        $mastertable='user_'.strtolower($mastertableid);
        $query='';
        $select='SELECT ';
        foreach ($columns as $key => $columns) 
        {
            if($key>0)
            {
                $select=$select.',';
            }
            $select=$select.' '.$columns['id'];
        }
        $query=$select." FROM $linkedtable WHERE recordid".strtolower($mastertableid)."_='$masterrecordid'";
        $data['data']['columns']=$columns;
        $data['data']['linkedtableid']=$linkedtableid;
        $data['data']['funzione']=$funzione;
        $data['data']['block']['datatable_records']=$this->load_block_datatable_records($linkedtableid, 'records_linkedtable',$query);
        return $this->load->view('sys/'.$interface.'/block/records_linkedtable',$data, TRUE);
    }
    
    
    public function load_block_records_linkedmaster($linkedmastertableid,$linkedmaster_recordid,$mastertableid,$funzione,$interface='desktop'){
         //$data['data']['records_linkedmaster']=  $this->Sys_model->get_records_linkedmaster($linkedmastertableid, $mastertableid);
        if(($linkedmaster_recordid!='null')&&($linkedmaster_recordid!=''))
        {
            $data['data']['fissi']=$this->load_block_fissi($linkedmastertableid, $linkedmaster_recordid); 
        }
        else
        {
            $data['data']['fissi']='';
        }
        $data['data']['linkedmastertableid']=$linkedmastertableid;
         $data['data']['funzione']=$funzione;
         $data['data']['linkedmaster_recordid']=$linkedmaster_recordid;
         $data['data']['mastertableid']=$mastertableid;
         return $this->load->view('sys/'.$interface.'/block/records_linkedmaster',$data, TRUE);
    }
    
    
     public function ajax_load_block_table($tableid,$label,$table_index,$table_param,$type,$mastertableid,$funzione,$recordid,$scheda_container,$interface='desktop')
    {
        $blocco=  $this->load_block_table($tableid, $label, $table_index, $table_param, $type, $mastertableid, $funzione, $recordid,$scheda_container, $interface);
        echo $blocco;
    }
    
   
    
    public function load_block_table($tableid,$label,$table_index,$table_param,$type,$mastertableid,$funzione,$recordid,$scheda_container,$interface='desktop'){
        
      /*  if(($recordid!='null')&&($recordid!=null)&&($recordid!='')&&($table_param!='add'))
        {
            if($type=='linkedmaster')
            {
                $dati_contratto=  $this->Sys_model->get_dati_record($mastertableid, $recordid);
                $recordid_linkedmaster=$dati_contratto["recordid".  strtolower($tableid)."_"];
                $data['data']['fields']=$this->load_block_filledfields_table($tableid, $label, $type, 'modifica', $recordid_linkedmaster, $interface);
                $recordid=$recordid_linkedmaster;
            }
            else
            {
            $data['data']['fields']=  $this->load_block_filledfields_table($tableid, $label, $type, $funzione, $recordid, $interface);
            }
            
        }
        else
        {
           $data['data']['fields']=  $this->load_block_emptyfields_table($tableid,$label,$table_index,$table_param,$type,$funzione); 
        }*/
        
       /* if(($funzione=='ricerca')||($funzione=='inserimento'))
        {
            $data['data']['fields']=  $this->load_block_emptyfields_table($tableid,$label,$table_index,$table_param,$type,$funzione); 
        }
        if(($funzione=='scheda')||($funzione=='modifica'))
        {
            $data['data']['fields']=$this->load_block_filledfields_table($tableid, $label, $type, $funzione, $recordid, $interface);
        }*/
        $data['data']['fields']=$this->load_block_fields_table($tableid, $label, $type,$table_index, $recordid, $funzione, $interface);
        $data['data']['tableid']=$tableid;
        $data['data']['recordid']=$recordid;
        $data['data']['funzione']=$funzione;
        $data['data']['type']=$type;
        $data['data']['scheda_container']=$scheda_container;
        $data['data']['table_index']=$table_index;
        $data['data']['table_param']=$table_param;
        $data['data']['label']=$label;
        $data['data']['settings']=  $this->Sys_model->get_settings();
        return $this->load->view('sys/'.$interface.'/block/table',$data, TRUE);
    }
    
    
    public function load_block_fields_table($tableid,$label='null',$type,$table_index,$recordid='null',$funzione='null',$interface='desktop'){
        $data['data']['tableid']=$tableid;
        $data['data']['recordid']=$recordid;
        $data['data']['funzione']=$funzione;
        $data['data']['label']=$label;
        $data['data']['type']=$type;
        $data['data']['table_index']=$table_index;
        $data['data']['table_param']='null';
        
        if($type=='ocr')
        {
            $valuecode[0]['value']='';
            $valuedcode[0]['code']='';
            $data['data']['fields'][0]=array(
                                    "tableid" => $tableid,
                                    "fieldid" => "ocr_",
                                    "fieldtypeid" => "Parola",
                                    "length" => "256",    
                                    "decimalposition" => "0",
                                    "description" => "Testo ocr",
                                    "fieldorder" => "0",
                                    "lookuptableid" => "",
                                    "label" => "Dati",
                                    "valuecode" => $valuecode    
                                );
        }
        else
        {
            $valuecode[0]['value']='';
            $valuedcode[0]['code']='';
            if($type=='tutti')
            {
                $data['data']['fields'][0]=array(
                                    "tableid" => $tableid,
                                    "fieldid" => "tutti",
                                    "fieldtypeid" => "Parola",
                                    "length" => "256",    
                                    "decimalposition" => "0",
                                    "description" => "Cerca ovunque",
                                    "fieldorder" => "0",
                                    "lookuptableid" => "",
                                    "label" => "Dati",
                                    "valuecode" => $valuecode
                                );
            }
            else 
            {
                $data['data']['fields']= $this->Sys_model->get_fields_table($tableid,$label,$recordid,$funzione,$type);  
            }
        }
        
        return $this->load->view('sys/'.$interface.'/block/fields_table',$data, TRUE);
    }
    
    /*public function load_block_filledfields_table($tableid,$label='null',$type,$funzione='null',$recordid='null',$interface='desktop'){
        $data['data']['tableid']=$tableid;
        $data['data']['recordid']=$recordid;
        $data['data']['funzione']=$funzione;
        $data['data']['label']=$label;
        $data['data']['type']=$type;
        $data['data']['table_index']=0;
        $data['data']['table_param']='null';
        
        $data['data']['fields']= $this->Sys_model->get_filledfields_table($tableid,$label,$recordid,$funzione,$type);  
        return $this->load->view('sys/'.$interface.'/block/fields_table',$data, TRUE);
    }*/
    
    
    /**
     * Carica i campi vuoti di una tabella sotto a una particolare label (per ricerca e inserimento)
     * 
     * @param String $tableid tabella principale
     * @param String $label label di cui visualizzare i campi
     * @param Int $table_index evetuale indice della tabella rispetto all'ordinamento
     * @param type $table_param eventuale parametro 
     * @param String $type
     * @param String $funzione
     * @param String $recordid
     * @param type $interface
     * @return type
     */
   /* public function load_block_emptyfields_table($tableid,$label='null',$table_index,$table_param,$type,$funzione='null',$recordid='null',$interface='desktop'){

        $data['data']['tableid']=$tableid;
        $data['data']['recordid']=$recordid;
        $data['data']['funzione']=$funzione;
        $data['data']['label']=$label;
        $data['data']['type']=$type;
        $data['data']['table_index']=$table_index;

            $data['data']['fields']= $this->Sys_model->get_emptyfields_table($tableid,$label,$funzione,$type);

        
        return $this->load->view('sys/'.$interface.'/block/fields_table',$data, TRUE);
    }*/
    
    public function ajax_get_users()
    {
        $users = $this->Sys_model->get_users();
        $items=array();
        foreach ($users as $key => $user) 
        {
            $items[$key]['value'] = $user['id'];
            $items[$key]['label'] = $user['firstname'].' '.$user['lastname'];
            $items[$key]['desc'] = '';
            $userid=$user['id'];
            if(file_exists("../JDocServer/avatar/$userid.jpg"))
            {
                $avatar_url=domain_url()."/JDocServer/avatar/$userid.jpg";
            }
            else
            {
                $avatar_url=  base_url('/assets/images/anon.png');
            }
            $items[$key]['icon'] = $avatar_url;
        }
        $json_items = json_encode($items);
        echo $json_items;
    }
    
    public function ajax_add_lookuptable_item($lookuptableid,$itemdesc)
    {
        $itemdesc=  urldecode($itemdesc);
        $itemcode=$this->Sys_model->add_lookuptable_item($lookuptableid,$itemdesc);
        echo $itemcode;
    }
    public function ajax_get_lookuptable($lookuptableid=null,$fieldid=null,$tableid=null)
    {
        
        $items=$this->Sys_model->get_lookuptable($lookuptableid,$fieldid,$tableid);  
        echo json_encode($items);
    }
    
    
    public function ajax_load_content_ricerca($tableid,$interface='desktop')
    {
        $content=  $this->load_content_ricerca($tableid, $interface);
        echo $content;
    }
    public function load_content_ricerca($tableid,$interface='desktop')
    {

            $data['data']['schede']['scheda_dati_ricerca']=$this->load_scheda_dati_ricerca($tableid);
            $data['data']['block']['block_risultati_ricerca']="";
            $data['data']['tableid']=$tableid;
            $data['data']['settings']=$this->Sys_model->get_settings();
            return $this->load->view('sys/desktop/content/ricerca',$data, TRUE);

    }
    
    public function ajax_load_scheda_dati_ricerca($tableid,$default_view="true",$interface='desktop')
    {
        $scheda=  $this->load_scheda_dati_ricerca($tableid,$default_view,$interface);
        echo $scheda;
    }
    
    public function load_scheda_dati_ricerca($tableid,$default_view="true",$interface='desktop')
    {
        $data['data']['tableid']=$tableid;
        $data['data']['recordid']='null';
        $data['data']['funzione']="ricerca";
        $data['data']['scheda_container']='null';
        $data['data']['block']['block_dati_labels']=  $this->load_block_dati_labels($tableid, 'null', 'ricerca', 'scheda_dati_ricerca', 'null');
        $data['data']['saved_views']=$this->Sys_model->get_saved_views($tableid);
        $data['data']['default_viewid']='';
        $default_viewid=  $this->Sys_model->get_default_viewid($tableid);
        if(($default_viewid!='')&&($default_view=='true'))
        {
            $query=$this->Sys_model->get_view($tableid,$default_viewid);
            $data['data']['default_viewid']=$default_viewid;
        }
        else
        {
            $query=  $this->Sys_model->get_search_query($tableid, array());
        }
        $data['data']['query']=$query;
        $data['data']['userid']=$this->session->userdata('idutente');
        return $this->load->view("sys/$interface/schede/scheda_dati_ricerca", $data,true);
    }
    
    public function ajax_set_default_view($tableid,$viewid)
    {
        $this->Sys_model->set_default_view($tableid,$viewid);
        echo 'ok';
    }
    public function load_block_riepilogo_ricerca(){
        return $this->load->view('sys/desktop/block/riepilogo_ricerca',null, TRUE);
    }
    
    
    public function ajax_load_block_risultati_ricerca($idarchivio=null){
         $block=  $this->load_block_risultati_ricerca($idarchivio);
         echo $block;
     }
     
    public function load_block_risultati_ricerca($tableid=null)
    {
        $post=$_POST;
        $data['data']['archivio']=$tableid;
         $data['data']['block']['datatable_records']=$this->load_block_datatable_records($tableid, 'risultati_ricerca');
         $data['data']['settings']=  $this->Sys_model->get_settings();  
         $fields=$this->Sys_model->get_fields_table($tableid);
         $data['data']['fields']=  $fields;
         //$data['data']['block']['reports_relativi']=  $this->load_block_reports_relativi($tableid,$fields,$post['query']);
         $data['data']['export_list']=$this->Sys_model->get_export_list($tableid);
        return $this->load->view('sys/desktop/block/risultati_ricerca',$data, TRUE);
    }
    
    public function ajax_load_block_datatable_records($tableid,$contesto,$query='')
    {  
        $block=  $this->load_block_datatable_records($tableid,$contesto,$query='');
        echo $block;
    }
    
    public function load_block_datatable_records($tableid,$contesto,$query='')
    {   
        $data['data']['archivio']=$tableid;
        $data['data']['query']=$query;
        $data['data']['columns']=  $this->Sys_model->get_colums($tableid, 1); 
        $data['data']['contesto']=$contesto;
        $data['data']['settings']=  $this->Sys_model->get_settings();
        return $this->load->view('sys/desktop/block/datatable_records',$data, TRUE);
    }
    
    public function ajax_load_block_calendar($tableid='')
    {
        //echo 'aspetta e spera';
        $post=$_POST;
        $query=$post['query'];
        // recupero data di inizio e di fine per il range del calendario
        $datainizio='';
        $datafine='';
        if(array_key_exists('tables', $post)){
                if(array_key_exists($tableid, $post['tables'])){
                        if(array_key_exists('search', $post['tables'][$tableid])){
                                if(array_key_exists('t_1', $post['tables'][$tableid]['search'])){
                                        if(array_key_exists('fields', $post['tables'][$tableid]['search']['t_1']))
                                        {
                                            foreach ($post['tables'][$tableid]['search']['t_1']['fields'] as $key => $field) {
                                                if(array_key_exists('f_0', $field))
                                                {
                                                    if($field['f_0']['type']=='data')
                                                    {
                                                        if($field['f_0']['param']!='')
                                                        {
                                                            if($field['f_0']['param']=='today')
                                                            {
                                                                $today=  date('Y-m-d');
                                                                $datainizio=$today;
                                                                $datafine=$today;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            $datainizio=$field['f_0']['value'][0];
                                                            $datafine=$field['f_0']['value'][1];
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                }
                        }
                }
        }
        $block=  $this->load_block_calendar($tableid,$query,$datainizio,$datafine);
        echo $block;
    }
    
    public function load_block_calendar($tableid,$query,$datainizio,$datafine)
    {
        $calendars=$this->Sys_model->get_calendars($tableid,$query);
        $data['data']['calendars']=$calendars;
        $data['data']['tableid']=$tableid;
        $data['data']['datainizio']=$datainizio;
        $data['data']['datafine']=$datafine;
        return $this->load->view('sys/desktop/block/calendar',$data, TRUE);
    }
    
    public function ajax_search_result($tableid)
    {
       $return= $this->Sys_model->get_ajax_search_result($tableid);
       $json= json_encode( $return );
       echo $json;
        
    }
    
    /**
     * Funzione che cicla la tabella timesheet
     * e per ogni utente prende le attiività pianificate del giorno stesso
     * e le invia per mail usando JDocServices
     */
    public function alert_giornalieri()
    {
        $rows=$this->Sys_model->select("SELECT id,email FROM sys_user ORDER BY id");
        
        //ciclo tutti gli utenti trovati nella query
        foreach ($rows as $row)
        {
            $idutente=$row['id'];
            $email=$row['email'];
            $sql = "SELECT * FROM user_timesheet WHERE idutente='$idutente' AND stato ILIKE 'pianificato' AND datainizio='". date("Y-m-d")."'";
            $righe=$this->Sys_model->select($sql);
            $testoMail = "<h4>ATTIVITA' DEL: ".date("d/m/Y")."</h4><br><br><ul>";
            
            $almenoUnRecord=false;
            foreach($righe as $riga)
            {
               $testoMail .= "<li>".$riga['note']."</li>";
               $almenoUnRecord=true;
            }
            
            $testoMail.="</ul><br><h4>ATTIVITA' IN SCADENZA ENTRO 7 GIORNI:</h4><br><ul>";
            
            //adesso prendo tutti gli eventi in scadenza nello stesso range precedente
            $datascadenza=date('Y-m-d',strtotime(date('Y-m-d')." + 7 days"));
            $righe=$this->Sys_model->select("SELECT * FROM user_timesheet WHERE idutente='$idutente' AND stato ILIKE 'pianificato' AND scadenza<='$datascadenza' ORDER BY scadenza");
            foreach($righe as $riga)
            {
                $testoMail.="<li>".date("d/m/Y",strtotime($riga['scadenza']))."&nbsp;".$riga['note']."</li>";
                $almenoUnRecord = true;
            }
            $testoMail.="</ul>";
            if($almenoUnRecord)
            {
                $command='cd ../JDocServices && JDocServices.exe "inviamail" "'.$testoMail.'" "Alert Giornaliero" "'.$email.';"';
                exec($command);
            }
        }
    }
    
    public function ajax_load_query($tableid=null,$funzione='')
    {
        $query=$this->load_query($_POST, $tableid,$funzione);
        echo $query;
    }
    
    public function load_query($POST,$tableid,$funzione)
    {
        $query='';
        if($funzione=='ricerca')
        {
            $query=  $this->Sys_model->get_search_query($tableid, $POST);
        }
 
        return $query;
    }
    
    public function ajax_load_block_scheda_record($idmaster,$recordid_,$layout='standard_dati',$target='self',$popuplvl_new=0,$funzione='scheda',$interface='desktop'){
        $blocco = $this->load_block_scheda_record($idmaster,$recordid_,$layout,$target,$popuplvl_new,$funzione,$interface);
        echo $blocco;
    }
    
    public function load_block_scheda_record($tableid,$recordid,$layout='standard_dati',$target='self',$popuplvl_new=0,$funzione='scheda',$interface='desktop'){
        if($recordid=='null')
        {   
            $funzione='inserimento';
            $navigatorField='nuovo';
        }
        else
        {
            //$funzione='scheda';
            $navigatorField=$this->Sys_model->get_navigatorField($tableid,$recordid);
        }
        $data['data']['block']['block_dati_labels']=  $this->load_block_dati_labels($tableid, $recordid,$funzione,'scheda_record','null');
        $data['data']['block']['block_fissi']=  $this->load_block_fissi($tableid, $recordid,$interface);
        $data['data']['block']['allegati']=  $this->load_block_allegati($tableid, $recordid,'scheda',$popuplvl_new,$interface);
        $data['data']['block']['block_visualizzatore']=  $this->load_block_visualizzatore("", "", "", $interface);
        $data['data']['block']['block_code']=  $this->load_block_code('modifica');
        $data['data']['block']['block_autobatch']=  $this->load_block_autobatch('inserimento');
        if((($recordid!=null))&&($recordid!='null'))
        {
            $rows= $this->Sys_model->get_allegati($tableid, $recordid);
        }
        else
        {
            $rows=array();
        }
        $data['data']['numfiles']=  count($rows);
        $data['data']['tableid']=$tableid;
        $data['data']['recordid']=$recordid;
        $data['data']['funzione']=$funzione;
        $data['data']['mode']='scheda';
        $data['data']['target']=$target;
        $data['data']['popuplvl']=$popuplvl_new;
        $data['data']['navigatorField']=$navigatorField;
        $data['data']['settings']= $this->Sys_model->get_settings($tableid);
        $data['data']['settings']['tableid']=$tableid;
        /*$user_settings=$this->Sys_model->get_sys_user_settings(1,'layout_scheda',$tableid);
        if($user_settings!=null)
        {
          $data['data']['settings']['layout_scheda']=$user_settings['layout_scheda']  ;  
        }
        else
        {
            $data['data']['settings']['layout_scheda']='standard_dati';
        }*/
        if($tableid=='archiviodocumenti')
        {
            $layout='standard_allegati';
        }
        if($tableid=='contrattimandato')
        {
            $layout='standard_allegati';
        }
        
        $data['data']['settings']['layout_scheda']=$layout;
        
        return $this->load->view('sys/'.$interface.'/schede/scheda_record',$data, TRUE);
    }
    
    public function ajax_load_block_fissi($tableid,$recordid_,$interface='desktop')
    {
        $blocco = $this->load_block_fissi($tableid,$recordid_,$interface);
        echo $blocco;
    }
        
    
    public function load_block_fissi($tableid,$recordid_,$interface='desktop'){
        if($recordid_!='null')
        {
            $fields_fissi= $this->Sys_model->get_fissi($tableid, $recordid_);
            $data['data']['recordid']=$recordid_;
            $data['data']['foto_path']=  $this->Sys_model->get_foto_path($tableid, $recordid_);
            $data['data']['fields']=$fields_fissi; 
            $data['data']['tableid']=$tableid;
            $data['data']['record_info']=  $this->Sys_model->get_record_info($tableid,$recordid_);
             $data['data']['recordid']=$recordid_;
            return $this->load->view('sys/'.$interface.'/block/fissi',$data, TRUE);
        }
        else
        {
            return '';    
        }
        $data['data']['recordid']=$recordid_;
        $data['data']['foto_path']=  $this->Sys_model->get_foto_path($tableid, $recordid_);
        $data['data']['fields']=$fields_fissi; 
        $data['data']['tableid']=$tableid;
        $data['data']['recordid']=$recordid_;
        return $this->load->view('sys/'.$interface.'/block/fissi',$data, TRUE);
        

    }
    
    public function ajax_load_block_allegati($tableid,$recordid,$funzione,$interface='desktop')
    {
        $block=$this->load_block_allegati($tableid,$recordid,$funzione,$interface='desktop');
        echo $block;
    }
    
    public function load_block_allegati($tableid=null,$recordid=null,$funzione,$popuplvl_new=0,$interface='desktop')
    {
        $funzione='inserimento';
        $data['data']['block']['lista_files']=$this->load_block_lista_files($funzione, 'allegati', null, $tableid, $recordid, $interface);
        if($funzione=='scheda')
        {
            $funzione='modifica';
        }
        if(($funzione=='modifica')||($funzione=='inserimento'))
        {
            $data['data']['block_upload_files']=$this->load_block_upload_files($funzione,$popuplvl_new);
        }
        $data['data']['tableid']=$tableid;
        $data['data']['recordid']=$recordid;
        $data['data']['funzione']=$funzione;
        return $this->load->view('sys/'.$interface.'/block/allegati',$data, TRUE);
       
    }
    
    
    public function ajax_load_block_modifica_record($tableid,$recordid){
          echo $this->load_block_modifica_record($tableid, $recordid);
    }
    
    
    public function load_block_modifica_record($tableid,$recordid){
        $userid=$this->session->userdata('idutente');
        $data['data']['block']['block_modifica_record_dati']=  $this->load_block_modifica_record_dati($tableid, $recordid, $userid);
        $data['data']['block']['block_modifica_record_allegati']=  $this->load_block_modifica_record_allegati($tableid, $recordid, $userid);
        $data['data']['tableid']=$tableid;
        $data['data']['recordid']=$recordid;
        return $this->load->view('sys/desktop/block/modifica_record',$data, TRUE);
    }
 
    public function load_block_modifica_record_dati($tableid,$recordid,$userid){
        $data['data']['block']['block_fields']=$this->load_block_fields($tableid,$recordid,'modifica','null');
        //$data['data']['block']['block_gestione_allegati']=  $this->load_block_gestione_allegati('modifica', $tableid, $recordid);
        $data['data']['tableid']=$tableid;
        $data['data']['recordid']=$recordid;
        return $this->load->view('sys/desktop/block/modifica_record_dati',$data, TRUE);
    }
    
    public function load_block_modifica_record_allegati($tableid,$recordid,$userid){
        $data['data']['block']['block_code']=  $this->load_block_code('modifica');
        $data['data']['block']['block_lista_files']=$this->load_block_lista_files('modifica', 'allegati', null, $tableid, $recordid);
        return $this->ajax_load_block_allegatiload->view('sys/desktop/block/modifica_record_allegati',$data, TRUE);
    }
    
    
    public function ajax_salva_modifiche_record($tableid,$recordid='null'){
        $post=$_POST;
        $return_recordid='';
        if(array_key_exists('custom', $post))
        {
            if(array_key_exists('contra', $post['custom']))
            {
                $wwws=$post['custom']['contra']['wwws'];
                if(array_key_exists('azienda', $post['custom']['contra']['destinatario']))
                {
                    $post['custom']['contra']['destinatario_contratto_attuale']='azienda';
                    $return_recordid=$this->Sys_model->salva_modifiche_record($tableid,$recordid, $post);
                    if(($return_recordid!=null))
                    {
                        $this->genera_contratti_azienda($return_recordid,$wwws);
                    }
                    //$return_recordid=$return_recordid.$return.';';
                }
                if(array_key_exists('dipendente', $post['custom']['contra']['destinatario']))
                {
                    $post['custom']['contra']['destinatario_contratto_attuale']='dipendente';
                    $return_recordid=$this->Sys_model->salva_modifiche_record($tableid,$recordid, $post);
                    if(($return_recordid!=null))
                    {
                        $this->genera_contratti_dipendente($return_recordid,$wwws);
                    }
                    //$return_recordid=$return_recordid.$return.';';
                }
            }
        }
        else
        {
            $return_recordid=$this->Sys_model->salva_modifiche_record($tableid,$recordid, $post);
            if($tableid=='assistenzekeysky')
            {
                $this->genera_bollettino_assistenzakeysky($return_recordid);
            }
            //$return_recordid=$return_recordid.$return.';';
        }
        echo $return_recordid;
    }
    
    
    public function ajax_elimina_record($tableid,$recordid){
        $return=$this->Sys_model->elimina_record($tableid, $recordid);
        echo $return;
    }
    
    
    
    public function importacoda($nomecoda="null")
    {
        $userid=$this->session->userdata('userid');
        $rows=$this->Sys_model->select("SELECT firstname,folder_serverside FROM sys_user WHERE id=$userid");
        $path=$rows[0]['folder_serverside'];
        if($nomecoda=='null')
        {
        $firstname=$rows[0]['firstname'];
        $nomecoda=$firstname.'_'.date('d_m_Y_H_s');
        }
        $command='cd ../JDocServices && JDocServices.exe "coda" "'.$nomecoda.'" "'.$path.'" '.$userid.' 0';
        exec($command);
        echo $nomecoda;
    }
    
    public function ajax_get_records_linkedmaster($linkedtableid,$mastertableid){
        $term = $_GET['term'];
        $records_linkedmaster=$this->Sys_model->get_records_linkedmaster($linkedtableid, $mastertableid,$term);
        $records_linkedmaster2=array();
        
        foreach ($records_linkedmaster as $keyrecord => $record) {
            
            $templabel='';
            foreach ($record as $keycolumn => $column) {
                if($keycolumn!='recordid_')
                {
                    $templabel=$templabel." ".$column;
                }
            }
            if(($term!='sys_recent')&&($term!='sys_all'))
            {
                if (strpos(strtolower($templabel),  strtolower($term)) !== false) {
                    $records_linkedmaster2[$keyrecord]['value']=$record['recordid_'];
                    $records_linkedmaster2[$keyrecord]['label']=$templabel;
                    $records_linkedmaster2[$keyrecord]['desc']='';
                    $records_linkedmaster2[$keyrecord]['icon']='';
                }
            }
            else
            {
               $records_linkedmaster2[$keyrecord]['value']=$record['recordid_'];
                $records_linkedmaster2[$keyrecord]['label']=$templabel; 
                $records_linkedmaster2[$keyrecord]['desc']=''; 
                $records_linkedmaster2[$keyrecord]['icon']='';
            }
        }
        
        $json_records_linkedmaster = json_encode($records_linkedmaster2);
        
        echo $json_records_linkedmaster;
    }
    
    public function ajax_get_field_linkedmaster($tableid,$recordid,$linkedmasterid)
    {
        
    }
    
    public function ajax_load_block_mastertable($linkedtableid,$label='null',$type='null',$funzione='null',$recordid='null',$interface='desktop'){
        $block=  $this->load_block_mastertable($linkedtableid, $label, $type,$funzione, $recordid, $interface);
        echo $block;
    }
    
    
    public function load_block_mastertable($linkedtableid,$label='null',$type='null',$funzione='null',$recordid='null',$interface='desktop'){
        //caricamento campi vuoti
        if(($funzione=='ricerca')||($funzione=='inserimento'))
        {
            $data['data']['fields']=  $this->load_block_emptyfields_table($linkedtableid,$label,0,'null','master',$funzione);
        }
        if(($funzione=='scheda')||($funzione=='modifica')||($type=='linkedmaster'))
        {
            $data['data']['fields']=$this->load_block_filledfields_table($linkedtableid,$label,'master',$funzione,$recordid);
        }
        $data['data']['tableid']=$linkedtableid;
        $data['data']['recordid']=$recordid;
        $data['data']['funzione']=$funzione;
        $data['data']['label']=$label;
        $data['data']['type']='master';
        $data['data']['table_index']=0;
        $data['data']['table_param']='null';
        $data['data']['label']=$label;
        return $this->load->view('sys/'.$interface.'/block/table',$data, TRUE);
    }
    
    
    public function ajax_load_block_fields_record_linkedtable($linkedtableid,$label='null',$type='null',$funzione='null',$recordid='null',$interface='desktop'){
        $blocco = $this->load_block_fields_record_linkedtable($linkedtableid, $label, $type,$funzione, $recordid, $interface);
        echo $blocco;
    }
    
    
    public function load_block_fields_record_linkedtable($linkedtableid, $label, $type,$funzione, $recordid, $interface){
        $block_mastertable=  $this->load_block_mastertable($linkedtableid, $label, $type,$funzione, $recordid, $interface);
        $data['data']['block']['block_mastertable']=$block_mastertable;
        $data['data']['linkedtableid']=$linkedtableid;
        $data['data']['recordid']=$recordid;
        return $this->load->view('sys/'.$interface.'/block/fields_record_linkedtable',$data, TRUE);
    }
    
    
    
    //inizio impostazioni
    public function ajax_load_content_impostazioni_preferenze($interface='desktop',$idarchivio='CANDID')
    {
        $block=$this->load_content_impostazioni_preferenze($interface,$idarchivio);
        echo $block;
    }
    
    public function load_content_impostazioni_preferenze($interface='desktop',$idarchivio='CANDID')
    {
        $listaarchivi=$this->Sys_model->get_archive_list();
        $content_data['data']['content_data']['archives_list']=$listaarchivi;
        $content_data['data']['content_data']['idarchivio']=$idarchivio;
        $content_data['data']['content']='impostazioni_preferenze';
        if($interface=='desktop')
        {
             $extraheader='ricerca_base';
        }
        else
        {
            $extraheader=null;
        }
        return $this->load->view("sys/$interface/content/impostazioni_preferenze", $content_data,true);
        //$this->view_generale('sys', $interface, 'impostazioni_preferenze',$content_data,$extraheader);
    }
    
    
    /**
     *Blocco ajax per caricare il macro gruppo preferenze dei campi di ricerca
     * @author Luca Giordano
     */
    public function ajax_load_block_macrogruppo_preferenze($interface='desktop')
    {
        echo $this->load_block_macrogruppo_preferenze($interface);
    }
    
    /**
     * 
     * @param type $interface
     * @return html blocco html da visualizzare
     */
    public function load_block_macrogruppo_preferenze($interface='desktop')
    {
        return $this->load->view('sys/'.$interface.'/block/macrogruppo_preferenze');
    }
    
    
    
    public function ajax_load_block_macrogruppo_layout($interface='desktop')
    {
        echo $this->load_block_macrogruppo_layout($interface);
    }
    public function load_block_macrogruppo_layout($interface='desktop')
    {
        return $this->load->view('sys/'.$interface.'/block/macrogruppo_layout');
    }
    
    
    /**
     * Funzione Ajax per richiamare i campi nelle impostazioni
     * @param type $interface interfaccia
     * @author Luca Giordao
     */
    public function ajax_load_block_impostazioni_campi($interface='desktop')
    {
        echo $this->load_block_impostazioni_campi($interface);
        //echo $blocco;
    }
    
    public function ajax_load_block_impostazioni_campi_archivi($interface='$desktop')
    {
        echo $this->load_block_impostazioni_campi_archivi($interface);
        //echo $blocco;
    }
    
    /**
     * @author Luca Giordano <l.giordano@about-x.com>
     */
    public function ajax_load_block_impostazioni_collega_tabelle()
    {
        echo $this->load_block_impostazioni_collega_tabelle();
    }
    
    /**
     * 
     * @param type $interface interfaccia desktop
     * @return type
     * @author Luca Giordano
     */
    public function load_block_impostazioni_collega_tabelle($interface='desktop')
    {
        $listaarchivi=$this->Sys_model->get_archive_list();
        $data['data']=$listaarchivi;
        return $this->load->view('sys/'.$interface.'/block/impostazioni_collega_tabelle',$data);
    }
    
    /**
     * @author Luca Giordano
     */
    public function ajax_load_block_tipi_campi()
    {
        echo $this->load_block_tipi_campi();
    }
    
    public function load_block_tipi_campi($interface='desktop')
    {
        return $this->load->view('sys/'.$interface.'/block/tipi_campi.php');
    }
    /**
     * 
     * @param string $idarchivio
     * @author Luca Giordano
     */
    public function ajax_load_block_LoadPreferencesNewVersion($idarchivio,$filtro)
    {
       $this->LoadPreferencesNewVersion($idarchivio,$filtro);
    }
    
    public function ajax_load_block_LoadPreferencesLabel($idarchivio,$typeLabel)
    {
        $this->LoadPreferencesLabel($idarchivio,$typeLabel);
    }
    
    public function LoadPreferencesLabel($idarchivio,$typeLabel)
    {
        $data['data']=$this->Sys_model->LoadPreferencesLabel($idarchivio,$typeLabel,$this->session->userdata('idutente'));
        $data['idarchivio'] = $idarchivio;
        return $this->load->view('sys/desktop/block/loaded_preferences_label',$data);
    }
    public function LoadPreferencesNewVersion($idarchivio,$tipopreferenza='campiInserimento')
    {
        if($tipopreferenza=='creazione_campi')
            $tipopreferenza='campiInserimento';
        $data['data']=$this->Sys_model->LoadPreferencesNewVersion($idarchivio,$this->session->userdata('idutente'),$tipopreferenza);
        $data['idarchivio'] = $idarchivio;
        return $this->load->view('sys/desktop/block/loaded_preferences',$data);
    }
    
    public function ajax_get_options_lookuptableid($lookuptableid)
    {
        $data=$this->Sys_model-> get_lookuptable($lookuptableid);
        echo json_encode($data);
    }
            
    public function load_block_impostazioni_campi($interface)
    {
            $listaarchivi=$this->Sys_model->get_archive_list();
            $data['data']=$listaarchivi;
            return $this->load->view('sys/'.$interface.'/block/impostazione_campi',$data);
    }
    
    public function load_block_impostazioni_campi_archivi($interface)
    {
            $listaarchivi=$this->Sys_model->get_archive_list();
            $data['data']=$listaarchivi;
            return $this->load->view('sys/'.$interface.'/block/impostazione_campi_archivi',$data); //in pratica richiamo la pagina che contiene la lista degli archivi
    }
    
    public function ajax_load_block_impostazioni_campi_collega_tabelle($idarchivio,$interface='desktop')
    {
        $data['data']=  $this->Sys_model->get_all_emptyfields($idarchivio);
        return $this->load->view('sys/'.$interface.'/block/campi_preferenze_collega_tabelle',$data);
    }
    
    public function ajax_load_block_campi_preferenze($interface='dekstop',$idarchivio='CANDID'){
        $blocco = $this->load_block_campi_preferenze($interface, $idarchivio);
        echo $blocco;
    }
    
    
    public function load_block_campi_preferenze($interface,$idarchivio)
    {
        $data['data']=  $this->Sys_model->get_all_emptyfields($idarchivio);
        return $this->load->view('sys/'.$interface.'/block/campi_preferenze',$data);
    }
    
    
    public function ajax_load_block_labels($idarchivio)
    {
        $data['data']['labels']=$this->Sys_model->get_label_list($idarchivio);
        $data['data']['idarchivio']=$idarchivio;
        echo $this->load->view('sys/desktop/block/impostazioni_campi_labels',$data);
    }
    
    
    public function get_label_for_option($idarchivio)
    {
        $elenco_label = array();
        $etichette = $this->Sys_model->get_labels($idarchivio);
        foreach($etichette as $etichetta)
            $elenco_label[] = $etichetta;
        echo json_encode($elenco_label);
    }
    
    
    public function ajax_load_block_creazione_labels()
    { echo $this->load->view('sys/desktop/block/block_creazione_label'); }
    
    public function ajax_load_block_creazione_campi()
    { echo $this->load->view('sys/desktop/block/block_creazione_campi'); }
    
    
    public function ajax_create_archive(){
        $post=$_POST;
        $this->Sys_model->create_archive($post);
        $this->create_office_fields($post['idarchivio']);
    }
    
    public function create_new_label()
    {
        $tablename = $_POST['idarchivio'];
        $labelname = str_replace("'", "''",$_POST['textlabel']);
        $this->Sys_model->execute_query("INSERT INTO sys_table_label(tableid,labelname) VALUES('$tablename','$labelname')");
    }
    
    public function deleteOption()
    {
        $itemcode=$_POST['itemcode'];
        $lookuptableid = $_POST['lookuptableid'];
        $sql="DELETE FROM sys_lookup_table_item WHERE itemcode ILIKE '$itemcode' AND lookuptableid ILIKE '$lookuptableid'";
        $this->Sys_model->execute_query($sql);
    }
    
    public function create_office_fields($tableid)
    {
        $sql = "INSERT INTO sys_field(tableid,fieldid,fieldtypeid,length,description,label) VALUES('$tableid','off_data','Data',10,'Data','Office')";
        $this->Sys_model->execute_query($sql);
        $sql = "INSERT INTO sys_field(tableid,fieldid,fieldtypeid,length,description,label) VALUES('$tableid','off_destinatario','Parola',100,'Destinatario','Office')";
        $this->Sys_model->execute_query($sql);
        $sql = "INSERT INTO sys_field(tableid,fieldid,fieldtypeid,length,description,label) VALUES('$tableid','off_mittente','Parola',100,'Mittente','Office')";
        $this->Sys_model->execute_query($sql);
        $sql = "INSERT INTO sys_field(tableid,fieldid,fieldtypeid,length,description,label) VALUES('$tableid','off_oggetto','Parola',100,'Oggetto','Office')";
        $this->Sys_model->execute_query($sql);
        $sql = "INSERT INTO sys_field(tableid,fieldid,fieldtypeid,length,description,label) VALUES('$tableid','off_originale','Parola',255,'Originale','Office')";
        $this->Sys_model->execute_query($sql);
        $sql = "INSERT INTO sys_field(tableid,fieldid,fieldtypeid,length,description,label) VALUES('$tableid','off_testo','Memo',1024,'Testo','Office')";
        $this->Sys_model->execute_query($sql);
        $sql = "INSERT INTO sys_field(tableid,fieldid,fieldtypeid,length,description,label) VALUES('$tableid','off_tipofile','Parola',100,'TipoFile','Office')";
        $this->Sys_model->execute_query($sql);
        $sql = "INSERT INTO sys_field(tableid,fieldid,fieldtypeid,length,description,label) VALUES('$tableid','off_titolo','Parola',100,'Titolo','Office')";
        $this->Sys_model->execute_query($sql);
        $sql = "INSERT INTO sys_field(tableid,fieldid,fieldtypeid,length,description,label) VALUES('$tableid','off_utente','Parola',100,'Utente','Office')";
        $this->Sys_model->execute_query($sql);
        
        //adesso inserisco i campi nella sys_user_order
        $arrayCampiOffice=array('off_data','off_destinatario','off_mittente','off_oggetto','off_originale','off_testo','off_tipofile','off_titolo','off_utente');
        $i=1;
        foreach ($arrayCampiOffice as $valore)
        {
            $sql="INSERT INTO sys_user_order(userid,tableid,fieldid,fieldorder,typepreference) VALUES (1,'$tableid','$valore',".$i.",'campiInserimento')";
            $this->Sys_model->execute_query($sql);
            $i++;
        }
    }
    public function save_creazione_campi()
    {
        $dbdriver=  $this->Sys_model->get_dbdriver();
        $post=$_POST;
        foreach($post['fields'] as $field)
        {
            $fieldid=$field['fieldid'];
            if($field['insertorupdate']=="insert")//ora ciclo tutti i campi di insert
            {
                //inserisco il campo nella sys_field
                $length=0;
                $description = $field['description'];
                $fieldtypeid=$field['tipocampo'];
                $label=$field['label'];
                $idarchivio=$field['idarchivio'];
                $position=$field['campoposition'];
                if($fieldtypeid!='categoria')
                {
                    if($fieldtypeid=='Numero')
                        $length=10;
                    else
                        $length=255;
                    $sql = "INSERT INTO sys_field(tableid,fieldid,fieldtypeid,length,description,label) VALUES('$idarchivio','$fieldid','$fieldtypeid',$length,'$description','$label')";
                    $this->Sys_model->execute_query($sql);

                    //adesso inserisco il campo come colonna nella tabella
                    $type_column='';
                    if($dbdriver=='postgre')
                    {
                        if(($fieldtypeid=='Parola')||($fieldtypeid=='Utente'))
                            $type_column="character varying(255)";
                        if($fieldtypeid=='Data')
                            $type_column='date';
                        if($fieldtypeid=='Numero')
                            $type_column='numeric';
                        if($fieldtypeid=='Memo')
                            $type_column='text';
                        if($fieldtypeid=='Ora')
                            $type_column='time without time zone';
                        if($fieldtypeid=='Seriale')
                            $type_column='serial';
                        $sql='ALTER TABLE user_'.$idarchivio.' ADD "'.$fieldid.'" '.$type_column;
                    }
                    if($dbdriver=='mysql')
                    {
                        if(($fieldtypeid=='Parola')||($fieldtypeid=='Utente'))
                            $type_column="varchar(255)";
                        if($fieldtypeid=='Data')
                            $type_column='date';
                        if($fieldtypeid=='Numero')
                            $type_column='float';
                        if($fieldtypeid=='Memo')
                            $type_column='longtext';
                        if($fieldtypeid=='Ora')
                            $type_column='time';
                        if($fieldtypeid=='Seriale')
                            $type_column='int';
                        $sql='ALTER TABLE user_'.$idarchivio.' ADD '.$fieldid.' '.$type_column;
                    }

                    
                    $this->Sys_model->execute_query($sql);
                }
                else //questo si verifica quando ho inserito una categoria
                {
                    //devo inserire il campo e le options
                    $lookuptableid=$fieldid."_".$idarchivio;
                    $sql="INSERT INTO sys_lookup_table (description,tableid,itemtype,codelen,desclen) VALUES ('$fieldid','$lookuptableid','Carattere',255,255)";
                    $this->Sys_model->execute_query($sql);

                    $sql = "INSERT INTO sys_field(tableid,fieldid,fieldtypeid,length,description,label,lookuptableid) VALUES('$idarchivio','$fieldid','Parola',255,'$description','$label','$lookuptableid')";
                    $this->Sys_model->execute_query($sql);

                    if($dbdriver=='postgre')
                    {
                        $sql='ALTER TABLE user_'.$idarchivio.' ADD "'.$fieldid.'" character varying(255)';
                    }
                    
                    if($dbdriver=='mysql')
                    {
                        $sql='ALTER TABLE user_'.$idarchivio.' ADD '.$fieldid.' varchar(255)';
                    }
                    $this->Sys_model->execute_query($sql);


                    //if(key_exists('options', $field))
                    //{
                        foreach ($field['options'] as $option)
                        {
                            $optionid=$option['id'];
                            $optiondesc=$option['description'];
                            $sql = "INSERT INTO sys_lookup_table_item(lookuptableid,itemcode,itemdesc) VALUES('$lookuptableid','$optionid','$optiondesc')";
                            $this->Sys_model->execute_query($sql);
                        }
                    //}
                }
                $sql="INSERT INTO sys_user_order (userid,tableid,fieldid,fieldorder,typepreference) VALUES(1,'$idarchivio','$fieldid',$position,'campiInserimento')";
                $this->Sys_model->execute_query($sql);
            }
            if($field['insertorupdate']=="update")//ciclo tutti i campi di update
            {
                $descrizione=$field['description'];
                $fieldid=$field['fieldid'];
                $posizione=$field['position'];
                $idarchivio=$field['idarchivio'];
                $label=$field['label'];
                $sql="UPDATE sys_user_order SET fieldorder=".$posizione." WHERE fieldid LIKE '".$fieldid."' AND tableid='$idarchivio' AND typepreference LIKE 'campiInserimento'";
                $this->Sys_model->execute_query($sql);
                
                $sql="UPDATE sys_field SET description='".str_replace("'", "''", $descrizione)."' WHERE fieldid LIKE '$fieldid' AND tableid LIKE '$idarchivio'";
                $this->Sys_model->execute_query($sql);
                
                //adesso ciclo tutte le option
                foreach($field['options'] as $option)
                {
                    $itemcode = str_replace("'", "''", $option['itemcode']);
                    $itemdesc = str_replace("'", "''", $option['itemdesc']);
                    $lookuptableid=  str_replace("'","''",$option['lookuptableid']);
                    if($option['insertorupdate']=='update')
                    {
                        $sql = "UPDATE sys_lookup_table_item SET itemdesc='$itemdesc' WHERE itemcode='$itemcode' AND lookuptableid='$lookuptableid'";
                        $this->Sys_model->execute_query($sql);
                    }
                    if($option['insertorupdate']=='insert')
                    {                        
                        $sql = "INSERT INTO sys_lookup_table_item(lookuptableid,itemcode,itemdesc) VALUES('$lookuptableid','$itemcode','$itemdesc')";
                        $this->Sys_model->execute_query($sql);
                    }
                }
            }
        }
    }
    
    public function check_autobatch(){
        $autobatches=  $this->Sys_model->get_autobatch();
        foreach ($autobatches as $key => $autobatch) {
            $autobatchid=$autobatch['id'];
            $autobatch_files=  $this->Sys_model->get_autobatch_files($autobatchid);
            $continue=false;
            foreach ($autobatch_files as $key => $autobatch_file) {
                //CUSTOM WW
                if($continue)
                {
                    $continue=false;
                    continue;
                }
                if($autobatchid=='contratti')
                {
                    $rif='';
                    $recordid='';
                    $autobatch_fileid=$autobatch_file['fileid'];
                    $ocr=$autobatch_file['ocr'];
                    $startrif_position=  strpos($ocr, "X");
                    $endrif_position=  strpos($ocr, "X", $startrif_position+1);
                    $rif=  substr($ocr, $startrif_position+1, $endrif_position-$startrif_position-1);
                    $rif=str_replace('o', '0', $rif);
                    $rif=str_replace('O', '0', $rif);
                    $rif=str_replace('l', '1', $rif);
                    $len=  strlen($rif);
                    if($len==5)
                    {
                    $recordid=  $this->Sys_model->get_recordid_byrif("CONTRA", $rif);
                    }
                    
                    //echo $recordid;
                    $len=  strlen($recordid);
                    if($len==32)
                    {
                       // if(strpos(strtolower(substr($ocr, 0, 100)), "missione")!==false)
                       // {
                            $autobatch_fileid2=$autobatch_files[$key+1]['fileid'];
                            $this->Sys_model->autobatch_fronteretro($autobatchid, $autobatch_fileid,$autobatch_fileid2, "CONTRA",$recordid);
                            $continue=true;
                       // }
                       // else
                      //  {
                      //      $this->Sys_model->autobatch_insert_file($autobatchid, $autobatch_fileid, "CONTRA",$recordid);
                     //   }
                    }
                    
                }
            }
        }
    }
    
    
    public function set_preferences_tabelle_collegate($mastertableid,$linkedtableid,$stringa)
    {
        //RECUPERO IL TESTO DELLA LABEL LINKED
        $data=$this->Sys_model->select("SELECT description FROM sys_table WHERE id LIKE '$linkedtableid'");
        $testolabel=$data[0]['description'];
        
        //INSERIMENTO TABELLA MASTER
        $sql="INSERT INTO sys_field(tableid,fieldid,fieldtypeid,length,decimalposition,description,fieldorder,lookuptableid,lookupcodedesc,lookupdesclen,label,tablelink)
              VALUES ('$mastertableid','_LINK_" . $linkedtableid ."','Parola',40,0,'$testolabel',1,'',0,0,'$linkedtableid','$linkedtableid')";
        $this->Sys_model->execute_query($sql);
                
        //splitto la stringa
        $campicollegare="";
        $array = split("___", $stringa);
        $i=0;
        foreach($array as $elemento)
        {
            $sottoelementisplit = split("-",$elemento);
            if($i!=0)
                $campicollegare.=",".$sottoelementisplit[0];
            else
                $campicollegare=$sottoelementisplit[0];
            $i++;
        }
        
                //RECUPERO TESTO DELLA LABEL LINKED
        $data=$this->Sys_model->select("SELECT description FROM sys_table WHERE id LIKE '$mastertableid'");
        $testolabel=$data[0]['description'];
        
        //INSERIMENTO TABELLA COLLEGATA
        $sql="INSERT INTO sys_field(tableid,fieldid,fieldtypeid,length,decimalposition,description,fieldorder,lookuptableid,lookupcodedesc,lookupdesclen,label,tablelink,keyfieldlink)
              VALUES ('$linkedtableid','_LINK_" . $mastertableid ."','Parola',40,0,'$testolabel',1,'',0,0,'$mastertableid','$mastertableid','$campicollegare')";
        $this->Sys_model->execute_query($sql);
        
        //INSERIMENTO TABELLA SYS_TABLE_LINK
        $sql="INSERT INTO sys_table_link(tableid,tablelinkid) VALUES('$mastertableid','$linkedtableid')";
        $this->Sys_model->execute_query($sql);
        
        //inserisco il campocollegato nella tabella slave
        $mastertableid=  strtolower($mastertableid);
        $linkedtableid= strtolower($linkedtableid);
        $sql="ALTER TABLE user_".$linkedtableid. " ADD recordid".$mastertableid.'_ character varying(32)';
        $this->Sys_model->execute_query($sql);
    }
    
    
    /**
     * 
     * @param type $campi stringa dei campi esempio lingua&candlingue,assegni&candid
     * @author Luca Giordano
     */
    public function set_preferences($typepreference,$idutente='1')
    {
        //ELIMINO LE PREFERENZE PRECEDENTEMENTE IMPOSTATE
        $post=$_POST;
        $idarchivio=$post['fields'][0]['idarchivio'];
        $this->Sys_model->delete_preferences($idutente,$typepreference,$idarchivio);
        
        //INSERISCO LE NUOVE PREFERENZE
        foreach($_POST['fields'] as $field)
        {
            $fieldid=$field['fieldid'];
            $tableid=$field['idarchivio'];
            $fieldorder = $field['position'];
            $this->Sys_model->set_preferences($idutente,$tableid,$fieldid,$fieldorder,$typepreference);
        }
    }
    
    
    //fine impostazioni
    
    
    public function stampa_elenco($idarchivio='CANDID',$order_key='recordid_',$order_ascdesc='DESC'){
        
        $post=$_POST;
        $campi_ricerca=array();
        foreach($post['tables'] as $keytable => $table)
        {
            if(array_key_exists('search', $table))
            {
                foreach($table['search'] as $keyT => $T)
                {
                    if(array_key_exists('fields', $T))
                    {
                        foreach($T['fields'] as $keyCampo => $campo)
                        {        
                            foreach($campo as $keyF => $F)
                            {
                                $campi_ricerca['tables'][$F['label']][$keyT]['table_param']=$T['table_param'];
                                $campi_ricerca['tables'][$F['label']][$keyT]['fields'][$keyCampo][$keyF]=$F;
                            }

                        }
                    }
                }
            }
            if(array_key_exists('tutti', $table))
            {
                foreach($table['tutti'] as $keyT => $T)
                {
                    if(array_key_exists('fields', $T))
                    {
                        foreach($T['fields'] as $keyCampo => $campo)
                        {        
                            foreach($campo as $keyF => $F)
                            {
                                $campi_ricerca['tables'][$F['label']][$keyT]['table_param']='';
                                $campi_ricerca['tables'][$F['label']][$keyT]['fields'][$keyCampo][$keyF]=$F;
                            }

                        }
                    }
                }
            }
        }
        $data['data']['userid']=  $this->session->userdata('userid');
        $query=$post['query'];
        $result=$this->Sys_model->get_search_result($idarchivio,$query,$order_key,$order_ascdesc);
        $data['data']['risultati']=$result;
        $data['data']['sql']=$query;
        $data['data']['archivio']=$idarchivio;
        $data['data']['campi_ricerca']=$campi_ricerca;
        echo $this->load->view('sys/desktop/stampe/stampa_elenco',$data);
        //var_dump($data['data']['campi_ricerca']['tables']);
    }
    
    
    public function download_elenco($nome_file='')
    {
        $percorso_file="stampe/".$this->session->userdata('userid')."/".$nome_file;
        header("Content-type: Application/octet-stream"); 
        header("Content-Disposition: attachment; filename=$nome_file"); 
        header("Content-Description: Download PHP"); 
        header("Content-Length: ".filesize($percorso_file)); 
        readfile($percorso_file); 
        unlink($percorso_file);
    }
    
    
    public function ajax_load_block_esporta_risultati($tableid,$query='null',$recordid='null'){
        echo $this->load_block_esporta_risultati($tableid,$query,$recordid);
    }
    
    public function load_block_esporta_risultati($tableid,$query,$recordid){
        $data['data']['tableid']=$tableid;
        $data['data']['query']=$query;
        $data['data']['recordid']=$recordid;
        $data['data']['columns']=  $this->Sys_model->get_all_columns($tableid);
        return $this->load->view('sys/desktop/block/esporta_risultati',$data, TRUE);
    }
    
    
    public function esporta_xls(){
        $post=$_POST;
        $tableid=$_POST['tableid'];
        $query=$_POST['query'];
        if(array_key_exists('exportid', $post))
        {
           $exportid=$post['exportid']; 
        }
        else
        {
            $exportid='';
        }
        
        $columns=array();
        //$recordid=$_POST['esporta_recordid'];
       /* if(array_key_exists("columns", $post))
        {
            $columns=$post['columns'];
            $all_columns=$this->Sys_model->get_all_columns($tableid);
            $result=array();

            $tablename='user_'.strtolower($tableid);
            $query=  strstr($query, 'FROM');
            $select='SELECT ';
            foreach ($columns as $key => $column) {
                $column_row=$all_columns[$column];
                if(($column_row['tablelink']!='')&&($column_row['tablelink']!=null)&&($column_row['keyfieldlink']!=null)&&($column_row['keyfieldlink']!=''))
                {
                    $column='recordid'.strtolower($column_row['tablelink']).'_';
                }

                if($select=='SELECT ')
                {
                  $select=$select.$tablename.'.'.$column;  
                }
                else
                {
                  $select=$select.','.$tablename.'.'.$column;  
                }

            }

            $query= $select.' '.$query;
        }*/
        
        if($exportid!='')
        {
            $export_query=  $this->Sys_model->get_export_query($exportid);
            $query="SELECT T2.* 
                    FROM
                    (
                    $query
                    ) AS T1
                     JOIN
                    (
                    $export_query
                    ) AS T2
                    ON T1.recordid_=T2.recordid_
                    ";
            $result=  $this->Sys_model->select($query);
            //$columns
        }
        else
        {
            $result=  $this->Sys_model->select($query);
        }
        

        if(count($result)>0)
        {
            
        }
        /*if($recordid!='null')
        {
            $result=  $this->Sys_model->select($query);
        }*/
        //CUSTOM WORK&WORK
        if($tableid=='CANDID')
        {
            $columns[]='Recordid_';
            $columns[]='Cognome';
            $columns[]='Nome';

            
        }
        $columns=$this->Sys_model->get_result_columns($tableid,$result);
        $result=  $this->Sys_model->get_result_converted($tableid,$columns,$result);
        $data['data']['columns']=$columns;
        $data['data']['records']=$result;
        $this->load->view('sys/desktop/moduli/esporta_xls',$data);
    }
    
    
    /**
     * Funzione che si occupa della generazione dei dati
     * @param type $recordid id del candidato
     * @param type $tipo (flash,cifrato,lgl)
     * @author Luca Giordano
     */
    public function stampa_profilo($recordid,$tipo='flash')
    {
        $nomefile='';
        if($tipo=='flash')
        {
             $data['data']['dati']=  $this->Sys_model->get_dati_stampa_profilo($recordid);
             $data['data']['dati']['foto']=$this->Sys_model->get_foto_path("CANDID", $recordid);
             echo $this->load->view('sys/desktop/stampe/stampa_profilo_flash',$data);  
        }
        if($tipo=='cifrato')
        {
              $data['data']['dati']=  $this->Sys_model->get_dati_stampa_profilo_cifrato($recordid);
              $data['data']['dati']['foto']=$this->Sys_model->get_foto_path("CANDID", $recordid);
              echo $this->load->view('sys/desktop/stampe/stampa_profilo_cifrato',$data); 
        }   
        if($tipo=='ws_lgl')
        {
              $data['data']['dati']=  $this->Sys_model->get_dati_stampa_profilo($recordid);
              $data['data']['dati']['foto']=$this->Sys_model->get_foto_path("CANDID", $recordid);
              echo $this->load->view('sys/desktop/stampe/stampa_profilo_ws_lgl',$data);
              //var_dump($data['data']['dati']);
        }               
        //$this->DownloadProfiloFlash();
    }
    
    public function stampa_vis_contratto($recordid)
    {
        //$data['data']['dati']=  $this->Sys_model->get_dati_stampa_contratto_vis($recordid);
       $data['data']['dati']=array();
        echo $this->load->view('sys/desktop/stampe/stampa_vis_contratto',$data);  
    }
    
    public function stampa_vis_profilorischio($recordid)
    {
        //$data['data']['dati']=  $this->Sys_model->get_dati_stampa_contratto_vis($recordid);
       $data['data']['dati']=array();
        echo $this->load->view('sys/desktop/stampe/stampa_vis_profilorischio',$data);  
    }
    
    
    public function stampa_contratti()
    {
        $contratti_stampare=  $this->Sys_model->get_contratti_stampare();
        foreach ($contratti_stampare as $key => $contratto) {
            $this->genera_LetteraInvioContratti_candidati($contratto['recordid_']);
        }
        
    }
    
    public function genera_contratti_azienda($recordid_contratto,$wwws)
    {

            $this->genera_LetteraInvioContratti_aziende($recordid_contratto,$wwws);
            $this->genera_ContrattoFornituraPersonalePrestito_WW($recordid_contratto,$wwws);
            $this->genera_ContrattoFornituraPersonalePrestito_Azienda($recordid_contratto,$wwws);
        
    }
    
    public function genera_contratti_dipendente($recordid_contratto,$wwws)
    {

            $this->genera_LetteraInvioContratti_candidati($recordid_contratto,$wwws);
            $this->genera_ContrattoAssunzionePersonalePrestito_Dipendente($recordid_contratto,$wwws);
            $this->genera_ContrattoAssunzionePersonalePrestito_WW($recordid_contratto,$wwws);
        
    }
    
    public function genera_LetteraInvioContratti_candidati($recordid_contratto,$wwws)
    {
        $userid=$this->session->userdata('userid');
        $data['data']['userid']=  $userid;
        $data['data']['wwws']=$wwws;
        $data['data']['dati']=  $this->Sys_model->get_dati_stampa_LetteraInvioContratti_candidati($recordid_contratto);
        $this->load->view('sys/desktop/stampe/stampa_LetteraInvioContratti_candidati',$data,true);
        $this->Sys_model->inserisci_allegato("stampe/$userid", "LetteraInvioContratti_candidati","docx", 'CONTRA', $recordid_contratto);
        
    }
    
    public function genera_LetteraInvioContratti_aziende($recordid_contratto,$wwws)
    {
        $userid=$this->session->userdata('userid');
        $data['data']['userid']=  $userid;
        $data['data']['wwws']=$wwws;
        $data['data']['dati']=  $this->Sys_model->get_dati_stampa_LetteraInvioContratti_aziende($recordid_contratto);
        $this->load->view('sys/desktop/stampe/stampa_LetteraInvioContratti_aziende',$data,true);
        $this->Sys_model->inserisci_allegato("stampe/$userid", "LetteraInvioContratti_aziende","docx", 'CONTRA', $recordid_contratto);
        
    }
    
    public function genera_ContrattoFornituraPersonalePrestito_WW($recordid_contratto,$wwws)
    {
        $userid=$this->session->userdata('userid');
        $data['data']['userid']=  $userid;
        $data['data']['wwws']=$wwws;
        $data['data']['dati']=  $this->Sys_model->get_dati_stampa_ContrattoFornituraPersonalePrestito_WW($recordid_contratto);
        $this->load->view('sys/desktop/stampe/stampa_ContrattoFornituraPersonalePrestito_WW',$data,true);
        $this->Sys_model->inserisci_allegato("stampe/$userid", "ContrattoFornituraPersonalePrestito_WW","docx", 'CONTRA', $recordid_contratto);
        
    }
    
    public function genera_ContrattoFornituraPersonalePrestito_Azienda($recordid_contratto,$wwws)
    {
        $userid=$this->session->userdata('userid');
        $data['data']['userid']=  $userid;
        $data['data']['wwws']=$wwws;
        $data['data']['dati']=  $this->Sys_model->get_dati_stampa_ContrattoFornituraPersonalePrestito_Azienda($recordid_contratto);
        $this->load->view('sys/desktop/stampe/stampa_ContrattoFornituraPersonalePrestito_Azienda',$data,true);
        $this->Sys_model->inserisci_allegato("stampe/$userid", "ContrattoFornituraPersonalePrestito_Azienda","docx", 'CONTRA', $recordid_contratto);
        
    }
    
    public function genera_ContrattoAssunzionePersonalePrestito_WW($recordid_contratto,$wwws)
    {
        $userid=$this->session->userdata('userid');
        $data['data']['userid']=  $userid;
        $data['data']['wwws']=$wwws;
        $data['data']['dati']=  $this->Sys_model->get_dati_stampa_ContrattoAssunzionePersonalePrestito_WW($recordid_contratto);
        $this->load->view('sys/desktop/stampe/stampa_ContrattoAssunzionePersonalePrestito_WW',$data,true);
        $this->Sys_model->inserisci_allegato("stampe/$userid", "ContrattoAssunzionePersonalePrestito_WW","docx", 'CONTRA', $recordid_contratto);
        
    }
    
    public function genera_ContrattoAssunzionePersonalePrestito_Dipendente($recordid_contratto,$wwws)
    {
        $userid=$this->session->userdata('userid');
        $data['data']['userid']=  $userid;
        $data['data']['wwws']=$wwws;
        $data['data']['dati']=  $this->Sys_model->get_dati_stampa_ContrattoAssunzionePersonalePrestito_Dipendente($recordid_contratto);
        $this->load->view('sys/desktop/stampe/stampa_ContrattoAssunzionePersonalePrestito_Dipendente',$data,true);
        $this->Sys_model->inserisci_allegato("stampe/$userid", "ContrattoAssunzionePersonalePrestito_Dipendente","docx", 'CONTRA', $recordid_contratto);
        
    }
    
    public function download_contratti($nome_file='')
    {
        $percorso_file="stampe/".$this->session->userdata('userid')."/".$nome_file;
        header("Content-type: Application/octet-stream"); 
        header("Content-Disposition: attachment; filename=$nome_file"); 
        header("Content-Description: Download PHP"); 
        header("Content-Length: ".filesize($percorso_file)); 
        readfile($percorso_file); 
        unlink($percorso_file);
    }
    
    //custom KEYSKY
    public function genera_bollettino_assistenzakeysky($recordid)
    {
        $userid=$this->session->userdata('userid');
        $data['data']['userid']=  $userid;
        $data['data']['dati']=  $this->Sys_model->get_dati_stampa_bollettino_assistenzakeysky($recordid);
        $this->load->view('sys/desktop/stampe/stampa_bollettino_assistenzakeysky',$data,true);
        $this->Sys_model->inserisci_allegato("stampe/$userid", "bollettino_assistenzakeysky","docx", 'assistenzekeysky', $recordid);
        
    }
    
    /**
     * Funzione che si occupa del download del documento word
     * @param stringa $tipo quale documento scaricare
     * @author Luca Giordano
     */
    public function download_profilo($nome_file='')
    {
        $nome_file=  urldecode($nome_file);
        $percorso_file="stampe/".$this->session->userdata('userid')."/".$nome_file;
        header("Content-type: Application/octet-stream"); 
        header("Content-Disposition: attachment; filename=$nome_file"); 
        header("Content-Description: Download PHP"); 
        header("Content-Length: ".filesize($percorso_file)); 
        readfile($percorso_file); 
        unlink($percorso_file);
    }
    
    
    public function ajax_load_block_jpgcrop($recordid,$cartella,$nomefile)
    {
        
        $block=  $this->load_block_jpgcrop($recordid,$cartella,$nomefile);
        echo $block;
    }
    
    public function load_block_jpgcrop($recordid,$cartella,$nomefile,$interface='Desktop')
    {
            
            $percorso_jpg=$this->pdfToJpg($cartella, $nomefile);
            $data['data']['percorso_jpg']="";
            $data['data']['recordid']=$recordid;
             $data['data']['cartella']=$cartella;
            for($x=0;$x<30;$x++)
            {
                
                if(!file_exists($percorso_jpg))
                {
                   $data['data']['percorso_jpg']=$percorso_jpg;
                   break;
                }
                sleep(1);
                
            }
            return $this->load->view('sys/'.$interface.'/block/jpgcrop',$data, TRUE);
    }
    
    
    public function pdfToJpg($cartella,$nomefile)
    {
        $userid=$this->session->userdata('userid');
        $cartella= str_replace("-", "/", $cartella);
        $percorso_pdf='../JDocServer/'.$cartella.'/'.$nomefile;
        if(!file_exists("../JDocServer/pdfjpg"))
        {
            mkdir("../JDocServer/pdfjpg");
        }
        if(!file_exists("../JDocServer/pdfjpg/$userid"))
        {
            mkdir("../JDocServer/pdfjpg/$userid");
        }
        $percorso_jpg="../JDocServer/pdfjpg/$userid";
        $command='cd ../JDocServices && JDocServices.exe "pdfjpg" "'.$percorso_pdf.'" "'.$percorso_jpg.'"';
        
        
        exec($command);
        $base_url=base_url();
        //$host=str_replace("/jdocwebtest","",base_url());
        //$host=str_replace("/JDocWebtest","",$host);
        $host=domain_url();
        $nomefile_jpg= str_replace(".pdf", ".jpg", $nomefile);
        $percorso_jpg=$host."JDocServer/pdfjpg/$userid/".$nomefile_jpg;
        return $percorso_jpg;
    }
    
    
    public function ajax_cropImg($tableid,$recordid){
        $targ_w = $targ_h = 160; 
        $jpeg_quality = 90; 
        $src = $_POST['percorso_jpg']; 
        $img_r = imagecreatefromjpeg($src); 
        $dst_r = ImageCreateTrueColor( $targ_w, $targ_h ); 
        imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'], $targ_w,$targ_h,$_POST['w'],$_POST['h']); 
        //header('Content-type: image/jpeg'); 
        //imagejpeg($dst_r,null,$jpeg_quality); exit;
        //$cartella=  str_replace("-", "/", $cartella);
        $cartella="record_preview/$tableid";
        imagejpeg($dst_r, "../JDocServer/$cartella"."/$recordid.jpg", $jpeg_quality);
    }
   
    public function ajax_stampa_selezionati($tableid,$recordid)
    {
         $post=$_POST;
         $allegati=$post['files']['checked'];
         $command='cd ../JDocServices && JDocServices.exe "mergepdf" "'; 
         $counter=0;
         $allegatimerged='';
         foreach ($allegati as $key => $allegato) {
             $path_allegato=$this->Sys_model->get_path_allegato_pdf($tableid,$recordid,$allegato);
            
             if($counter>0)
             {
                 $command=$command.'|';
             }
             $command=$command.$path_allegato;
             $allegatimerged=$allegatimerged.$allegato;
             $counter++;
         }
        $userid=$this->session->userdata('userid');
        if (!file_exists("../JDocServer/generati/"))
        {
            mkdir("../JDocServer/generati/");
        }
        if (!file_exists("../JDocServer/generati/$userid")) 
        {
            mkdir("../JDocServer/generati/$userid");
        }
        $allegatimerged=$allegatimerged.'.pdf';
         $command=$command.'" "JDocServer\generati\\'.$userid.'\\'.$allegatimerged.'"';
        exec($command);
        $host_url=  domain_url();
        $return= $host_url."JDocServer/generati/$userid/$allegatimerged";
        echo $return;
    }
    
    public function elimina_campo($typepreference)
    {
        $post=$_POST;
        $idarchivio=$post['tableid'];
        $fieldid=$post['fieldid'];
        $lookuptableid=$post['lookuptableid'];
        if($typepreference=='creazione_campi')
        {
            $this->Sys_model->execute_query("DELETE FROM sys_field WHERE fieldid = '%$fieldid%' AND tableid = '%$idarchivio%'");
            $this->Sys_model->execute_query("ALTER TABLE user_$idarchivio DROP $fieldid");

            if(($lookuptableid!=null)&&($lookuptableid!=""))
            {
                $this->Sys_model->execute_query("DELETE FROM sys_lookup_table WHERE tableid = '$lookuptableid'");
                $this->Sys_model->execute_query("DELETE FROM sys_lookup_table_item WHERE lookuptableid = '$lookuptableid'");
            }
            $typepreference='campiInserimento';
        }
        $idutente=$this->session->userdata('idutente');
        $sql="DELETE FROM sys_user_order WHERE tableid = '$idarchivio' AND fieldid = '$fieldid' AND userid=$idutente AND typepreference = '$typepreference'";
        $this->Sys_model->execute_query($sql);
        /*else
        {
           $idutente=$this->session->userdata('idutente');
           $sql="DELETE FROM sys_user_order WHERE tableid ILIKE '$idarchivio' AND fieldid ILIKE '$fieldid' AND userid=$idutente AND typepreference ILIKE '$typepreference'";
           $this->Sys_model->execute_query($sql);
        }*/
    }
    
    public function script_addcolumns_user($tableid)
    {
       $this->Sys_model->script_addcolumns_user($tableid); 
       echo "create!";
    }
    
    public function script_lookup_items()
    {
       $this->Sys_model->script_lookup_items(); 
       echo "create!";
    }
    
    public function script_reset_thumbnail()
    {
       $this->Sys_model->script_reset_thumbnail(); 
       echo "create!";
    }
    
    public function script_trim_ext()
    {
       $this->Sys_model->script_trim_ext(); 
       echo "create!";
    }
    
    
    public function script_sync_timesheet_attivitacommerciali()
    {
       $this->Sys_model->script_sync_timesheet_attivitacommerciali(); 
       echo "sicronizzati!";
    }
    
    public function script_fix_timesheet()
    {
        $this->Sys_model->script_fix_timesheet();
        echo "ok";
    }
    
    public function script_fix_thumbnail()
    {
        $this->Sys_model->script_fix_thumbnail();
        echo "ok";
    }
    
    public function script_durata_assistenze()
    {
        $this->Sys_model->script_durata_assistenze();
        echo "ok";
    }
    
    public function script_durata_timesheet()
    {
        $this->Sys_model->script_durata_timesheet();
        echo "ok";
    }
    
    public function script_totaleimporto_ordiniconsumabili()
    {
        $this->Sys_model->script_totaleimporto_ordiniconsumabili();
        echo "ok";
    }
    
    public function get_new_records($master_tableid)
    {
        //$json = file_get_contents("http://localhost:8822/jdoconlinecv/index.php/sys_viewcontroller/ajax_get_new_records/$master_tableid");
        $json = file_get_contents("http://workandwork.com/OnlineCV/index.php/sys_viewcontroller/ajax_get_new_records/$master_tableid");
        $new_records = json_decode($json,true);
        $this->Sys_model->set_new_records($master_tableid,$new_records);
    }
    
    public function ajax_get_allfields($tableid,$recordid)
    {
        $allfields=$this->Sys_model->get_allfields($tableid,$recordid);
        $json_allfields=  json_encode($allfields);
        echo $json_allfields;
    }
    
    public function ajax_valida_record($tableid,$recordid)
    {
        $this->Sys_model->valida_record($tableid,$recordid);
    }
    
    public function ajax_valida_tutto_record($tableid,$recordid)
    {
        $this->Sys_model->valida_tutto_record($tableid,$recordid);
    }

    public function ajax_load_block_manuale()
    {
        $block=  $this->load_block_manuale();
        echo $block;
    }
    
    public function load_block_manuale()
    {
        $data=array();
        return $this->load->view('sys/desktop/manuale/manuale_generale',$data, TRUE);
    }
    
    public function ajax_load_block_segnalazioni()
    {
        $block=  $this->load_block_segnalazioni();
        echo $block;
    }
    
    public function load_block_segnalazioni()
    {
        $settings=$this->Sys_model->get_settings();
        $data['data']['settore']='software';
        $data['data']['recordid_azienda']=$settings['recordid_azienda'];
        $data['data']['recordid_progetto']=$settings['recordid_progetto'];
        $data['data']['segnalatore']=$settings['firstname']." ".$settings['lastname'];
        return $this->load->view('sys/desktop/block/segnalazioni',$data, TRUE);
    }
    
    public function ajax_invia_segnalazione()
    {
        header("Access-Control-Allow-Methods: POST, GET");
        header("Access-Control-Allow-Origin: *");
       // require_once('phpmailer/class.phpmailer.php');
        $post=$_POST;
        
      
        
        $messaggio=$post['tipo']." - ".$post['priorita']." - ".$post['testo'];
        /*
        $mail = new PHPMailer();
        $mail->Host = "smtp.about-x.info";
        $mail->From = "ilease@about-x.info";
        $mail->Password = "Eraclea2014.24";
        $mail->Port = 587;
        $mail->addAddress("ryuluca@gmail.com");
        $mail->Subject = "SEGNALAZIONE";
        $mail->Body = $messaggio;
        
        if(!$mail->send())
            echo "Mailer Error: ".$mail->ErrorInfo;
        else
            echo 'Message has been sent';*/
        /*$headmail ="From: JDocWeb <ilease@about-x.info>\n";
        $headmail.="Return-Path: ilease@about-x.info\n";
        $headmail.="User-Agent: Php Mail Function\n";
        $headmail.="X-Accept-Language: en-us, en\n";
        $headmail.="MIME-Version: 1.0\n";
        $headmail.="X-Priority: 1 (Highest)\n";
        $headmail.="Content-Type: text/plain; charset=UTF-8; format=flowed\n";
        $headmail.="Content-Transfer-Encoding: 7bit\n";
        
        $ResultMail=mail('ryuluca@gmail.com','test da codice','test da codice',"'".$headmail."'");
        
        echo $ResultMail;*/
        //mail('galli8822@gmail.com','test da codice','test da codice');
        //mail("a.galli@about-x.com", 'segnalazione', $messaggio);
        $this->Sys_model->salva_segnalazione($post);
        
    }
    
    public function ajax_prestampa_scheda_record_completa($tableid,$recordid)
    {
        $data=array();
        $labels=  $this->Sys_model->get_labels_table($tableid,'scheda',$recordid);
        foreach ($labels as $label_key => $label) {
            if($label['type']=='master')
            {
                $fields=$this->Sys_model->get_fields_table($label['tableid'],$label['label'],$recordid,'scheda','master');
                if(count($fields)>0)
                {
                   $data['data']['labels'][$label_key]=$label['label']; 
                }
                
            }
            if($label['type']=='linked')
            {
                $linked_table=$label['tableid'];
                $linked_records=$this->Sys_model->get_records_linkedtable($linked_table, $tableid, $recordid);
                if(count($linked_records)>0)
                {
                    $data['data']['labels'][$label_key]=$label['label'];
                }
                
            }
            if($label['type']=='linkedmaster')
            {
                if(($label['linkedmaster_recordid']!='')&&($label['linkedmaster_recordid']!=null))
                {
                    $data['data']['labels'][$label_key]=$label['label'];
                }
            }
        }

        $data['data']['tableid']=$tableid;
        $data['data']['recordid']=$recordid;
        echo $this->load->view('sys/desktop/stampe/prestampa_scheda_record_completa',$data, TRUE);
    }
    
    public function ajax_stampa_scheda_record_completa($tableid,$recordid)
    {
        $data=array();
        //$fissi=$data['data']['block']['block_fissi']=  $this->load_block_fissi($tableid, $recordid,$interface);
        $post=$_POST;
        $labels=  $this->Sys_model->get_labels_table($tableid,'scheda',$recordid);
        foreach ($post as $key_selected_label => $selected_label) {
           $label= $labels[$key_selected_label];
            if($label['type']=='master')
            {
                $fields=$this->Sys_model->get_fields_table($label['tableid'],$label['label'],$recordid,'all','master');
                if(count($fields)>0)
                {
                   $labels_return[$key_selected_label]= $label;
                   $labels_return[$key_selected_label]['fields']=$fields; 
                }
                
            }
            if($label['type']=='linked')
            {
                $linked_table=$label['tableid'];
                $linked_records=$this->Sys_model->get_records_linkedtable($linked_table, $tableid, $recordid);
                if(count($linked_records)>0)
                {
                    $label_return= $label;
                    foreach ($linked_records as $key => $linked_record) {
                        $linked_recordid=$linked_record['recordid_'];
                        $label_return['records'][$linked_recordid]['recordid']=$linked_recordid;
                        $fields=$this->Sys_model->get_fields_table($label['tableid'],'null',$linked_recordid,'all','linked');
                        $label_return['records'][$linked_recordid]['fields']=$fields;
                    }
                    $labels_return[$key_selected_label]=$label_return;
                }
                
            }
            if($label['type']=='linkedmaster')
            {
                if(($label['linkedmaster_recordid']!='')&&($label['linkedmaster_recordid']!=null))
                {
                    $fields=$this->Sys_model->get_fields_table($label['tableid'],$label['label'],$label['linkedmaster_recordid'],'all','linkedmaster');
                    $labels_return[$key_selected_label]= $label;
                    $labels_return[$key_selected_label]['fields']=$fields;
                }
            }
        }
        $data['labels']=$labels_return;
        echo $this->load->view('sys/desktop/stampe/stampa_scheda_record_completa',$data, TRUE);
    }
    
    public function ajax_save_view($tableid)
    {
        
        $post=$_POST;
        $view_name=$post['view_name'];
        $this->Sys_model->save_view($tableid,$view_name,$post);
    }
    
    public function ajax_view_changed($tableid,$viewid)
    {
        $query=$this->Sys_model->get_view($tableid,$viewid);
        echo $query;
    }
    
    public function ajax_save_report($tableid)
    {
        $post=$_POST;
        $this->Sys_model->save_report($tableid,$post);
    }
    
    public function ajax_load_block_reports_relativi($tableid)
    {
        $post=$_POST;
        $fields=$this->Sys_model->get_fields_table($tableid);
        $block=  $this->load_block_reports_relativi($tableid,$fields,$post['query']);
        echo $block;
    }
    
    public function load_block_reports_relativi($tableid,$fields,$query)
    {
        $data=array();
        $data['data']['block']['reports']=array();
        $data['data']['tableid']=$tableid;
        $data['data']['fields']=$fields;
        $data['data']['userid']=  $userid=$this->session->userdata('userid');
        $reports=  $this->Sys_model->get_reports($tableid,$query);
         foreach ($reports as $key => $report) {
             $data['data']['block']['reports'][]=  $this->load_block_report($report);
         }
        return $this->load->view('sys/desktop/block/reports_relativi',$data, TRUE);
    }
    
    public function ajax_load_block_report($report)
    {
        $block=  $this->load_block_report($report);
        echo $block;
    }
    
    public function load_block_report($report)
    {
        $data=array();
        $layout=$report['layout'];
        $data['data']['report']=$report;
        $data['data']['fieldtype']=$report['fieldtype'];
        return $this->load->view("sys/desktop/report/report_$layout",$data, TRUE);
    }
    
    public function invio_mail_modulo()
    {
        $recordid='null';
        $tableid='CANDID';
        $funzione='inserimento';
        $navigatorField='nuovo';
        $popuplvl_new=1;
        $layout='allargata';
        $interface='desktop';

        $data['data']['block']['block_dati_labels']=  $this->load_block_dati_labels_invio_mail_modulo($tableid, $recordid,$funzione,'scheda_record','null');
        $data['data']['block']['block_fissi']=  '';
        $data['data']['block']['allegati']=  $this->load_block_allegati($tableid, 'null','scheda',1,'desktop');
        $data['data']['block']['block_visualizzatore']=  $this->load_block_visualizzatore("", "", "", 'desktop');
        $data['data']['block']['block_code']=  $this->load_block_code('modifica');
        $data['data']['block']['block_autobatch']=  $this->load_block_autobatch('inserimento');
        if((($recordid!=null))&&($recordid!='null'))
        {
            $rows= $this->Sys_model->get_allegati($tableid, $recordid);
        }
        else
        {
            $rows=array();
        }
        $data['data']['numfiles']=  count($rows);
        $data['data']['tableid']=$tableid;
        $data['data']['recordid']=$recordid;
        $data['data']['funzione']=$funzione;
        $data['data']['mode']='scheda';
        $data['data']['target']='popup';
        $data['data']['popuplvl']=$popuplvl_new;
        $data['data']['navigatorField']=$navigatorField;
        $data['data']['settings']= $this->Sys_model->get_settings($tableid);
        $data['data']['settings']['tableid']=$tableid;

        if($tableid=='archiviodocumenti')
        {
            $layout='standard_allegati';
        }
        if($tableid=='contrattimandato')
        {
            $layout='standard_allegati';
        }
        
        $data['data']['settings']['layout_scheda']=$layout;
        
        echo $this->load->view('sys/'.$interface.'/custom/ww/scheda_record_invio_mail_modulo',$data);
    }
    

}
?>
