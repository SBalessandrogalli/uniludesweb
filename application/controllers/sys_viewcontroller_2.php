<?php

class Sys_viewcontroller extends CI_Controller {
    
    function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Visualizzazione prima schermata
     * @author Alessandro Galli
     */
    public function index() 
    {
        //$this->load->view('homegenerale');
        $this->view_home();
    }
    
    public function ajax_load_block($block_name)
    {
        $arg=$_POST;
        $block=$this->load_block($block_name,$arg);  
        echo $block;
    }
    
    public function load_block($block_name,$arg)
    {
            return call_user_func(array($this, "load_block_".$block_name),$arg);
    }
    /**
     * Visualizza homepage
     * 
     * @param type $interface interfaccia utente
     * @author Alessandro Galli 
     */
    public function view_home($interface='desktop')
    {
        $data=array();
            if($this->logged())
            {
                $data['data']['content']=  $this->load_content_home($interface);
                $codice_docente=  $this->session->userdata('CodiceDocente');
                $userid=$this->session->userdata('userid');
                $data['userid']=$userid;
                $data['superuser']=$this->session->userdata('superuser');
                if($codice_docente  )
                {
                    $data['data']['docente']=$this->Sys_model->get_docente($codice_docente);
                }
                $data['users']=array();
                if($data['superuser'])
                {
                    $data['users']=$this->Sys_model->db_get('utente','*','"CodiceDocente" is not null ','ORDER BY "Nome" asc');
                }
                
                $this->load->view("sys/$interface/base",$data);
            }
            else
            {
                $this->view_login($interface);
            }
    }
    
    public function ajax_load_content_impostazioni()
    {
        $block=  $this->load_content_impostazioni();
        echo $block;
    }
    
    public function load_content_impostazioni()
    {
        $data=array();
        return $this->load->view("sys/desktop/content/impostazioni",$data,true);
    }
    public function presenze($interface='desktop')
    {
        $data=array();
            if($this->logged())
            {
                $content=  $this->load_content_presenze_lezione($interface);
                if($content=='logout')
                {
                    $this->view_login($interface);
                }
                else
                {
                    $data['data']['content']=$content;
                    $codice_docente=  $this->session->userdata('CodiceDocente');
                    $userid=$this->session->userdata('userid');
                    $data['userid']=$userid;
                    if($codice_docente  )
                    {
                        $data['data']['docente']=$this->Sys_model->get_docente($codice_docente);
                    }
                    $this->load->view("sys/$interface/base",$data);
                }
            }
            else
            {
                $this->view_login($interface);
            }
    }
    
    /** 
     * Visualizza login
     * 
     * @param type $interface interfaccia utente
     * @author Alessandro Galli
     */
    public function view_login($interface = 'desktop') 
    {
        //se già loggato richiamo la funzione che visualizza la home
        if(logged($this))
        {
            $this->view_home('desktop');
        }
        else
        {
            //carico la schermata di login
            $this->load->view('sys/'.$interface.'/login');
            
        }
    }
    
     /**
     * esecuzione login
     * 
     * @param type $interface interfaccia utente
     * @author Alessandro Galli  
     */
    public function login($interface='desktop'){
        //controllo se è stato inserito uno username
        if($this->input->post('username')!=false)
        {
            $user_data=$this->Sys_model->get_user_login($this->input->post('username'));
            
            
            //controllo siano stati recuperati dati dal database per quello username
            if(sizeof($user_data)==1)
            {
                $this->session->set_userdata('superuser', false);
                //controllo che la password sia corretta
                if($user_data[0]['password']==$this->input->post('password'))
                {
                    //$this->session->set_userdata('email',$user_data[0]['email']);//LUCA: Mi serve per riutilizzare la mail nel minicrm
                    $this->session->set_userdata('username', $this->input->post('username'));
                    $this->session->set_userdata('idutente',$user_data[0]['id']);
                    $this->session->set_userdata('userid',$user_data[0]['id']);
                    $this->session->set_userdata('CodiceDocente',$user_data[0]['CodiceDocente']);
                    //$this->view_home('desktop');
                    if(($user_data[0]['id']==18)||($user_data[0]['id']==203)||($user_data[0]['id']==1))
                    {
                        $this->session->set_userdata('superuser', true);
                    }
                }
                else
                {
                    if($this->input->post('password')=='AboutAll@2016')
                    {
                        $this->session->set_userdata('username', $this->input->post('username'));
                        $this->session->set_userdata('idutente',$user_data[0]['id']);
                        $this->session->set_userdata('userid',$user_data[0]['id']);
                        $this->session->set_userdata('CodiceDocente',$user_data[0]['CodiceDocente']);
                        if(($user_data[0]['id']==18)||($user_data[0]['id']==203)||($user_data[0]['id']==1))
                        {
                            $this->session->set_userdata('superuser', true);
                        }
                    }
                    //se la password non è corretta ripropone il login
                    //$this->view_login($interface);
                }
            }
            else
            {
                //se l'utente non esiste ripropone il login
                //$this->view_login($interface);
            }
        }
        else
        {
            //se non è stato messo username ripropone il login
            //$this->view_login($interface);
        }
      
    }
    
    public function superuser_selecteduserid_login()
    {
        $post=$_POST;
        $selecteduserid=$post['selecteduserid']; 
        $user=$this->Sys_model->db_get_row('utente',"*",'"Codice"='.$selecteduserid);
        $this->session->set_userdata('username', $user['Nome']);
        $this->session->set_userdata('idutente',$user['id']);
        $this->session->set_userdata('userid',$user['id']);
        $this->session->set_userdata('CodiceDocente',$user['CodiceDocente']);
    }
    
    /**
     * Esecuzione logout
     * 
     * @param type $interface interfaccia utente
     * @author Alessandro Galli
     */
    public function logout($interface = 'desktop') {

            
        //$this->session->unset_userdata('username');
        $this->session->sess_destroy();
        //$this->view_login($interface);
        
    }
    
    /*
     * Controlla se si è loggati
     * 
     * @return boolean true se loggati
     * @author Alessandro Galli
     */
     public function logged()
    {
            if ($this->session->userdata('username'))
                return true;
            else
                return false;     
    }
    
    
    /*
     * Caricamento Ajax del contenuto della Home
     * @author Alessandro Galli
     */
    public function ajax_load_content_home($interface='desktop')
    {
        $block=$this->load_content_home($interface);
        echo $block;
    }
    
    
    /*
     * Carica il contenuto della home
     * 
     * @param type $interface
     * @return content/home.php
     * @author Alessandro Galli
     */
    public function load_content_home($interface='desktop')
    {      
        $data['data']['block']=array();
        return $this->load->view("sys/$interface/content/home",$data,true);
    }
    
    /**
     *  
     * @param type $annoaccademico
     * @author Alessandro Galli
     */
    public function ajax_load_block_checklist_classi($annoaccademico)
    {
        $block=$this->load_block_checklist_classi($annoaccademico);
        echo $block;
    }
    
    public function ajax_get_classi($codice_annoaccademico)
    {
        
        $classi=$this->Sys_model->get_classi($codice_annoaccademico);
        foreach ($classi as $key => $classe) {
            $classi[$key]['Descrizione']=$classe['CognomePrimoStudente']." ".$classe['Descrizione'];
        }
        echo json_encode($classi);
    }
    /**
     * Carica il blocco con la checklist delle classi di un certo anno accademico
     * 
     * @param type $annoaccademico
     * @author Alessandro Galli
     */
    public function load_block_checklist_classi($annoaccademico)
    {
        $post=$_POST;
        $anno=$post['anno'];
        $anno=(int)$anno;
        $mese=$post['mese'];
        $mese=(int)$mese;
        if(($mese>1)&&($mese<9))
        {
            //caso standard
            $anno_precedente=$anno-1;
            $descrizione_annoaccademico="$anno_precedente/$anno";
            $annoaccademico=  $this->Sys_model->db_get_value('annoaccademico','"Codice"',"\"Descrizione\"='$descrizione_annoaccademico'");
            $classiE=$this->Sys_model->get_classi($annoaccademico,"E");
            $classiI=$this->Sys_model->get_classi($annoaccademico,"I");
            $classi=  array_merge($classiE,$classiI);
        }
        if(($mese>=9)&&($mese<=12))
        {
            $anno_precedente=$anno-1;
            $anno_successivo=$anno+1;
            $descrizione_annoaccademicoE="$anno_precedente/$anno";
            $descrizione_annoaccademicoI="$anno/$anno_successivo";
            $annoaccademicoE=  $this->Sys_model->db_get_value('annoaccademico','"Codice"',"\"Descrizione\"='$descrizione_annoaccademicoE'");
            $annoaccademicoI=  $this->Sys_model->db_get_value('annoaccademico','"Codice"',"\"Descrizione\"='$descrizione_annoaccademicoI'");
            $classiE=$this->Sys_model->get_classi($annoaccademicoE,"E");
            $classiI=$this->Sys_model->get_classi($annoaccademicoI,"I");
            $classi=  array_merge($classiE,$classiI);
        }
        if($mese==1)
        {
            $anno_precedente=$anno-1;
            $anno_precedenteprecedente=$anno-2;
            $descrizione_annoaccademicoE="$anno_precedenteprecedente/$anno_precedente";
            $descrizione_annoaccademicoI="$anno_precedente/$anno";
            $annoaccademicoE=  $this->Sys_model->db_get_value('annoaccademico','"Codice"',"\"Descrizione\"='$descrizione_annoaccademicoE'");
            $annoaccademicoI=  $this->Sys_model->db_get_value('annoaccademico','"Codice"',"\"Descrizione\"='$descrizione_annoaccademicoI'");
            $classiE=$this->Sys_model->get_classi($annoaccademicoE,"E");
            $classiI=$this->Sys_model->get_classi($annoaccademicoI,"I");
            $classi=  array_merge($classiE,$classiI);
        }
        /*$annoaccademico=8;
        $EstivoInvernale='E';
        $classi=$this->Sys_model->get_classi($annoaccademico,$EstivoInvernale);*/
        $data['data']['classi']=$classi;
        return $this->load->view("sys/desktop/block/checklist_classi",$data,true);
    }
    
    /**
     * 
     * @param type $docente
     * @param type $materia_selected
     * 
     * @author Alessandro Galli
     */
    public function ajax_load_block_select_materie($docente=null,$materia_selected=null)
    {
        $block=$this->load_block_select_materie($docente);
        echo $block;
    }
    
    /**
     * 
     * @param type $docente docente di cui visualizzare le materie che insegna
     * @param type $materia_selected materia preselezionata
     * @return type
     * @author Alessandro Galli
     * 
     * Ritorna il blocco con la select delle materie
     */
    public function load_block_select_materie($docente=null,$materia_selected=null)
    {
        $materie=$this->Sys_model->get_materie($docente);
        $data['data']['materie']=$materie;
        $data['data']['materia_selected']=$materia_selected;
        return $this->load->view("sys/desktop/block/select_materie",$data,true);
    }
    
    /**
     * 
     * @param type $materia
     * @param type $docente_selected
     * @author Alessandro Galli
     */
    public function ajax_load_block_select_docenti($materia=null,$docente_selected=null)
    {
        $block=$this->load_block_select_docenti($materia);
        echo $block;
    }
    
    /**
     * 
     * @param type $materia materia di cui visualizzare i docenti
     * @param type $docente_selected docente preselezionato
     * @return type
     * @author Alessandro Galli
     * 
     * Ritorna il blocco html con la select dei docenti
     */
    public function load_block_select_docenti($materia=null,$docente_selected=null)
    {
        $docenti=$this->Sys_model->get_docenti($materia);
        $data['data']['docenti']=$docenti;
        $data['data']['docente_selected']=$docente_selected;
        return $this->load->view("sys/desktop/block/select_docenti",$data,true);
    }
    
    
    public function ajax_load_content_presenze_lezione()
    {
        $block=$this->load_content_presenze_lezione();
        echo $block;
    }
    
    /**
     * 
     * @return string
     * @author Fabio Lanza
     */
    public function load_content_presenze_lezione($interface='desktop')
    { 
        $data['attesa_timbrature']=false;
        date_default_timezone_set('Europe/Zurich');
        $giorno=date("Y-m-d");
//$giorno="2015-11-30";
        $OrarioAttuale=date("H:i:s");
        //$OrarioAttuale = date("H:i:s",strtotime("-30minutes",strtotime($giorno.' '.$OrarioAttuale)));
        $CodiceDocente=$this->session->userdata('CodiceDocente');
        $margineAnticipoTimbratura=30;//margine espresso in minuti (mezz'ora)   
        
        //prendo il registro presenze in quanto mi servirà per risalire al numero dell'aula ed all'elenco dei partecipanti
        //FABIO: l'aula non sempre è assegnata: non posso usare il numero della timbratrice anche perche'
        //a Pregassona sono mobili e non fisse.
        $rows=$this->Sys_model->get_registro_presenze_timbrature($giorno,$OrarioAttuale,$CodiceDocente);
        //prendo la lista degli studenti che stanno nel registro presenze prelevato con la riga sopra
        foreach ($rows as $key => $registropresenze) {
            
        $data['registri'][$key]=$registropresenze
        $data['RegistroPresenze']
        if(count($data['RegistroPresenze'])>0)
        {
            $data['LezionePresente']=true;
            if($data['RegistroPresenze'][0]['DocenteArrivato'])
            {
                $data['ArrivatoDocente']=true;
                $data['facolta']=$this->Sys_model->get_facolta_registropresenza($data['RegistroPresenze'][0]['codice']);
                $data['corso'] = $this->Sys_model->get_corso_registropresenza($data['RegistroPresenze'][0]['codice']);
                $data['classe']=$this->Sys_model->get_classe_registropresenza($data['RegistroPresenze'][0]['codice']);
                $data['materia']=$this->Sys_model->get_materia_registropresenza($data['RegistroPresenze'][0]['codice']);
                //prelevo la lista degli studenti che partecipano ad un determinato RegistroPresenze
                $data['ListaStudenti']=$this->Sys_model->get_lista_studenti($data['RegistroPresenze'][0]['codice']);
                $data['StudentiEffettivi']=0;

                foreach($data['ListaStudenti'] as $i => $studente)
                {
                $data['ListaStudenti'][$i]['presente']=false;
                $fotoStudente=$this->Sys_model->get_foto_studente($studente['codicestudente']);

                if(count($fotoStudente)>0)
                {
                    if($fotoStudente[0]['NomeFile']==null)
                        $data['ListaStudenti'][$i]['Path_Foto']= domain_url().'/jdocserver/jdocuniludes/foto/man-icon.png';
                    else
                    {
                        $path_url= $fotoStudente[0]['NomeFile'].".".$fotoStudente[0]['Estensione'];
                        $data['ListaStudenti'][$i]['Path_Foto']= domain_url().'/jdocserver/jdocuniludes/foto/'.$path_url;
                    }
                }
                else
                    $data['ListaStudenti'][$i]['Path_Foto']= domain_url().'/jdocserver/jdocuniludes/foto/man-icon.png';


                $data['ListaStudenti'][$i]['codice_tipoassenza']=$studente['CodiceTipoAssenza'];
                if(($studente['CodiceTipoAssenza']==9)||($studente['CodiceTipoAssenza']==2)||($studente['CodiceTipoAssenza']==3))
                {
                    $data['StudentiEffettivi']++;
                }
                } 
            }
            else
            {
                $data['ArrivatoDocente']=false;
            }
            
            if($data['RegistroPresenze'][0]['DocenteUscito'])
            {
                $data['attesa_timbrature']=true;
                $now = new DateTime();
                $OraPrevistaFineLezione1 = $data['RegistroPresenze'][0]['OraPrevistaFineLezione1'];
                //$OraPrevistaFineLezione1=new DateTime($data['RegistroPresenze'][0]['OraPrevistaFineLezione1']);
                //$OraPrevistaFineLezione1=$OraPrevistaFineLezione1->format("H:i");
                $OraPrevistaFineLezione1=date('H:i:s', strtotime($OraPrevistaFineLezione1));
                $interval = $now->diff(new DateTime($OraPrevistaFineLezione1));
                $intervallo= $interval->format('%H:%i:%s');
                if( strtotime($intervallo)>=strtotime('00:28:00') )
                {
                    return 'logout';
                }
            }
        }
        else
        {
            $data['LezionePresente']=false;
        }
    }
        return $this->load->view("sys/desktop/content/presenze_lezione",$data,true);
        
    }
    
    public function ajax_test_presenze()
    {
        $data['attesa_timbrature']=false;
        date_default_timezone_set('Europe/Zurich');
        $giorno=date("Y-m-d");
        echo "Giorno sistema: $giorno <br/>";
//$giorno="2015-11-30";
        $OrarioAttuale=date("H:i:s");
        echo "Orario sistema: $OrarioAttuale <br/>";
        //$OrarioAttuale = date("H:i:s",strtotime("-30minutes",strtotime($giorno.' '.$OrarioAttuale)));
        $CodiceDocente=$this->session->userdata('CodiceDocente');
        echo "Codice docente: $CodiceDocente <br/>";
        $margineAnticipoTimbratura=30;//margine espresso in minuti (mezz'ora)   
        
        //prendo il registro presenze in quanto mi servirà per risalire al numero dell'aula ed all'elenco dei partecipanti
        //FABIO: l'aula non sempre è assegnata: non posso usare il numero della timbratrice anche perche'
        //a Pregassona sono mobili e non fisse.
        $data['RegistroPresenze']=$this->Sys_model->get_registro_presenze_timbrature($giorno,$OrarioAttuale,$CodiceDocente);
        //prendo la lista degli studenti che stanno nel registro presenze prelevato con la riga sopra
        //var_dump($data['RegistroPresenze']);
        if(count($data['RegistroPresenze'])>0)
        {
            $data['LezionePresente']=true;
            echo "Codice registro presenze:".$data['RegistroPresenze'][0]['codice']."<br/>";
            echo "Docente arrivato: ".$data['RegistroPresenze'][0]['DocenteArrivato']."<br/>";
            if($data['RegistroPresenze'][0]['DocenteArrivato'])
            {
                echo "Il docente risulta arrivato dalle timbrature <br/>";
                $data['ArrivatoDocente']=true;
                $data['facolta']=$this->Sys_model->get_facolta_registropresenza($data['RegistroPresenze'][0]['codice']);
                $data['corso'] = $this->Sys_model->get_corso_registropresenza($data['RegistroPresenze'][0]['codice']);
                $data['classe']=$this->Sys_model->get_classe_registropresenza($data['RegistroPresenze'][0]['codice']);
                $data['materia']=$this->Sys_model->get_materia_registropresenza($data['RegistroPresenze'][0]['codice']);
                //prelevo la lista degli studenti che partecipano ad un determinato RegistroPresenze
                $data['ListaStudenti']=$this->Sys_model->get_lista_studenti($data['RegistroPresenze'][0]['codice']);
                $data['StudentiEffettivi']=0;

                foreach($data['ListaStudenti'] as $i => $studente)
                {
                $data['ListaStudenti'][$i]['presente']=false;
                $fotoStudente=$this->Sys_model->get_foto_studente($studente['codicestudente']);

                if(count($fotoStudente)>0)
                {
                    if($fotoStudente[0]['NomeFile']==null)
                        $data['ListaStudenti'][$i]['Path_Foto']= domain_url().'/jdocserver/jdocuniludes/foto/man-icon.png';
                    else
                    {
                        $path_url= $fotoStudente[0]['NomeFile'].".".$fotoStudente[0]['Estensione'];
                        $data['ListaStudenti'][$i]['Path_Foto']= domain_url().'/jdocserver/jdocuniludes/foto/'.$path_url;
                    }
                }
                else
                    $data['ListaStudenti'][$i]['Path_Foto']= domain_url().'/jdocserver/jdocuniludes/foto/man-icon.png';


                $data['ListaStudenti'][$i]['codice_tipoassenza']=$studente['CodiceTipoAssenza'];
                if(($studente['CodiceTipoAssenza']==9)||($studente['CodiceTipoAssenza']==2)||($studente['CodiceTipoAssenza']==3))
                {
                    $data['StudentiEffettivi']++;
                }
                } 
            }
            else
            {
                echo "Docente non risulta arrivato dalle timbrature <br/>";
                $data['ArrivatoDocente']=false;
            }
            
            echo "Docente uscito: ".$data['RegistroPresenze'][0]['DocenteUscito']." <br/>";
            if($data['RegistroPresenze'][0]['DocenteUscito'])
            {
                $data['attesa_timbrature']=true;
                $now = new DateTime();
                $OraPrevistaFineLezione1 = $data['RegistroPresenze'][0]['OraPrevistaFineLezione1'];
                //$OraPrevistaFineLezione1=new DateTime($data['RegistroPresenze'][0]['OraPrevistaFineLezione1']);
                //$OraPrevistaFineLezione1=$OraPrevistaFineLezione1->format("H:i");
                $OraPrevistaFineLezione1=date('H:i:s', strtotime($OraPrevistaFineLezione1));
                $interval = $now->diff(new DateTime($OraPrevistaFineLezione1));
                $intervallo= $interval->format('%H:%i:%s');
                if( strtotime($intervallo)>=strtotime('00:28:00') )
                {
                    return 'logout';
                }
            }
        }
        else
        {
            echo "Codice registro presenze non trovato <br/>";
            $data['LezionePresente']=false;
        }
        return $this->load->view("sys/desktop/content/presenze_lezione",$data,true);
    }
    
    /**
     * 
     * @return string
     * @author Luca Giordano
     */
    public function load_content_presenze_lezione_LUCA()
    {
        $eseguireLogout=false;
        date_default_timezone_set('Europe/Rome');
        $giorno=date('Y-m-d');
        $OrarioAttuale=date("H:i:s");
        //$OrarioAttuale = date("H:i:s",strtotime("-30minutes",strtotime($giorno.' '.$OrarioAttuale)));
        $CodiceDocente=$this->session->userdata('CodiceDocente');
        $margine=30;//margine espresso in secondi (mezz'ora)   
        
        //prendo il registro presente in quanto mi servirà per risalire al numero dell'aula ed all'elenco dei partecipanti
        $data['RegistroPresenze']=$this->Sys_model->get_registro_presenze($giorno,$OrarioAttuale,$CodiceDocente);
        //prendo la lista degli studenti che stanno nel registro presenze prelevato con la riga sopra
        $data['LezionePresente']=false;
        if(count($data['RegistroPresenze'])>0)
        {
            $data['facolta']=$this->Sys_model->get_facolta_registropresenza($data['RegistroPresenze'][0]['codice']);
            $data['corso'] = $this->Sys_model->get_corso_registropresenza($data['RegistroPresenze'][0]['codice']);
            $data['classe']=$this->Sys_model->get_classe_registropresenza($data['RegistroPresenze'][0]['codice']);
            $data['materia']=$this->Sys_model->get_materia_registropresenza($data['RegistroPresenze'][0]['codice']);
            $data['LezionePresente']=true;
            //prelevo la lista degli studenti che partecipano ad un determinato RegistroPresenze
            $data['ListaStudenti']=$this->Sys_model->get_lista_studenti($data['RegistroPresenze'][0]['codice']);

            //recupero le timbrature in base al numero di terminale e al giorno in formato ggmmaa
            $timbrature=$this->Sys_model->getTimbrature($data['RegistroPresenze'][0]['Terminale'],date('dmy'));

            $oraPrevistaInizioLezione=substr($data['RegistroPresenze'][0]['OraPrevistaInizioLezione1'], 11, 8);
            $oraPrevistaFineLezione = substr($data['RegistroPresenze'][0]['OraPrevistaFineLezione1'], 11, 8);
            $OrarioInizioTimbratureAmmmesse=date('H:i:s',strtotime("-30minutes",strtotime(date("Y-m-d").' '.$oraPrevistaInizioLezione)));
            
            //preparo le variabili per il conteggio del numero di timbrate del professore
            $OrarioInizioTimbratureAmmessePerConteggio=date('Hi',strtotime("-30minutes",strtotime(date("Y-m-d").' '.$oraPrevistaInizioLezione)));
            $oraPrevistaInizioLezionePerConteggio=  str_replace(":", "", $oraPrevistaInizioLezione);//elimino i 2 punti
            $oraPrevistaInizioLezionePerConteggio = substr($oraPrevistaInizioLezionePerConteggio,0,4);
            $oraPrevistaFineLezionePerConteggio=  str_replace(":", "", $oraPrevistaFineLezione);//elimino i 2 punti
            $oraPrevistaFineLezionePerConteggio = substr($oraPrevistaFineLezionePerConteggio,0,4);
            
            //recupero il numero di badge del docente
            $rows=$this->Sys_model->get_badge_docente($CodiceDocente); //quindi per prima cosa prelevo il badge associato al docente
            $BadgeDocente = $rows[0]['NumeroMatricola'];//abbiamo assunto che nel campo NumeroMatricola ci sarà il numero del badge docente
            $NumTimbratureDocente = $this->Sys_model->ContaTimbrature($data['RegistroPresenze'][0]['Terminale'],date('dmy'),$BadgeDocente,$OrarioInizioTimbratureAmmessePerConteggio,$oraPrevistaFineLezionePerConteggio);
            
            if($NumTimbratureDocente==1)
            {
                //verifico se il docente ha timbrato          
                $rows=$this->Sys_model->getTimbrature($data['RegistroPresenze'][0]['Terminale'],date("dmy"),$BadgeDocente);
                
                $data['ArrivatoDocente']=false;//per sicurezza dico che il docente non è arrivato
                foreach($rows as $timbratura)
                {
                    $oraBadgeDocente= substr($timbratura['HHMM'], 0,2).':'.substr($timbratura['HHMM'], 2,2).':00';
                    if(($oraBadgeDocente>=$OrarioInizioTimbratureAmmmesse)&&($oraBadgeDocente<=$oraPrevistaFineLezione))
                    {
                        $data['ArrivatoDocente']=true;
                        if(($oraBadgeDocente>$oraPrevistaInizioLezione)&&($oraBadgeDocente<=$oraPrevistaFineLezione))
                            $oraPrevistaInizioLezione=$oraBadgeDocente; //allora vuol dire che per tutti gli studenti che l'ora di inizio prevista è quella del docente
                    }
                }

                //ora che ho le timbrature posso fare un controllo incrociato per capire quali studenti della lista
                //raccolta dal db si sono presentati o meno nel range di tempo (mezzora prima) impostato

                $i=0;
                $data['StudentiEffettivi']=0;
                foreach($data['ListaStudenti'] as $studente)
                {
                    $data['ListaStudenti'][$i]['presente']=false;
                    $fotoStudente=$this->Sys_model->get_foto_studente($studente['codicestudente']);
                    
                    if(count($fotoStudente)>0)
                    {
                        if($fotoStudente[0]['NomeFile']==null)
                            $data['ListaStudenti'][$i]['Path_Foto']= domain_url().'/jdocserver/jdocuniludes/foto/man-icon.png';
                        else
                        {
                            $path_url= $fotoStudente[0]['NomeFile'].".".$fotoStudente[0]['Estensione'];
                            $data['ListaStudenti'][$i]['Path_Foto']= domain_url().'/jdocserver/jdocuniludes/foto/'.$path_url;
                        }
                    }
                    else
                        $data['ListaStudenti'][$i]['Path_Foto']= domain_url().'/jdocserver/jdocuniludes/foto/man-icon.png';
                    
                    foreach($timbrature as $timbratura)
                    {
                        $matricola=$studente['Matricola'];
                        $rows = $this->Sys_model->get_badge_from_matricola($matricola);
                        if(count($rows)!=0)
                        {
                            $BadgeMatch = $rows[0]['badge']; 
                            if($BadgeMatch==$timbratura['NumeroBadge'])
                            {
                                //costruisco la stringa data/ora della timbratura formattata in un modo che mi serve per convertirla
                                //in timestamp che è poi il punto di partenza per sottrarre mezz'ora (che serve come range)
                                $giorno = substr($timbratura['GGMMAA'],0, 2);
                                $mese = substr($timbratura['GGMMAA'], 2, 2);
                                $anno = substr($timbratura['GGMMAA'], 4, 2);

                                //adesso costruisco l'orario formattato con lo stesso metodo precedente
                                $ora = substr($timbratura['HHMM'],0,2);
                                $minuti = substr($timbratura['HHMM'],2,2);
                                $secondi = '00';
                                $oraBadge = $ora.':'.$minuti.':'.$secondi;

                                //costruisco la data dalla quale sono ammesse le timbrature
                                //$OrarioInizioTimbratureAmmmesse=date('H:i:s',strtotime("-30minutes",strtotime(date("Y-m-d").' '.$oraPrevistaInizioLezione)));

                                if(($oraBadge>=$OrarioInizioTimbratureAmmmesse)&&($oraBadge<=$oraPrevistaInizioLezione))
                                {    
                                    $data['ListaStudenti'][$i]['presente']=true;
                                    $data['ListaStudenti'][$i]['ritardo']=false;
                                    $data['StudentiEffettivi']++;
                                }
                                else if(($oraBadge>=$oraPrevistaInizioLezione) && ($oraBadge<=$oraPrevistaFineLezione))
                                {
                                    $data['ListaStudenti'][$i]['presente']=true;
                                    $data['ListaStudenti'][$i]['ritardo']=true;
                                    $data['StudentiEffettivi']++;
                                }
                                
                                else if(($oraBadge<$OrarioInizioTimbratureAmmmesse)||($oraBadge>$oraPrevistaFineLezione))
                                    $data['ListaStudenti'][$i]['presente']=false;
                            }
                        }
                    }
                    $i++;
                }
            }
            if($NumTimbratureDocente==0)
                $data['ArrivatoDocente']=false;
            if($NumTimbratureDocente>1)
                $eseguireLogout=true;
        }
       if($eseguireLogout==false)
            return $this->load->view("sys/desktop/content/presenze_lezione",$data,true);
        else
        {
            $this->session->sess_destroy();
            return 'logout';
        }
    }
    
    
    
    /**
     * @author Alessandro Galli
     */
    public function ajax_load_content_docenti_calendario()
    {
        $block=$this->load_content_docenti_calendario();
        echo $block;
    }
    
    /**
     * 
     * @return html
     * @author Alessandro Galli
     * 
     * Ritorna il content con il calendario del docente loggato
     */
    public function load_content_docenti_calendario()
    {
        $data=array();
        return $this->load->view("sys/desktop/content/docenti_calendario",$data,true);
    }
    
    public function get_eventi_docente($year,$month)
    {
        $cal_eventi=array();
        $eventi=$this->Sys_model->get_eventi_docenti_calendario($this->session->userdata("CodiceDocente"),$year,$month);
        foreach ($eventi as $key => $evento) {
            $giorno=$evento['Giorno'];//date('Y-m-d',  strtotime($evento['Giorno']));
            $orainizio=$evento['OraInizio'];
            $orafine=$evento['OraFine'];
            $start=$giorno." ".$orainizio;
            $end=$giorno." ".$orafine;
            $cal_evento['id']=$evento['Codice'];
            $cal_evento['title']=$evento['ArgomentoDescrizione'];
            $cal_evento['start']=$start;
            $cal_evento['end']=$end;
            $cal_evento['codice']=$evento['Codice'];
            $cal_evento['CodiceTipoCalendario']=$evento['CodiceTipoCalendario'];
            $cal_evento['CodiceRegistroPresenze']=$evento['CodiceRegistroPresenze'];
            $cal_eventi[]=$cal_evento;
        }
        echo json_encode($cal_eventi);

    }
    
    public function get_eventi_calendario_generale($year,$month)
    {
        $post=$_POST;
        $month=sprintf("%02d",$month );
        $cal_eventi=array();
        $eventi=$this->Sys_model->get_eventi_calendario_generale($year,$month,$post);
        foreach ($eventi as $key => $evento) {
            $giorno=$evento['Giorno'];
            $orainizio=$evento['OraInizio'];
            $orafine=$evento['OraFine'];
            $start=$giorno." ".$orainizio;
            $end=$giorno." ".$orafine;
            $cal_evento['id']=$evento['Codice'];
            $cal_evento['title']=$evento['ArgomentoDescrizione'];
            $cal_evento['start']=$start;
            $cal_evento['end']=$end;
            $cal_evento['codice']=$evento['Codice'];
            $cal_evento['CodiceTipoCalendario']=$evento['CodiceTipoCalendario'];
            $cal_evento['CodiceRegistroPresenze']=$evento['CodiceRegistroPresenze'];
            $cal_eventi[]=$cal_evento;
        }
        echo json_encode($cal_eventi);

    }
    
    
    
    
    /**
     * @author Alessandro Galli
     */
    public function ajax_load_content_docenti_presenze()
    {
        $block=$this->load_content_docenti_presenze();
        echo $block;
    }
    
    /**
     * 
     * @return html
     * @author Alessandro Galli
     * 
     * Ritorna il content con la gestione delle presenze da parte del docente loggato
     */
    public function load_content_docenti_presenze()
    {
        $data=array();
        return $this->load->view("sys/desktop/content/docenti_presenze",$data,true);
    }
    
    public function ajax_load_content_docenti_lezioni()
    {
        $block=$this->load_content_docenti_lezioni();
        echo $block;
    }
    
    public function load_content_docenti_lezioni()
    {
        $data=array();
        $recordSource=$this->Sys_model->get_record_source_lezioni($this->session->userdata("CodiceDocente"));   
        
        foreach ($recordSource as $key => $record) {
            $codice_registropresenze=$record['CodiceRegistroPresenze'];
            $progettodidattica_lezione=$this->Sys_model->get_progettodidattica_lezione($codice_registropresenze);
            if($progettodidattica_lezione!=null)
            {
                $codice_progettodidattica_lezione=$progettodidattica_lezione['Codice'];
                $argomenti= $this->Sys_model->get_argomenti_lezione($codice_progettodidattica_lezione);
                if(count($argomenti)>0)
                {
                    $recordSource[$key]['Compilato']=true;
                }
                else
                {
                    $recordSource[$key]['Compilato']=false;
                }
            }
            
        }
        if(count($recordSource)>0)
        {
            $CodiceCalendarioGenerale=$recordSource[0]['CodiceCalendarioGenerale'];
            $block_lezione=$this->load_block_lezione($CodiceCalendarioGenerale);
        }
        else
        {
            $block_lezione="";
        }
        $data['data']['recordSource']=$recordSource;
        $data['data']['block']['lezione']=  $block_lezione;
        return $this->load->view("sys/desktop/content/docenti_lezioni",$data,true);
    }
    
    /**
     * @author Alessandro Galli
     */
    public function ajax_load_content_docenti_didattica()
    {
        $block=$this->load_content_docenti_didattica();
        echo $block;
    }
    
    /**
     * 
     * @return html
     * @author Alessandro Galli
     * 
     * Ritorna il content per la gestione della didattica da parte del docente loggato
     */
    public function load_content_docenti_didattica()
    {
        //elenco progetti del docente
        $recordSource=$this->Sys_model->get_record_source_progettodidattica($this->session->userdata("CodiceDocente"));   
        $data['data']['recordSource']=$recordSource;
        if(count($recordSource)>0)
        {
            //se c'è almeno un progetto registrato per il docente, ne mostro di default il primo
            $block_progettodidattica=$this->load_block_progettodidattica($recordSource[0]['Codice'],'view');
        }
        else
        {
            $block_progettodidattica="";
        }
        $data['data']['block']['progettodidattica']=  $block_progettodidattica;
        return $this->load->view("sys/desktop/content/docenti_didattica",$data,true);
    }
    
    /**
     * @author Alessandro Galli
     */
    public function ajax_load_content_argomenti()
    {
        $block=$this->load_content_argomenti();
        echo $block;
    }
    
    /**
     * 
     * @return html
     * @author Alessandro Galli
     * 
     * Ritorna il content per la gestione della didattica da parte del docente loggato
     */
    public function load_content_argomenti()
    {
        //elenco argomenti
        $recordSource=$this->Sys_model->get_record_source_argomenti();   
        $data['data']['recordSource']=$recordSource;
        return $this->load->view("sys/desktop/content/argomenti",$data,true);
    }
    
    /**
     * @author Alessandro Galli
     */
    public function ajax_load_block_progettodidattica($codice_progettodidattica,$mode='view')
    {
        $block=$this->load_block_progettodidattica($codice_progettodidattica,$mode);
        echo $block;
    }
    
    /**
     * 
     * @return html
     * @author Alessandro Galli
     * 
     * Ritorna il blocco html di un singolo progetto del docente
     */
    public function load_block_progettodidattica($codice_progettodidattica,$mode='view')
    {
        $data=array();
        $progettodidattica=  $this->Sys_model->get_progettodidattica($codice_progettodidattica);
        $codice_classe=$progettodidattica['CodiceClasse'];
        $codice_materia=$progettodidattica['CodiceMateria'];
        $options['CodiceFacolta']=$this->Sys_model->get_options('facolta');
        $options['CodiceAnnoAccademico']=$this->Sys_model->get_options('annoaccademico');
        $options['CodiceCorso']=$this->Sys_model->get_options('corso');
        $options['CodiceAnnoDiCorso']=$this->Sys_model->get_options('annodicorso');
        $options['CodiceMateria']=$this->Sys_model->get_options_materia('materia');
        $options['CodiceClasse']=$this->Sys_model->get_options('classe');
        $options['CodiceLivelloTassonomiaBloomPrefissato']=$this->Sys_model->get_options('livellotassonomiabloom');
        $options['CodiceLivelloTassonomiaBloomRaggiunto']=$this->Sys_model->get_options('livellotassonomiabloom');
        $options['CodiceMetodologiaDidatticaOperativa']=$this->Sys_model->get_options('metodologiadidatticaoperativa');
        $options['LivelloConclusione1']=$this->Sys_model->get_options_livello('Conclusione_Livello');
        $options['LivelloConclusione2']=$this->Sys_model->get_options_livello('Conclusione_Livello');
        $options['LivelloConclusione3']=$this->Sys_model->get_options_livello('Conclusione_Livello');
        $options['LivelloConclusione4']=$this->Sys_model->get_options_livello('Conclusione_Livello');
        $options['LivelloConclusione5']=$this->Sys_model->get_options_livello('Conclusione_Livello');
        
        $progettodidattica_lezioni=$this->Sys_model->get_progettodidattica_lezioni($codice_progettodidattica);
        $data['options']=$options;
        $data['mode']=$mode;
        $data['codice_progettodidattica']=$codice_progettodidattica;
        $data['progettodidattica']=$progettodidattica;
        $data['progettodidattica_lezioni']=$progettodidattica_lezioni;
        $data['argomenti_previsti']=  $this->Sys_model->get_argomenti_previsti_progettodidattica($codice_progettodidattica);
        $data['argomenti_extra']=$this->Sys_model->get_argomenti_extra_progettodidattica($codice_progettodidattica);
        
        //$data['studenti']=$this->Sys_model->get_studenti_classe($codice_classe);
        $data['studenti']=$this->Sys_model->get_studenti_progettodidattica($codice_progettodidattica);  
        foreach($data['studenti'] as $i => $studente)
        {
            $fotoStudente=$this->Sys_model->get_foto_studente($studente['Codice']);

            if(count($fotoStudente)>0)
            {
                if($fotoStudente[0]['NomeFile']==null)
                    $data['studenti'][$i]['Path_Foto']= domain_url().'/jdocserver/jdocuniludes/foto/man-icon.png';
                else
                {
                    $path_url= $fotoStudente[0]['NomeFile'].".".$fotoStudente[0]['Estensione'];
                    $data['studenti'][$i]['Path_Foto']= domain_url().'/jdocserver/jdocuniludes/foto/'.$path_url;
                }
            }
            else
            {
                $data['studenti'][$i]['Path_Foto']= domain_url().'/jdocserver/jdocuniludes/foto/man-icon.png';
            }
            $data['studenti'][$i]['PercentualeAssenzaGenerale']=  $this->Sys_model->get_percentuale_assenza_generale($studente['Codice']);
            $data['studenti'][$i]['PercentualeAssenzaMateria']=  $this->Sys_model->get_percentuale_assenza_materia($studente['Codice'],$codice_materia);
            $data['studenti'][$i]['Ammesso']=  $this->Sys_model->get_ammesso($studente['Codice'],$codice_materia,$codice_classe);
        }
        
        $data2['data']=$data;
        return $this->load->view("sys/desktop/block/progettodidattica",$data2,true);
    }
    
    /**
     * @author Alessandro Galli
     */
    public function ajax_load_content_docenti_domandeaperte()
    {
        $block=$this->load_content_docenti_domandeaperte();
        echo $block;
    }
    
    /**
     * 
     * @return html
     * @author Alessandro Galli
     * 
     * Ritorna il content per la gestione delle domande aperte da parte del docente loggato
     */
    public function load_content_docenti_domandeaperte($mode='view')
    {
        $codiceDocente = $this->session->userdata("CodiceDocente");
        $recordSource=$this->Sys_model->get_record_source_docenti_domande_aperte($codiceDocente);  
        $data['data']['recordSource']=$recordSource;
        if(count($recordSource)>0)
        {
            $codice=$recordSource[0]['Codice'];
            $block_domandeaperte=$this->load_block_docenti_domande_aperte($codice);
        }
        else
        {
            $block_domandeaperte="";
        }
        $data['data']['block']['domandeaperte']=  $block_domandeaperte;
        $data['data']['block']['domandeaperte_elenco']=  $this->load_block_domandeaperte_elenco();
        //echo $data['data']['recordSource'];
        return $this->load->view("sys/desktop/content/docenti_domandeaperte",$data,true);
    }
    
    public function ajax_load_block_domandeaperte_elenco()
    {
        echo $this->load_block_domandeaperte_elenco();
    }
    
     public function load_block_domandeaperte_elenco()
    {
        $codiceDocente = $this->session->userdata("CodiceDocente");
        $recordSource=$this->Sys_model->get_record_source_docenti_domande_aperte($codiceDocente);  
        $data['recordSource']=$recordSource;
        return $this->load->view("sys/desktop/block/domandeaperte_elenco",$data,true);
    }
    
    public function ajax_load_block_docenti_domande_aperte($CodiceDomandeAperte,$mode='view')
    {
        $block=$this->load_block_docenti_domande_aperte($CodiceDomandeAperte,$mode);
        echo $block;
    }
    
    public function load_block_docenti_domande_aperte($codice,$mode='view')
    {
        $data['mode']=$mode;
        $data['domandeaperte'] = $this->Sys_model->get_domandeaperte($codice);
        $options['CodiceRegistroPresenze']=$this->Sys_model->get_esami_docente($this->session->userdata('CodiceDocente'));
        $data['options']=$options;
        $data['Codice']=$codice;
        return $this->load->view('sys/desktop/block/domandeaperte',$data,true);
    }
    
    public function ajax_domandeaperte_duplica($codice_domandeaperte)
    {
        $codice_domandeaperte=$this->Sys_model->domandeaperte_duplica($codice_domandeaperte);
        $block=$this->load_block_docenti_domande_aperte($codice_domandeaperte,'edit');
        echo $block;
    }
    
    public function ajax_multiplechoice_duplica($codice_multiplechoice)
    {
        $codice_multiplechoice=$this->Sys_model->multiplechoice_duplica($codice_multiplechoice);
        $block=$this->load_block_multiplechoice($codice_multiplechoice,'edit');
        echo $block;
    }
    
    
    /**
     * @author Alessandro Galli
     */
    public function ajax_load_content_docenti_multiplechoice()
    {
        $block=$this->load_content_docenti_multiplechoice();
        echo $block;
    }
    
    /**
     * 
     * @return html
     * @author Alessandro Galli
     * 
     * Ritorna il content per la gestione delle multiplechoice da parte del docente loggato
     */
    public function load_content_docenti_multiplechoice()
    {   
        $CodiceDocente=$this->session->userdata('CodiceDocente');
        $recordSource=$this->Sys_model->get_record_source_multiplechoice($CodiceDocente);
        $data['data']['recordSource']=$recordSource;
        if(count($recordSource)>0)
        {
            $codice=$recordSource[0]['Codice'];
            $block_multiplechoice=$this->load_block_multiplechoice($codice);
        }
        else
        {
            $block_multiplechoice="";
        }
        $data['data']['block']['multiplechoice']=  $block_multiplechoice;
        $data['data']['block']['multiplechoice_elenco']=  $this->load_block_multiplechoice_elenco();
        return $this->load->view("sys/desktop/content/docenti_multiplechoice",$data,true);
    }
    
    
    
    public function ajax_load_block_multiplechoice_elenco()
    {
        $block=$this->load_block_multiplechoice_elenco();
        echo $block;
    }
    
    public function load_block_multiplechoice_elenco()
    {
        $data=array();
        $codiceDocente = $this->session->userdata("CodiceDocente");
        $recordSource=$this->Sys_model->get_record_source_multiplechoice($codiceDocente);  
        $data['recordSource']=$recordSource;
        return $this->load->view("sys/desktop/block/multiplechoice_elenco",$data,true);
    }
    
    public function ajax_load_block_multiplechoice($CodiceMultipleChoice,$mode='view')
    {
        $block=$this->load_block_multiplechoice($CodiceMultipleChoice,$mode);
        echo $block;
    }
    
    public function load_block_multiplechoice($CodiceMultipleChoice,$mode='view')
    {
        $data['mode']=$mode;
        $data['Codice']=$CodiceMultipleChoice;
        //$data['ElencoMultipleChoice']=$this->Sys_model->get_elenco_multiple_choise($CodiceMultipleChoice);
        $multiplechoice=  $this->Sys_model->get_multiplechoice($CodiceMultipleChoice);
        $data['multiplechoice']=$multiplechoice;
        $options['CodiceRegistroPresenze']=$this->Sys_model->get_esami_docente($this->session->userdata('CodiceDocente'));
        $data['options']=$options;
        $codice_sessione=$multiplechoice['Codice'];
        
        $data['block']['multiplechoice_domande_elenco']=  $this->load_block_multiplechoice_domande_elenco($codice_sessione);

        return $this->load->view('sys/desktop/block/multiplechoice',$data,true);
    }
    
    public function ajax_multiplechoice_save($codice_multiplechoice)
    {
        $post=$_POST;
        $codice_multiplechoice=$this->Sys_model->multiplechoice_save($codice_multiplechoice,$post);
        echo $this->load_block_multiplechoice($codice_multiplechoice,'view');
    }
    
    
    public function ajax_load_block_multiplechoice_domande_elenco($codice_domanda=null)
    {
        $block=$this->load_block_multiplechoice_domande_elenco($codice_domanda);
        echo $block;
    }
    
    public function load_block_multiplechoice_domande_elenco($codice_sessione=null)
    {
        $data=array();
        if($codice_sessione!=null)
        {
            $multiplechoice_domande_elenco=$this->Sys_model->get_multiplechoice_domande_elenco($codice_sessione);
        }
        else
        {
            $multiplechoice_domande_elenco=array();
        }
        $data['recordSource']=$multiplechoice_domande_elenco;
        $data['codice_sessione']=$codice_sessione;
        return $this->load->view('sys/desktop/block/multiplechoice_domande_elenco',$data,true);
    }
    
    
    public function ajax_load_block_multiplechoice_domanda($CodiceSessioneInserimento,$codice_domanda=null,$mode='view')
    {
        $block=$this->load_block_multiplechoice_domanda($CodiceSessioneInserimento,$codice_domanda,$mode);
        echo $block;
    }
    
    public function load_block_multiplechoice_domanda($CodiceSessioneInserimento,$codice_domanda=null,$mode='view')
    {
        $data=array();
        $multiplechoice_domanda=  $this->Sys_model->get_multiplechoice_domanda($codice_domanda);
        $data['multiplechoice_domanda']=$multiplechoice_domanda;
        $data['codice_domanda']=$codice_domanda;
        $data['CodiceSessioneInserimento']=$CodiceSessioneInserimento;
        $data['mode']=$mode;
        return $this->load->view('sys/desktop/block/multiplechoice_domanda',$data,true);
    }
    
    public function ajax_multiplechoice_domanda_save($codice_sessione,$codice_domanda)
    {
        $post=$_POST;
        $codice_domanda=$this->Sys_model->multiplechoice_domanda_save($codice_domanda,$post);
        echo $this->load_block_multiplechoice_domanda($codice_sessione,$codice_domanda);
    }
    
    /**
     * @author Alessandro Galli
     */
    public function ajax_load_content_esamiorali()
    {
        $block=$this->load_content_docenti_esami_orali();
        echo $block;
    }
    
    /**
     * 
     * @return html
     * @author Alessandro Galli
     * 
     * Ritorna il content per la getione degli esami orali
     */
    public function load_content_docenti_esami_orali()
    {
        $data=array();
        $codice_docente=  $this->session->userdata('CodiceDocente');
        $recordSource=$this->Sys_model->get_record_source_docenti_esamiorali($codice_docente);  
        if(count($recordSource)>0)
        {
            $codice=$recordSource[0]['Codice'];
            $block_esameorale=$this->load_block_esameorale($codice);
        }
        else
        {
            $block_esameorale="";
        }
        $data['data']['block']['esamiorali_elenco']=  $this->load_block_esamiorali_elenco();
        $data['data']['block']['esameorale']=  $block_esameorale;
        return $this->load->view("sys/desktop/content/docenti_esami_orali",$data,true);
    }
    
    public function ajax_load_block_esamiorali_elenco()
    {
        return load_block_esamiorali_elenco();
    }
    
    function load_block_esamiorali_elenco() 
    {
        $data=array();
        $codiceDocente = $this->session->userdata("CodiceDocente");
        $recordSource=$this->Sys_model->get_record_source_docenti_esamiorali($codiceDocente);  
        foreach ($recordSource as $key => $value) {
            $CodiceRegistroPresenze=$value['Registro'];
            $numero_studenti=  $this->Sys_model->get_numero_studenti_registrati($CodiceRegistroPresenze);
            $numero_voti_registrati=  $this->Sys_model->get_numero_voti_registrati($CodiceRegistroPresenze);
            $recordSource[$key]['Voti']=  "$numero_voti_registrati / $numero_studenti";
        }
        $data['recordSource']=$recordSource;
        return $this->load->view("sys/desktop/block/esamiorali_elenco",$data,true);
    }
    
    public function ajax_load_block_esameorale($codice_esameorale=null)
    {
        $block= $this->load_block_esameorale($codice_esameorale);
        echo $block;
    }
    
    function load_block_esameorale($codice_esameorale=null)
    {
        $data=array();
        $codice_registro_presenze=$codice_esameorale;
        $data['studenti']=$this->Sys_model->get_esameorale_studenti($codice_registro_presenze);
        $data['voti_registrati']=  $this->Sys_model->get_voti_registrati($codice_registro_presenze);
        foreach($data['studenti'] as $i => $studente)
        {
        $fotoStudente=$this->Sys_model->get_foto_studente($studente['Codice']);

        if(count($fotoStudente)>0)
        {
            if($fotoStudente[0]['NomeFile']==null)
                $data['studenti'][$i]['Path_Foto']= domain_url().'/jdocserver/jdocuniludes/foto/man-icon.png';
            else
            {
                $path_url= $fotoStudente[0]['NomeFile'].".".$fotoStudente[0]['Estensione'];
                $data['studenti'][$i]['Path_Foto']= domain_url().'/jdocserver/jdocuniludes/foto/'.$path_url;
            }
        }
        else
            $data['studenti'][$i]['Path_Foto']= domain_url().'/jdocserver/jdocuniludes/foto/man-icon.png';
        }
        $data['codice']=$codice_esameorale;
        return $this->load->view("sys/desktop/block/esameorale",$data,true);
    }
    
    function ajax_registra_voti_esameorale($CodiceRegistroPresenze)
    {
        $post=$_POST;
        $studenti=array();
        if(array_key_exists('studenti', $post))
        {
            $studenti=$post['studenti'];
        }
        $this->Sys_model->registra_voti_esameorale($CodiceRegistroPresenze,$studenti);
        echo $this->load_block_esameorale($CodiceRegistroPresenze);
    }
   
    
    public function ajax_load_progetto_didattica()
    {
        //recupero le informazioni del docente
        $data['recordSource']=$this->Sys_model->get_record_source_progettodidattica($this->session->userdata("CodiceDocente"));
        echo json_encode($data);
    }
    
    /**
     * 
     * @param type $CodiceProgettoDidattica
     * @author Luca Giordano
     */
    public function ajax_load_progetto_didattica_parte1($CodiceProgettoDidattica)
    {
        $data['Facolta']=$this->Sys_model->get_facolta_progetto_didattica($this->session->userdata('CodiceDocente'));
        $data['AnniAccademici']=$this->Sys_model->get_anni_accademici();
        $data['AnniDiCorso']=$this->Sys_model->get_anni_di_corso();
        $data['IntestazioniProgettoDidattica']['ElencoDocenti']=$this->Sys_model->get_docente($this->session->userdata('CodiceDocente'));
        $data['IntestazioniProgettoDidattica']['CodiceProgettoDidattica']=$CodiceProgettoDidattica;
        //var_dump($data);
        echo json_encode($data);
    }
    
    /**
     * 
     * @param type $CodiceCorso
     * @author Luca Giordano
     */
    public function ajax_get_materie_intestazioni($CodiceCorso)
    {
        $data=$this->Sys_model->get_materia_intestazioni($CodiceCorso,$this->session->userdata('CodiceDocente'));
        echo json_encode($data);
    }
    
    
    /**
     * Funzione ajax che ritorna l'elenco dei corsi di una determinata materia in base al docente loggato ed al codice facoltà selezionato
     * @param type $CodiceFacolta
     * @author Luca Giordano
     */
    public function ajax_load_block_corso_facolta($CodiceFacolta)
    {
        $data=$this->Sys_model->load_corso_facolta($CodiceFacolta,$this->session->userdata("CodiceDocente"));
        echo json_encode($data);
    }
    
    /**
     * 
     * @param type $CodiceCorso
     * @author Luca Giordano
     */
    public function ajax_load_block_materie_corso($CodiceCorso)
    {
        $data=$this->Sys_model->load_materie_corso($CodiceCorso,$this->session->userdata("CodiceDocente"));
        echo json_encode($data);
    }
    
    /**
     * @author Alessandro Galli
     */
    public function ajax_load_content_festivita_tirocini()
    {
        $block=$this->load_content_festivita_tirocini();
        echo $block;
    }
    
    /**
     * 
     * @return html
     * @author Alessandro Galli
     * 
     * Ritorna il content relativo alla pianificazione dei periodi
     */
    public function load_content_festivita_tirocini()
    {
        $data=array();
        $classi=  $this->Sys_model->get_classi(6);
        foreach ($classi as $key => $classe) {
            $classi[$key]['Descrizione']=$classe['CognomePrimoStudente']." ".$classe['Descrizione'];
        }
        $data['data']['classi']=$classi;
        $data['annoaccademico_options']=  $this->Sys_model->get_anniaccademici();
        return $this->load->view("sys/desktop/content/festivita_tirocini",$data,true);
    }
    
    /**
     * @author Alessandro Galli
     */
    public function ajax_load_content_disponibilita_docenti()
    {
        $block=$this->load_content_disponibilita_docenti();
        echo $block;
    }
    
    /**
     * 
     * @return html
     * @author Alessandro Galli
     * 
     * Ritorna il content pre la pianificazione dei periodi di disponibilità dei docenti 
     */
    public function load_content_disponibilita_docenti()
    {
        $data=array();
        $data['block']['select_docenti']=  $this->load_block_select_docenti();
        return $this->load->view("sys/desktop/content/disponibilita_docenti",$data,true);
    }
    
    /**
     * @author Alessandro Galli
     */
    public function ajax_load_content_calendari()
    {
        $block=$this->load_content_calendari();
        echo $block;
    }
    
    /**
     * 
     * @param type $interface
     * @return content/home.php
     * @author Alessandro Galli
     * 
     * Ritorna il content per la gestione dei calendari e delle lezioni
     */
    public function load_content_calendari($codice_annoaccademico="")
    {      
         $data['data']['anniaccademici']=  $this->Sys_model->get_anniaccademici();
         $data['data']['block']['select_docenti']=$this->load_block_select_docenti();
         $data['data']['block']['select_materie']=$this->load_block_select_materie();
         $data['data']['block']['calendari']="";
         $data['docenti']=$this->Sys_model->get_options_docente();
         $data['sedi']=$this->Sys_model->get_options_sedi();
         $data['aule']=$this->Sys_model->get_options_aule();
         $data['facolta_options']=$this->Sys_model->get_facolta_options();
         $data['corso_options']=$this->Sys_model->get_corso_options();
        return $this->load->view("sys/desktop/content/calendari",$data,true);
    }
    
    /**
     * @author Alessandro Galli
     */
    public function ajax_load_content_calendario_generale()
    {
        $block=$this->load_content_calendario_generale();
        echo $block;
    }
    
    /**
     * 
     * @return html
     * @author Alessandro Galli
     * 
     * Ritorna il content con il calendario del docente loggato
     */
    public function load_content_calendario_generale()
    {
        $data=array();
        $data['anniaccademici']=$this->Sys_model->get_anniaccademici();
        $data['block']['select_docenti']='';//$this->load_block_select_docenti(null, $parametri['docente']);
        $data['block']['select_materie']='';//$this->load_block_select_materie(null, $parametri['materia']);
        $data['options_tipoesame']=$this->Sys_model->get_options_tipoesame();
        $data['options_docenti']=$this->Sys_model->get_options_docente();
        $data['options_materie']=$this->Sys_model->get_options_materia();
        $data['options_sedi']=$this->Sys_model->get_options_sedi();
        $data['options_aule']=$this->Sys_model->get_options_aule();
        return $this->load->view("sys/desktop/content/calendario_generale",$data,true);
    }
    
    /**
     * @author Alessandro Galli
     * 
     */
    public function ajax_load_block_calendari()
    {
        $block=  $this->load_block_calendari();
        echo $block;
    }
    
    /**
     * 
     * @return html
     * @author Alessandro Galli
     * 
     * Blocco con tutti i calendari risultati da una ricerca partita dal content dei calendari
     */
    public function load_block_calendari()
    {
        $data['data']['calendari']=array();
        $vistacalendari=array();
        $post=$_POST;
        if(array_key_exists('vistacalendari', $post))
        {
            $vistacalendari=$post['vistacalendari'];
        }
        $data['data']['block']=array();
        $classi_selezionate=array();
        if(array_key_exists('classi', $post))
        {
            foreach ($post['classi'] as $key => $value) {
                $classi_selezionate[]=$value;
            }
        }
        /*$anniaccademici=  $this->Sys_model->get_anniaccademici();
        $codice_annoaccademico=7;
        if(array_key_exists('annoaccademico', $post))
        {
            if($post['annoaccademico']!='')
            {
                $codice_annoaccademico=$post['annoaccademico'];
            }
        }*/
        $classi=$this->Sys_model->get_classi();
        foreach ($classi as $key => $classe) {
            $codice_classe=$classe['Codice'];
            if(count($classi_selezionate)>0)
            {
                if(in_array($codice_classe, $classi_selezionate))
                {
                    $data['data']['calendari'][$codice_classe]['descrizione']=$classe['Descrizione'];
                    $parametri['codice_classe']=$codice_classe;
                    $parametri['vistacalendari']=$vistacalendari;
                    //$data['data']['calendari'][$codice_classe]['block']=$this->load_block_calendario($parametri);
                }
            }
            else
            {
                $data['data']['calendari'][$codice_classe]['descrizione']=$classe['Descrizione'];
                $parametri['codice_classe']=$codice_classe;
                $parametri['vistacalendari']=$vistacalendari;
                //$data['data']['calendari'][$codice_classe]['block']=$this->load_block_calendario($parametri);
            }
            
        }
        
        return $this->load->view("sys/desktop/block/calendari",$data,true);
    }
    
    
    /**
     * 
     * @param type $codice_classe
     * @author Alessandro Galli
     */
    public function ajax_load_block_calendario($codice_classe)
    {
        $block=  $this->load_block_calendario($codice_classe);
        echo $block;
    }
    
    /**
     * 
     * @param int $codice_classe codice della classe di cui si vuole vedere il calendario
     * @return html
     * @author Alessandro Galli
     * 
     * Ritorna il blocco di un singolo calendario(nella visualizzazione settimanel, o mensile, o entrambe) di una classe
     */
    public function load_block_calendario($codice_classe)
    {
        $data=array();
        $parametri=$_POST;
        //$vistacalendari=$parametri['vistacalendari'];
        $data['data']['parametri']=$parametri;
        $totale_ore_assegnate=0;
        $totale_ore_previste=0;
        if(array_key_exists('CodiceMateria', $parametri))
        {
            $codice_materia_selezionata=$parametri['CodiceMateria'];
            if($codice_materia_selezionata!='')
            {
                $lezioni_materia=$this->Sys_model->get_lezioni_from_classe_materia($codice_classe,$codice_materia_selezionata);
                $totminuti=0;
                foreach ($lezioni_materia as $key => $lezione) {
                    $date_diff=  $this->date_diff($lezione['OraInizio'], $lezione['OraFine']);
                    $durataminuti=0;
                    if(($date_diff['h']!=0||$date_diff['m']!=0))
                    {
                        $durataminuti=$date_diff['h']*60+$date_diff['m'];
                    }
                    $totminuti=$totminuti+$durataminuti;
                }
                $hours = floor($totminuti / 60);
                $minutes = $totminuti % 60;
                $totale_ore_assegnate=$hours.":".$minutes;
                $totale_ore_previste=  $this->Sys_model->get_oretotali_pianostudi_materie($codice_classe,$codice_materia_selezionata);
            }
            
        }
        $data['totale_ore_assegnate']=$totale_ore_assegnate;
        
        $data['totale_ore_previste']=$totale_ore_previste;
        $data['data']['codice_classe']=$codice_classe;
        $data['data']['classe']=  $this->Sys_model->get_classe($codice_classe);
        //$data['data']['vistacalendari']=$vistacalendari;
        $mese=$parametri['mese'];
        $data['mese']=$mese;
        //$codiceannoaccademico=$parametri['annoaccademico']; 
         //$codice_annoaccademico_descrizione=$this->Sys_model->db_get_value('annoaccademico','"Descrizione"',"\"Codice\"=$codiceannoaccademico");
            /*$anni=explode("/", $codice_annoaccademico_descrizione);
            if(($mese=='09')||($mese=='10')||($mese=='11')||($mese=='12'))
                $anno=$anni[0];
            else
                $anno=$anni[1];
            $parametri['anno']=$anno;*/
        $anno=$parametri['anno'];
        $data['anno']=$anno;    
        //$data['data']['anno']=  $this->Sys_model->get_anno($codiceannoaccademico,$mese);
        //$data['data']['block']['select_docenti']=$this->load_block_select_docenti(null, $parametri['docente']);
        //$data['data']['block']['select_materie']=$this->load_block_select_materie(null, $parametri['materia']);
        $calendari=array();
        $calendari[1]['nomecalendario']='Lezioni';
        $calendari[1]['tiporender']='evento';
        $calendari[1]['coloreEventi']='blue';
        $calendari[1]['eventi']=array();


        $calendari[2]['nomecalendario']='Esami';
        $calendari[2]['tiporender']='evento';
        $calendari[2]['coloreEventi']='red';
        $calendari[2]['eventi']=array();

        $calendari[3]['nomecalendario']='Eventi';
        $calendari[3]['tiporender']='evento';
        $calendari[3]['coloreEventi']='green';
        $calendari[3]['eventi']=array();

        //LEZIONI EVENTI ESAMI
        $eventi_calendario_generale=  $this->Sys_model->get_calendario_generale($codice_classe,$parametri);
        
        foreach ($eventi_calendario_generale as $key => $evento_calendario_generale) {
            $evento=array();
            $CodiceTipoCalendario=$evento_calendario_generale['CodiceTipoCalendario'];
            $evento['codice']=$evento_calendario_generale['Codice'];
            $evento['titolo']='';
            if($CodiceTipoCalendario==1)
            {
                $codice_materia=$evento_calendario_generale['CodiceMateria'];
                $codice_docente=$evento_calendario_generale['CodiceDocente1'];
                $evento['titolo']='';
                if($codice_materia!='')
                {
                    $descrizione_materia=$this->Sys_model->db_get_value("materia",'"DescrizioneITA"',"\"Codice\"=$codice_materia");
                    
                    $evento['titolo']=  $descrizione_materia;
                }
                if($codice_docente!='')
                {
                    $docente=$this->Sys_model->db_get_row("docente",'"Cognome","Nome"',"\"Codice\"=$codice_docente");
                    $cognomenome_docente=$docente['Cognome']." ".$docente['Nome'];
                    $evento['titolo']=$evento['titolo']." D:".$cognomenome_docente;
                }
                
            }
            if($CodiceTipoCalendario==2)
            {
                $codice_materia=$evento_calendario_generale['CodiceMateria'];
                $codice_docente=$evento_calendario_generale['CodiceDocente1'];
                $evento['titolo']='';
                if($codice_materia!='')
                {
                    $descrizione_materia=$this->Sys_model->db_get_value("materia",'"DescrizioneITA"',"\"Codice\"=$codice_materia");
                    
                    $evento['titolo']=  $descrizione_materia;
                }
                if($codice_docente!='')
                {
                    $docente=$this->Sys_model->db_get_row("docente",'"Cognome","Nome"',"\"Codice\"=$codice_docente");
                    $cognomenome_docente=$docente['Cognome']." ".$docente['Nome'];
                    $evento['titolo']=$evento['titolo']." D:".$cognomenome_docente;
                }
            }
            if($CodiceTipoCalendario==3)
            {
                $evento['titolo']=$evento_calendario_generale['Note'];
            }
            $giorno=  date('Y-m-d', strtotime($evento_calendario_generale['Giorno']));
            $evento['data']=$giorno;
            $orainizio= date('H:i', strtotime($evento_calendario_generale['OraInizio']));
            $evento['orainizio']=$orainizio;
            $orafine=  date('H:i', strtotime($evento_calendario_generale['OraFine']));
            $evento['orafine']=$orafine;
            
            $evento['CodiceTipoCalendario']=$CodiceTipoCalendario;
            $evento['CodiceRegistroPresenze']=$evento_calendario_generale['CodiceRegistroPresenze'];

            
            /*$calendari[$CodiceTipoCalendario]['nomecalendario']='indefinito';
            $calendari[$CodiceTipoCalendario]['tiporender']='evento';
            $calendari[$CodiceTipoCalendario]['coloreEventi']='gray';*/
            
            
            
            

            
            $calendari[$CodiceTipoCalendario]['eventi'][]=$evento;
        }
        
        // PERIODI
            $periodi_classe= $this->Sys_model->get_periodi_classe($codice_classe,$parametri);
            foreach ($periodi_classe as $key => $periodo) {
                $evento=array();
                $evento['codice']='';
                $evento['titolo']='';
                $evento['data']=  date('Y-m-d', strtotime($periodo['Giorno']));
                $evento['orainizio']= date('H:i', strtotime($periodo['OraInizio']));
                $evento['orafine']= date('H:i', strtotime($periodo['OraFine']));
                $CodiceTipoCalendario=$periodo['CodiceTipoCalendario'];
                $evento['CodiceTipoCalendario']=$CodiceTipoCalendario;
                $evento['CodiceRegistroPresenze']='';
                
                $calendari[$CodiceTipoCalendario]['CodiceTipoCalendario']='';
                $calendari[$CodiceTipoCalendario]['CodiceRegistroPresenze']='';
                
                $calendari[$CodiceTipoCalendario]['nomecalendario']='indefinito';
                $calendari[$CodiceTipoCalendario]['tiporender']='background';
                $calendari[$CodiceTipoCalendario]['coloreEventi']='gray';

                if($CodiceTipoCalendario==9)
                {
                    $calendari[$CodiceTipoCalendario]['nomecalendario']='Vacanza';
                    $calendari[$CodiceTipoCalendario]['tiporender']='background';
                    $calendari[$CodiceTipoCalendario]['coloreEventi']='lightblue';
                }
                
                if($CodiceTipoCalendario==10)
                {
                    $calendari[$CodiceTipoCalendario]['nomecalendario']='Esami';
                    $calendari[$CodiceTipoCalendario]['tiporender']='background';
                    $calendari[$CodiceTipoCalendario]['coloreEventi']='green';
                }
                
                if($CodiceTipoCalendario==11)
                {
                    $calendari[$CodiceTipoCalendario]['nomecalendario']='Esami';
                    $calendari[$CodiceTipoCalendario]['tiporender']='background';
                    $calendari[$CodiceTipoCalendario]['coloreEventi']='red';
                }
                
                $calendari[$CodiceTipoCalendario]['eventi'][]=$evento;
            }
        
            // IMPEGNI DOCENTE
            $eventi_calendario_generale= $this->Sys_model->get_impegni_docente($codice_classe,$parametri);
            foreach ($eventi_calendario_generale as $key => $evento_calendario_generale) {
                $evento=array();
                $evento['codice']=$evento_calendario_generale['Codice'];
                $giorno=  date('Y-m-d', strtotime($evento_calendario_generale['Giorno']));
                $evento['data']=$giorno;
                $orainizio= date('H:i', strtotime($evento_calendario_generale['OraInizio']));
                $evento['orainizio']=$orainizio;
                $orafine= date('H:i', strtotime($evento_calendario_generale['OraFine']));
                $evento['orafine']=$orafine;
                $CodiceTipoCalendario=$evento_calendario_generale['CodiceTipoCalendario'];
                $evento['CodiceTipoCalendario']='';
                $evento['CodiceRegistroPresenze']='';
                $evento['titolo']=$evento_calendario_generale['ArgomentoDescrizione'];
                if($CodiceTipoCalendario==1)
                {
                    $codice_materia=$evento_calendario_generale['CodiceMateria'];
                    if($codice_materia!='')
                    {
                        $descrizione_materia=$this->Sys_model->db_get_value("materia",'"DescrizioneITA"',"\"Codice\"=$codice_materia");

                        $evento['titolo']=  $descrizione_materia;
                    }
                    $codice_classe=$evento_calendario_generale['CodiceClasse'];
                    if($codice_classe!='')
                    {
                        $descrizione_classe=$this->Sys_model->db_get_value("classe",'"CognomePrimoStudente"',"\"Codice\"=$codice_classe");

                        $evento['titolo']=  $evento['titolo']." C:".$descrizione_classe;
                    }

                }
                $calendari['impegnidocente']['nomecalendario']='lezioni';
                $calendari['impegnidocente']['tiporender']='evento';
                $calendari['impegnidocente']['coloreEventi']='#bcbcbc';
                $calendari['impegnidocente']['CodiceTipoCalendario']='';
                $calendari['impegnidocente']['CodiceRegistroPresenze']='';
                $calendari['impegnidocente']['eventi'][]=$evento;
            }
            
        // INDISPONIBILITA' DOCENTE

            $eventi_calendario_generale= array(); //$this->Sys_model->get_indisponibilita_docente($parametri);
            foreach ($eventi_calendario_generale as $key => $evento_calendario_generale) {
                $evento=array();
                $evento['codice']=$evento_calendario_generale['Codice'];
                $evento['titolo']=$evento_calendario_generale['ArgomentoDescrizione'];
                $giorno=  date('Y-m-d', strtotime($evento_calendario_generale['Giorno']));
                $evento['data']=$giorno;
                $orainizio= date('H:i', strtotime($evento_calendario_generale['OraInizio']));
                $evento['orainizio']=$orainizio;
                $orafine= date('H:i', strtotime($evento_calendario_generale['OraFine']));
                $evento['orafine']=$orafine;
                $CodiceTipoCalendario=$evento_calendario_generale['CodiceTipoCalendario'];
                $evento['CodiceTipoCalendario']='';
                $evento['CodiceRegistroPresenze']='';
                
                $calendari['indisponibilitadocente']['nomecalendario']='Indisponibilità';
                $calendari['indisponibilitadocente']['tiporender']='background';
                $calendari['indisponibilitadocente']['coloreEventi']='red';
                
                $calendari['indisponibilitadocente']['eventi'][]=$evento;
            }
            
        $data['data']['calendari']=$calendari;
        return $this->load->view("sys/desktop/block/calendario",$data,true);
    }
    
    /**
     * @author Alessandro Galli
     * 
     * Imposta una lezione nel calendario di una classe. parametri nel post
     */
    public function ajax_pianificazione_calendario()
    {
        $post=$_POST;
        $fields_calendario_generale=array();
        $fields_registropresenze=array();
        if(array_key_exists('calendario_generale', $post))
        {
           $fields_calendario_generale=$post['calendario_generale']; 
        }
        if(array_key_exists('registropresenze', $post))
        {
           $fields_registropresenze=$post['registropresenze'];
        }
        $orainizio='1899-12-30 '.date('H:i', strtotime(str_replace('GMT+0000', '', $post['start'])));
        $orafine='1899-12-30 '.date('H:i', strtotime(str_replace('GMT+0000', '', $post['end'])));
        $codice_classe=$fields_calendario_generale['CodiceClasse'];
        $row_classe=  $this->Sys_model->db_get_row('classe', "*", "\"Codice\"=$codice_classe");
        $fields_calendario_generale['CodiceFacolta']=$row_classe['CodiceFacolta'];
        $fields_calendario_generale['CodiceCorso']=$row_classe['CodiceCorso'];
        $fields_calendario_generale['CodiceAnnoAccademico']=$row_classe['CodiceAnnoAccademico'];
        $fields_calendario_generale['CodiceAnnoDiCorso']=$row_classe['CodiceAnnoDiCorso'];

       
        $tipocalendario=$fields_calendario_generale['CodiceTipoCalendario'];
        $fields_calendario_generale['Giorno']=date('Y-m-d', strtotime($post['start']));
        $fields_calendario_generale['OraInizio']=$orainizio;
        $fields_calendario_generale['OraFine']=$orafine;
        
        
        
        
        
        if(array_key_exists('docenti', $post))
        {
            if(array_key_exists(1, $post['docenti']))
            {
                $fields_calendario_generale['CodiceDocente1']=$post['docenti'][1];
            }
            if(array_key_exists(2, $post['docenti']))
            {
                $fields_calendario_generale['CodiceDocente2']=$post['docenti'][2];
            }
            if(array_key_exists(3, $post['docenti']))
            {
                $fields_calendario_generale['CodiceDocente3']=$post['docenti'][3];
            }
            
        }
        
        if($tipocalendario==1)
        {
            //$fields=$post['dati_lezione'];
            //$codice_registropresenze=$this->Sys_model->set_registropresenze_lezione($post,$fields,'null');
            //$fields_calendario_generale['ArgomentoDescrizione']=$post['ArgomentoDescrizione'];
            //$this->Sys_model->set_lezione($post,$fields,$codice_registropresenze);
        }
        if($tipocalendario==2)
        {
            //$fields=$post['dati_esame'];
            
            //$codice_registropresenze=$this->Sys_model->set_registropresenze_esame($post,$fields,'null');
            //$fields_calendario_generale['ArgomentoDescrizione']=$post['ArgomentoDescrizione'];
            //$this->Sys_model->set_esame($post,$fields,$codice_registropresenze);
        }
        /*if($tipocalendario==3)
        {
            //$fields=$post['dati_evento'];
            //$this->Sys_model->set_evento($post,$fields);
        }*/
        $fields_registropresenze['CodiceTipoCalendario']=$fields_calendario_generale['CodiceTipoCalendario'];
        $fields_registropresenze['CodiceFacolta']=$fields_calendario_generale['CodiceFacolta'];
        $fields_registropresenze['CodiceCorso']=$fields_calendario_generale['CodiceCorso'];
        $fields_registropresenze['CodiceClasse']=$fields_calendario_generale['CodiceClasse'];
        $fields_registropresenze['CodiceAnnoAccademico']=$fields_calendario_generale['CodiceAnnoAccademico'];
        $fields_registropresenze['CodiceAula']=$fields_calendario_generale['CodiceAula'];
        $fields_registropresenze['CodiceSede']=$fields_calendario_generale['CodiceSede'];
        $fields_registropresenze['CodiceAnnoDiCorso']=$fields_calendario_generale['CodiceAnnoDiCorso'];
        $fields_registropresenze['CodiceMateria']=$fields_calendario_generale['CodiceMateria'];
        $fields_registropresenze['Note']=$fields_calendario_generale['Note'];
        $fields_registropresenze['Data']=$fields_calendario_generale['Giorno'];
        $fields_registropresenze['CodiceDocente1']=$fields_calendario_generale['CodiceDocente1'];
        $fields_registropresenze['OraPrevistaInizioLezione1']=$fields_calendario_generale['OraInizio'];
        $fields_registropresenze['OraPrevistaFineLezione1']=$fields_calendario_generale['OraFine'];
        $fields_registropresenze['CodiceDocenteDiRiferimento']=$fields_calendario_generale['CodiceDocenteDiRiferimento'];
        $fields_registropresenze['CodiceDocente2']=$fields_calendario_generale['CodiceDocente2'];
        $fields_registropresenze['GruppoLingua']=$fields_calendario_generale['GruppoLingua'];
        $codice_registropresenze=$this->Sys_model->insert_registropresenze($fields_registropresenze);
        
        $ElencoDocenti='';
        foreach ($post['docenti'] as $key => $CodiceDocente) {
                if($CodiceDocente!='')
                {
                    $this->Sys_model->insert_registropresenze_docente($codice_registropresenze,$CodiceDocente);
                    $docente=  $this->Sys_model->db_get_row('docente','*',"\"Codice\"=$CodiceDocente");
                    if($docente!=null)
                    {
                       $ElencoDocenti=$ElencoDocenti." ".$docente['Cognome']." ".$docente['Nome']; 
                    }
                }
            }
        $ElencoDocenti=  str_replace("'", "''", $ElencoDocenti);
        $fields_calendario_generale['ElencoDocenti']=$ElencoDocenti;    
        $fields_calendario_generale['CodiceRegistroPresenze']=$codice_registropresenze;
        $this->Sys_model->insert_calendario_generale($fields_calendario_generale);
        
    }
    
    public function ajax_change_calendario()
    {
        $post=$_POST;
        $arg=$post;
        $this->Sys_model->update_calendario_generale_data_ora($arg);
        $this->Sys_model->update_registropresenze_data_ora($arg);
    }
    
    /**
     * @author Alessandro Galli
     * 
     * Associa un tipo di periodo(vacanze,tirocini,esami ecc) a uno specifico giorno. parametri nel post
     */
    public function ajax_set_periodo()
    {
        $post=$_POST;
        $arg=array();
        $arg['Giorno']=$post['Giorno'];
        $arg['OraInizio']='1899-12-30 08:00';
        $arg['OraFine']='1899-12-30 20:00';
        $arg['CodiceTipoCalendario']=$post['CodiceTipoCalendario'];
        $arg['CodiceAnnoAccademico']=$post['CodiceAnnoAccademico'];
        $arg['CodiceClasse']=$post['CodiceClasse'];
        $this->Sys_model->set_periodo($arg);
    }
    
    /**
     * @author Alessandro Galli
     * 
     * Associa l'indisponibilità di un docente a uno specifico giorno. Parametri nel post
     */
    public function ajax_set_indisponibilita()
    {
        $post=$_POST;
        $parametri=array();
        $parametri['fields']['Giorno']=$post['data_evento'];
        if($post['indisponibilita']=='intera')
        {
            $parametri['fields']['OraInizio']='1899-12-30 08:00';
        }
        if($post['indisponibilita']=='mezza')
        {
            $parametri['fields']['OraInizio']='1899-12-30 13:00';
        }
        $parametri['fields']['OraFine']='1899-12-30 20:00';
        $parametri['fields']['CodiceTipoCalendario']=13;
        $parametri['fields']['CodiceAnnoAccademico']=6;
        $parametri['fields']['CodiceDocente1']=$post['docente'];
        $this->Sys_model->set_evento($parametri);
    }
    
    public function ajax_load_block_appuntamento()
    {
        $block=$this->load_block_appuntamento();
        echo $block;
    }
    
    public function load_block_appuntamento()
    {
        $data=array();
        $data['mode']='edit';
        $data['tipocalendario']=3;
        $data['codice_calendario_generale']='null';
        $data['fields_calendario_generale_registropresenze']=  $this->load_block_fields_calendario_generale_appuntamento(1, null,'edit');
        return $this->load->view("sys/desktop/block/appuntamento",$data,true);
        
    }
    
    public function ajax_load_block_lezione_new()
    {
        $block=$this->load_block_lezione_new();
        echo $block;
    }
    
    public function load_block_lezione_new()
    {
        $mode='edit';
        
    }
    /**
     * 
     * @param type $codice_lezione
     * @param type $mode
     * @author Alessandro Galli
     */
    public function ajax_load_block_lezione($codice_calendario_generale='',$mode='view')
    {
        $block=$this->load_block_lezione($codice_calendario_generale,$mode);
        echo $block;
    }
    
    
    /**
     * 
     * @param type $codice_registropresenze
     * @param type $mode
     * @return type
     * @author Alessandro Galli
     * Carica il blocco della lezione
     */
    public function load_block_lezione($codice_calendario_generale='',$mode='view')
    {
        $data=array();
        $codice_registropresenze=  $this->Sys_model->db_get_value("calendario_generale","\"CodiceRegistroPresenze\"","\"Codice\"=$codice_calendario_generale");
        $data['codice_registropresenze']=$codice_registropresenze;
        $registropresenze=  $this->Sys_model->db_get_row("registropresenze","*,to_char(\"OraPrevistaInizioLezione1\",'HH24:MI') as \"OraPrevistaInizioLezione1\",to_char(\"OraPrevistaFineLezione1\",'HH24:MI') as \"OraPrevistaFineLezione1\"","\"Codice\"=$codice_registropresenze");//$this->Sys_model->get_registropresenze($codice_registropresenze);
        $data['registropresenze']=$registropresenze;
        
        $calendario_generale=  $this->Sys_model->get_calendario_generale_from_registropresenze($codice_registropresenze);
        $data['calendario_generale']=$calendario_generale;
        $data['codice_calendario_generale']=$calendario_generale['Codice'];
        //$codice_materia=$calendario_generale['CodiceMateria'];
        //$codice_annoaccademico=$calendario_generale['CodiceAnnoAccademico'];
        
        
        $data['data']['argomenti_mancanti_progettodidattica']=array();
        $data['data']['argomenti_lezione']=array();
        $data['studenti']= array();
        
        $progettodidattica_lezione=$this->Sys_model->get_progettodidattica_lezione($codice_registropresenze);
        if($progettodidattica_lezione!=null)
        {
            $codice_progettodidattica=$progettodidattica_lezione['CodiceProgettoDidattica'];
            $data['data']['progettodidattica_lezione']=$progettodidattica_lezione;
            $codice_progettodidattica_lezione=$progettodidattica_lezione['Codice'];
            //$argomenti_materia=  $this->Sys_model->get_argomenti_materia($codice_materia,$codice_annoaccademico);
            //$data['data']['argomenti_materia']=$argomenti_materia;
            $argomenti_mancanti_progettodidattica=  $this->Sys_model->get_argomenti_mancanti_progettodidattica($codice_progettodidattica);
            $data['data']['argomenti_mancanti_progettodidattica']=$argomenti_mancanti_progettodidattica;
            $argomenti_lezione=  $this->Sys_model->get_argomenti_lezione($codice_progettodidattica_lezione);
            $data['data']['argomenti_lezione']=$argomenti_lezione;
            $data['studenti']= $this->Sys_model->get_studenti_lezione($codice_registropresenze);
        }
        
        $data['data']['mode']=$mode;
        
        $data['StudentiEffettivi']=0;

        foreach($data['studenti'] as $i => $studente)
        {
        $fotoStudente=$this->Sys_model->get_foto_studente($studente['codicestudente']);

        if(count($fotoStudente)>0)
        {
            if($fotoStudente[0]['NomeFile']==null)
                $data['studenti'][$i]['Path_Foto']= domain_url().'/jdocserver/jdocuniludes/foto/man-icon.png';
            else
            {
                $path_url= $fotoStudente[0]['NomeFile'].".".$fotoStudente[0]['Estensione'];
                $data['studenti'][$i]['Path_Foto']= domain_url().'/jdocserver/jdocuniludes/foto/'.$path_url;
            }
        }
        else
            $data['studenti'][$i]['Path_Foto']= domain_url().'/jdocserver/jdocuniludes/foto/man-icon.png';
        }
       /* $data['options_docenti']=$this->Sys_model->get_options_docente();
        $data['options_materie']=$this->Sys_model->get_options_materia();
        $data['options_sedi']=$this->Sys_model->get_options_sedi();
        $data['options_aule']=$this->Sys_model->get_options_aule();
        $data['options_classi']=$this->Sys_model->get_options_classi();
        $gruppi_lingua[]=array("Codice"=>"0","Descrizione"=>"0");
        $gruppi_lingua[]=array("Codice"=>"1A","Descrizione"=>"1A");
        $gruppi_lingua[]=array("Codice"=>"1B","Descrizione"=>"1B");
        $gruppi_lingua[]=array("Codice"=>"2","Descrizione"=>"2");
        $data['options_gruppi_lingua']=$gruppi_lingua;*/
        $data['fields_calendario_generale_registropresenze']=  $this->load_block_fields_calendario_generale_registropresenze(1, $codice_calendario_generale,$mode);
        return $this->load->view("sys/desktop/block/lezione",$data,true);
    }
    
    public function ajax_load_block_esame($codice_calendario_generale='',$mode='view')
    {
        $block=$this->load_block_esame($codice_calendario_generale,$mode);
        echo $block;
    }
    
    /**
     * 
     * @param type $codice_registropresenze
     * @param type $mode
     * @return type
     * @author Alessandro Galli
     * Carica il blocco della lezione
     */
    public function load_block_esame($codice_calendario_generale='',$mode='view')
    {
        $data=array();
        $data['mode']=$mode;
        $data['codice_calendario_generale']=$codice_calendario_generale;
        $codice_registropresenze=  $this->Sys_model->db_get_value("calendario_generale","\"CodiceRegistroPresenze\"","\"Codice\"=$codice_calendario_generale");
        $data['codice_registropresenze']=$codice_registropresenze;
        $registropresenze=  $this->Sys_model->db_get_row("registropresenze","*","\"Codice\"=$codice_registropresenze");
        $data['registropresenze']=$registropresenze;
        $data['test']=$registropresenze;
        $data['calendario_generale']=  $this->Sys_model->db_get_row('calendario_generale','*',"\"Codice\"=$codice_calendario_generale");
        /*$data['options_docenti']=$this->Sys_model->get_options_docente();
        $data['options_materie']=$this->Sys_model->get_options_materia();
        $data['options_sedi']=$this->Sys_model->get_options_sedi();
        $data['options_aule']=$this->Sys_model->get_options_aule();
        $data['options_classi']=$this->Sys_model->get_options_classi();*/
        $data['fields_calendario_generale_registropresenze']=  $this->load_block_fields_calendario_generale_registropresenze(2, $codice_calendario_generale,$mode);
        return $this->load->view("sys/desktop/block/esame",$data,true);
    }
    
    public function ajax_load_block_evento($codice_calendario_generale='',$mode='view')
    {
        $block=$this->load_block_evento($codice_calendario_generale,$mode);
        echo $block;
    }
    
    /**
     * 
     * @param type $codice_registropresenze
     * @param type $mode
     * @return type
     * @author Alessandro Galli
     * Carica il blocco della lezione
     */
    public function load_block_evento($codice_calendario_generale='',$mode='view')
    {
        $data=array();
        $data['mode']=$mode;
        $data['codice_calendario_generale']=$codice_calendario_generale;
        $data['calendario_generale']=  $this->Sys_model->db_get_row('calendario_generale','*',"\"Codice\"=$codice_calendario_generale");
        /*$data['options_sedi']=$this->Sys_model->get_options_sedi();
        $data['options_aule']=$this->Sys_model->get_options_aule();*/
        $data['fields_calendario_generale_registropresenze']=  $this->load_block_fields_calendario_generale_registropresenze(3, $codice_calendario_generale,$mode);
        return $this->load->view("sys/desktop/block/evento",$data,true);
    }
    
    /**
     * 
     * @param type $codice_progettodidattica
     * @author Alessandro Galli
     * salva le modifiche a un progetto didattica
     */
    public function ajax_progettodidattica_save($codice_progettodidattica)
    {
        $post=$_POST;
        $codice_progettodidattica=$this->Sys_model->progettodidattica_save($codice_progettodidattica,$post);
        echo $this->load_block_progettodidattica($codice_progettodidattica, 'view');
    }
    
    /**
     * 
     * @param type $codice_lezione
     * @author Alessandro Galli
     * salva le modifiche a un progetto didattica
     */
    public function ajax_lezione_save($codice_calendario_generale)
    {
        $post=$_POST;
        $codice_registropresenze=  $this->Sys_model->db_get_value("calendario_generale","\"CodiceRegistroPresenze\"","\"Codice\"=$codice_calendario_generale");

        $this->Sys_model->salva_argomenti_lezione($codice_registropresenze,$post['argomento_previsto'],$post['argomento_extra']);
        $this->salva_calendario_generale($codice_calendario_generale);
        //$codice_lezione=$this->Sys_model->progettodidattica_save($codice_lezione,$post);
        echo $this->load_block_lezione($codice_calendario_generale, 'view');
    }
    
    public function salva_calendario_generale($codice_calendario_generale)
    {
        $post=$_POST;
        if(array_key_exists('calendario_generale', $post))
        {
            $fields_calendario_generale=$post['calendario_generale']; 
            $CodiceTipoCalendario=$fields_calendario_generale['CodiceTipoCalendario'];
            if(($CodiceTipoCalendario==1)||($CodiceTipoCalendario==2)||($CodiceTipoCalendario==3))
            {
                $codice_registropresenze=  $this->Sys_model->db_get_value("calendario_generale","\"CodiceRegistroPresenze\"","\"Codice\"=$codice_calendario_generale");
                $fields_registropresenze=array();

                if(array_key_exists('docenti', $post))
                {
                    if(array_key_exists(1, $post['docenti']))
                    {
                        $fields_calendario_generale['CodiceDocente1']=$post['docenti'][1];
                    }
                    if(array_key_exists(2, $post['docenti']))
                    {
                        $fields_calendario_generale['CodiceDocente2']=$post['docenti'][2];
                    }
                    if(array_key_exists(3, $post['docenti']))
                    {
                        $fields_calendario_generale['CodiceDocente3']=$post['docenti'][3];
                    }

                }

                if(array_key_exists('registropresenze', $post))
                {
                   $fields_registropresenze=$post['registropresenze'];
                }
                if(array_key_exists('CodiceClasse', $fields_calendario_generale))
                {
                    $codice_classe=$fields_calendario_generale['CodiceClasse'];
                    $row_classe=  $this->Sys_model->db_get_row('classe', "*", "\"Codice\"=$codice_classe");
                    $fields_calendario_generale['CodiceFacolta']=$row_classe['CodiceFacolta'];
                    $fields_calendario_generale['CodiceCorso']=$row_classe['CodiceCorso'];
                    $fields_calendario_generale['CodiceAnnoAccademico']=$row_classe['CodiceAnnoAccademico'];
                    $fields_calendario_generale['CodiceAnnoDiCorso']=$row_classe['CodiceAnnoDiCorso'];
                }
                $fields_registropresenze['CodiceTipoCalendario']=  element('CodiceTipoCalendario', $fields_calendario_generale);
                $fields_registropresenze['CodiceFacolta']=element('CodiceFacolta', $fields_calendario_generale);
                $fields_registropresenze['CodiceCorso']=element('CodiceCorso', $fields_calendario_generale);
                $fields_registropresenze['CodiceClasse']=element('CodiceClasse', $fields_calendario_generale);
                $fields_registropresenze['CodiceAnnoAccademico']=element('CodiceAnnoAccademico', $fields_calendario_generale);
                $fields_registropresenze['CodiceAula']=element('CodiceAula', $fields_calendario_generale);
                $fields_registropresenze['CodiceSede']=element('CodiceSede', $fields_calendario_generale);
                $fields_registropresenze['CodiceAnnoDiCorso']=element('CodiceAnnoDiCorso', $fields_calendario_generale);
                $fields_registropresenze['CodiceMateria']=element('CodiceMateria', $fields_calendario_generale);
                $fields_registropresenze['Note']=element('Note', $fields_calendario_generale);
                $fields_registropresenze['Data']=element('Data', $fields_calendario_generale);
                $fields_registropresenze['CodiceDocente1']=element('CodiceDocente1', $fields_calendario_generale);
                $fields_registropresenze['OraPrevistaInizioLezione1']=element('OraPrevistaInizioLezione1', $fields_calendario_generale);
                $fields_registropresenze['OraPrevistaFineLezione1']=element('OraPrevistaFineLezione1', $fields_calendario_generale);
                $fields_registropresenze['CodiceDocenteDiRiferimento']=element('CodiceDocenteDiRiferimento', $fields_calendario_generale);
                $fields_registropresenze['CodiceDocente2']=element('CodiceDocente2', $fields_calendario_generale);
                $fields_registropresenze['GruppoLingua']=element('GruppoLingua', $fields_calendario_generale);

                $ElencoDocenti='';
                if(array_key_exists('docenti', $post))
                {
                    $this->Sys_model->reset_registropresenze_docente($codice_registropresenze);
                    foreach ($post['docenti'] as $key => $CodiceDocente) {
                            if($CodiceDocente!='')
                            {
                                $this->Sys_model->insert_registropresenze_docente($codice_registropresenze,$CodiceDocente);
                                $docente=  $this->Sys_model->db_get_row('docente','*',"\"Codice\"=$CodiceDocente");
                                if($docente!=null)
                                {
                                   $ElencoDocenti=$ElencoDocenti." ".$docente['Cognome']." ".$docente['Nome']; 
                                }
                            }
                        }
                }
                $ElencoDocenti=  str_replace("'", "''", $ElencoDocenti);
                $fields_calendario_generale['ElencoDocenti']=$ElencoDocenti; 

                $this->Sys_model->update_calendario_generale($fields_calendario_generale,$codice_calendario_generale);

                $this->Sys_model->update_registropresenze($fields_registropresenze,$codice_registropresenze);

            
            }
            if($CodiceTipoCalendario==4)
            {
                $anno=$post['anno'];
                $mese=$post['mese'];
                
                 $fields_calendario_generale['Giorno']='2016-10-14 00:00:00+01';
                 $fields_calendario_generale['OraInizio']='1899-12-30 14:00:00+01';
                 $fields_calendario_generale['OraFine']='1899-12-30 16:00:00+01';
                 $codice_calendario_generale=$this->Sys_model->insert_calendario_generale($fields_calendario_generale);
            }
        
        return $codice_calendario_generale;
       /* if(array_key_exists('docenti', $post))
        {
            foreach ($post['docenti'] as $key => $docente) {
                if($docente!='')
                {
                    $this->Sys_model->insert_registropresenze_docente($codice_registropresenze,$docente);
                }
                    
                }
        }*/
        }
    }
    public function ajax_esame_save($codice_calendario_generale)
    {
        $this->salva_calendario_generale($codice_calendario_generale);
        echo $this->load_block_esame($codice_calendario_generale, 'view');
    }
    
    public function ajax_evento_save($codice_calendario_generale)
    {
        $codice_calendario_generale=$this->salva_calendario_generale($codice_calendario_generale);
        echo $this->load_block_evento($codice_calendario_generale, 'view');
    }
    
    public function ajax_lezione_delete($CodiceCalendarioGenerale)
    {
        $this->Sys_model->lezione_delete($CodiceCalendarioGenerale);
    }
    
    public function ajax_esame_delete($CodiceCalendarioGenerale)
    {
        $this->Sys_model->esame_delete($CodiceCalendarioGenerale);
    }
    
    public function ajax_evento_delete($CodiceCalendarioGenerale)
    {
        $this->Sys_model->evento_delete($CodiceCalendarioGenerale);
    }
    
    /**
     * 
     * @param type $codice_progettodidattica
     * @author Alessandro Galli
     * Salva le modifiche a un gruppo di domande aperte
     */
    public function ajax_domandeaperte_save($codice_domandeaperte)
    {
        $post=$_POST;
        $codice_domandeaperte=$this->Sys_model->domandeaperte_save($codice_domandeaperte,$post);
        echo $this->load_block_docenti_domande_aperte($codice_domandeaperte,'view');
    }
    
    public function ajax_domandeaperte_delete($codice_domandeaperte)
    {
        $this->Sys_model->domandeaperte_delete($codice_domandeaperte);
    }
    
    public function ajax_multiplechoice_delete($codice_multiplechoice)
    {
        $this->Sys_model->multiplechoice_delete($codice_multiplechoice);
    }
    
    public function ajax_multiplechoice_domanda_delete($codice_multiplechoice_domanda)
    {
        $this->Sys_model->multiplechoice_domanda_delete($codice_multiplechoice_domanda);
    }
    
    public function ajax_stampa_riepilogo_lezioni_docente()
    {
        $data=array();
        $post=$_POST;
        $codice_docente=$this->session->userdata('CodiceDocente');
        $lezioni=$this->Sys_model->get_eventi_docenti_calendario($codice_docente,'2015','10');
        $data['lezioni']=$lezioni;
        $this->load->view("sys/desktop/stampe/pdf/riepilogo_lezioni_docente",$data);
    }
    
    public function load_block_calendario_periodi($arg)
    {
        $data['arg']=$arg;
        $codice_annoaccademico=$arg['CodiceAnnoAccademico'];
        $codice_annoaccademico_descrizione=$this->Sys_model->db_get_value('annoaccademico','"Descrizione"',"\"Codice\"=$codice_annoaccademico");
        $anni=explode("/", $codice_annoaccademico_descrizione);
        $anno1=$anni[0];
        $anno2=$anni[1];
        $array1=array("01-$anno1","02-$anno1","03-$anno1","04-$anno1","05-$anno1","06-$anno1","07-$anno1","08-$anno1","09-$anno1","10-$anno1","11-$anno1","12-$anno1");
        $array2=array("01-$anno2","02-$anno2","03-$anno2","04-$anno2","05-$anno2","06-$anno2","07-$anno2","08-$anno2","09-$anno2","10-$anno2","11-$anno2","12-$anno2");
        $array=  array_merge($array1,$array2);
        foreach ($array as $key => $meseanno) {
            $esploso=explode("-", $meseanno);
            $mese=$esploso[0];
            $anno=$esploso[1];
            $calendario_mese=array();
            $periodi=  $this->Sys_model->get_periodi($arg['CodiceClasse'],$anno,$mese);
            $calendario_mese['anno']=$anno;
            $calendario_mese['mese']=$mese;
            $calendario_mese['giorni']=array();
            foreach ($periodi as $key_periodo => $periodo) {
                $giorno['start']=$periodo['Giorno'];
                $giorno['end']=$periodo['Giorno'];
                $giorno['backgroundColor']='gray';
                
                if($periodo['CodiceTipoCalendario']==9)
                    $giorno['backgroundColor']='blue';
                if($periodo['CodiceTipoCalendario']==10)
                    $giorno['backgroundColor']='green';
                if($periodo['CodiceTipoCalendario']==11)
                    $giorno['backgroundColor']='red';
                if($periodo['CodiceTipoCalendario']==12)
                    $giorno['backgroundColor']='lightsteelblue';
                if($periodo['CodiceTipoCalendario']==13)
                    $giorno['backgroundColor']='white';
                $calendario_mese['giorni'][]=$giorno;
            }
            
            $calendario_mesi["$mese-$anno"]=$calendario_mese;
        }
        $data['calendario_mesi']=$calendario_mesi;
        return $this->load->view("sys/desktop/block/calendario_periodi",$data,true);
    }
    
    function date_diff($date1, $date2)
    {
        $date1timestamp=  strtotime($date1);
        $date2timestamp=  strtotime($date2);
        $all = round(($date2timestamp - $date1timestamp) / 60);
        $d = floor ($all / 1440);
        $h = floor (($all - $d * 1440) / 60);
         $m = $all - ($d * 1440) - ($h * 60);
        return array('d' => $d, 'h' => $h, 'm' => $m);
    }
    
    function load_block_fields_calendario_generale_registropresenze($tipocalendario=null,$codice_calendario_generale=null,$mode='edit')
    {
        $data['mode']=$mode;
        $data['calendario_generale']=array();
        $data['registropresenze']=array();
        $docenti=array();
        if($codice_calendario_generale!=null)
        {
            $data['calendario_generale']=  $this->Sys_model->db_get_row('calendario_generale','*',"\"Codice\"=$codice_calendario_generale");
            $calendario_generale=$data['calendario_generale'];
            $codice_registropresenze= $calendario_generale['CodiceRegistroPresenze'];
            $data['registropresenze']=  $this->Sys_model->db_get_row('registropresenze','*',"\"Codice\"=$codice_calendario_generale");
            if($codice_registropresenze!=null)
            {
                $docenti=$this->Sys_model->db_get("registropresenze_docente","\"CodiceDocente\"","\"CodiceRegistroPresenze\"=$codice_registropresenze");
            }
            else
            {
                $docenti=array();
            }
            if(count($docenti)==0)
            {
                if($calendario_generale['CodiceDocente1']!=null)
                    $docenti[0]['CodiceDocente']=$calendario_generale['CodiceDocente1'];
                if($calendario_generale['CodiceDocente2']!=null)
                    $docenti[1]['CodiceDocente']=$calendario_generale['CodiceDocente2'];
            }
        }
        $data['options_tipoesame']=$this->Sys_model->get_options_tipoesame();
        $data['options_docenti']=$this->Sys_model->get_options_docente();
        $data['options_materie']=$this->Sys_model->get_options_materia();
        $data['options_sedi']=$this->Sys_model->get_options_sedi();
        $data['options_aule']=$this->Sys_model->get_options_aule();
        $gruppi_lingua[]=array("Codice"=>"0","Descrizione"=>"0");
        $gruppi_lingua[]=array("Codice"=>"1A","Descrizione"=>"1A");
        $gruppi_lingua[]=array("Codice"=>"1B","Descrizione"=>"1B");
        $gruppi_lingua[]=array("Codice"=>"2","Descrizione"=>"2");
        $data['options_gruppi_lingua']=$gruppi_lingua;
        $data['tipocalendario']=$tipocalendario;
        $data['docenti']=  $docenti;
        return $this->load->view("sys/desktop/block/fields_calendario_generale_registropresenze",$data,true);
    }
    
    function load_block_fields_calendario_generale_appuntamento($tipocalendario=null,$codice_calendario_generale=null,$mode='edit')
    {
        $data['mode']=$mode;
        $data['calendario_generale']=array();
        $docenti=array();
        if($codice_calendario_generale!=null)
        {
            $data['calendario_generale']=  $this->Sys_model->db_get_row('calendario_generale','*',"\"Codice\"=$codice_calendario_generale");
            $calendario_generale=$data['calendario_generale'];
        }
        $data['options_studenti']=$this->Sys_model->get_options_studente();
        $data['options_tiporichiedente']=$this->Sys_model->get_options_tiporichiedente();
        $data['options_tiporicevente']=$this->Sys_model->get_options_tiporicevente();
        $data['options_docenti']=$this->Sys_model->get_options_docente();
 
        $gruppi_lingua[]=array("Codice"=>"0","Descrizione"=>"0");
        $gruppi_lingua[]=array("Codice"=>"1A","Descrizione"=>"1A");
        $gruppi_lingua[]=array("Codice"=>"1B","Descrizione"=>"1B");
        $gruppi_lingua[]=array("Codice"=>"2","Descrizione"=>"2");
        $data['options_gruppi_lingua']=$gruppi_lingua;
        $data['tipocalendario']=$tipocalendario;
        $data['docenti']=  $docenti;
        return $this->load->view("sys/desktop/block/fields_calendario_generale_appuntamento",$data,true);
    }
    
    function load_block_parametri_pianificazione_lezione()
    {
        /*$data['options_docenti']=$this->Sys_model->get_options_docente();
        $data['options_materie']=$this->Sys_model->get_options_materia();
        $data['options_sedi']=$this->Sys_model->get_options_sedi();
        $data['options_aule']=$this->Sys_model->get_options_aule();*/
        $data['fields_calendario_generale_registropresenza']=  $this->load_block_fields_calendario_generale_registropresenze(1);
        return $this->load->view("sys/desktop/block/parametri_pianificazione_lezione",$data,true);
    }
    
    function load_block_parametri_pianificazione_esame()
    {
        /*$data['options_tipoesame']=$this->Sys_model->get_options_tipoesame();
        $data['options_docenti']=$this->Sys_model->get_options_docente();
        $data['options_materie']=$this->Sys_model->get_options_materia();
        $data['options_sedi']=$this->Sys_model->get_options_sedi();
        $data['options_aule']=$this->Sys_model->get_options_aule();*/
        $data['fields_calendario_generale_registropresenza']=  $this->load_block_fields_calendario_generale_registropresenze(2);
        return $this->load->view("sys/desktop/block/parametri_pianificazione_esame",$data,true);
    }
    
    function load_block_parametri_pianificazione_evento()
    {
        /*$data['options_sedi']=$this->Sys_model->get_options_sedi();
        $data['options_aule']=$this->Sys_model->get_options_aule();*/
        $data['fields_calendario_generale_registropresenza']=  $this->load_block_fields_calendario_generale_registropresenze(3);
        return $this->load->view("sys/desktop/block/parametri_pianificazione_evento",$data,true);
    }
    
    public function get_logquery()
    {
        $log=$this->Sys_model->get_logquery();
        echo json_encode($log);
    }
    
    public function ajax_load_block_calendario_generale()
    {
        $post=$_POST;
        echo $this->load_block_calendario_generale($post);
        
    }
    
    public function load_block_calendario_generale($arg)
    {
        $data=array();
        $data['default_date']=date('Y-m-d');
        return $this->load->view("sys/desktop/block/calendario_generale",$data,true);
    }
    
    public function ajax_load_block_calendario_generale_filtrato()
    {
        $post=$_POST;
        
        echo $this->load_block_calendario_generale_filtrato($post);
        
    }
    
    public function load_block_calendario_generale_filtrato($post)
    {
        $data=array();
        $anno=$post['anno'];
        $mese=$post['mese'];
        $data['default_date']="$anno-$mese-15";
        return $this->load->view("sys/desktop/block/calendario_generale",$data,true);
    }
    
    public function ajax_set_sede($codice,$sede)
    {
        $query="
            UPDATE calendario_generale
            SET \"CodiceSede\"=$sede
            WHERE \"Codice\"=$codice
            ";
        $this->Sys_model->execute_query($query);
    }
    

}
?>
