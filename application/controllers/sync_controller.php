<?php

class Sync_controller extends CI_Controller {
    
    function __construct()
    {
        parent::__construct();
    }
    
    public function get_path_postgres()
    {
        $path_postgres=  $this->Sync_model->get_param("PathPostgres");
        return $path_postgres;
    }
    public function get_new_records()
    {
        $tables=  $this->get_tables_to_sync();
        $new_records=$this->Sync_model->get_new_local_records($tables); 
        $json_new_records=  json_encode($new_records);
        echo $json_new_records;
    }
    
    public function get_new_local_records()
    {
        
        $tables=  $this->get_tables_to_sync();
        $tables_new_records=$this->Sync_model->get_new_local_records($tables); 
        foreach ($tables_new_records as $key_tables_new_records => $table_new_records) {
            echo $key_tables_new_records.':<br/>';
            foreach ($table_new_records as $key_table_new_records => $new_records) {
                echo $key_table_new_records.'<br/>';
                if(count($new_records)>0)
                {
                    echo '<table border="1">';
                    echo '<thead>';
                    foreach ($new_records[0] as $col => $value) {
                        echo '<th>'.$col.'</th>';
                    }
                    echo '</thead>';
                    echo '<tbody>';
                    foreach ($new_records as $key_new_records => $record) {
                        echo '<tr>';
                        foreach ($record as $key_record => $val) {
                            echo '<td>'.$val.'</td>';
                        }
                        echo '</tr>';
                    }
                    echo '</tbody>';
                    echo '</table>';
                }
            }
            echo '<br/><br/><br/>';
        }
        return $tables_new_records;
    }
    
    public function  get_new_remote_records()
    {
        $json = file_get_contents("http://uniludes.cloudapp.net:8822/uniludesweb/index.php/sync_controller/get_new_records/");
        $tables_new_records = json_decode($json,true);
        foreach ($tables_new_records as $key_tables_new_records => $table_new_records) {
            echo $key_tables_new_records.':<br/>';
            foreach ($table_new_records as $key_table_new_records => $new_records) {
                echo $key_table_new_records.'<br/>';
                if(count($new_records)>0)
                {
                    echo '<table border="1">';
                    echo '<thead>';
                    foreach ($new_records[0] as $col => $value) {
                        echo '<th>'.$col.'</th>';
                    }
                    echo '</thead>';
                    echo '<tbody>';
                    foreach ($new_records as $key_new_records => $record) {
                        echo '<tr>';
                        foreach ($record as $key_record => $val) {
                            echo '<td>'.$val.'</td>';
                        }
                        echo '</tr>';
                    }
                    echo '</tbody>';
                    echo '</table>';
                }
            }
            echo '<br/><br/><br/>';
        }
        return $tables_new_records;
    }
    
    
    public function  get_local_logquery()
    {
        $logquery = $this->Sys_model->get_logquery();
        if(count($logquery)>0)
        {
            echo '<table border="1">';
            echo '<thead>';
            foreach ($logquery[0] as $col => $value) {
                echo '<th>'.$col.'</th>';
            }
            echo '</thead>';
            echo '<tbody>';
            foreach ($logquery as $key_new_records => $logquery_row) {
                echo '<tr>';
                foreach ($logquery_row as $key_record => $val) {
                    echo '<td>'.$val.'</td>';
                }
                echo '</tr>';
            }
            echo '</tbody>';
            echo '</table>';
        }
    }
    
    
    public function  get_remote_logquery()
    {
        $json = file_get_contents("http://uniludes.cloudapp.net:8822/uniludesweb/index.php/sys_viewcontroller/get_logquery/");
        $logquery = json_decode($json,true);
        if(count($logquery)>0)
        {
            echo '<table border="1">';
            echo '<thead>';
            foreach ($logquery[0] as $col => $value) {
                echo '<th>'.$col.'</th>';
            }
            echo '</thead>';
            echo '<tbody>';
            foreach ($logquery as $key_new_records => $logquery_row) {
                echo '<tr>';
                foreach ($logquery_row as $key_record => $val) {
                    echo '<td>'.$val.'</td>';
                }
                echo '</tr>';
            }
            echo '</tbody>';
            echo '</table>';
        }
    }
    
    public function get_tables_to_sync()
    {
        $tables=array();
        //$tables[]='utente';
        $tables[]='domandeaperte_sessione';
        $tables[]='domande_sessione';
        $tables[]='domande_elenco';
        //$tables[]='calendario_generale';
        //$tables[]='registropresenze';
        //$tables[]='registropresenze_teorico_studente_esame';
        $tables[]='studente_esami';
        $tables[]='progettodidattica';
        $tables[]='programma_materia';
        $tables[]='programma_materia_argomenti';
        $tables[]='progettodidattica_lezioni_argomenti';
        $tables[]='studente_materia_disapprovato';
        return $tables;
    }
    public function backup_sync_tables()
    {
        echo "BACKUP SYNC TABLES <br/>";
        $path_postgres=  $this->get_path_postgres();
        $tables=  $this->get_tables_to_sync();
        foreach ($tables as $key => $table) {
            echo "backup $table <br/>";
            $command='SET PGPASSWORD=postgres&&"'.$path_postgres.'/bin\pg_dump.exe" --host localhost --port 5432 --username "postgres" --no-password  --format tar --verbose --file "..\\JDocServer\sync\\'.$table.'.backup" --table "public.'.$table.'" "jdocuniludes_tables"';
            exec($command);
        }
    }
    
    public function upload_sync_tables()
    {
        echo "UPLOAD SYNC TABLES <br/>";
        //$this->backup_sync_tables();
        $tables=  $this->get_tables_to_sync();
        // connect and login to FTP server
        $ftp_server = "ftp.jdocweb.com";
        $ftp_conn = ftp_connect($ftp_server) or die("Could not connect to $ftp_server");
        $login = ftp_login($ftp_conn, '5003801@aruba.it', 's1pkjl88uh');
        ftp_pasv($ftp_conn, true);
        foreach ($tables as $key => $table) {
            echo "upload $table <br/>";
            $file = "..\\JDocServer\sync\\$table.backup";

            // upload file
            if (ftp_put($ftp_conn, "./www.jdocweb.com/servizi/sync/cliente227/$table.backup", $file, FTP_BINARY))
              {
              echo "Successfully uploaded $file <br/>";
              }
            else
              {
              echo "Error uploading $file <br/>";
              }
        }
        // close connection
        ftp_close($ftp_conn);
    }
    
    public function download_sync_tables()
    {
        echo "DOWNLOAD SYNC TABLES <br/>";
        $tables=  $this->get_tables_to_sync();
        foreach ($tables as $key => $table) {
            echo "download backup $table <br/>";
            file_put_contents("..\\JDocServer\sync\\$table.backup", file_get_contents("http://www.jdocweb.com/servizi/sync/cliente227/$table.backup"));
        
            $file = "./www.jdocweb.com/servizi/sync/cliente227/$table.backup";
            // set up basic connection
            $ftp_server = "ftp.jdocweb.com";
            $ftp_conn = ftp_connect($ftp_server);

            // login with username and password
            $login_result = ftp_login($ftp_conn, '5003801@aruba.it', 's1pkjl88uh');
            ftp_pasv($ftp_conn, true);
            // try to delete $file
            /*echo "delete tables";
            if (ftp_delete($ftp_conn, $file)) {
             echo "$file deleted successful <br/>";
            } else {
             echo "could not delete $file <br/>";
            }*/

            // close the connection
            ftp_close($ftp_conn);
        }
        
        $this->reset_sync_tables();
        $this->restore_sync_tables();
        $this->reset_sync_tables_record_status();
        
    }
    
    public function reset_sync_tables()
    {
        echo "RESET SYNC TABLES <br/>";
       $tables=  $this->get_tables_to_sync();
        foreach ($tables as $key => $table) {
            $this->Sync_model->reset_table($table);
            echo "$table reset <br/>";
        } 
    }
    
    public function restore_sync_tables()
    {
        echo "RESTORE SYNC TABLES <br/>";
        $path_postgres=  $this->get_path_postgres();
        $tables=  $this->get_tables_to_sync();
        foreach ($tables as $key => $table) {
            $command='SET PGPASSWORD=postgres&&"'.$path_postgres.'/bin\pg_restore.exe" --host localhost --port 5432 --username "postgres" --dbname "jdocuniludes_tables" --no-password  --data-only --table '.$table.' --schema public --verbose "C:\xampp\htdocs\JDocServer\sync\\'.$table.'.backup"';
            exec($command);
            echo "$table restored<br/>";
        }
    }
    
    public function reset_record_status()
    {
        echo "RESET SYNC TABLES RECORD STATUS <br/>";
       $tables=  $this->get_tables_to_sync();
        foreach ($tables as $key => $table) {
            $this->Sync_model->reset_record_status($table);
            echo "$table reset status<br/>";
        } 
    }
    
    public function reset_logquery()
    {
        $this->Sync_model->reset_logquery();
    }
    
    public function sync_tables()
    {
        echo "SYNC TABLES <br/>";
        $new_records=$this->get_new_remote_records();
        $this->Sync_model->set_new_records($new_records);
        //$this->backup_sync_tables();
        //$this->upload_sync_tables();
        //echo "file_get_contents remote sync <br/>";
        //$remote_sync_log=file_get_contents("http://uniludes.cloudapp.net:8822/uniludesweb/index.php/sync_controller/download_sync_tables/");
        //echo $remote_sync_log;
    }
    
    
}
    
