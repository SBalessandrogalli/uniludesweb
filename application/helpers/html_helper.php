<?php
function generate_select($id,$label,$options=null,$value=null,$mode='view',$class='')
{
    $name=$id;
    $id=  str_replace("[", "_", $id);
    $id=  str_replace("]", "_", $id);
    if($options==null)
    {
        $options=array();
    }
    if($value==null)
    {
        $value='';
    }
    if($mode=='view')
    {
        $disabled='disabled';
    }
    else
    {
        $disabled='';
    }
    if(($mode!='view')||(($mode=='view')&&($value!='')))
    {
?>    
        <div class="input-field">
            <select <?=$disabled?> id="<?=$id?>" class="<?=$class?>" name="<?=$name?>">
                <option value=""></option>
                <?php  
                foreach ($options as $key => $option) 
                {
                    $selected='';
                    if($option['Codice']==$value)
                    {
                        $selected='selected';
                    }
                ?>
                    <option value="<?=$option['Codice']?>" <?=$selected?>><?=$option['Descrizione']?></option>
                <?php
                }
                ?>
            </select>
            <label for="<?=$id?>" ><?=$label?></label>
        </div>
<?php
    }
}
?>






<?php
function generate_textinput($id,$label,$value=null,$mode='view')
{
    $name=$id;
    $id=  str_replace("[", "_", $id);
    $id=  str_replace("]", "_", $id);
    if($mode=='view')
    {
        $disabled='disabled';
    }
    else
    {
        $disabled='';
    }
    if($value==null)
    {
        $value='';
    }
    
    if($value!='')
    {
        $active='active';
    }
    else
    {
        $active='';
    }
    if(($mode!='view')||(($mode=='view')&&($value!='')))
    {
    ?>
        <div class="input-field">
            <input <?=$disabled?> id="<?=$id?>" name="<?=$name?>" type="text" value="<?=$value?>">
            <label for="<?=$id?>" class='<?=$active?>'><?=$label?></label>
        </div>
    <?php
    }
}
?>

<?php
function generate_passwordinput($id,$label,$value,$mode='view')
{
    $name=$id;
    $id=  str_replace("[", "_", $id);
    $id=  str_replace("]", "_", $id);
    if($mode=='view')
    {
        $disabled='disabled';
    }
    else
    {
        $disabled='';
    }
    if($value==null)
    {
        $value='';
    }
    if($value!='')
    {
        $active='active';
    }
    else
    {
        $active='';
    }
    if(($mode!='view')||(($mode=='view')&&($value!='')))
    {
    ?>
        <div class="input-field">
            <input <?=$disabled?> id="<?=$id?>" name="<?=$name?>" type="password" value="<?=$value?>">
            <label for="<?=$id?>" class='<?=$active?>'><?=$label?></label>
        </div>
    <?php
    }
}
?>









<?php
function generate_textarea($id,$label,$value=null,$mode='view')
{
    $name=$id;
    $id=  str_replace("[", "_", $id);
    $id=  str_replace("]", "_", $id);
if($mode=='view')
{
    $disabled='disabled';
}
else
{
    $disabled='';
}
if($value==null)
{
    $value='';
}
if($value!='')
{
    $active='active';
}
else
{
    $active='';
}
if(($mode!='view')||(($mode=='view')&&($value!='')))
{
    ?>
    <div class="input-field">
        <textarea <?=$disabled?> id="<?=$id?>" name="<?=$name?>" class="materialize-textarea"><?=$value?></textarea>
        <label for="<?=$id?>" class='<?=$active?>'><?=$label?></label>
    </div>
    <?php
}
}
?>


<?php
function generate_checkbox($id,$label,$value=null,$mode='view')
{
    $name=$id;
    $id=  str_replace("[", "_", $id);
    $id=  str_replace("]", "_", $id);
    $checked='';
    if($mode=='view')
    {
        $disabled='disabled';
    }
    else
    {
        $disabled='';
    }
    if($value==null)
    {
        $value='';
    }
    if($value!='')
    {
        $active='active';
        if((strtolower($value)=='true')||($value=='checked')||($value=='t'))
        {
            $checked='checked';
        }
    }
    else
    {
        $active='';
    }
    if(($mode!='view')||(($mode=='view')&&($value!='')))
    {
    ?>
        <div class="input-field">
            
            <input <?=$disabled?> <?=$checked?> id="<?=$id?>" name="<?=$name?>" type="checkbox" value="TRUE">
            <label for="<?=$id?>" class='<?=$active?>'><?=$label?></label>
        </div>
    <?php
    }
}
?>


<?php
function generate_dateinput($id,$label,$value,$mode='view')
{
    $name=$id;
    $id=  str_replace("[", "_", $id);
    $id=  str_replace("]", "_", $id);
    if($mode=='view')
    {
        $disabled='disabled';
    }
    else
    {
        $disabled='';
    }
    if($value==null)
    {
        $value='';
    }

    if(($mode!='view')||(($mode=='view')&&($value!='')))
    {
    ?>
        <div class="input-field">
            <input <?=$disabled?> id="<?=$id?>" name="<?=$name?>" type="date" class="datepicker" value="<?=$value?>">
            <label for="<?=$id?>" class='active'><?=$label?></label>
        </div>
    <?php
    }
}
?>