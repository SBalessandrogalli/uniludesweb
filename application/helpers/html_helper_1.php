<?php
function generate_select($id,$label,$all_options=null,$values=null,$mode='view')
{
    if($all_options!=null)
    {
        if(array_key_exists($id, $all_options))
        {
            $options=$all_options[$id]; 
        }
        else
        {
            $options=array();
        }
    }
    else
    {
        $options=array();
    }
    if($values!=null)
    {
        if(array_key_exists($id, $values))
        {
           $value=$values[$id]; 
        }
        else
        {
            $value='';
        }
        
    }
    else
    {
        $value='';
    }
    if($mode=='view')
    {
        $disabled='disabled';
    }
    else
    {
        $disabled='';
    }
    if(($mode!='view')||(($mode=='view')&&($value!='')))
    {
?>    
        <div class="input-field">
            <select <?=$disabled?> id="<?=$id?>" name="<?=$id?>">
                <option value=""></option>
                <?php  
                foreach ($options as $key => $option) 
                {
                    $selected='';
                    if($option['Codice']==$value)
                    {
                        $selected='selected';
                    }
                ?>
                    <option value="<?=$option['Codice']?>" <?=$selected?>><?=$option['Descrizione']?></option>
                <?php
                }
                ?>
            </select>
            <label for="<?=$id?>" ><?=$label?></label>
        </div>
<?php
    }
}
?>






<?php
function generate_textinput($id,$label,$values=null,$mode='view')
{
    if($mode=='view')
    {
        $disabled='disabled';
    }
    else
    {
        $disabled='';
    }
    if($values!=null)
    {
       $value=$values[$id]; 
    }
    else
    {
        $value='';
    }
    
    if($value!='')
    {
        $active='active';
    }
    else
    {
        $active='';
    }
    if(($mode!='view')||(($mode=='view')&&($value!='')))
    {
    ?>
        <div class="input-field">
            <input <?=$disabled?> id="<?=$id?>" name="<?=$id?>" type="text" value="<?=$value?>">
            <label for="<?=$id?>" class='<?=$active?>'><?=$label?></label>
        </div>
    <?php
    }
}
?>

<?php
function generate_passwordinput($id,$label,$values,$mode='view')
{
    if($mode=='view')
    {
        $disabled='disabled';
    }
    else
    {
        $disabled='';
    }
    if($values!=null)
    {
       $value=$values[$id]; 
    }
    else
    {
        $value='';
    }
    if($value!='')
    {
        $active='active';
    }
    else
    {
        $active='';
    }
    if(($mode!='view')||(($mode=='view')&&($value!='')))
    {
    ?>
        <div class="input-field">
            <input <?=$disabled?> id="<?=$id?>" name="<?=$id?>" type="password" value="<?=$value?>">
            <label for="<?=$id?>" class='<?=$active?>'><?=$label?></label>
        </div>
    <?php
    }
}
?>









<?php
function generate_textarea($id,$label,$values=null,$mode='view')
{
if($mode=='view')
{
    $disabled='disabled';
}
else
{
    $disabled='';
}
if($values!=null)
{
   $value=$values[$id]; 
}
else
{
    $value='';
}
if($value!='')
{
    $active='active';
}
else
{
    $active='';
}
if(($mode!='view')||(($mode=='view')&&($value!='')))
{
    ?>
    <div class="input-field">
        <textarea <?=$disabled?> id="<?=$id?>" name="<?=$id?>" class="materialize-textarea"><?=$value?></textarea>
        <label for="<?=$id?>" class='<?=$active?>'><?=$label?></label>
    </div>
    <?php
}
}
?>


<?php
function generate_checkbox($id,$label,$values=null,$mode='view')
{
    $checked='';
    if($mode=='view')
    {
        $disabled='disabled';
    }
    else
    {
        $disabled='';
    }
    if($values!=null)
    {
       $value=$values[$id]; 
    }
    else
    {
        $value='';
    }
    if($value!='')
    {
        $active='active';
        if($value=='TRUE')
        {
            $checked='checked';
        }
    }
    else
    {
        $active='';
    }
    if(($mode!='view')||(($mode=='view')&&($value!='')))
    {
    ?>
        <div class="input-field">
            <input <?=$disabled?> <?=$checked?> id="<?=$id?>" name="<?=$id?>" type="checkbox" value="<?=$value?>">
            <label for="<?=$id?>" class='<?=$active?>'><?=$label?></label>
        </div>
    <?php
    }
}
?>


<?php
function generate_dateinput($id,$label,$values,$mode='view')
{
    if($mode=='view')
    {
        $disabled='disabled';
    }
    else
    {
        $disabled='';
    }
    if($values!=null)
    {
       $value=$values[$id]; 
    }
    else
    {
        $value='';
    }

    if(($mode!='view')||(($mode=='view')&&($value!='')))
    {
    ?>
        <div class="input-field">
            <input <?=$disabled?> id="<?=$id?>" name="<?=$id?>" type="date" class="datepicker" value="<?=$value?>">
            <label for="<?=$id?>" class='active'><?=$label?></label>
        </div>
    <?php
    }
}
?>