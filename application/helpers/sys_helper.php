<?php

/**
 * Genera il recordid per l'inserimento di nuovi record nella struttura di JDoc
 * 
 * @param type $record
 */
function generate_recordid($record)
{
    $temp=intval($record);
    $temp+=1;
    $record=strval($temp);
}

function domain_url()
{
    return "http://".$_SERVER['HTTP_HOST']."/";
}

/**
 * Verifica se l'utente è loggato
 * @param type $controller
 * @return boolean
 * @author Alessandro Galli 
 */
function logged($controller) 
{
        if ($controller->session->userdata('username')/*=="a.galli"*/) 
        {
            return true;
        } else 
        {
            return false;
        }
}

/**
 * Visualizza una view in base a diversi parametri
 * 
 * @param type $controller controllore da cui parte la richiesta
 * @param array $arg array di parametri presi in considerazione per la visualizzazione
 * @author Alessandro Galli
 */
function view_general($controller,$arg)
{     
    if(isset($arg['module']))
    {
        $module=$arg['module'];
    }
    else
    {
        $module='sys';
    }
    
    if(isset($arg['interface']))
    {
        $controller->template->set_template($module.'_'.$arg['interface']);
        $interface=$arg['interface'];

    }
    else
    {
        $interface='desktop';
    }
    
    
    if(isset($arg['content']))
    {
        $content=$arg['content'];

    }
    else
    {
        echo 'content non definito';
    }
    
    
    if(isset($arg['content_data']))
    {
        $data['data']['content_data']=$arg['content_data'];
    }
    else
    {
        $data['data']['content_data']=null;
    }
    
    if(isset($arg['sys_data']))
    {
        $data['data']['sys_data']=$arg['sys_data'];
    }
    else
    {
        $data['data']['sys_data']=null;
    }
    
    if(isset($arg['menu_data']))
    {
        $data['data']['menu_data']=$arg['menu_data'];
    }
    else
    {
        $data['data']['menu_data']=null;
    }
    
    if(isset($arg['block']))
    {
        $data['data']['block']=$arg['block'];
    }
    else
    {
        $data['data']['block']=null;
    }
    
    
    if(isset($arg['extraheader']))
    {
    $controller->template->write_view('extraheader', $module.'/'.$interface.'/extraheader/'.$arg['extraheader']);
    }

    if(isset($arg['backmenu']))
    {
        
        $controller->template->write('backmenu', createbackmenu($arg['backmenu'],$arg['module']) );
    }
    if(isset($arg['menu']))
    {
    $controller->template->write_view('menu', $module.'/'.$interface.'/menu/'.$arg['menu'],$menu_data);
    }
    
    if(($module=='sys')&&($interface=='desktop'))
    {
    $controller->template->write_view('sys_menu', $module.'/'.$interface.'/menu/sys_menu',$data);
    }
    
    $controller->template->write_view('content', $module.'/'.$interface.'/content/'.$content,$data);
    $controller->template->render(); 
}
    

/**
 * 
 * Genera il pulsante per tornare indietro nel menu mobile
 * 
 * @param type $backfun
 * @param type $modulo
 * @return string
 */
function createbackmenu($backfun,$modulo='crm')
{
       $html='<a data-role="button" data-icon="back" href="'.site_url().'/'.$modulo.'_viewcontroller/'.$backfun.'/tablet">Indietro</a>';
       return $html;
}


?>
