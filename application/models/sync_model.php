<?php
class Sync_model extends CI_Model {
    
    function __construct()
    {
        parent::__construct();
    }
    
    function get_param($column)
    {
        $sql="SELECT * FROM parametri";
        $result=  $this->select($sql);
        if(array_key_exists($column, $result[0]))
        {
            return $result[0][$column];
        }
        else
        {
            return null;
        }
    }
    function select($sql)
    {
        $query=$this->db->query($sql);
        $rows = $query->result_array();
        return $rows;
    }
    
    function execute_query($sql)
    {
        $this->set_logquery($sql);
        $query = $this->db->query($sql);
        return $query;
    }
    
    public function set_logquery($sql_finale)
    {
        $sql_finale=  str_replace("'", "''", $sql_finale);
        $sql_finale='SYNC: '.$sql_finale;
        $userid=0;
        $data=$date=  date('Y-m-d');
        $ora=$date=  date('H:i:s');
        $sql_log="INSERT INTO logquery (\"CodiceUtente\",\"Data\",\"Ora\",\"Query\") VALUES ($userid,'$data','$ora','$sql_finale') ";
        $this->db->query($sql_log);
    }
    
    public function get_new_local_records($tables)
    {
         foreach ($tables as $key => $table) {
             $sql="SELECT * FROM $table WHERE \"RecordStatus\"='new' ORDER BY \"Codice\" ASC";
            $rows=  $this->select($sql);
            $return[$table]['new']=$rows;
            $sql="SELECT * FROM $table WHERE \"RecordStatus\"='new_existingmaster' ORDER BY \"Codice\" ASC";
            $rows=  $this->select($sql);
            $return[$table]['new_existingmaster']=$rows;
            $sql="SELECT * FROM $table WHERE \"RecordStatus\"='edit' ORDER BY \"Codice\" ASC";
            $rows=  $this->select($sql);
            $return[$table]['edit']=$rows;
            $sql="SELECT * FROM $table WHERE \"RecordStatus\"='del' ORDER BY \"Codice\" ASC";
            $rows=  $this->select($sql);
            $return[$table]['del']=$rows;
         }
        return $return;
    }
    
    public function reset_RecordCodiceRemoto($new_record_table_key)
    {
        $sql="
            UPDATE $new_record_table_key
            SET \"RecordCodiceRemoto\"=null
            ";
        $this->execute_query($sql);
    }
    
    public function set_new_records($new_records_tables)
    {
        foreach ($new_records_tables as $new_record_table_key => $new_record_table) 
        {
            $this->reset_RecordCodiceRemoto($new_record_table_key);
            if(array_key_exists('new', $new_record_table))
            {
                foreach ($new_record_table['new'] as $key => $new_record) 
                {
                    //$new_codice=  $this->generate_seriale($new_record_table_key,'Codice');
                    //$new_record['Codice']=$new_codice;
                    $new_record['RecordCodiceRemoto']=$new_record['Codice'];
                    $new_record['RecordStatus']='sync';
                    unset($new_record['Codice']);
                    if($new_record_table_key=='domande_sessione')
                    {
                        $ProgressivoFormularioEsame_new=  $this->generate_seriale($new_record_table_key,'ProgressivoFormularioEsame');
                        $new_record['ProgressivoFormularioEsame']=$ProgressivoFormularioEsame_new;  
                    }
                    if($new_record_table_key=='domande_elenco')
                    {
                        $RecordCodiceRemoto=$new_record['CodiceSessioneInserimento'];
                        $sql="
                            SELECT \"Codice\",\"ProgressivoFormularioEsame\"
                            FROM domande_sessione
                            WHERE \"RecordCodiceRemoto\"=$RecordCodiceRemoto
                            ";
                        $domande_sessione_new=  $this->select($sql);
                        if(count($domande_sessione_new)>0)
                        {
                            $domanda_new=$domande_sessione_new[0];
                            $CodiceSessioneInserimento=$domanda_new['Codice'];
                            $ProgressivoFormularioEsame=$domanda_new['ProgressivoFormularioEsame'];
                            $new_record['CodiceSessioneInserimento']=$CodiceSessioneInserimento;
                            $new_record['ProgressivoFormularioEsame']=$ProgressivoFormularioEsame;
                        }
                    }
                    if($new_record_table_key=='programma_materia_argomenti')
                    {
                        $CodiceProgrammaMateria_remoto=$new_record['CodiceProgrammaMateria'];
                        $sql="
                            SELECT \"Codice\"
                            FROM programma_materia
                            WHERE \"RecordCodiceRemoto\"=$CodiceProgrammaMateria_remoto
                            ";
                        $programmi_materia_new=  $this->select($sql);
                        if(count($programmi_materia_new)>0)
                        {
                            $programma_materia_new=$programmi_materia_new[0];
                            $CodiceProgrammaMateria_locale=$argomento_new['Codice'];
                            $new_record['CodiceProgrammaMateria']=$CodiceProgrammaMateria_locale;
                        }
                    }
                    if($new_record_table_key=='progettodidattica_lezioni_argomenti')
                    {
                        $CodiceProgrammaMateriaArgomento_remoto=$new_record['CodiceProgrammaMateriaArgomento'];
                        $sql="
                            SELECT \"Codice\"
                            FROM programma_materia_argomenti
                            WHERE \"RecordCodiceRemoto\"=$CodiceProgrammaMateriaArgomento_remoto
                            ";
                        $programma_materia_argomenti_new=  $this->select($sql);
                        if(count($programma_materia_argomenti_new)>0)
                        {
                            $argomento_new=$programma_materia_argomenti_new[0];
                            $CodiceProgrammaMateriaArgomento_locale=$argomento_new['Codice'];
                            $new_record['CodiceProgrammaMateriaArgomento']=$CodiceProgrammaMateriaArgomento_locale;
                        }
                    }
                    $sql="";
                    $insert="";
                    $values="";
                    $insert="INSERT INTO $new_record_table_key";
                    $counter=0;
                    foreach ($new_record as $key => $value) 
                    {
                        $value = str_replace("'", "''", $value);
                        if($value!='')
                        {
                            if($counter==0)
                            {
                                $insert=$insert."(\"$key\"";
                                $values=$values." VALUES ('$value'"; 
                            }
                            else
                            {
                                $insert=$insert.",\"$key\"";
                                $values=$values.",'$value'";
                            }
                            $counter++;
                        }
                    }
                    $insert=$insert.")";
                    $values=$values.")";
                    $sql=$insert.$values;
                    echo "insert new $new_record_table_key <br/>";
                    $this->execute_query($sql);
                    echo "insert new $new_record_table_key ok <br/>";
                } 
            }
            
            if(array_key_exists('new_existingmaster', $new_record_table))
            {
                foreach ($new_record_table['new_existingmaster'] as $key => $new_record) 
                {
                    
                    $new_record['RecordCodiceRemoto']=$new_record['Codice'];
                    $new_record['RecordStatus']='sync';
                    unset($new_record['Codice']);
                    if($new_record_table_key=='domande_sessione')
                    {
                        $ProgressivoFormularioEsame_new=  $this->generate_seriale($new_record_table_key,'ProgressivoFormularioEsame');
                        $new_record['ProgressivoFormularioEsame']=$ProgressivoFormularioEsame_new;  
                    }

                    $sql="";
                    $insert="";
                    $values="";
                    $insert="INSERT INTO $new_record_table_key";
                    $counter=0;
                    foreach ($new_record as $key => $value) 
                    {
                        $value = str_replace("'", "''", $value);
                        if($value!='')
                        {
                            if($counter==0)
                            {
                                $insert=$insert."(\"$key\"";
                                $values=$values." VALUES ('$value'"; 
                            }
                            else
                            {
                                $insert=$insert.",\"$key\"";
                                $values=$values.",'$value'";
                            }
                            $counter++;
                        }
                    }
                    $insert=$insert.")";
                    $values=$values.")";
                    $sql=$insert.$values;
                    echo "insert new_existingmaster $new_record_table_key <br/>";
                    $this->execute_query($sql);
                    echo "insert new_existingmaster $new_record_table_key ok <br/>";
                
                }
            }
            
            if(array_key_exists('edit', $new_record_table))
            {
                foreach ($new_record_table['edit'] as $key => $new_record) 
                {
                    $codice=$new_record['Codice'];
                    $sql="UPDATE $new_record_table_key";
                    $counter=0;
                    foreach ($new_record as $key => $value) {
                        $value = str_replace("'", "''", $value);
                        if($value!='')
                        {
                            if($counter==0)
                            {
                                $sql=$sql." SET \"$key\"='$value'"; 
                            }
                            else
                            {
                                $sql=$sql." ,\"$key\"='$value'";
                            }
                            $counter++;
                        }
                    }
                    $sql=$sql." WHERE \"Codice\"='$codice'";
                    echo "edit $new_record_table_key <br/>";
                    $this->execute_query($sql);
                    echo "edit $new_record_table_key ok <br/>";
                }
            }
            
        }
            
        
        
    }
    
    public function generate_seriale($table,$field){
        $seriale=0;
        $sql="SELECT MAX(\"$field\") as max FROM $table where \"$field\" > 0";
        $result=  $this->select($sql);
        if(count($result)>0)
        {
            $seriale=$result[0]['max'];
        }
        $new_seriale=$seriale+1;
        return $new_seriale;
    }
    
    public function reset_candid($master_recordid)
    {
        $sql="DELETE FROM user_candid WHERE recordid_='$master_recordid'";
        $this->execute_query($sql);
        $linked_tables=$this->get_linkedtables('CANDID');
        foreach ($linked_tables as $key => $linked_table) {
            $sql="DELETE FROM user_".strtolower($linked_table)." WHERE recordidcandid_='$master_recordid'";
            $this->execute_query($sql);
        }
    }
    
    public function get_all_local_records($tableid,$recordid)
    {
        $linkedtables=  $this->get_linkedtables($tableid);
        $allfields[$tableid][0]['tableid']=$tableid;
        $allfields[$tableid][0]['fields']=  $this->get_fields_record($tableid, $recordid);
        foreach ($linkedtables as $key => $linkedtable) 
        {
             $records=$this->get_allrecords_linkedtable($linkedtable,$tableid, $recordid);
             foreach ($records as $key => $record) {
                 $allfields[$linkedtable][$key]['tableid']=$linkedtable;
                  $allfields[$linkedtable][$key]['fields']= $record;
             }
        }
        return $allfields;
    }
    
    public function set_all_records($allfields)
    {
        foreach ($allfields as $key => $records) 
        {
  
            foreach ($records as $key => $record) 
            {
                $tableid=$record['tableid'];
                $fields=$record['fields'];
                
                $insert="INSERT INTO user_".strtolower($tableid);
                $values=" VALUES";
                $counter=0;
                foreach ($fields as $key => $field) 
                {
                    if(($field!='')&&($field!=null))
                    {
                        $field=  str_replace("'", "''", $field);
                        if($counter==0)
                        {
                            $insert=$insert." ($key";  
                            $values=$values." ('$field'";
                        }
                        else
                        {
                            $insert=$insert.",$key";  
                            $values=$values.",'$field'";
                        }
                        $counter++;
                    }
                }
                $insert=$insert.")";
                $values=$values.")";
                $sql=$insert.$values;
                $this->execute_query($sql);
            }
        }
        
    }
    
    public function reset_table($table)
    {
        $sql="DELETE FROM $table";
        $this->execute_query($sql);
    }
    
    public function reset_record_status($table)
    {
       $sql="UPDATE $table SET \"RecordStatus\"=null";
       $this->execute_query($sql);
    }
    
    public function reset_logquery()
    {
        $sql="DELETE FROM logquery";
        $this->execute_query($sql);
    }
               
    
         
    
    
}
?>
