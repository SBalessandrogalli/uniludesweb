<?php

class Sys_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    /**
     *
     * @param type $sql
     * @return array risultato della query
     * @author Alessandro Galli
     *
     * Ritorna il risultato di una query
     */
    function select($sql)
    {
        $query=$this->db->query($sql);
        $rows = $query->result_array();
        return $rows;
    }

    /**
     *
     * @param type $sql
     * @return array risultato della query
     * @author Alessandro Galli
     *
     * Ritorna l'array della prima riga
     */
    function select_row($sql)
    {
        $query=$this->db->query($sql);
        $rows = $query->result_array();
        if(count($rows)>0)
        {
           return $rows[0];
        }
        else
        {
            return null;
        }
    }

    /**
     * helper per eseguire update o insert passando l'sql
     * @param type $sql testuale
     * @author Alessandro Galli
     * @return string ultimo id inserito se la query va a buon fine
     */
     function execute_query($sql)
    {
        $this->set_logquery($sql);
        $query = $this->db->query($sql);
    }

    public function set_logquery($sql_finale)
    {
        $sql_finale=  str_replace("'", "''", $sql_finale);
        $userid=$this->get_userid();
        if(($userid==null)||($userid==''))
        {
            $userid=0;
        }
        $data=$date=  date('Y-m-d');
        $ora=$date=  date('H:i:s');
        $sql_log="INSERT INTO logquery (\"CodiceUtente\",\"Data\",\"Ora\",\"Query\") VALUES ($userid,'$data','$ora','$sql_finale') ";
        $this->db->query($sql_log);
    }

    public function get_logquery($date='')
    {
        if($date!='')
        {
            $sql="
                SELECT \"Data\",\"Ora\",\"CodiceUtente\",\"Query\"
                FROM logquery
                WHERE \"Data\"='$date'
                ";
        }
        else
        {
            $sql="
                SELECT \"Data\",\"Ora\",\"Nome\",\"Query\"
                FROM logquery
                JOIN utente ON logquery.\"CodiceUtente\"=utente.\"Codice\"
                ORDER BY LOGQUERY.\"Codice\" DESC
                ";
        }
        return $this->select($sql);
    }

    /**
     *
     * @param type $table tabella in cui fare l'inserimento
     * @param type $fields campi(colonna come chiave,e valore da inserire) da inserire
     * @author Alessandro Galli
     *
     * Helper per inserire valori in una tabella
     */
    function insert($table,$fields)
    {
        $x=0;
        $columns="";
        $values="";
        foreach ($fields as $key => $value) {
            if($x!=0)
            {
                $columns=$columns.",";
                $values=$values.",";
            }
            $columns=$columns."\"$key\"";

            if($value=='')
            {
                $value='null';
            }
            else
            {
                $value=  str_replace("'", "''", $value);
                $value="'$value'";
            }
            $values=$values."$value";
            $x++;
        }
        $sql="INSERT INTO $table ($columns) VALUES ($values)";
        $this->execute_query($sql);
        $lastid=  $this->db_get_value($table, "\"Codice\"", 'true','',"ORDER BY \"Codice\" DESC");
        return $lastid;
    }
    
    function update($table,$fields,$condition='true')
    {
        $x=0;
        $set="";
        foreach ($fields as $key => $value) {
            if($value!=false)
            {
                if($x!=0)
                {
                    $set=$set.",";
                }

                if($value=='')
                {
                    $value="null";
                }
                else
                {
                    $value="'$value'";
                }

                $set=$set."\"$key\"=$value";
                $x++;
            }
        }
        if($set!='')
        {
            $sql="UPDATE $table SET $set WHERE $condition";
            $this->execute_query($sql); 
        }
        
    }

    function db_duplicate_row($tableid,$conditions,$pk_id)
    {

        $row=  $this->db_get_row($tableid, "*", $conditions);+
        $insert="INSERT INTO $tableid (";
        $values="VALUES (";
        $counter=0;
        foreach ($row as $key => $value) {
            if($key!=$pk_id)
            {
                if($counter!=0)
                {
                    $insert=$insert.',';
                    $values=$values.',';
                }

                if($key=='RecordStatus')
                {
                    $value='new';
                }
                if($key=='RecordDataCreazione')
                {
                    $value=  $this->get_today_timestamp();
                }
                if($key=='RecordCreatoDa')
                {
                    $value=  $this->get_userid();
                }
                if(($key=='RecordModificatoDa')||($key=='RecordDataModifica'))
                {
                    $value='';
                }
                if($value=='')
                {
                    $value='null';
                }
                else
                {
                    $value="'$value'";
                }
                $insert=$insert."\"$key\"";
                $values=$values.$value;
                $counter++;
            }
        }
        $insert=$insert.")";
        $values=$values.")";
        $sql=$insert." ".$values;
        $this->execute_query($sql);
        return $this->get_last_codice($tableid);
    }
    /**
     *
     * @param type $tableid
     * @param type $columns
     * @param type $conditions
     * @param type $limit
     * @param type $order
     * @return type
     * @author Alessandro Galli
     *
     * Helper per fare select dal database
     */
    function db_get($tableid,$columns='*',$conditions='true',$order='',$limit='')
    {
        $sql="
            SELECT $columns
            FROM $tableid
            WHERE $conditions
            $order
            $limit
                ";
        $result=  $this->select($sql);
        return $result;
    }

    /**
     *
     * @param type $tableid
     * @param type $columns
     * @param type $conditions
     * @param type $limit
     * @param type $order
     * @return type
     * @author Alessandro Galli
     *
     * Helper per ottenere l'array con la prima riga trovata dalla select generata
     */
    function db_get_row($tableid,$columns='*',$conditions='true',$limit='',$order='')
    {
        $rows=$this->db_get($tableid, $columns, $conditions, $limit, $order);
        if(count($rows)>0)
        {
            $return=$rows[0];
        }
        else
        {
            $return=null;
        }
        return $return;
    }

    function db_get_value($tableid,$column='Codice',$conditions='true',$limit='',$order='')
    {
        $row=  $this->db_get_row($tableid, $column, $conditions,$limit,$order);
        if($row!=null)
        {
            $column=  str_replace('"', '', $column);
            $return=$row[$column];
        }
        else
        {
            $return=null;
        }
        return $return;
    }

    function db_get_max($tableid,$column,$conditions='true')
    {
        $row=  $this->db_get_row($tableid, "MAX(\"$column\") as \"$column\"" , $conditions);
        if($row!=null)
        {
            $column=  str_replace('"', '', $column);
            $return=$row[$column];
        }
        else
        {
            $return=null;
        }
        return $return;
    }
    function get_last_codice($table)
    {
        return $this->db_get_value($table, "\"Codice\"", 'true','',"ORDER BY \"Codice\" DESC");
    }

    /**
     *
     * @param type $value
     * @return boolean
     * @author Alessandro Galli
     *
     * Helper per verificare se un valore non è vuoto
     */
    function isnotempty($value)
    {
        if(($value!='')&&($value!=null))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function get_today_timestamp()
    {
        $date=  date('Y-m-d H:i:s');
        return $date;
    }
    
    public function get_today()
    {
        $date=  date('Y-m-d');
        return $date;
    }
    
    public function get_now()
    {
        $date=  date('H:i:s');
        return $date;
    }
    
    public function get_userid()
    {
        return $this->session->userdata('userid');
    }

    public function get_username()
    {
        return $this->session->userdata('username');
    }

    /**
     *
     * @param type $value
     * @return boolean
     * @author Alessandro Galli
     *
     * Helper per verificare se un valore è vuoto
     */
    function isempty($value)
    {
        if(($value=='')||($value==null))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     *
     * @param type $matricola
     * @return type
     * @author Luca Giordano

    public function get_badge_from_matricola($matricola)
    {
        $sql = "SELECT * FROM badgematricola WHERE \"Matricola\" LIKE '$matricola'";
        return $this->select($sql);
    }
    */

    /**
     *
     * @param type $CodiceProgettoDidattica
     * @param type $NLezione
     * @return type
     * @author Luca Giordano
     */
    public function get_docente_progettodidattica_lezioni_argomenti($CodiceProgettoDidattica,$NLezione)
    {
        $sql="SELECT * FROM Docente_ProgettoDidattica_Lezioni_Argomenti WHERE \"CodiceProgettoDidattica\"=".$CodiceProgettoDidattica." AND \"NLezione\"=".$NLezione;
        $data=$this->select($sql);
        return $data;
    }

    /**
     *
     * @param type $CodiceDocente  Parametro per la quale fare la where
     * @return type ritorna tutti i campi di un docente
     * @author Luca Giordano
     */
    public function get_dati_docente($CodiceDocente)
    {
        $sql = "SELECT * FROM Docente WHERE \"Codice\"=".$CodiceDocente;
        $data=$this->select($sql);
        return $data;
    }

    /**
     *
     * @return type
     * @author Luca Giordano
     */
    public function get_qualifiche_universitarie()
    {
        $sql="SELECT * FROM Docente_QualificaUniversitaria";
        $data=$this->select($sql);
        return $data;
    }

    /**
     *
     * @param type $CodiceDocente
     * @return type
     * @author Luca Giordano
     */
    public function get_facolta_progetto_didattica($CodiceDocente)
    {
        $sql="SELECT Facolta.\"Codice\",Facolta.\"Descrizione\"
              FROM Calendario_Generale INNER JOIN Facolta ON Calendario_Generale.\"CodiceFacolta\" = Facolta.\"Codice\"
              WHERE ((Calendario_Generale.\"CodiceDocente1\"=".$CodiceDocente.") OR (Calendario_Generale.\"CodiceDocente2\"=".$CodiceDocente.") OR (Calendario_Generale.\"CodiceDocente3\"=".$CodiceDocente."))
              GROUP BY Facolta.\"Codice\", Facolta.\"Descrizione\"";
        $data=$this->select($sql);
        return $data;
    }

    /**
     *
     * @param type $CodiceFacolta
     * @param type $CodiceDocente
     * @return type
     * @author Luca Giordano
     */
    public function load_corso_facolta($CodiceFacolta,$CodiceDocente)
    {
        $sql=  "SELECT Corso.\"Codice\", Corso.\"Descrizione\"
                FROM Calendario_Generale INNER JOIN Corso ON Calendario_Generale.\"CodiceCorso\" = Corso.\"Codice\"
                WHERE (((Corso.\"CodiceFacolta\")=".$CodiceFacolta.") AND ((Calendario_Generale.\"CodiceDocente1\")=".$CodiceDocente.")) OR (((Corso.\"CodiceFacolta\")=".$CodiceFacolta.") AND ((Calendario_Generale.\"CodiceDocente2\")=".$CodiceDocente.")) OR (((Corso.\"CodiceFacolta\")=".$CodiceFacolta.") AND ((Calendario_Generale.\"CodiceDocente3\")=".$CodiceDocente."))
                GROUP BY Corso.\"Codice\", Corso.\"Descrizione\"
                ORDER BY Corso.\"Descrizione\";";
        $data=$this->select($sql);
        return $data;
    }

    /**
     *
     * @param type $CodiceCorso
     * @param type $CodiceDocente
     * @return type array contenente le materie di un determinato corso
     * @author Luca Giordano
     */
    public function load_materie_corso($CodiceCorso,$CodiceDocente)
    {
        $sql=  "SELECT Materia.\"Codice\", CONCAT(Materia.\"DescrizioneENG\",' - ', Materia.\"Sigla\") AS Materia
                FROM Calendario_Generale INNER JOIN Materia ON Calendario_Generale.\"CodiceMateria\" = Materia.\"Codice\"
                WHERE (((LENGTH(CONCAT(Materia.\"DescrizioneENG\",' - ', Materia.\"Sigla\")))>3) AND ((Materia.\"CodiceCorso\")=".$CodiceCorso.") AND ((Calendario_Generale.\"CodiceDocente1\")=".$CodiceDocente.") AND ((Calendario_Generale.\"CodiceTipoCalendario\")=1)) OR (((LENGTH(CONCAT(Materia.\"DescrizioneENG\",' - ', Materia.\"Sigla\")))>3) AND ((Materia.\"CodiceCorso\")=".$CodiceCorso.") AND ((Calendario_Generale.\"CodiceTipoCalendario\")=1) AND ((Calendario_Generale.\"CodiceDocente2\")=".$CodiceDocente.")) OR (((LENGTH(CONCAT(Materia.\"DescrizioneENG\",' - ', Materia.\"Sigla\")))>3) AND ((Materia.\"CodiceCorso\")=".$CodiceCorso.") AND ((Calendario_Generale.\"CodiceTipoCalendario\")=1) AND ((Calendario_Generale.\"CodiceDocente3\")=".$CodiceDocente."))
                GROUP BY Materia.\"Codice\", CONCAT(Materia.\"DescrizioneENG\",' - ', Materia.\"Sigla\")
                ORDER BY CONCAT(Materia.\"DescrizioneENG\",' - ', Materia.\"Sigla\");";
        $data=$this->select($sql);
        return $data;
    }

    /**
     *
     * @param type $CodiceCorso
     * @param type $CodiceDocente
     * @return type
     * @author Luca Giordano
     */
    public function get_materia_intestazioni($CodiceCorso,$CodiceDocente)
    {
        $sql="  SELECT Materia.\"Codice\", CONCAT(Materia.\"DescrizioneENG\",' - ',Materia.\"Sigla\") AS Materia
                FROM Calendario_Generale INNER JOIN Materia
                        ON Calendario_Generale.\"CodiceMateria\" = Materia.\"Codice\"
                WHERE (((LENGTH(CONCAT(Materia.\"DescrizioneENG\",' - ',Materia.\"Sigla\"))>3) AND ((Materia.\"CodiceCorso\")=".$CodiceCorso.") AND ((Calendario_Generale.\"CodiceDocente1\")=".$CodiceDocente.") AND ((Calendario_Generale.\"CodiceTipoCalendario\")=1)) OR (((LENGTH(CONCAT(Materia.\"DescrizioneENG\",' - ',Materia.\"Sigla\")))>3) AND ((Materia.\"CodiceCorso\")=".$CodiceCorso.") AND ((Calendario_Generale.\"CodiceTipoCalendario\")=1) AND ((Calendario_Generale.\"CodiceDocente2\")=".$CodiceDocente.")) OR (((LENGTH(CONCAT(Materia.\"DescrizioneENG\",' - ',Materia.\"Sigla\"))>3) AND ((Materia.\"CodiceCorso\")=".$CodiceCorso.") AND ((Calendario_Generale.\"CodiceTipoCalendario\")=1) AND ((Calendario_Generale.\"CodiceDocente3\")=".$CodiceDocente."))))
                GROUP BY Materia.\"Codice\", CONCAT(Materia.\"DescrizioneENG\",' - ',Materia.\"Sigla\")
                ORDER BY CONCAT(Materia.\"DescrizioneENG\",' - ', Materia.\"Sigla\");";
        $data=$this->select($sql);
        return $data;
    }

    /**
     *
     * @return type
     * @author Luca Giordano
     */
    public function get_anni_di_corso()
    {
        $sql="  SELECT AnnoDiCorso.\"Codice\", AnnoDiCorso.\"Descrizione\"
                FROM AnnoDiCorso
                WHERE (((AnnoDiCorso.\"Codice\")<6));";
        $data=$this->select($sql);
        return $data;
    }

    /**
     *
     * @param type $codiceDocente
     * @return type
     * @author Luca Giordano
     */
    public function get_docente($codiceDocente)
    {
       return $this->db_get_row('docente', '"Cognome","Nome","Titolo","Professione"', '"Codice"='.$codiceDocente);
    }

    /**
     *
     * @return type
     * @author Luca Giordano
     */
    public function get_anni_accademici()
    {
        $sql=   "SELECT AnnoAccademico.\"Codice\", AnnoAccademico.\"Descrizione\"
                FROM AnnoAccademico
                WHERE CAST(SUBSTRING(AnnoAccademico.\"Descrizione\",1,4) AS integer) >= 2013
                ORDER BY AnnoAccademico.\"Descrizione\"";
        $data=$this->select($sql);
        return $data;
    }


    /*public function get_timbrature_by_parameters($numeroBadge,$ggmmaa,$terminale)
    {
        $sql="SELECT * FROM timbrature WHERE GGMMAA LIKE '$ggmmaa' AND NumeroBadge LIKE '$numeroBadge' AND NumeroTerminale LIKE '$terminale'";
        $rows=$this->select($sql);
        return $rows;
    }*/

    /**
     *
     * @param type $codiceDocente
     * @return type
     * @author Luca Giordano
     */
    public function get_badge_docente($codiceDocente)
    {
        $sql="SELECT \"NBadge\" FROM Docente WHERE \"Codice\"=".$codiceDocente;
        return $this->select($sql);
    }

    /**
     *
     * @param type $codiceStudente
     * @return type
     * @author Luca Giordano
     */
    public function get_foto_studente($codiceStudente)
    {
        $sql="SELECT \"NomeFile\",\"Estensione\" FROM studente_allegati WHERE \"CodiceTipoAllegatoStudente\"=1 AND \"CodiceStudente\"=".$codiceStudente;
        $rows=$this->select($sql);
        return $rows;
    }

    /**
     *
     * @param type $codiceRegistroPresenza
     * @return type
     * @author Luca Giordano
     */
    public function get_facolta_registropresenza($codiceRegistroPresenza)
    {
        $sql="SELECT facolta.\"Descrizione\" FROM facolta JOIN RegistroPresenze ON facolta.\"Codice\"=RegistroPresenze.\"CodiceFacolta\" WHERE RegistroPresenze.\"Codice\"=".$codiceRegistroPresenza;
        $data=$this->select($sql);
        return $data;
    }

    /**
     *
     * @param type $codiceRegistroPresenza
     * @return type
     * @author Luca Giordano
     */
    public function get_corso_registropresenza($codiceRegistroPresenza)
    {
        $sql="SELECT corso.\"Descrizione\" FROM corso JOIN RegistroPresenze ON corso.\"Codice\"=RegistroPresenze.\"CodiceCorso\" WHERE RegistroPresenze.\"Codice\"=".$codiceRegistroPresenza;
        $data=$this->select($sql);
        return $data;
    }

    public function get_codice_docente()
    {
        return $this->session->userdata('CodiceDocente');
    }

    public function get_elenco_multiple_choise($CodiceSessioneInserimento)
    {
        $sql="  SELECT *
                FROM Domande_Elenco JOIN Domande_Sessione ON Domande_Elenco.\"CodiceSessioneInserimento\"=Domande_Sessione.\"Codice\"
                WHERE Domande_Sessione.\"Codice\"=".$CodiceSessioneInserimento." ORDER BY \"NumeroDomanda\"";
        $data=$this->select($sql);
        return $data;
    }
    /**
     *
     * @param type $codiceRegistroPresenza
     * @return type
     * @author Luca Giordano
     */
    public function get_classe_registropresenza($codiceRegistroPresenza)
    {
        $sql="SELECT corso.\"Descrizione\" FROM corso JOIN RegistroPresenze ON corso.\"Codice\"=RegistroPresenze.\"CodiceCorso\" WHERE RegistroPresenze.\"Codice\"=".$codiceRegistroPresenza;
        $data=$this->select($sql);
        return $data;
    }

    public function get_record_source_multiplechoice($codiceDocente)
    {
        $sql="SELECT Domande_Sessione.\"Codice\", Domande_Sessione.\"Codice\" as \"N.\",Domande_Sessione.\"ProgressivoFormularioEsame\" as \"Formulario\", (\"DescrizioneITA\") AS \"Materia\",to_char(Domande_Sessione.\"DataEsame\",'DD/MM/YYYY') as \"DataEsame\",Domande_Sessione.\"CodiceRegistroPresenze\" as \"Registro\",Facolta.\"Descrizione\" AS \"Facolta\", Domande_Sessione.\"NoteEsame\"
                FROM (((Domande_Sessione LEFT JOIN Docente
                                ON Domande_Sessione.\"CodiceDocente\" = Docente.\"Codice\") LEFT JOIN Facolta
                                ON Domande_Sessione.\"CodiceFacolta\" = Facolta.\"Codice\") LEFT JOIN Corso
                                ON Domande_Sessione.\"CodiceCorso\" = Corso.\"Codice\") LEFT JOIN Materia
                                ON Domande_Sessione.\"CodiceMateria\" = Materia.\"Codice\"
                                WHERE Domande_Sessione.\"CodiceDocente\"=".$codiceDocente." AND (\"RecordStatus\"!='deleted' OR \"RecordStatus\" is null)
                ORDER BY  Domande_Sessione.\"Codice\" DESC
                ";
        $data=$this->select($sql);
        return $data;
    }


    public function get_record_source_progettodidattica($codiceDocente)
    {
        $sql=  "SELECT annoaccademico.\"Descrizione\" as \"Anno accademico\",progettodidattica.\"Codice\",materia.\"DescrizioneITA\" as \"Materia\", classe.\"CognomePrimoStudente\" as \"Classe\"
                FROM progettodidattica
                JOIN progettodidattica_docente ON progettodidattica_docente.\"CodiceProgettoDidattica\"=progettodidattica.\"Codice\"
                JOIN materia ON progettodidattica.\"CodiceMateria\"=materia.\"Codice\"
                JOIN classe ON progettodidattica.\"CodiceClasse\"=classe.\"Codice\"
                JOIN annoaccademico on progettodidattica.\"CodiceAnnoAccademico\"=annoaccademico.\"Codice\"
                WHERE (((progettodidattica_docente.\"CodiceDocente\")=".$codiceDocente."))
                ORDER BY annoaccademico.\"Codice\" DESC;";
        $data=$this->select($sql);
        return $data;
    }
    
    public function get_record_source_argomenti()
    {
        $sql=  '
            SELECT programma_materia_argomenti."Codice","Cognome","Nome","Argomento"
            FROM progettodidattica_lezioni_argomenti
            JOIN programma_materia_argomenti ON progettodidattica_lezioni_argomenti."CodiceProgrammaMateriaArgomento"=programma_materia_argomenti."Codice"
            JOIN docente ON programma_materia_argomenti."CodiceDocente"=docente."Codice"
            ORDER BY programma_materia_argomenti."Codice" DESC
            LIMIT 100
                ';
        $data=$this->select($sql);
        return $data;
    }

    public function get_record_source_lezioni($codiceDocente)
    {
        $sql="
           SELECT calendario_generale.\"Codice\" as \"CodiceCalendarioGenerale\",calendario_generale.\"CodiceRegistroPresenze\" AS \"Registro n.\",calendario_generale.\"CodiceRegistroPresenze\",to_char(calendario_generale.\"Giorno\",'DD/MM/YYYY') as \"Giorno\",to_char(calendario_generale.\"OraInizio\",'HH24:MI') as \"Inizio\",to_char(\"OraPrevistaFineLezione1\",'HH24:MI') as \"Fine\",materia.\"DescrizioneITA\" as \"Materia\", classe.\"CognomePrimoStudente\" as \"Classe\",false as \"Compilato\"
            FROM calendario_generale
            JOIN registropresenze ON calendario_generale.\"CodiceRegistroPresenze\"=registropresenze.\"Codice\"
            JOIN materia ON materia.\"Codice\"=registropresenze.\"CodiceMateria\"
            JOIN classe ON classe.\"Codice\"=registropresenze.\"CodiceClasse\"
           WHERE calendario_generale.\"CodiceDocente1\"=$codiceDocente OR calendario_generale.\"CodiceDocente2\"=$codiceDocente
            ORDER BY calendario_generale.\"Giorno\" DESC
        ";
        /*$sql="
           SELECT progettodidattica_lezioni.\"CodiceRegistroPresenze\" AS \"Registro n.\",progettodidattica_lezioni.\"CodiceRegistroPresenze\",to_char(\"Data\",'DD/MM/YYYY') as \"Giorno\",to_char(\"OraPrevistaInizioLezione1\",'HH24:MI') as \"Inizio\",to_char(\"OraPrevistaFineLezione1\",'HH24:MI') as \"Fine\",materia.\"DescrizioneITA\" as \"Materia\", classe.\"CognomePrimoStudente\" as \"Classe\",false as \"Compilato\"
            FROM progettodidattica_lezioni
            JOIN progettodidattica ON progettodidattica.\"Codice\" = progettodidattica_lezioni.\"CodiceProgettoDidattica\"
            JOIN registropresenze ON progettodidattica_lezioni.\"CodiceRegistroPresenze\"=registropresenze.\"Codice\"
            JOIN materia ON materia.\"Codice\"=registropresenze.\"CodiceMateria\"
            JOIN classe ON classe.\"Codice\"=registropresenze.\"CodiceClasse\"
           WHERE registropresenze.\"CodiceDocente1\"=$codiceDocente OR registropresenze.\"CodiceDocente2\"=$codiceDocente
            ORDER BY \"Data\" DESC
        ";*/
        $data=$this->select($sql);
        return $data;
    }

    /**
     *
     * @param type $codice_progettodidattica
     * @param type $post
     * @return type
     * @author Alessandro Galli
     */
    public function progettodidattica_save($codice_progettodidattica,$post)
    {
        if($codice_progettodidattica!='null')
        {
            $sql="UPDATE progettodidattica";
            $set="SET ";
            $counter=0;
            foreach ($post as $key => $value) {
                if($key!='ammissioni')
                {
                    if($value=='')
                    {
                        $value="null";
                    }
                    else
                    {
                        $value=  str_replace("'", "''", $value);
                        $value="'$value'";
                    }
                    if($counter!=0)
                    {
                        $set=$set.',';
                    }
                    $set=$set." \"$key\"=$value";
                    $counter++;
                }
                
            }
            $where="WHERE \"Codice\"=$codice_progettodidattica";
            $sql="$sql $set $where";

            $this->execute_query($sql);
            $ammissioni=$post['ammissioni'];
            $codice_materia=  $this->db_get_value('progettodidattica', "\"CodiceMateria\"", "\"Codice\"=$codice_progettodidattica", '', '');
            $codice_classe= $this->db_get_value('progettodidattica', "\"CodiceClasse\"", "\"Codice\"=$codice_progettodidattica", '', '');
            $studenti_classe=$this->get_studenti_progettodidattica($codice_progettodidattica); 
            foreach ($studenti_classe as $key => $studente) {
                $codice_studente=$studente['Codice'];
                $studente_materia_disapprovato=  $this->db_get_row('studente_materia_disapprovato', '*', "\"CodiceStudente\"=$codice_studente AND \"CodiceMateria\"=$codice_materia AND \"CodiceClasse\"=$codice_classe");
                $percentuale_assenza_generale=  $this->get_percentuale_assenza_generale($codice_studente);
                if($studente_materia_disapprovato!=null) 
                {
                    $codice_studente_materia_disapprovato=$studente_materia_disapprovato['Codice'];
                    $sql="
                        DELETE FROM studente_materia_disapprovato
                        WHERE \"Codice\"=$codice_studente_materia_disapprovato
                        ";
                    $this->execute_query($sql);
                }
                // verifico se lo studente è stato segnato come ammesso
                if(array_key_exists($codice_studente, $ammissioni)) //lo studente è segnato come ammesso
                {
                    
                    if($percentuale_assenza_generale>15)
                    {
                        $codice_docente=  $this->get_codice_docente();
                        $userid=  $this->get_userid();
                        $giorno_ora=date('Y-m-d H:i');
                        $sql="
                            INSERT INTO studente_materia_disapprovato
                            (\"CodiceStudente\",\"CodiceClasse\",\"CodiceMateria\",\"CodiceDocente\",\"Approvato\",\"Data\",\"RecordCreatoDa\",\"RecordStatus\")
                            VALUES
                            ($codice_studente,$codice_classe,$codice_materia,$codice_docente,true,'$giorno_ora',$userid,'new')
                            ";
                        $this->execute_query($sql);
                    }
                }
                else //lo studente non è segnato
                {
                    if($percentuale_assenza_generale<=15)
                    {
                        $codice_docente=  $this->get_codice_docente();
                        $userid=  $this->get_userid();
                        $giorno_ora=date('Y-m-d H:i');
                        $sql="
                            INSERT INTO studente_materia_disapprovato
                            (\"CodiceStudente\",\"CodiceClasse\",\"CodiceMateria\",\"CodiceDocente\",\"Data\",\"RecordCreatoDa\",\"RecordStatus\")
                            VALUES
                            ($codice_studente,$codice_classe,$codice_materia,$codice_docente,'$giorno_ora',$userid,'new')
                            ";
                        $this->execute_query($sql);
                    }
                    
                }
            }
            
        }

        return $codice_progettodidattica;
    }

    /**
     *
     * @param type $codice_progettodidattica
     * @param type $post
     * @return type
     * @author Alessandro Galli
     */
    public function domandeaperte_save($codice_domandeaperte,$post)
    {
        $today_timestamp=  $this->get_today_timestamp();
        $userid=  $this->get_userid();
        if($codice_domandeaperte!='null')
        {
            $status= $this->db_get_value('domandeaperte_sessione', "\"RecordStatus\"", "\"Codice\"=$codice_domandeaperte");
            if($status==null)
            {
                $status='edit';
            }
            $sql="UPDATE domandeaperte_sessione";
            $set="SET \"RecordStatus\"='$status',\"RecordDataModifica\"='$today_timestamp',\"RecordModificatoDa\"=$userid";
            $counter=0;
            foreach ($post as $key => $value) {
                if($value=='')
                {
                    $value="null";
                }
                else
                {
                    $value=  str_replace("'", "''", $value);
                    $value="'$value'";
                }
                    $set=$set.',';
                    $set=$set." \"$key\"=$value";
                    $counter++;
            }
            $where="WHERE \"Codice\"=$codice_domandeaperte";
            $sql="$sql $set $where";
            $this->execute_query($sql);
        }
        else
        {
            $codice_docente=  $this->get_codice_docente();
            $insert="INSERT INTO domandeaperte_sessione(\"CodiceDocente\",\"RecordStatus\",\"RecordCreatoDa\"";
            $values="VALUES($codice_docente,'new',$userid";
            $counter=0;
            foreach ($post as $key => $value) {
                if($value=='')
                {
                    $value="null";
                }
                else
                {
                    $value=  str_replace("'", "''", $value);
                    $value="'$value'";
                }
                $insert=$insert.",\"$key\"";
                $values=$values.",$value";
                $counter++;
            }
            $insert=$insert.")";
            $values=$values.")";
            $sql=$insert." ".$values;
            $this->execute_query($sql);
            $codice_domandeaperte=  $this->get_last_codice('domandeaperte_sessione');
        }

        $domandeaperte=  $this->db_get_row('domandeaperte_sessione', '*', "\"Codice\"=$codice_domandeaperte");
        $codice_registro_presenze=$domandeaperte['CodiceRegistroPresenze'];
        // se è stato impostato un registro presenze, aggiorno i relativi campi ridondanti di domandeaperte_sessione
        if($codice_registro_presenze!=null)
        {
            $registro_presenze=  $this->db_get_row('registropresenze', '*', "\"Codice\"=$codice_registro_presenze");
            $codice_facolta=$registro_presenze['CodiceFacolta'];
            $codice_corso=$registro_presenze['CodiceCorso'];
            $codice_materia=$registro_presenze['CodiceMateria'];
            $data_esame=$registro_presenze['Data'];
            $sql="UPDATE domandeaperte_sessione SET \"CodiceFacolta\"=$codice_facolta,\"CodiceCorso\"=$codice_corso,\"CodiceMateria\"=$codice_materia,\"DataEsame\"='$data_esame' WHERE \"Codice\"=$codice_domandeaperte";
            $this->execute_query($sql);
        }
        return $codice_domandeaperte;
    }
    
    public function multiplechoice_save($codice_multiplechoice,$post)
    {
        $today_timestamp=  $this->get_today_timestamp();
        $userid=  $this->get_userid();
        if($codice_multiplechoice!='null') 
        {
            $status= $this->db_get_value('domande_sessione', "\"RecordStatus\"", "\"Codice\"=$codice_multiplechoice");
            if($status==null)
            {
                $status='edit';
            }
            $sql="UPDATE domande_sessione";
            $set="SET \"RecordStatus\"='$status',\"RecordDataModifica\"='$today_timestamp',\"RecordModificatoDa\"=$userid ";
            $counter=0;
            foreach ($post as $key => $value) {
                if($value=='')
                {
                    $value="null";
                }
                else
                {
                    $value=  str_replace("'", "''", $value);
                    $value="'$value'";
                }
                    $set=$set.',';
                    $set=$set." \"$key\"=$value";
                    $counter++;
            }
            $where="WHERE \"Codice\"=$codice_multiplechoice";
            $sql="$sql $set $where";
            $this->execute_query($sql);
        }
        else
        {
            $codice_docente=  $this->get_codice_docente();
            $ProgressivoFormularioEsame=  $this->db_get_max('domande_sessione', "ProgressivoFormularioEsame", "TRUE");
            $ProgressivoFormularioEsame=$ProgressivoFormularioEsame+1;
            //$codice_multiplechoice=  $this->generate_seriale('domande_sessione', 'Codice');
            $insert="INSERT INTO domande_sessione(\"CodiceDocente\",\"ProgressivoFormularioEsame\",\"RecordStatus\",\"RecordCreatoDa\"";
            $values="VALUES($codice_docente,$ProgressivoFormularioEsame,'new',$userid";
            $counter=0;
            foreach ($post as $key => $value) {
                if($value=='')
                {
                    $value="null";
                }
                else
                {
                    $value=  str_replace("'", "''", $value);
                    $value="'$value'";
                }
                $insert=$insert.",\"$key\"";
                $values=$values.",$value";
                $counter++;
            }
            $insert=$insert.")";
            $values=$values.")";
            $sql=$insert." ".$values;
            $this->execute_query($sql);
            $codice_multiplechoice=  $this->get_last_codice('domande_sessione');
        }


        $multiplechoice=  $this->db_get_row('domande_sessione', '*', "\"Codice\"=$codice_multiplechoice");
        $codice_registro_presenze=$multiplechoice['CodiceRegistroPresenze'];
        // se è stato impostato un registro presenze, aggiorno i relativi campi ridondanti di domandeaperte_sessione
        if($codice_registro_presenze!=null)
        {
            $registro_presenze=  $this->db_get_row('registropresenze', '*', "\"Codice\"=$codice_registro_presenze");
            $codice_facolta=$registro_presenze['CodiceFacolta'];
            $codice_corso=$registro_presenze['CodiceCorso'];
            $codice_materia=$registro_presenze['CodiceMateria'];
            $data_esame=$registro_presenze['Data'];
            $sql="UPDATE domande_sessione SET \"CodiceFacolta\"=$codice_facolta,\"CodiceCorso\"=$codice_corso,\"CodiceMateria\"=$codice_materia,\"DataEsame\"='$data_esame' WHERE \"Codice\"=$codice_multiplechoice";
            $this->execute_query($sql);

            $sql="UPDATE domande_elenco SET \"CodiceRegistroPresenze\"=$codice_registro_presenze WHERE \"CodiceSessioneInserimento\"=$codice_multiplechoice";
            $this->execute_query($sql);
        }
        return $codice_multiplechoice;
    }
    
    public function multiplechoice_domanda_save($codice_domanda,$post)
    {
        $today_timestamp=  $this->get_today_timestamp();
        $userid=  $this->get_userid();
        if($codice_domanda!='null')
        {
            $status= $this->db_get_value('domande_elenco', "\"RecordStatus\"", "\"Codice\"=$codice_domanda");
            if($status==null)
            {
                $status='edit';
            }
            $sql="UPDATE domande_elenco";
            $set="SET \"RecordStatus\"='$status',\"RecordDataModifica\"='$today_timestamp',\"RecordModificatoDa\"=$userid ";
            $counter=0;
            foreach ($post as $key => $value) {
                if($value=='')
                {
                    $value="null";
                }
                else
                {
                    $value=  str_replace("'", "''", $value);
                    $value="'$value'";
                }
                    $set=$set.',';
                    $set=$set." \"$key\"=$value";
                    $counter++;
            }
            $where="WHERE \"Codice\"=$codice_domanda";
            $sql="$sql $set $where";
            $this->execute_query($sql);
        }
        else
        {
            $codice_docente=  $this->get_codice_docente();
            //$codice_domanda=  $this->generate_seriale('domande_elenco', 'Codice');
            $CodiceSessioneInserimento=$post['CodiceSessioneInserimento'];
            $domande_sessione=  $this->db_get_row('domande_sessione','*',"\"Codice\"=$CodiceSessioneInserimento");
            $CodiceRegistroPresenze=$domande_sessione['CodiceRegistroPresenze'];
            if($this->isempty($CodiceRegistroPresenze))
            {
                $CodiceRegistroPresenze='null';
            }
            $ProgressivoFormularioEsame=$domande_sessione['ProgressivoFormularioEsame'];
            $domande_sessione_RecordStatus=$domande_sessione['RecordStatus'];
            $RecordStatus='new';
            if(($domande_sessione_RecordStatus==null)||($domande_sessione_RecordStatus=='edit'))
            {
                $RecordStatus='new_existingmaster';
            }
            $RispostaEsatta='null';
            //$max_numerodomanda=$this->db_get_value('domande_elenco', "MAX(\"NumeroDomanda\")", "\"CodiceSessioneInserimento\"=$CodiceSessioneInserimento");
            $max_numerodomanda=  $this->db_get_max('domande_elenco', 'NumeroDomanda', "\"CodiceSessioneInserimento\"=$CodiceSessioneInserimento");
            if($this->isempty($max_numerodomanda))
            {
                $max_numerodomanda=0;
            }
            $numerodomanda=$max_numerodomanda+1;
            $insert="INSERT INTO domande_elenco (\"NumeroDomanda\",\"RispostaEsatta\",\"CodiceRegistroPresenze\",\"ProgressivoFormularioEsame\",\"ImmagineDomanda\",\"RecordStatus\",\"RecordCreatoDa\"";
            $values="VALUES($numerodomanda,$RispostaEsatta,$CodiceRegistroPresenze,$ProgressivoFormularioEsame,'(nessuna)','$RecordStatus',$userid";
            $counter=0;
            foreach ($post as $key => $value) {
                if($value=='')
                {
                    $value="null";
                }
                else
                {
                    $value=  str_replace("'", "''", $value);
                    $value="'$value'";
                }
                $insert=$insert.",\"$key\"";
                $values=$values.",$value";
                $counter++;
            }
            $insert=$insert.")";
            $values=$values.")";
            $sql=$insert." ".$values;
            $this->execute_query($sql);
            $codice_domanda=  $this->get_last_codice('domande_elenco');
        }


        return $codice_domanda;
    }

    

    public function domandeaperte_delete($codice_domandeaperte)
    {
        $sql="
            UPDATE domandeaperte_sessione
            SET \"RecordStatus\"='deleted'
            WHERE \"Codice\"=$codice_domandeaperte
            ";
        $this->execute_query($sql);
    }

    public function multiplechoice_delete($codice_multiplechoice)
    {
        $sql="
            UPDATE domande_sessione
            SET \"RecordStatus\"='deleted'
            WHERE \"Codice\"=$codice_multiplechoice
            ";
        $this->execute_query($sql);
    }

    public function multiplechoice_domanda_delete($codice_multiplechoice_domanda)
    {
        $sql="
            UPDATE domande_elenco
            SET \"RecordStatus\"='deleted'
            WHERE \"Codice\"=$codice_multiplechoice_domanda
            ";
        $this->execute_query($sql);
    }

    

    public function get_record_source_docenti_domande_aperte($codiceDocente)
    {
        $sql=  "SELECT DomandeAperte_Sessione.\"Codice\", DomandeAperte_Sessione.\"Codice\" as \"N.\", (\"DescrizioneITA\") AS \"Materia\",to_char(DomandeAperte_Sessione.\"DataEsame\",'DD/MM/YYYY') as \"DataEsame\",DomandeAperte_Sessione.\"CodiceRegistroPresenze\" as \"Registro\",Facolta.\"Descrizione\" AS \"Facolta\", DomandeAperte_Sessione.\"NoteEsame\"
                FROM (((DomandeAperte_Sessione LEFT JOIN Docente
                                ON DomandeAperte_Sessione.\"CodiceDocente\" = Docente.\"Codice\") LEFT JOIN Facolta
                                ON DomandeAperte_Sessione.\"CodiceFacolta\" = Facolta.\"Codice\") LEFT JOIN Corso
                                ON DomandeAperte_Sessione.\"CodiceCorso\" = Corso.\"Codice\") LEFT JOIN Materia
                                ON DomandeAperte_Sessione.\"CodiceMateria\" = Materia.\"Codice\"
                                WHERE DomandeAperte_Sessione.\"CodiceDocente\"=".$codiceDocente." AND (\"RecordStatus\"!='deleted' OR \"RecordStatus\" is null)
                ORDER BY  DomandeAperte_Sessione.\"Codice\" DESC
                ";


        return $this->select($sql);
    }

    public function get_record_source_docenti_esamiorali($codiceDocente)
    {
        $sql=  "SELECT to_char(registropresenze.\"Data\",'DD/MM/YYYY') as \"DataEsame\",registropresenze.\"Codice\", registropresenze.\"Codice\" as \"Registro\", (\"DescrizioneITA\") AS \"Materia\",classe.\"CognomePrimoStudente\" as \"Classe\"
                FROM (((registropresenze LEFT JOIN Docente
                                ON registropresenze.\"CodiceDocente1\" = Docente.\"Codice\") LEFT JOIN Facolta
                                ON registropresenze.\"CodiceFacolta\" = Facolta.\"Codice\") LEFT JOIN Corso
                                ON registropresenze.\"CodiceCorso\" = Corso.\"Codice\") LEFT JOIN Materia
                                ON registropresenze.\"CodiceMateria\" = Materia.\"Codice\" JOIN classe
                                ON registropresenze.\"CodiceClasse\"= classe.\"Codice\"
                                WHERE registropresenze.\"CodiceDocente1\"=".$codiceDocente." AND registropresenze.\"CodiceTipoCalendario\"=2
                ORDER BY  registropresenze.\"Data\" DESC
                ";


        return $this->select($sql);
    }
    
    public function get_numero_studenti_registrati($CodiceRegistroPresenze)
    {
        $sql="
            SELECT COUNT(*) as numero
            FROM RegistroPresenze_StampaUnione_Studenti
            WHERE \"Codice\"=$CodiceRegistroPresenze and \"Selezionato\"=true
            ";
        $result= $this->select($sql);
        if(count($result)==1)
        {
            return $result[0]['numero'];
        }
        else
        {
            return 0;
        }
    }
    
    public function get_numero_voti_registrati($CodiceRegistroPresenze)
    {
        $sql="
            SELECT COUNT(*) as numero
            FROM studente_esami
            WHERE \"CodiceRegistroPresenze\"=$CodiceRegistroPresenze
            ";
        $result= $this->select($sql);
        if(count($result)==1)
        {
            return $result[0]['numero'];
        }
        else
        {
            return 0;
        }
    }

    function domandeaperte_duplica($codice_domandeaperte)
    {
        //$codice_domandeaperte_new=  $this->generate_seriale('domandeaperte_sessione', 'Codice');
        $codice_domandeaperte_new=$this->db_duplicate_row("domandeaperte_sessione","\"Codice\"=$codice_domandeaperte","Codice");
        $sql="UPDATE domandeaperte_sessione SET \"CodiceRegistroPresenze\"=null WHERE \"Codice\"=$codice_domandeaperte_new";
        $this->execute_query($sql);
        return $codice_domandeaperte_new;
    }

    function multiplechoice_duplica($codice_multiplechoice)
    {
       // $codice_multiplechoice_new=  $this->generate_seriale('domande_sessione', 'Codice');
        $codice_multiplechoice_new=$this->db_duplicate_row("domande_sessione","\"Codice\"=$codice_multiplechoice","Codice");
        $ProgressivoFormularioEsame_new=  $this->generate_seriale('domande_sessione', 'ProgressivoFormularioEsame');
        $sql="UPDATE domande_sessione SET \"CodiceRegistroPresenze\"=null,\"ProgressivoFormularioEsame\"=$ProgressivoFormularioEsame_new WHERE \"Codice\"=$codice_multiplechoice_new";
        $this->execute_query($sql);
        $multiplechoice_domande_elenco=  $this->db_get('domande_elenco', '*', "\"CodiceSessioneInserimento\"=$codice_multiplechoice",'ORDER BY "Codice" ASC');
        foreach ($multiplechoice_domande_elenco as $key => $domanda) {
            $codice_domanda=$domanda['Codice'];
            //$codice_domanda_new=$this->generate_seriale('domande_elenco', 'Codice');
            $codice_domanda_new=$this->db_duplicate_row("domande_elenco","\"Codice\"=$codice_domanda","Codice");
            $sql="UPDATE domande_elenco SET \"CodiceSessioneInserimento\"=$codice_multiplechoice_new,\"ProgressivoFormularioEsame\"=$ProgressivoFormularioEsame_new,\"CodiceRegistroPresenze\"=null WHERE \"Codice\"=$codice_domanda_new";
            $this->execute_query($sql);
        }

        return $codice_multiplechoice_new;
    }

    public function get_domandeaperte($Codice)
    {
        $domandeaperte=  $this->db_get_row('domandeaperte_sessione', "*", "\"Codice\"=$Codice");
        return $domandeaperte;
    }

    public function get_multiplechoice($Codice)
    {
        $multiplechoice=  $this->db_get_row('domande_sessione', "*", "\"Codice\"=$Codice");
        return $multiplechoice;
    }

    public function get_multiplechoice_domanda($codice)
    {
        $multiplechoice_domanda=  $this->db_get_row('domande_elenco', "*", "\"Codice\"=$codice");
        return $multiplechoice_domanda;
    }

    public function get_multiplechoice_domande_elenco($codice_sessione)
    {
        $multiplechoice_domande_elenco=  $this->db_get('domande_elenco', '"Codice","TestoDomanda"', "\"CodiceSessioneInserimento\"=$codice_sessione AND (\"RecordStatus\" is null OR \"RecordStatus\"!='deleted')");
        return $multiplechoice_domande_elenco;
    }

    public function get_esami_docente($codiceDocente)
    {
        $sql="SELECT RegistroPresenze.\"Data\", ( to_char(RegistroPresenze.\"Data\",'DD/MM/YYYY') || ' - ' || RegistroPresenze.\"Codice\" || ' - ' || \"DescrizioneITA\") AS \"Descrizione\", RegistroPresenze.\"CodiceFacolta\", RegistroPresenze.\"CodiceCorso\", RegistroPresenze.\"CodiceMateria\", RegistroPresenze.\"Codice\", RegistroPresenze.\"CodiceClasse\", RegistroPresenze.\"CodiceDocenteDiRiferimento\"
              FROM RegistroPresenze INNER JOIN materia ON RegistroPresenze.\"CodiceMateria\" = Materia.\"Codice\"
              WHERE (((RegistroPresenze.\"CodiceTipoCalendario\")=2))
              GROUP BY RegistroPresenze.\"Data\", (to_char(RegistroPresenze.\"Data\",'DD/MM/YYYY') || ' - ' || RegistroPresenze.\"Codice\" || ' - ' || \"DescrizioneITA\"), RegistroPresenze.\"CodiceFacolta\", RegistroPresenze.\"CodiceCorso\", RegistroPresenze.\"CodiceMateria\", RegistroPresenze.\"Codice\", RegistroPresenze.\"CodiceClasse\", RegistroPresenze.\"CodiceDocenteDiRiferimento\"
              HAVING (((RegistroPresenze.\"CodiceDocenteDiRiferimento\")=".$codiceDocente."))
              ORDER BY RegistroPresenze.\"Data\" DESC;";

        return $this->select($sql);
    }

    public function get_materia_registropresenza($codiceRegistroPresenza)
    {
        $sql="SELECT \"DescrizioneITA\" FROM materia JOIN RegistroPresenze ON RegistroPresenze.\"CodiceMateria\"=materia.\"Codice\" WHERE RegistroPresenze.\"Codice\"=".$codiceRegistroPresenza;
        $data=$this->select($sql);
        return $data;
    }
    /**
     *
     * @param type $giorno
     * @param type $OraAttuale
     * @param type $codiceDocente
     * @return ritorna l'elenco dei registri presenza in base al giorno attuale, ora attuale e il codice docente
     *
     */
    public function get_registro_presenze_LUCA($giorno,$OraAttuale,$codiceDocente)
    {
        $sql="SELECT RegistroPresenze.\"Codice\" AS Codice, \"Terminale\",\"OraPrevistaInizioLezione1\",\"OraPrevistaFineLezione1\"
                FROM RegistroPresenze JOIN Aula ON RegistroPresenze.\"CodiceAula\"=Aula.\"Codice\"
                WHERE to_char(\"Data\",'YYYY-MM-DD') LIKE '$giorno'
                AND ('$OraAttuale' BETWEEN to_char(\"OraPrevistaInizioLezione1\" - (interval '30 minutes'),'HH24:MI:SS') AND to_char(\"OraPrevistaFineLezione1\",'HH24:MI:SS')) AND \"CodiceDocente1\"=".$codiceDocente." AND \"CodiceTipoCalendario\" IN(1,2,3,7)";

        $rows=$this->select($sql);
        return $rows;
    }

    public function get_registro_presenze_timbrature($giorno,$OraAttuale,$codiceDocente)
    {
        $sql="SELECT timbrature_registripresenze_oggi.\"CodiceRegistroPresenze\" AS Codice, \"Terminale\",to_char(\"OraPrevistaInizioLezione1\",'HH24:MI:SS') as \"OraPrevistaInizioLezione1\",to_char(\"OraPrevistaFineLezione1\",'HH24:MI:SS') as \"OraPrevistaFineLezione1\",\"DocenteArrivato\",\"DocenteUscito\"
                FROM timbrature_registripresenze_oggi
                WHERE to_char(\"Data\",'YYYY-MM-DD') LIKE '$giorno'
                AND ('$OraAttuale' BETWEEN to_char(\"OraPrevistaInizioLezione1\" - (interval '30 minutes'),'HH24:MI:SS')
                AND to_char(\"OraPrevistaFineLezione1\" + (interval '30 minutes'),'HH24:MI:SS')) AND \"CodiceDocente\"=".$codiceDocente." AND \"CodiceTipoCalendario\" IN(1,2,3,7)";

        $rows=$this->select($sql);
        return $rows;
    }

    public function get_registropresenze($codice_registropresenze)
    {
        $sql="
            SELECT registropresenze.*,registropresenze.\"Data\",to_char(\"OraPrevistaInizioLezione1\",'HH24:MI') as \"OraPrevistaInizioLezione1\",to_char(\"OraPrevistaFineLezione1\",'HH24:MI') as \"OraPrevistaFineLezione1\",registropresenze.\"CodiceMateria\",registropresenze.\"CodiceClasse\",registropresenze.\"CodiceAnnoAccademico\",\"DescrizioneITA\",\"Descrizione\",docente.\"Cognome\",docente.\"Nome\"
            FROM registropresenze
            JOIN materia ON materia.\"Codice\"=registropresenze.\"CodiceMateria\"
            JOIN classe ON classe.\"Codice\"=registropresenze.\"CodiceClasse\"
            JOIN docente ON docente.\"Codice\"=registropresenze.\"CodiceDocente1\"
            WHERE registropresenze.\"Codice\"=$codice_registropresenze
            ";
        $result= $this->select($sql);
        if(count($result)>0)
        {
            return $result[0];
        }
        else
        {
            return array();
        }
    }
    
    public function get_calendario_generale_from_registropresenze($codice_registropresenze)
    {
       $calendario_generale= $this->db_get_row('calendario_generale', '*', "\"CodiceRegistroPresenze\"=$codice_registropresenze");
       return $calendario_generale;
    }

    public function  get_lista_studenti_LUCA($codiceRegistroPresenze)
    {
        $sql='SELECT Studente."Cognome", Studente."Nome", Studente."Matricola", Studente."Codice" AS codicestudente, RegistroPresenze."Codice", "Path_Foto"
                FROM RegistroPresenze INNER JOIN (Classe_Studente INNER JOIN Studente ON Classe_Studente."CodiceStudente" = Studente."Codice") ON RegistroPresenze."CodiceClasse" = Classe_Studente."CodiceClasse"
                WHERE (((RegistroPresenze."Codice")='.$codiceRegistroPresenze.'))
                ORDER BY Studente."Cognome", Studente."Nome";';

        return $this->select($sql);
    }

    public function  get_lista_studenti($codiceRegistroPresenze)
    {
        $sql='SELECT Studente."Cognome", Studente."Nome", Studente."NBadge", Studente."Codice" AS codicestudente, "Path_Foto","CodiceTipoAssenza"
                FROM timbrature_registropresenze_storico_studente_oggi INNER JOIN Studente ON
                    timbrature_registropresenze_storico_studente_oggi."CodiceStudente" = Studente."Codice"
                WHERE timbrature_registropresenze_storico_studente_oggi."CodiceRegistroPresenze"='.$codiceRegistroPresenze.'
                ORDER BY Studente."Cognome", Studente."Nome";';

        return $this->select($sql);
    }

    public function get_esameorale_studenti($codice_registropresenze)
    {
       $studenti=array();
       /*$sql="SELECT registropresenze_storico_studente.*,studente.*, studente.\"Codice\" as \"CodiceStudente\"
            FROM registropresenze_storico_studente
            JOIN studente
            ON registropresenze_storico_studente.\"CodiceStudente\"=studente.\"Codice\"
            WHERE registropresenze_storico_studente.\"CodiceRegistroPresenze\"=$codice_registropresenze
            ";

       $studenti= $this->select($sql);
       if(count($studenti)>0)
       {
           return $studenti;
       }*/

       $sql="SELECT RegistroPresenze_StampaUnione_Studenti.*,studente.*,RegistroPresenze_StampaUnione_Studenti.\"Codice\" as CodiceRegistroPresenze
            FROM RegistroPresenze_StampaUnione_Studenti
            JOIN studente
            ON RegistroPresenze_StampaUnione_Studenti.\"CodiceStudente\"=studente.\"Codice\"
            WHERE RegistroPresenze_StampaUnione_Studenti.\"Codice\"=$codice_registropresenze AND RegistroPresenze_StampaUnione_Studenti.\"Selezionato\"=true
            ORDER BY studente.\"Cognome\",studente.\"Nome\"
            ";
       $studenti= $this->select($sql);
       if(count($studenti)>0)
       {
           return $studenti;
       }

       /*$sql="SELECT registropresenze.*,studente.*,studente.\"Codice\" as \"CodiceStudente\"
            FROM registropresenze
            JOIN classe_studente
            ON classe_studente.\"CodiceClasse\"=registropresenze.\"CodiceClasse\"
            JOIN studente
            ON classe_studente.\"CodiceStudente\"=studente.\"Codice\"
            WHERE registropresenze.\"Codice\"=$codice_registropresenze
            ";
       $studenti= $this->select($sql);
       if(count($studenti)>0)
       {
           return $studenti;
       }*/

       return $studenti;
    }
    
    public function get_voti_registrati($CodiceRegistroPresenze)
    {
        $voti_registrati=array();
        $studenti=  $this->db_get('studente_esami', '*', "\"CodiceRegistroPresenze\"=$CodiceRegistroPresenze");
        foreach ($studenti as $key => $studente) {
            $voti_registrati[$studente['CodiceStudente']]=$studente;
        }
        return $voti_registrati;
    }

    public function get_studenti_lezione($codice_registropresenze)
    {
        $sql='SELECT Studente."Cognome", Studente."Nome", Studente."NBadge", Studente."Codice" AS codicestudente, "Path_Foto","CodiceTipoAssenza"
                FROM registropresenze_storico_studente INNER JOIN Studente ON
                    registropresenze_storico_studente."CodiceStudente" = Studente."Codice"
                WHERE registropresenze_storico_studente."CodiceRegistroPresenze"='.$codice_registropresenze.'
                ORDER BY Studente."Cognome", Studente."Nome";';

        return $this->select($sql);
    }

    public function get_studenti_classe($codice_classe)
    {
        $sql="
            SELECT studente.*
            FROM classe_studente JOIN studente ON classe_studente.\"CodiceStudente\"=studente.\"Codice\"
            WHERE classe_studente.\"CodiceClasse\"=$codice_classe
                ORDER BY studente.\"Cognome\"
            ";
        return $this->select($sql);
    }
    
    public function get_studenti_progettodidattica($codice_progettodidattica)
    {
        $sql="
            SELECT studente.*
            FROM progettodidattica_studente JOIN studente ON progettodidattica_studente.\"CodiceStudente\"=studente.\"Codice\"
            WHERE progettodidattica_studente.\"CodiceProgettoDidattica\"=$codice_progettodidattica
                ORDER BY studente.\"Cognome\"
            ";
        return $this->select($sql);
    }

    public function get_appuntamenti_calendario_generale($giorno,$oraInizioRange,$oraFineRange,$CodiceAula)
    {
        $sql='SELECT Codice,CodiceClasse,CodiceAula,OraInizio FROM Calendario_Generale WHERE DATE_FORMAT(giorno,"%Y-%m-%d") LIKE "'.$giorno.'" AND DATE_FORMAT( OraInizio, "%H:%i:%s" ) >= "'.$oraInizioRange.'" AND DATE_FORMAT( OraInizio, "%H:%i:%s" ) <= "'.$oraFineRange.'" AND CodiceAula='.$CodiceAula;
        $data=$this->select($sql);
        return $data;
    }

    public function ContaTimbrature_LUCA($NumeroTerminale,$ggmmaa,$numeroBadge,$OraInizio,$OraFine)
    {
        $sql="SELECT COUNT(*) as conteggio FROM timbrature WHERE \"NumeroTerminale\"='$NumeroTerminale' AND \"GGMMAA\" LIKE '$ggmmaa' AND \"NumeroBadge\" LIKE '%$numeroBadge%' AND \"HHMM\"::INTEGER BETWEEN ". $OraInizio." AND ". $OraFine;
        $rows=$this->select($sql);
        return $rows[0]['conteggio'];
    }

    public function ContaTimbrature($numeroBadge,$OraInizio,$OraFine)
    {
        $sql="SELECT COUNT(*) as conteggio FROM timbrature_oggi
              WHERE \"NumeroBadge\" LIKE '%$numeroBadge%' AND \"HHMM\"::INTEGER BETWEEN ". $OraInizio." AND ". $OraFine;
        $rows=$this->select($sql);
        return $rows[0]['conteggio'];
    }

    /***
     * Ritorna l'elenco totale della tabella timbrature in base al numero di terminale, giorno e numero di badge
     * @author Luca Giordano
     */
    public function getTimbratureLUCA($NumeroTerminale,$ggmmaa,$numeroBadge='')
    {
       $sql="SELECT * FROM timbrature WHERE \"NumeroTerminale\"='$NumeroTerminale' AND \"GGMMAA\" LIKE '$ggmmaa' AND \"NumeroBadge\" LIKE '%$numeroBadge%'";
       $data=$this->select($sql);
       return $data;
    }

    public function getTimbrature($numeroBadge='')
    {
       //Prelevo tutte le timbrature del giorno corrente7
        -
       $sql="SELECT * FROM timbrature_oggi WHERE \"NumeroBadge\" LIKE '%$numeroBadge%'";
       $data=$this->select($sql);
       return $data;
    }



    public function get_usergroup($userid)
    {
        $sql="SELECT * FROM sys_group_user JOIN sys_group on sys_group_user.groupid=sys_group.id where sys_group_user.userid=$userid";
        $result=  $this->select($sql);
        $return['code']='';
        $return['value']='';
        if(count($result)>0)
        {
            $return['code']=$result['id'];
            $return['value']=$result['name'];

        }
        return $return;
    }

    public function get_dbdriver()
    {
        return $this->db->dbdriver;
    }


    /**
     * ritorna dati utente per il login
     *
     * @param type $username nome utente
     * @return  array[username;password,firstname,id]
     * @author Alessandro Galli
     */
    function get_user_login($username)
    {
        //$sys_settings=  $this->get_sys_settings();
        $like='like';
        if($this->get_dbdriver()=='postgre')
            $like='ilike';
        $sql="
            SELECT \"Nome\" as username,\"Password\" as password,\"Nome\" as firstname,\"Codice\" as id, \"CodiceDocente\" as \"CodiceDocente\"
            FROM utente
            WHERE \"Nome\" $like '$username'
        ";
        $result =  $this->select($sql);
        return $result;
    }

    function get_anniaccademici()
    {
        $sql="SELECT * FROM annoaccademico";
        $result=  $this->select($sql);
        return $result;
    }


    function get_docenti($materia=null)
    {
        if($materia==null)
        {
            $sql="SELECT * FROM docente order by \"Cognome\",\"Nome\"";
        }
        else
        {
            $sql="SELECT * FROM docente order by \"Cognome\",\"Nome\"";
        }
        $result=  $this->select($sql);
        return $result;
    }

    function get_tipicalendario($codice_annoaccademico)
    {
       return array();
    }

    function get_classi($codice_annoaccademico=null,$EstivoInvernale=null)
    {
        if($EstivoInvernale==null)
        {
            if($codice_annoaccademico==null)
            {
                $sql="SELECT * FROM classe UNION SELECT * from CLASSE where \"Codice\"=117 ORDER BY \"CognomePrimoStudente\"";
            }
            else
            {
            $sql="SELECT * FROM classe WHERE \"CodiceAnnoAccademico\"=$codice_annoaccademico UNION SELECT * from CLASSE where \"Codice\"=117 ORDER BY \"CognomePrimoStudente\"";
            }
        }
        else
        {
                $sql="SELECT * FROM classe WHERE \"CodiceAnnoAccademico\"=$codice_annoaccademico AND \"EstivoInvernale\"='$EstivoInvernale' UNION SELECT * from CLASSE where \"Codice\"=117 ORDER BY \"CognomePrimoStudente\"";
        }
        $result=  $this->select($sql);
        //aggiunta classe dipendenti
        

        return $result;
    }

    function get_options_sedi()
    {
        $sql="SELECT * FROM sede ORDER BY \"Descrizione\"";
        $result=  $this->select($sql);
        return $result;
    }

    function get_options_aule()
    {
        $sql="SELECT * FROM aula ORDER BY \"Descrizione\"";
        $result=  $this->select($sql);
        return $result;
    }
    
    function get_options_tiporicevente()
    {
        $sql="SELECT * FROM tiporicevente ORDER BY \"Descrizione\"";
        $result=  $this->select($sql);
        return $result;
    }
    
    function get_options_tiporichiedente()
    {
        $sql="SELECT * FROM tiporichiedente ORDER BY \"Descrizione\"";
        $result=  $this->select($sql);
        return $result;
    }
    
    function get_options_classi()
    {
        $sql="SELECT * FROM classe ORDER BY \"Descrizione\"";
        $result=  $this->select($sql);
        return $result;
    }

    function get_facolta_options()
    {
        $sql="SELECT * FROM facolta ORDER BY \"Descrizione\"";
        $result=  $this->select($sql);
        return $result;
    }

    function get_corso_options()
    {
        $sql="SELECT * FROM corso ORDER BY \"Descrizione\"";
        $result=  $this->select($sql);
        return $result;
    }


    function get_materie($codice_docente=null)
    {
        if($codice_docente==null)
        {
            $sql="SELECT * FROM materia order by \"DescrizioneITA\" ASC";
        }
        else
        {
           //$sql="SELECT * FROM materia JOIN materia_docente ON materia.\"Codice\"=materia_docente.\"CodiceMateria\" WHERE materia_docente.\"CodiceDocente\"=$codice_docente";
            $sql="SELECT materia.\"Codice\",materia.\"DescrizioneITA\",materia.\"DescrizioneENG\",materia.\"Sigla\" FROM materia JOIN progettodidattica ON materia.\"Codice\"=progettodidattica.\"CodiceMateria\" JOIN progettodidattica_docente ON progettodidattica.\"Codice\"=progettodidattica_docente.\"CodiceProgettoDidattica\" WHERE progettodidattica_docente.\"CodiceDocente\"=$codice_docente GROUP BY materia.\"Codice\",materia.\"DescrizioneITA\" ORDER BY materia.\"DescrizioneITA\" ASC";
        }

        $result=  $this->select($sql);
        return $result;
    }

    function get_anno($codiceannoaccademico,$mese)
    {
        $sql="SELECT \"Descrizione\" FROM annoaccademico WHERE \"Codice\"=$codiceannoaccademico";
        $result=  $this->select($sql);
        $annoaccademico=$result[0]['Descrizione'];
        $anni=  explode("/", $annoaccademico);
        $anno0=$anni[0];
        $anno1=$anni[1];
        if($mese=='01')
        {
            $anno=$anno1;
        }
        return $anno;
    }
    function get_calendario_generale($codice_classe,$parametri=array())
    {
        $mese=$parametri['mese'];
        $meseprima=intval($mese)-1;
        $meseprima=sprintf("%02d",$meseprima );
        $mesedopo=intval($mese)+1;
        $mesedopo=sprintf("%02d", $mesedopo );
        //$annoaccademico=$parametri['annoaccademico'];
        $sql="SELECT *,\"Codice\",\"CodiceRegistroPresenze\",\"ArgomentoDescrizione\",to_char(\"Giorno\",'YYYY-MM-DD') as \"Giorno\",to_char(\"OraInizio\",'HH24:MI') as \"OraInizio\",to_char(\"OraFine\",'HH24:MI') as \"OraFine\",\"CodiceTipoCalendario\" "
                . "FROM calendario_generale "
                . "WHERE \"CodiceClasse\"=$codice_classe  AND (to_char(\"Giorno\",'MM')='$mese' OR to_char(\"Giorno\",'MM')='$meseprima' OR to_char(\"Giorno\",'MM')='$mesedopo' )";
        $result=  $this->select($sql);
        return $result;
    }

    function get_periodi_classe($codice_classe,$parametri)
    {

        $mese=$parametri['mese'];
        $sql="SELECT \"Codice\",to_char(\"Giorno\",'YYYY-MM-DD') as \"Giorno\",to_char(\"OraInizio\",'HH24:MI') as \"OraInizio\",to_char(\"OraFine\",'HH24:MI') as \"OraFine\",\"CodiceTipoCalendario\" FROM calendario_periodi_classi WHERE  \"CodiceClasse\"=$codice_classe AND to_char(\"Giorno\",'MM')='$mese' ";
        $result=  $this->select($sql);

        return $result;
    }

    function get_impegni_docente($codice_classe,$parametri)
    {
        $codice_docente='';
        if(array_key_exists('CodiceDocente1', $parametri))
            $codice_docente=$parametri['CodiceDocente1'];

        $mese=$parametri['mese'];
        if($this->isnotempty($codice_docente))
        {
            $sql="SELECT *,\"Codice\",\"ArgomentoDescrizione\",to_char(\"Giorno\",'YYYY-MM-DD') as \"Giorno\",to_char(\"OraInizio\",'HH24:MI') as \"OraInizio\",to_char(\"OraFine\",'HH24:MI') as \"OraFine\",\"CodiceTipoCalendario\" FROM calendario_generale WHERE \"CodiceDocente1\"=$codice_docente AND \"CodiceClasse\"!=$codice_classe AND to_char(\"Giorno\",'MM')='$mese' AND \"CodiceTipoCalendario\"!=13 ";
            $result=  $this->select($sql);
        }
        else
        {
            $result=array();
        }
        return $result;
    }

    function get_indisponibilita_docente($parametri)
    {
        $codice_docente='';
        if(array_key_exists('docente', $parametri))
            $codice_docente=$parametri['docente'];
        $mese=$parametri['mese'];
        if($this->isnotempty($codice_docente))
        {
            $sql="SELECT \"Codice\",\"ArgomentoDescrizione\",to_char(\"Giorno\",'YYYY-MM-DD') as \"Giorno\",to_char(\"OraInizio\",'HH24:MI') as \"OraInizio\",to_char(\"OraFine\",'HH24:MI') as \"OraFine\",\"CodiceTipoCalendario\" FROM calendario_generale WHERE \"CodiceDocente1\"=$codice_docente AND to_char(\"Giorno\",'MM')='$mese' AND \"CodiceTipoCalendario\"=13";
            $result=  $this->select($sql);
        }
        else
        {
            $result=array();
        }
        return $result;
    }

    function get_eventi()
    {
        return array();
    }

    function get_eventi_docenti_calendario($codice_docente,$year,$month)
    {
        $month_next=$month+1;
        $month_prev=$month-1;
        $sql="
            SELECT *,to_char(\"Giorno\",'YYYY-MM-DD') as \"Giorno\",to_char(\"OraInizio\",'HH24:MI') as \"OraInizio\",to_char(\"OraFine\",'HH24:MI') as \"OraFine\"
            FROM calendario_generale
            WHERE \"CodiceDocente1\"=$codice_docente AND to_char(\"Giorno\",'MM')='$month' AND to_char(\"Giorno\",'YYYY')='$year'
            ";
        $rows=$this->select($sql);
        return $rows;
    }

    function get_eventi_calendario_generale($anno,$mese,$post)
    {

        $meseprima=intval($mese)-1;
        $meseprima=sprintf("%02d",$meseprima );
        $mesedopo=intval($mese)+1;
        $mesedopo=sprintf("%02d", $mesedopo );
        $where="TRUE";
        if(array_key_exists("classi", $post))
        {
            $where=$where." AND (";
            foreach ($post['classi'] as $key => $classe) {
                if($key!=0)
                {
                    $where=$where." OR";
                }
                $where=$where." \"CodiceClasse\"=$classe";
            }
            $where=$where.")";
        }
        if(array_key_exists("docente", $post))
        {
            $CodiceDocente=$post['docente'];
            if($this->isnotempty($CodiceDocente))
            {
                $where=$where." AND \"CodiceDocente1\"=$CodiceDocente";
            }
            
        }
        if(array_key_exists("CodiceTipoCalendario", $post))
        {
            $CodiceTipoCalendario=$post['CodiceTipoCalendario'];
            if($this->isnotempty($CodiceTipoCalendario))
            {
                $where=$where." AND \"CodiceTipoCalendario\"=$CodiceTipoCalendario";
            }
            
        }
        
        $sql="
            SELECT *,to_char(\"Giorno\",'YYYY-MM-DD') as \"Giorno\",to_char(\"OraInizio\",'HH24:MI') as \"OraInizio\",to_char(\"OraFine\",'HH24:MI') as \"OraFine\"
            FROM calendario_generale
            WHERE $where AND to_char(\"Giorno\",'YYYY')='$anno' AND ( to_char(\"Giorno\",'MM')='$mese' OR to_char(\"Giorno\",'MM')='$meseprima' OR to_char(\"Giorno\",'MM')='$mesedopo' )
            ";
        if($mese=="00")
        {
            $annodopo=$anno;
            $anno=intval($anno)-1;
            $sql="
            SELECT *,to_char(\"Giorno\",'YYYY-MM-DD') as \"Giorno\",to_char(\"OraInizio\",'HH24:MI') as \"OraInizio\",to_char(\"OraFine\",'HH24:MI') as \"OraFine\"
            FROM calendario_generale
            WHERE $where AND (to_char(\"Giorno\",'YYYY')='$anno' AND ( to_char(\"Giorno\",'MM')='12' OR to_char(\"Giorno\",'MM')='11') ) OR (to_char(\"Giorno\",'YYYY')='$annodopo' AND  to_char(\"Giorno\",'MM')='01')
            ";
        }

        if($mese=="12")
        {
            $annodopo=intval($anno)+1;
            $sql="
            SELECT *,to_char(\"Giorno\",'YYYY-MM-DD') as \"Giorno\",to_char(\"OraInizio\",'HH24:MI') as \"OraInizio\",to_char(\"OraFine\",'HH24:MI') as \"OraFine\"
            FROM calendario_generale
            WHERE $where AND (to_char(\"Giorno\",'YYYY')='$anno' AND ( to_char(\"Giorno\",'MM')='12' OR to_char(\"Giorno\",'MM')='11') ) OR (to_char(\"Giorno\",'YYYY')='$annodopo' AND  to_char(\"Giorno\",'MM')='01')
            ";
        }

        if($mese=="01")
        {
            $annoprima=intval($anno)-1;
            $sql="
            SELECT *,to_char(\"Giorno\",'YYYY-MM-DD') as \"Giorno\",to_char(\"OraInizio\",'HH24:MI') as \"OraInizio\",to_char(\"OraFine\",'HH24:MI') as \"OraFine\"
            FROM calendario_generale
            WHERE $where AND (to_char(\"Giorno\",'YYYY')='$anno' AND ( to_char(\"Giorno\",'MM')='01' OR to_char(\"Giorno\",'MM')='02') ) OR (to_char(\"Giorno\",'YYYY')='$annoprima' AND  to_char(\"Giorno\",'MM')='12')
            ";
        }

        $rows=$this->select($sql);
        return $rows;
    }

    function get_periodi($codice_classe,$year,$month)
    {
        $sql="
            SELECT *,to_char(\"Giorno\",'YYYY-MM-DD') as \"Giorno\",to_char(\"OraInizio\",'HH24:MI') as \"OraInizio\",to_char(\"OraFine\",'HH24:MI') as \"OraFine\"
            FROM calendario_periodi_classi
            WHERE to_char(\"Giorno\",'MM')='$month' AND to_char(\"Giorno\",'YYYY')='$year' AND \"CodiceClasse\"=$codice_classe
            ";
        $rows=$this->select($sql);
        return $rows;
    }

    function insert_registropresenze($fields)
    {
        $codice_registropresenze=$this->insert('registropresenze', $fields);
        
        return $codice_registropresenze;
    }
    
    function insert_calendario_generale($fields)
    {
        $fields['CodiceUtenteModifica']=  $this->get_userid();
        /*$codice_materia=$fields['CodiceMateria'];
        $codice_docente=$fields['CodiceDocente1'];
        $codice_classe=$post['CodiceClasse'];
        $row_classe=  $this->db_get_row('classe', "*", "\"Codice\"=$codice_classe");*/


        /*$fields['CodiceTipoCalendario']=1;
        $fields['Giorno']=date('Y-m-d', strtotime($post['start']));
        $fields['OraInizio']='1899-12-30 '.date('H:i', strtotime(str_replace('GMT+0000', '', $post['start'])));
        $fields['OraFine']='1899-12-30 '.date('H:i', strtotime(str_replace('GMT+0000', '', $post['end'])));
        $fields['CodiceFacolta']=$row_classe['CodiceFacolta'];
        $fields['CodiceCorso']=$row_classe['CodiceCorso'];
        $fields['CodiceAnnoAccademico']=$row_classe['CodiceAnnoAccademico'];
        $fields['CodiceAnnoDiCorso']=$row_classe['CodiceAnnoDiCorso'];
        $fields['CodiceClasse']=$codice_classe;
        $fields['CodiceUtenteModifica']=  $this->get_userid();
        $fields['CodiceRegistroPresenze']=$codice_registropresenze;
        $codice_calendario=  $this->db_get_value('calendario_generale', "\"Codice\"", "\"CodiceRegistroPresenze\"=$codice_registropresenze");*/
        $codice_calendario=$this->insert('calendario_generale', $fields);
        return  $codice_calendario;
        
        
    }
    
    function update_calendario_generale($fields,$codice_calendario)
    {
        $this->update('calendario_generale', $fields,"\"Codice\"=$codice_calendario");
    }
    
    function set_registropresenze_lezione($post,$fields,$codice_registropresenze)
    {
        $codice_classe=$post['CodiceClasse'];
        $row_classe=  $this->db_get_row('classe', "*", "\"Codice\"=$codice_classe");

        $fields['CodiceFacolta']=$row_classe['CodiceFacolta'];
        $fields['CodiceCorso']=$row_classe['CodiceCorso'];
        $fields['CodiceAnnoAccademico']=$row_classe['CodiceAnnoAccademico'];
       
        $fields['CodiceDocenteDiRiferimento']=$fields['CodiceDocente1'];

        $fields['CodiceClasse']=$codice_classe;
        $fields['CodiceTipoCalendario']=1;
        $fields['Data']=date('Y-m-d', strtotime($post['start']));
        $fields['OraPrevistaInizioLezione1']='1899-12-30 '.date('H:i', strtotime(str_replace('GMT+0000', '', $post['start'])));
        $fields['OraPrevistaFineLezione1']='1899-12-30 '.date('H:i', strtotime(str_replace('GMT+0000', '', $arg['end'])));
        $fields['OraInizioLezione1']='1899-12-30 '.date('H:i', strtotime(str_replace('GMT+0000', '', $arg['start'])));
        $fields['OraFineLezione1']='1899-12-30 '.date('H:i', strtotime(str_replace('GMT+0000', '', $arg['end'])));
        $codice_registropresenze=$this->insert('registropresenze', $fields);
        
        
        return $codice_registropresenze;
    }
    
    
        

    function set_registropresenze_esame($post,$fields,$codice_registropresenze)
    {
        $codice_classe=$post['CodiceClasse'];
        $row_classe=  $this->db_get_row('classe', "*", "\"Codice\"=$codice_classe");
        $fields['CodiceTipoEsame']=$post['CodiceTipoEsame'];
        $fields['CodiceFacolta']=$row_classe['CodiceFacolta'];
        $fields['CodiceCorso']=$row_classe['CodiceCorso'];
        $fields['CodiceAnnoAccademico']=$row_classe['CodiceAnnoAccademico'];
       
        $fields['CodiceDocenteDiRiferimento']=$fields['CodiceDocente1'];

        $fields['CodiceClasse']=$codice_classe;
        $fields['CodiceTipoCalendario']=2;
        $fields['Data']=date('Y-m-d', strtotime($post['start']));
        $fields['OraPrevistaInizioLezione1']='1899-12-30 '.date('H:i', strtotime($post['start']));
        $fields['OraPrevistaFineLezione1']='1899-12-30 '.date('H:i', strtotime($post['end']));
        $fields['OraInizioLezione1']='1899-12-30 '.date('H:i', strtotime($post['start']));
        $fields['OraFineLezione1']='1899-12-30 '.date('H:i', strtotime($post['end']));
        $codice_registropresenze=$this->insert('registropresenze', $fields);
        
        
        return $codice_registropresenze;
    }
    
    function set_registropresenze_esameBAK($post)
    {
        $codice_classe=$post['classe'];
        $row_classe=  $this->db_get_row('classe', "*", "\"Codice\"=$codice_classe");
        //$codice_registropresenze=$this->generate_seriale('registropresenze','Codice');
        //$fields['Codice']=$codice_registropresenze;
        $fields['Barcode']="";
        $fields['NumeroProtocollo']="";
        $fields['CodiceTipoCalendario']=2;//TODO
        $fields['CodiceTipoPresenza']="";
        $fields['CodiceFacolta']=$row_classe['CodiceFacolta'];
        $fields['CodiceCorso']=$row_classe['CodiceCorso'];
        $fields['CodiceAnnoAccademico']=$row_classe['CodiceAnnoAccademico'];
        $fields['CodiceAula']=$post['CodiceAula'];
        $fields['CodiceSede']=$post['CodiceSede'];
        $fields['CodiceAnnoDiCorso']="";
        $fields['CodiceClasse']=$codice_classe;
        $fields['CodiceMateria']=$post['materia'];
        $fields['Note']=$post['Note'];
        $fields['Data']=date('Y-m-d', strtotime($post['start']));
        $fields['AttivitaInLinguaStraniera']="";
        $fields['CodiceDocente1']=$post['docente'];
        $fields['OraPrevistaInizioLezione1']='1899-12-30 '.date('H:i', strtotime($post['start']));
        $fields['OraPrevistaFineLezione1']='1899-12-30 '.date('H:i', strtotime($post['end']));
        $fields['OraInizioLezione1']='1899-12-30 '.date('H:i', strtotime($post['start']));
        $fields['OraFineLezione1']='1899-12-30 '.date('H:i', strtotime($post['end']));
        $fields['CodiceDocenteDiRiferimento']=$post['CodiceDocente1'];
        $fields['CodiceDocente2']=$post['CodiceDocente2'];
        $fields['OraPrevistaInizioLezione2']="";
        $fields['OraPrevistaFineLezione2']="";
        $fields['OraInizioLezione2']="";
        $fields['OraFineLezione2']="";
        $fields['CodiceDocente3']="";
        $fields['OraPrevistaInizioLezione3']="";
        $fields['OraPrevistaFineLezione3']="";
        $fields['OraInizioLezione3']="";
        $fields['OraFineLezione3']="";
        $fields['OraInizioPausa1']="";
        $fields['OraFinePausa1']="";
        $fields['OraInizioPausa2']="";
        $fields['OraFinePausa2']="";
        $fields['OraInizioPausa3']="";
        $fields['OraFinePausa3']="";
        $fields['DaStampare']="";
        $fields['CodiceTraduttore1']="";
        $fields['CodiceTraduttore2']="";
        $fields['CodiceAssistente1']="";
        $fields['CodiceAssistente2']="";
        $fields['GruppoLingua']=$post['GruppoLingua'];
        $fields['GruppoNazionalita']="";
        $fields['GruppoEducazioneFisica']="";
        $fields['Elaborato']="";
        $fields['Stampato']="";
        $fields['TestIngresso_Valutativo']="";
        $fields['CodiceSemestre']="";
        $fields['ElaboratoPerEsame']="";
        $fields['CodiceTipoEsame']=$post['CodiceTipoEsame'];

        $codice_registropresenze=$this->insert('registropresenze', $fields);
        return $codice_registropresenze;
    }

    function set_lezione($post,$fields,$codice_registropresenze)
    {
        $codice_materia=$fields['CodiceMateria'];
        $codice_docente=$fields['CodiceDocente1'];
        $codice_classe=$post['CodiceClasse'];
        $row_classe=  $this->db_get_row('classe', "*", "\"Codice\"=$codice_classe");


        $fields['CodiceTipoCalendario']=1;
        $fields['Giorno']=date('Y-m-d', strtotime($post['start']));
        $fields['OraInizio']='1899-12-30 '.date('H:i', strtotime(str_replace('GMT+0000', '', $post['start'])));
        $fields['OraFine']='1899-12-30 '.date('H:i', strtotime(str_replace('GMT+0000', '', $post['end'])));
        $fields['CodiceFacolta']=$row_classe['CodiceFacolta'];
        $fields['CodiceCorso']=$row_classe['CodiceCorso'];
        $fields['CodiceAnnoAccademico']=$row_classe['CodiceAnnoAccademico'];
        $fields['CodiceAnnoDiCorso']=$row_classe['CodiceAnnoDiCorso'];
        $fields['CodiceClasse']=$codice_classe;
        $fields['CodiceUtenteModifica']=  $this->get_userid();
        $fields['CodiceRegistroPresenze']=$codice_registropresenze;
        $codice_calendario=  $this->db_get_value('calendario_generale', "\"Codice\"", "\"CodiceRegistroPresenze\"=$codice_registropresenze");
        $codice_calendario=$this->insert('calendario_generale', $fields);
        
        
        
    }
    
    

    function set_esame($post,$fields,$codice_registropresenze)
    {
        $codice_materia=$fields['CodiceMateria'];
        $codice_docente=$fields['CodiceDocente1'];
        $codice_classe=$post['CodiceClasse'];
        $row_classe=  $this->db_get_row('classe', "*", "\"Codice\"=$codice_classe");


        $fields['CodiceTipoCalendario']=2;
        $fields['Giorno']=date('Y-m-d', strtotime($post['start']));
        $fields['OraInizio']='1899-12-30 '.date('H:i', strtotime(str_replace('GMT+0000', '', $post['start'])));
        $fields['OraFine']='1899-12-30 '.date('H:i', strtotime(str_replace('GMT+0000', '', $post['end'])));
        $fields['CodiceFacolta']=$row_classe['CodiceFacolta'];
        $fields['CodiceCorso']=$row_classe['CodiceCorso'];
        $fields['CodiceAnnoAccademico']=$row_classe['CodiceAnnoAccademico'];
        $fields['CodiceAnnoDiCorso']=$row_classe['CodiceAnnoDiCorso'];
        $fields['CodiceClasse']=$codice_classe;
        $fields['CodiceUtenteModifica']=  $this->get_userid();
        $fields['CodiceRegistroPresenze']=$codice_registropresenze;
        $codice_calendario=  $this->db_get_value('calendario_generale', "\"Codice\"", "\"CodiceRegistroPresenze\"=$codice_registropresenze");
        $codice_calendario=$this->insert('calendario_generale', $fields);
        
        
        
    }
    
    function set_esameBAK($post,$codice_registropresenze)
    {
        $codice_materia=$post['CodiceMateria'];
        $codice_docente=$post['CodiceDocente1'];
        $codice_classe=$post['classe'];
        $row_classe=  $this->db_get_row('classe', "*", "\"Codice\"=$codice_classe");

        //$fields['Codice']=  $this->generate_seriale('calendario_generale', 'Codice');
        $fields['CodiceTipoCalendario']=2;
        $fields['ArgomentoDescrizione']=$post['argomentodescrizione'];
        $fields['CodiceDipendente']="";
        $fields['Assente']=FALSE;
        $fields['Note']=$post['Note'];
        $fields['Giorno']=date('Y-m-d', strtotime($post['start']));
        $fields['OraInizio']='1899-12-30 '.date('H:i', strtotime(str_replace('GMT+0000', '', $post['start'])));
        $fields['OraFine']='1899-12-30 '.date('H:i', strtotime(str_replace('GMT+0000', '', $post['end'])));
        $fields['CodiceTipoRichiedente']="";
        $fields['CodiceRichiedente']="";
        $fields['Richiedente']="";
        $fields['CodiceTipoRicevente']="";
        $fields['CodiceRicevente']="";
        $fields['Luogo']="";
        $fields['CodiceFacolta']=$row_classe['CodiceFacolta'];
        $fields['CodiceCorso']=$row_classe['CodiceCorso'];
        $fields['CodiceAnnoAccademico']=$row_classe['CodiceAnnoAccademico'];
        $fields['CodiceAnnoDiCorso']=$row_classe['CodiceAnnoDiCorso'];
        $fields['CodiceClasse']=$codice_classe;
        $fields['GruppoLingua']=$post['GruppoLingua'];
        $fields['GruppoNazionalita']="";
        $fields['GruppoEducazioneFisica']="";
        $fields['CodiceSede']=$post['CodiceSede'];
        $fields['CodiceAula']=$post['CodiceAula'];
        $fields['CodiceMateria']=$codice_materia;
        $fields['AttivitaInLinguaStraniera']="";
        $fields['CodiceDocente1']=$codice_docente;
        $fields['CodiceDocente2']=$post['Docente2'];
        $fields['CodiceDocente3']="";
        $fields['CodiceTraduttore1']="";
        $fields['CodiceTraduttore2']="";
        $fields['CodiceAssistente1']="";
        $fields['CodiceAssistente2']="";
        $fields['NoteDipendenti']="";
        $fields['CodiceTipoModificaCalendario']="";
        $fields['DataModifica']="";
        $fields['MotivoModificaCalendario']="";
        $fields['CodiceUtenteModifica']=  $this->get_userid();
        $fields['CodiceRecordCollegato']="";
        $fields['Modificato']=FALSE;
        $fields['NumeroRevisione']="";
        $fields['CodiceRegistroPresenze']=$codice_registropresenze;
        $fields['EventoRiguardaClassi']=FALSE;
        $fields['TestIngresso_Valutativo']=FALSE;
        $fields['CodiceSemestre']="";
        $fields['SiglaUtenteAppuntamento']="";
        $fields['CodiceTipoRichiedenteEffettivo']="";
        $fields['CodiceRichiedenteEffettivo']="";
        $fields['RichiedenteEffettivo']="";
        $fields['CodiceClasseAppuntamento']="";
        $fields['CodiceTipoRiceventeEffettivo']="";
        $fields['CodiceRiceventeEffettivo']="";
        $fields['DataEffettivaAppuntamento']="";
        $fields['OraInizioEffettivaAppuntamento']="";
        $fields['OraFineEffettivaAppuntamento']="";
        $fields['MotivoEffettivoAppuntamento']="";
        $fields['LuogoEffettivoAppuntamento']="";
        $fields['Scan']=FALSE;
        $fields['AppuntamentoEffettivo']=FALSE;


        $this->insert('calendario_generale', $fields);
    }

    function set_evento($post,$fields)
    {
        $codice_classe=$post['CodiceClasse'];
        $row_classe=  $this->db_get_row('classe', "*", "\"Codice\"=$codice_classe");

        //$fields['Codice']=  $this->generate_seriale('calendario_generale', 'Codice');
        $fields['CodiceTipoCalendario']=3;
        $fields['Giorno']=date('Y-m-d', strtotime($post['start']));
        $fields['OraInizio']='1899-12-30 '.date('H:i', strtotime(str_replace('GMT+0000', '', $post['start'])));
        $fields['OraFine']='1899-12-30 '.date('H:i', strtotime(str_replace('GMT+0000', '', $post['end'])));
        $fields['CodiceFacolta']=$row_classe['CodiceFacolta'];
        $fields['CodiceCorso']=$row_classe['CodiceCorso'];
        $fields['CodiceAnnoAccademico']=$row_classe['CodiceAnnoAccademico'];
        $fields['CodiceAnnoDiCorso']=$row_classe['CodiceAnnoDiCorso'];
        $fields['CodiceClasse']=$codice_classe;

        $this->insert('calendario_generale', $fields);
    }

    function set_periodo($arg)
    {
        $codice_classe=$arg['CodiceClasse'];
        $row_classe=  $this->db_get_row('classe', "*", "\"Codice\"=$codice_classe");

        $fields['CodiceTipoCalendario']=$arg['CodiceTipoCalendario'];
        $giorno=$arg['Giorno'];
        $fields['Giorno']=$arg['Giorno'];
        $orainizio=$arg['OraInizio'];
        $fields['OraInizio']=$arg['OraInizio'];
        $fields['OraFine']=$arg['OraFine'];
        $orafine=$arg['OraFine'];
        $fields['CodiceClasse']=$codice_classe;
        
        $sql="
            DELETE FROM calendario_periodi_classi
            WHERE (\"Giorno\"='$giorno') AND (\"OraInizio\"='$orainizio') AND (\"OraFine\"='$orafine') AND (\"CodiceClasse\"=$codice_classe)
            ";
        $this->execute_query($sql);
        if($fields['CodiceTipoCalendario']!=0)
        {
            $this->insert('calendario_periodi_classi', $fields);
        }
    }


    /**
     *
     * @param type $codice_progettodidattica
     * @return type
     * @author Alessandro Galli
     */
    function get_progettodidattica($codice_progettodidattica)
    {
        $result=$this->db_get_row('progettodidattica', '*', "\"Codice\"='$codice_progettodidattica'");
        return $result;
    }

    function get_progettodidattica_lezione($codice_registropresenze)
    {
        $sql="
            SELECT progettodidattica_lezioni.*,registropresenze.\"Data\",to_char(\"OraPrevistaInizioLezione1\",'HH24:MI') as \"OraPrevistaInizioLezione1\",to_char(\"OraPrevistaFineLezione1\",'HH24:MI') as \"OraPrevistaFineLezione1\",registropresenze.\"CodiceMateria\",registropresenze.\"CodiceClasse\",registropresenze.\"CodiceAnnoAccademico\",\"DescrizioneITA\",\"Descrizione\",docente.\"Cognome\",docente.\"Nome\"
            FROM progettodidattica_lezioni
            JOIN registropresenze ON progettodidattica_lezioni.\"CodiceRegistroPresenze\"=registropresenze.\"Codice\"
            JOIN materia ON materia.\"Codice\"=registropresenze.\"CodiceMateria\"
            JOIN classe ON classe.\"Codice\"=registropresenze.\"CodiceClasse\"
            JOIN docente ON docente.\"Codice\"=registropresenze.\"CodiceDocente1\"
            WHERE \"CodiceRegistroPresenze\"=$codice_registropresenze
            ";
        $result=  $this->select($sql);
        if(count($result)>0)
        {
            $return=$result[0];
        }
        else
        {
            $return=null;
        }
        return $return;
    }

    /**
     *
     * @param type $codice_materia
     * @author Alessandro Galli
     * Ritorna gli argomenti ufficiali del programma di una certa materia
     */
    function get_argomenti_materia($codice_materia,$codice_annoaccademico)
    {
        $sql="
            SELECT *
            FROM programma_materia
            JOIN programma_materia_argomenti ON programma_materia.\"Codice\"=programma_materia_argomenti.\"CodiceProgrammaMateria\"
            WHERE programma_materia.\"CodiceMateria\"=$codice_materia AND programma_materia.\"CodiceAnnoAccademico\"=$codice_annoaccademico AND programma_materia_argomenti.\"Extra\"=false
            "
            ;

        $rows=  $this->select($sql);
        return $rows;
    }

    function get_argomenti_previsti_progettodidattica($codice_progettodidattica)
    {
        $codice_materia=  $this->db_get_value('progettodidattica',"\"CodiceMateria\"","\"Codice\"=$codice_progettodidattica");
        $codice_annoaccademico=  $this->db_get_value('progettodidattica',"\"CodiceAnnoAccademico\"","\"Codice\"=$codice_progettodidattica");
        $sql="
            SELECT programma_materia_argomenti.*, progettodidattica_lezioni_argomenti.\"Svolto\"
            FROM programma_materia
            JOIN programma_materia_argomenti ON programma_materia.\"Codice\"=programma_materia_argomenti.\"CodiceProgrammaMateria\"
            LEFT JOIN progettodidattica_lezioni_argomenti ON progettodidattica_lezioni_argomenti.\"CodiceProgrammaMateriaArgomento\"=programma_materia_argomenti.\"Codice\"
            WHERE programma_materia.\"CodiceMateria\"=$codice_materia AND programma_materia.\"CodiceAnnoAccademico\"=$codice_annoaccademico AND programma_materia_argomenti.\"Extra\"=false
            "
            ;

        $rows=  $this->select($sql);
        return $rows;
    }

    function get_argomenti_extra_progettodidattica($codice_progettodidattica)
    {
        $codice_materia=  $this->db_get_value('progettodidattica',"\"CodiceMateria\"","\"Codice\"=$codice_progettodidattica");
        $codice_annoaccademico=  $this->db_get_value('progettodidattica',"\"CodiceAnnoAccademico\"","\"Codice\"=$codice_progettodidattica");
        $sql="
            SELECT *
            FROM programma_materia
            JOIN programma_materia_argomenti ON programma_materia.\"Codice\"=programma_materia_argomenti.\"CodiceProgrammaMateria\"
            WHERE programma_materia.\"CodiceMateria\"=$codice_materia AND programma_materia.\"CodiceAnnoAccademico\"=$codice_annoaccademico AND programma_materia_argomenti.\"Extra\"=true
            ";
        $rows=  $this->select($sql);
        return $rows;
    }

    function get_argomenti_mancanti_progettodidattica($codice_progettodidattica)
    {
        $codice_materia=  $this->db_get_value('progettodidattica',"\"CodiceMateria\"","\"Codice\"=$codice_progettodidattica");
        $codice_annoaccademico=  $this->db_get_value('progettodidattica',"\"CodiceAnnoAccademico\"","\"Codice\"=$codice_progettodidattica");
        $sql="
            SELECT programma_materia_argomenti.*, progettodidattica_lezioni_argomenti.\"Svolto\"
            FROM programma_materia
            JOIN programma_materia_argomenti ON programma_materia.\"Codice\"=programma_materia_argomenti.\"CodiceProgrammaMateria\"
            LEFT JOIN progettodidattica_lezioni_argomenti ON progettodidattica_lezioni_argomenti.\"CodiceProgrammaMateriaArgomento\"=programma_materia_argomenti.\"Codice\"
            WHERE programma_materia.\"CodiceMateria\"=$codice_materia AND programma_materia.\"CodiceAnnoAccademico\"=$codice_annoaccademico AND programma_materia_argomenti.\"Extra\"=false AND \"Svolto\" is null
            "
            ;
        $rows=  $this->select($sql);
        return $rows;
    }

    /**
     *
     * @param type $codice_progettodidattica_lezione
     * @return type
     * @author Alessandro Galli
     * Ritorna gli argomenti fatti in una particolare lezione
     */
    function get_argomenti_lezione($codice_progettodidattica_lezione)
    {
        $sql="
            SELECT *
            FROM progettodidattica_lezioni_argomenti JOIN programma_materia_argomenti ON progettodidattica_lezioni_argomenti.\"CodiceProgrammaMateriaArgomento\"=programma_materia_argomenti.\"Codice\"
            WHERE \"CodiceProgettoDidatticaLezione\"=$codice_progettodidattica_lezione
            ";
        $rows=  $this->select($sql);
        return $rows;
    }

    /**
     *
     * @param type $codice_progettodidattica_lezione
     * $param type $post
     * @author Alessandro Galli
     * Salva gli argomenti trattati in una certa lezione
     */
    function set_argomenti_lezione($codice_progettodidattica_lezione,$post)
    {

    }

    /**
     *
     * @param type $codice_progettodidattica
     * @author Alessandro Galli
     * ritorna le lezioni di un particolare progettodidattica
     */
    function get_progettodidattica_lezioni($codice_progettodidattica)
    {
        /*$sql="
           SELECT progettodidattica_lezioni.\"CodiceRegistroPresenze\",to_char(\"Data\",'YYYY / MM / DD') as \"Data\",to_char(\"OraPrevistaInizioLezione1\",'HH24:MI') as \"Inizio\",to_char(\"OraPrevistaFineLezione1\",'HH24:MI') as \"Fine\",\"CodiceProgettoDidattica\"
            FROM progettodidattica_lezioni
            JOIN registropresenze ON progettodidattica_lezioni.\"CodiceRegistroPresenze\"=registropresenze.\"Codice\"
            JOIN materia ON materia.\"Codice\"=registropresenze.\"CodiceMateria\"
            JOIN classe ON classe.\"Codice\"=registropresenze.\"CodiceClasse\"
           WHERE \"CodiceProgettoDidattica\"=$codice_progettodidattica
        ";*/
        $sql="
           SELECT progettodidattica_lezioni.\"CodiceRegistroPresenze\",calendario_generale.\"Codice\" AS \"CodiceCalendarioGenerale\",to_char(\"Data\",'DD/MM/YYYY') as \"Data\",to_char(\"OraPrevistaInizioLezione1\",'HH24:MI') as \"Inizio\",to_char(\"OraPrevistaFineLezione1\",'HH24:MI') as \"Fine\",docente.\"Cognome\" as Docente1
            FROM progettodidattica_lezioni
            JOIN registropresenze ON progettodidattica_lezioni.\"CodiceRegistroPresenze\"=registropresenze.\"Codice\"
            JOIN calendario_generale ON progettodidattica_lezioni.\"CodiceRegistroPresenze\"=calendario_generale.\"CodiceRegistroPresenze\"
            JOIN materia ON materia.\"Codice\"=registropresenze.\"CodiceMateria\"
            JOIN classe ON classe.\"Codice\"=registropresenze.\"CodiceClasse\"
            JOIN docente ON docente.\"Codice\"=calendario_generale.\"CodiceDocente1\"
           WHERE \"CodiceProgettoDidattica\"=$codice_progettodidattica
           ORDER BY calendario_generale.\"Giorno\" DESC    
        ";
            

        $result=  $this->select($sql);
        return $result;
    }
    /*function get_options_docente()
    {
        $sql = "SELECT \"Codice\",(\"Cognome\" || ' ' || \"Nome\") AS \"Descrizione\" FROM docente";
        $data=$this->select($sql);
        return $data;
    }*/

    function get_options($table)
    {
        return $this->db_get($table);
    }

    function get_options_materia()
    {
        $sql="SELECT *,\"Codice\", \"DescrizioneENG\" as \"Descrizione\""
                . "FROM materia "
                . "ORDER BY \"Descrizione\" ASC";
        return $this->select($sql);
    }

    function get_options_livello()
    {
        $sql = "SELECT \"Codice\", \"Raggiunto\" AS \"Descrizione\" FROM Conclusione_Livello";
        return $this->select($sql);
    }

    function get_options_docente()
    {
        $sql="SELECT \"Codice\",(\"Cognome\" || ' ' || \"Nome\") AS \"Descrizione\" FROM docente order by \"Cognome\",\"Nome\"";
        return $this->select($sql);
    }

    function get_options_studente()
    {
        $sql="SELECT \"Codice\",(\"Cognome\" || ' ' || \"Nome\") AS \"Descrizione\" FROM studente order by \"Cognome\",\"Nome\"";
        return $this->select($sql);
    }
    
    function get_options_tipoesame()
    {
        $sql="SELECT \"Codice\",\"Descrizione\" FROM tipoesame order by \"Descrizione\"";
        return $this->select($sql);
    }

    public function get_domandeaperte_sessione($CodiceRegistroPresenza)
    {
        $sql="SELECT \"TestoDomanda1\",\"TestoDomanda2\",\"TestoDomanda3\",\"TestoDomanda4\",\"TestoDomanda5\",\"TestoDomanda6\",\"TestoDomanda7\",\"TestoDomanda8\"
              FROM DomandeAperte_Sessione
              WHERE CodiceRegistroPresenza=";
    }

    public function generate_seriale($table,$field){
        $seriale=0;
        $sql="SELECT MAX(\"$field\") as max FROM $table where \"$field\" > 0";
        $result=  $this->select($sql);
        if(count($result)>0)
        {
            $seriale=$result[0]['max'];
        }
        $new_seriale=$seriale+1;
        return $new_seriale;
    }


    public function salva_argomenti_lezione($codice_registropresenze,$argomenti_previsti,$argomenti_extra)
    {
        $userid=  $this->get_userid();
        $progettodidattica_lezione=  $this->db_get_row('progettodidattica_lezioni', '*', "\"CodiceRegistroPresenze\"=$codice_registropresenze");
        if($progettodidattica_lezione!=null)
        {
            $codice_progettodidattica_lezione=$progettodidattica_lezione['Codice'];
            $codice_progettodidattica=  $progettodidattica_lezione['CodiceProgettoDidattica'];
            $progetto_didattica=  $this->db_get_row('progettodidattica', '*',"\"Codice\"=$codice_progettodidattica");
            $codice_docente=  $this->db_get_value('progettodidattica_docente', "\"CodiceDocente\"", "\"CodiceProgettoDidattica\"=$codice_progettodidattica");
            $codice_materia=$progetto_didattica['CodiceMateria'];
            $codice_anno_accademico=$progetto_didattica['CodiceAnnoAccademico'];
            $codice_programma_materia=  $this->db_get_value('programma_materia', "\"Codice\"","\"CodiceMateria\"=$codice_materia AND \"CodiceAnnoAccademico\"=$codice_anno_accademico");
            if($codice_programma_materia==null)
            {
                //$codice_programma_materia=  $this->generate_seriale('programma_materia', 'Codice');
                $sql="INSERT INTO programma_materia (\"CodiceMateria\",\"CodiceAnnoAccademico\",\"RecordStatus\") VALUES($codice_materia,$codice_anno_accademico,'new')";
                $this->execute_query($sql);
                $codice_programma_materia=  $this->db_get_value("programma_materia", "\"Codice\"", 'true','',"ORDER BY \"Codice\" DESC");
            }
            $argomenti=array();
            foreach ($argomenti_previsti as $key => $argomento_previsto) {
                if($argomento_previsto!='')
                {
                    $argomento['value']=$argomento_previsto;
                    $argomento['type']='previsto';
                    $argomenti[]=$argomento;
                }
            }
            foreach ($argomenti_extra as $key => $argomento_extra) {
                if($argomento_extra!='')
                {
                    $argomento['value']=$argomento_extra;
                    $argomento['type']='extra';
                    $argomenti[]=$argomento;
                }

            }
            foreach ($argomenti as $key => $argomento) {
                if($argomento['type']=='extra')
                {
                    //$codice_programma_materia_argomento=  $this->generate_seriale('programma_materia_argomenti', 'Codice');
                    $testo_argomento=$argomento['value'];
                    $testo_argomento=  str_replace("'", "''", $testo_argomento);
                    $sql="INSERT INTO programma_materia_argomenti "
                            . "(\"CodiceProgrammaMateria\",\"NArgomento\",\"Argomento\",\"Extra\",\"CodiceDocente\",\"RecordStatus\") "
                            . "VALUES ($codice_programma_materia,1,'$testo_argomento',true,$codice_docente,'new')";
                    $this->execute_query($sql);
                    $codice_programma_materia_argomento=  $this->db_get_value("programma_materia_argomenti", "\"Codice\"", 'true','',"ORDER BY \"Codice\" DESC");

                }
                else
                {
                    $codice_programma_materia_argomento=$argomento['value'];
                }

                //$codice_progettodidattica_lezioni_argomento=  $this->generate_seriale('progettodidattica_lezioni_argomenti', 'Codice');
                $sql="INSERT INTO progettodidattica_lezioni_argomenti "
                        . "(\"CodiceProgettoDidattica\",\"CodiceProgettoDidatticaLezione\",\"NArgomento\",\"Svolto\",\"CodiceProgrammaMateriaArgomento\",\"RecordStatus\") "
                        . "VALUES ($codice_progettodidattica,$codice_progettodidattica_lezione,1,true,$codice_programma_materia_argomento,'new')";
                $this->execute_query($sql);
            }
        }


    }

    public function lezione_delete($CodiceCalendarioGenerale)
    {
        $CodiceRegistroPresenze=  $this->db_get_value('calendario_generale', "\"CodiceRegistroPresenze\"", "\"Codice\"=$CodiceCalendarioGenerale");
        $sql="
            DELETE FROM calendario_generale
            WHERE \"Codice\"=$CodiceCalendarioGenerale
            ";
        $this->execute_query($sql);
        $sql="
            DELETE FROM registropresenze
            WHERE \"Codice\"=$CodiceRegistroPresenze
            ";
        $this->execute_query($sql);
    }
    
    public function esame_delete($CodiceCalendarioGenerale)
    {
        $CodiceRegistroPresenze=  $this->db_get_value('calendario_generale', "\"CodiceRegistroPresenze\"", "\"Codice\"=$CodiceCalendarioGenerale");

        $sql="
            DELETE FROM calendario_generale
            WHERE \"Codice\"=$CodiceCalendarioGenerale
            ";
        $this->execute_query($sql);
        $sql="
            DELETE FROM registropresenze
            WHERE \"Codice\"=$CodiceRegistroPresenze
            ";
        $this->execute_query($sql);
    }
    
    public function evento_delete($CodiceCalendarioGenerale)
    {
        $sql="
            DELETE FROM calendario_generale
            WHERE \"Codice\"=$CodiceCalendarioGenerale
            ";
        $this->execute_query($sql);
    }
    
    public function get_classe($codice_classe)
    {
        return $this->db_get_row('classe', '*', "\"Codice\"=$codice_classe");
    }

    public function get_lezioni_from_classe_materia($codice_classe,$codice_materia)
    {
        $sql="
            SELECT to_char(\"OraInizio\",'HH24:MI') as \"OraInizio\",to_char(\"OraFine\",'HH24:MI') as \"OraFine\"
            FROM calendario_generale
            WHERE \"CodiceTipoCalendario\"=1 AND \"CodiceClasse\"=$codice_classe AND \"CodiceMateria\"=$codice_materia
            ";
        $return=  $this->select($sql);
        return $return;
    }
    
    public function get_oretotali_pianostudi_materie($codice_classe,$codice_materia)
    {
        $return=0;
        $CodicePianoStudi=  $this->db_get_value('classe', '"CodicePianoStudi"', "\"Codice\"=$codice_classe");
        if($this->isnotempty($CodicePianoStudi)&&($this->isnotempty($codice_materia)))
        {
            $sql="
            SELECT \"OreTotali\"
            FROM pianostudi_materie
            WHERE \"CodicePianoStudi\"=$CodicePianoStudi AND \"CodiceMateria\"=$codice_materia
            ";
            $result=  $this->select($sql);
            
            if(count($result)>0)
            {
                $return=$result[0]['OreTotali'];
            }
        }
       
        return $return; 
    }
    
    function date_diff($date1, $date2)
    {
        $date1timestamp=  strtotime($date1);
        $date2timestamp=  strtotime($date2);
        $all = round(($date2timestamp - $date1timestamp) / 60);
        $d = floor ($all / 1440);
        $h = floor (($all - $d * 1440) / 60);
         $m = $all - ($d * 1440) - ($h * 60);
        return array('d' => $d, 'h' => $h, 'm' => $m);
    }

    function update_calendario_generale_data_ora($arg)
    {
        $codice=$arg['Codice'];
        $giorno=date('Y-m-d', strtotime($arg['Giorno']));
        $orainizio='1899-12-30 '.date('H:i', strtotime(str_replace('GMT+0000', '', $arg['OraInizio'])));
        $orafine='1899-12-30 '.date('H:i', strtotime(str_replace('GMT+0000', '', $arg['OraFine'])));
        $sql="
            UPDATE calendario_generale
            SET \"Giorno\"='$giorno',\"OraInizio\"='$orainizio',\"OraFine\"='$orafine'
            WHERE \"Codice\"=$codice
            ";
        $this->execute_query($sql);
    }
    
    function reset_registropresenze_docente($codice_registropresenze)
    {
        $sql="
            DELETE FROM registropresenze_docente
            WHERE \"CodiceRegistroPresenze\"=$codice_registropresenze
            ";
        $this->execute_query($sql);
    }
    
    function insert_registropresenze_docente($codice_registropresenze,$codice_docente)
    {
        $sql="
            INSERT INTO registropresenze_docente
            (\"CodiceRegistroPresenze\",\"CodiceDocente\")
            VALUES
            ($codice_registropresenze,$codice_docente)
            ";
        $this->execute_query($sql);
    }
    function update_registropresenze_data_ora($arg)
    {
        $codice=$arg['CodiceRegistroPresenze'];
        $giorno=date('Y-m-d', strtotime($arg['Giorno']));
        $orainizio='1899-12-30 '.date('H:i', strtotime(str_replace('GMT+0000', '', $arg['OraInizio'])));
        $orafine='1899-12-30 '.date('H:i', strtotime(str_replace('GMT+0000', '', $arg['OraFine'])));
        $sql="
            UPDATE registropresenze
            SET \"Data\"='$giorno',\"OraPrevistaInizioLezione1\"='$orainizio',\"OraPrevistaFineLezione1\"='$orafine',\"OraInizioLezione1\"='$orainizio',\"OraFineLezione1\"='$orafine'
            WHERE \"Codice\"=$codice
            ";
        $this->execute_query($sql);
    }
    
    function update_registropresenze($fields,$codice_registropresenze)
    {
        $this->update('registropresenze', $fields,"\"Codice\"=$codice_registropresenze");
    }

    function get_percentuale_assenza_generale($codice_studente)
    {
        return $this->db_get_value('calcolo_percentuale_assenza', "\"PercentualeAssenza\"", "\"CodiceStudente\"=$codice_studente");
    }

    function get_percentuale_assenza_materia($codice_studente,$codice_materia)
    {
        return $this->db_get_value('calcolo_percentuale_assenza_permateria', "\"PercentualeAssenza\"", "\"CodiceStudente\"=$codice_studente AND \"CodiceMateria\"=$codice_materia");
    }

    function get_ammesso($codice_studente,$codice_materia,$codice_classe)
    {
        $studente_materia_disapprovato=  $this->db_get_row('studente_materia_disapprovato', '*', "\"CodiceStudente\"=$codice_studente AND \"CodiceMateria\"=$codice_materia AND \"CodiceClasse\"=$codice_classe");
        if($studente_materia_disapprovato==null)
        {
            return true;
        }
        else
        {
            if($studente_materia_disapprovato['Approvato'])
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }
        
    }
    
    function registra_voti_esameorale($CodiceRegistroPresenze,$studenti)
    {
        $userid=  $this->get_userid();
        $registro_presenze=$this->db_get_row('registropresenze', "*,to_char(\"Data\",'DD.MM.YYYY') as \"Data\"", "\"Codice\"=$CodiceRegistroPresenze");
        $CodiceMateria=$registro_presenze['CodiceMateria'];
        $CodiceAnnoDiCorso=$registro_presenze['CodiceAnnoDiCorso'];
        $data=$registro_presenze['Data'];
        $data_convertita=$data;
        //$data_convertita=  date('d.m.Y', strtotime($data));
        //$cls_date = new DateTime($Data);
        //$data_convertita= $cls_date->format('d.m.Y'); 
        $EsameFinaleNonPrevisto='false';
        $EsameFinaleData='null';
        $EsameSpecificoNonPrevisto='true';
        $EsameSpecificoData='null';
        
        foreach ($studenti as $CodiceStudente => $studente) {
            $note='';
            if($studente['note']!='')
                $note=$studente['note'];
            if(($studente['voto']!='')&&(!array_key_exists('bocciato', $studente))&&(!array_key_exists('assente', $studente)))
            {
                $EsameFinaleVoto='null';
                $EsameFinaleVoto=$studente['voto']; 
                $sql="
                    INSERT INTO studente_esami
                    (\"RecordStatus\",\"RecordCreatoDa\",\"CodiceRegistroPresenze\",\"CodiceStudente\",\"CodiceMateria\",\"CodiceAnnoDiCorso\",\"Note\",\"EsameFinaleData\",\"EsameFinaleVoto\")
                    VALUES
                    ('new',$userid,$CodiceRegistroPresenze,$CodiceStudente,$CodiceMateria,$CodiceAnnoDiCorso,'$note','$data_convertita',$EsameFinaleVoto)
                    ";
                $this->execute_query($sql);
            }
            if((array_key_exists('bocciato', $studente))||(array_key_exists('assente', $studente)))
            {
                $EsameBocciato1Voto='null';
                if(array_key_exists('bocciato', $studente))
                {
                    $EsameBocciato1Voto='1';
                }
                if(array_key_exists('assente', $studente))
                {
                    $EsameBocciato1Voto='A';
                }
                $EsameFinaleVoto=$studente['voto']; 
                $sql="
                    INSERT INTO studente_esami
                    (\"RecordStatus\",\"RecordCreatoDa\",\"CodiceRegistroPresenze\",\"CodiceStudente\",\"CodiceMateria\",\"CodiceAnnoDiCorso\",\"Note\",\"EsameBocciato1Data\",\"EsameBocciato1Voto\")
                    VALUES
                    ('new',$userid,$CodiceRegistroPresenze,$CodiceStudente,$CodiceMateria,$CodiceAnnoDiCorso,'$note','$data_convertita','$EsameBocciato1Voto')
                    ";
                $this->execute_query($sql);
            }
            
        }
        
    }
}

?>
