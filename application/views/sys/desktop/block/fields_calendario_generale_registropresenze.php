<?php
$docenti_counter=count($docenti);
if($docenti_counter==0)
{
    $docenti_counter=1;
}
?>
<script type="text/javascript" src="<?php echo base_url('/assets/js/materialize.min.js') ?>"></script>
<script type="text/javascript">
    $('#block_calendari').ready(function(){
        $('#fields_calendario_generale_registropresenza').find('select').not(".initialized").material_select();
            
    });
</script>

<div id="docente_hidden" class="row" style="display: none" data-cloned_counter="<?=$docenti_counter?>">
    <div class="col s12">
        <?=  generate_select('select_docente_hidden', 'Docente',$options_docenti, null,'edit','initialized')?>
    </div>
</div>

<div id="fields_calendario_generale_registropresenza" class="fields_calendario_generale_registropresenza">
    <input type="hidden" name="calendario_generale[CodiceTipoCalendario]" value="<?=$tipocalendario?>">
    <?php
    if($tipocalendario==2)
    {
    ?>
    <div class="row">
        <div class="col s12">
        <?=  generate_select('registropresenze[CodiceTipoEsame]', 'Tipo esame',$options_tipoesame, element('CodiceTipoEsame', $registropresenze),$mode)?>
        </div>
    </div>
    <?php
    }
    ?>
    <!--DOCENTE-->
    <?php
    foreach ($docenti as $key => $docente) {
    ?>
    <div class="row">
        <div class="col s12">
            <?php  
            $docente_counter=$key+1;
            echo generate_select("docenti[$docente_counter]", "Docente $docente_counter",$options_docenti, $docente['CodiceDocente'],$mode);
            ?>
        </div>
    </div>
    <?php
    }
    ?>
    <?php
    if(count($docenti)==0)
    {
    ?>
    <div class="row">
        <div class="col s12">
            
            <script type="text/javascript">
                $('#docenti_1_').change(function(){
                    docente_changed(this)
                })
            </script>
            <?=  generate_select('docenti[1]', 'Docente',$options_docenti, null,$mode)?>
        </div>
    </div>
    <?php
    }
    ?>
    <?php
    if($mode=='edit')
    {
    ?>
    <a class="btn-floating green" onclick="add_docente(this)">
        <i class="material-icons">add</i>
    </a>
    <?php
    }
    ?>
    <!--MATERIA-->
    <?php
    if(($tipocalendario==2)||($tipocalendario==1))
    {
    ?>
    <div class="row">
        <div class="col s12">
            <script type="text/javascript">
                $('#calendario_generale_CodiceMateria_').change(function(){
                    materia_changed(this)
                })
            </script>
            <?php
            $id="calendario_generale_CodiceMateria_";
            $name="calendario_generale[CodiceMateria]";
            $value=  element('CodiceMateria', $calendario_generale);
            if(($value==null)||($value==false))
            {
                $value='';
            }
            if($mode=='view')
            {
                $disabled='disabled';
            }
            else
            {
                $disabled='';
            }
            if(($mode!='view')||(($mode=='view')&&($value!='')))
            {
            ?>    
                <div class="input-field">
                    <select <?=$disabled?> id="<?=$id?>" name="<?=$name?>">
                        <option value=""></option>
                        <?php  
                        foreach ($options_materie as $key => $option) 
                        {
                            $selected='';
                            if($option['Codice']==$value)
                            {
                                $selected='selected';
                            }
                        ?>
                            <option value="<?=$option['Codice']?>" data-sigla="<?=$option['Sigla']?>" data-descrizioneita="<?=$option['DescrizioneITA']?>" data-descrizioneeng="<?=$option['DescrizioneENG']?>" <?=$selected?>><?=$option['Descrizione']." - ".$option['Sigla']?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <label for="<?=$id?>" >Materia</label>
                </div>
            <?php
            }
            ?>
            <!--<div class="input-field">
                <select  id="calendario_generale_CodiceMateria_" name="calendario_generale[CodiceMateria]">
                    <option value=""></option>
                    <?php  
                    foreach ($options_materie as $key => $materia) 
                    {
                    ?>
                        <option  value="<?=$materia['Codice']?>" data-sigla="<?=$materia['Sigla']?>" data-descrizioneita="<?=$materia['DescrizioneITA']?>" data-descrizioneeng="<?=$materia['DescrizioneENG']?>"><?=$materia['DescrizioneITA']." ".$materia['Sigla']?></option>
                    <?php
                    }
                    ?>
                </select>
                <label for="calendario_generale_CodiceMateria_" >Materia</label>
            </div>-->
        </div>
    </div>
    <?php
    }
    ?>
    <!--SEDE-->
    <div class="row">
        <div class="col s12">
        <?=  generate_select('calendario_generale[CodiceSede]', 'Sede',$options_sedi, element('CodiceSede', $calendario_generale),$mode)?>
        </div>
    </div>
    <!--AULA-->
    <div class="row">
        <div class="col s12">
        <?=  generate_select('calendario_generale[CodiceAula]', 'Aula',$options_aule, element('CodiceAula', $calendario_generale),$mode)?>
        </div>
    </div>
    <!--GRUPPO LINGUA-->
    <?php
    if(($tipocalendario==2)||($tipocalendario==1))
    {
    ?>
    <div class="row">
        <div class="col s12">
        <?php generate_select('calendario_generale[GruppoLingua]', 'Gruppo per inglese',$options_gruppi_lingua, element('GruppoLingua', $calendario_generale),$mode)?>
        </div>
    </div>
    <?php
    }
    ?>
    <?php
    if($tipocalendario==3)
    {
    ?>
    <!--LUOGO-->
    <div class="row">
        <div class="col s12">
        <?=  generate_textinput('calendario_generale[Luogo]', 'Luogo', element('Luogo', $calendario_generale),$mode)?>
        </div>
    </div>
    <?php
    }
    ?>
    <!--NOTE-->
    <div class="row">
        <script type="text/javascript">
            $('#calendario_generale_Note_').change(function(){
                change_testo_timbro(this)
            })
        </script>
        <div class="col s12">
            Note <textarea id="calendario_generale_Note_" name="calendario_generale[Note]"><?=element('Note', $calendario_generale)?></textarea>
            <!--<?= generate_textarea('calendario_generale[Note]', 'Note',element('Note', $calendario_generale),$mode)?>-->
        </div>
    </div>
    <!--ARGOMENTO DESCRIZIONE-->
    <input type="hidden" name="calendario_generale[ArgomentoDescrizione]" id="calendario_generale_ArgomentoDescrizione_">

</div>