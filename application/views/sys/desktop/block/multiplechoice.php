<?php
?>
<script type="text/javascript">
    $(document).ready(function(){
        $('ul.tabs').tabs();
         
        $('.datepicker').pickadate({
          selectMonths: true, // Creates a dropdown to control month
          selectYears: 15 // Creates a dropdown of 15 years to control year
        });
        
        $('select').not(".initialized").material_select();
         setTimeout(function(){
             $('.tabs').find('.active').click();
         },500);
    });
</script>
<div id="multiplechoice_<?= $Codice; ?>" class="block multiplechoice" style="position: relative;">
    <form id="form_save" style="display: none">
    </form>
    <!-- IMPLEMENTO IL BOTTONE -->
    
        <?php
        if($mode=='view')
        {
        ?>
            <div class="fixed-action-btn" title="duplica" style="top: -10px; right: 108px;position: absolute">
                <a class="btn-floating orange" onclick="multiplechoice_duplica(this,<?= $Codice; ?>);">
                      <i class="large material-icons">content_copy</i>
                </a>
            </div>
            <div class="fixed-action-btn" title="Modifica" style="top: -10px; right: 10px;right: 65px;position: absolute">
                <a class="btn-floating red" onclick="multiplechoice_edit(this,<?= $Codice; ?>);">
                      <i class="large material-icons">edit</i>
                </a>
            </div>
            <div class="fixed-action-btn" title="Elimina"  style="top: -10px; position: absolute">
                <a class="btn-floating red" onclick="multiplechoice_delete(this,<?= $Codice; ?>);">
                      <i class="large material-icons">delete</i>
                </a>
            </div>
        <?php
        }
        ?>
        <?php
        if($mode=='edit')
        {
        ?>
            <div class="fixed-action-btn" style="top: -10px; right: 10px;position: absolute">
                    <a class="btn-floating green" onclick="multiplechoice_save(this,<?= $Codice; ?>);">
                      <i class="large material-icons">save</i>
                    </a>
            </div>
        <?php
        }
        ?>
    
    <!-- IMPOSTO LE VARIE TAB -->
    <div id="tabs" class="row">
        <div class="col s12" style="margin-bottom: 20px;" class="block progettodidattica" style="position: relative">
            <ul class="tabs">
              <!--<li class="tab col s6"><a class="active" href="#EsamiConmultiplechoice">Esami con Domande Aperte</a></li>-->
              <?php
              if($Codice=='null')
              {
                  $codice_formulario='Nuovo';
              }
              else
              {
                  $codice_formulario=$Codice;
              }
              ?>
              <li class="tab col s6"><a class="active" href="#multiplechoice">MultipleChoice Formulario <?=$codice_formulario?></a></li>
            </ul>
        </div>
        
        
        <div id="multiplechoice" class="col s12" style="overflow-y: scroll;height: 90%">
            <div class="row">
                <div class="col s12">
                    <?= generate_select('CodiceRegistroPresenze', 'Esami del Docente', $options['CodiceRegistroPresenze'], $multiplechoice['CodiceRegistroPresenze'], $mode); ?>
                </div>
            </div>
            <?php
            if($Codice!='null')
            {
            ?>
            <div class="row">
                <div class="col s12">
                    <div class="container block_container">
                        <?=$block['multiplechoice_domande_elenco']?>
                    </div>
                </div>
            </div>
            <?php
            }
            ?>
            <div class="row">
                <div class="col s12">
                    <?=  generate_textarea('NoteEsame', 'Note esame', $multiplechoice["NoteEsame"], $mode)?>
                </div>
            </div>
        </div>
    </div>
</div>