<div id="block_multiplechoice_elencodomande" class="block" style="width: 100%;position: relative" data-codice="<?=$codice_sessione?>">
    <div class="title">
        Domande del formulario
    </div>
    <div class="fixed-action-btn" style="top: -10px; right: 10px;position: absolute">
        <a class="btn-floating red" onclick="multiplechoice_domanda_insert(this,'<?=$codice_sessione?>');">
              <i class="large material-icons">add</i>
        </a>
    </div>
    <?php
        if(count($recordSource)>0)
        {
        ?>
        <table id='tabellaRecord' class="bordered hoverable" style="">
            <thead>
                <tr>
                    <?php 
                    foreach($recordSource[0] as $key => $value)
                    { 
                        if($key!='Codice')
                        {
                        ?>
                            <th><div style="height: 50px;line-height: 50px;overflow: hidden"><?= $key; ?></div></th>
                        <?php 
                        }
                    } 
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php 
                foreach($recordSource as $key => $record) 
                { 
                   
                ?>
                    <tr onclick='multiplechoice_domanda_open_popup(this,<?=$codice_sessione?>,<?= $record['Codice']; ?>);'>
                        <?php foreach($record as $chiave=>$valore)
                        { 
                            if($chiave!='Codice')
                            {
                            ?>
                                <td style="height: 50px;">
                                    <div style="max-height: 100px;line-height: 25px;overflow: hidden;max-width: 100%;"><?= $valore;?></div>
                                </td>
                            <?php 
                            }
                        } ?>
                    </tr>
                    <?php 
                } ?>
            </tbody>
        </table>
        <?php
        }
        else
        {
        ?>
        <div class="row">
            <div class="col s12">
                Non risultano esserci ancora domande per questo formulario multiplechoice
            </div>
        </div>
        
        <?php
        }
        ?>
</div>