<?php
?>
<script type="text/javascript" src="<?php echo base_url('/assets/js/materialize.min.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('ul.tabs').tabs();
         
        $('.datepicker').pickadate({
          selectMonths: true, // Creates a dropdown to control month
          selectYears: 15 // Creates a dropdown of 15 years to control year
        });
        
        $('select').not(".initialized").material_select();
         setTimeout(function(){
             $('.tabs').find('.active').click();
         },500);
    });
</script>
<div id="domandeaperte_<?= $Codice; ?>" class="block domandeaperte" style="position: relative;">
    <form id="form_save" style="display: none">
    </form>
    <!-- IMPLEMENTO IL BOTTONE -->
    
        <?php
        if($mode=='view')
        {
        ?>
            <div class="fixed-action-btn" title="duplica" style="top: -10px; right: 108px;position: absolute">
                <a class="btn-floating orange" onclick="domandeaperte_duplica(this,<?= $Codice; ?>);">
                      <i class="large material-icons">content_copy</i>
                </a>
            </div>
            <div class="fixed-action-btn" title="Modifica" style="top: -10px; right: 10px;right: 65px;position: absolute">
                <a class="btn-floating red" onclick="domandeaperte_edit(this,<?= $Codice; ?>);">
                      <i class="large material-icons">edit</i>
                </a>
            </div>
            <div class="fixed-action-btn" title="Elimina"  style="top: -10px; position: absolute">
                <a class="btn-floating red" onclick="domandeaperte_delete(this,<?= $Codice; ?>);">
                      <i class="large material-icons">delete</i>
                </a>
            </div>
        <?php
        }
        ?>
        <?php
        if($mode=='edit')
        {
        ?>
            <div class="fixed-action-btn" style="top: -10px; right: 10px;position: absolute">
                <a class="btn-floating green" title="Salva" onclick="domandeaperte_save(this,<?= $Codice; ?>);">
                      <i class="large material-icons">save</i>
                    </a>
            </div>
        <?php
        }
        ?>
    
    <!-- IMPOSTO LE VARIE TAB -->
    <div id="tabs" class="row">
        <div class="col s12" style="margin-bottom: 20px;" class="block progettodidattica" style="position: relative">
            <ul class="tabs">
              <!--<li class="tab col s6"><a class="active" href="#EsamiConDomandeAperte">Esami con Domande Aperte</a></li>-->
              <?php
              if($Codice=='null')
              {
                  $codice_formulario='Nuovo';
              }
              else
              {
                  $codice_formulario=$Codice;
              }
              ?>
              <li class="tab col s6"><a class="active" href="#DomandeAperte">Domande Aperte Formulario <?=$codice_formulario?></a></li>
            </ul>
        </div>
        
        
        <div id="DomandeAperte" class="col s12" style="overflow-y: scroll;height: 90%">
            <div class="row">
                <div class="col s12">
                    <?= generate_select('CodiceRegistroPresenze', 'Esami del Docente', $options['CodiceRegistroPresenze'], $domandeaperte['CodiceRegistroPresenze'], $mode); ?>
                </div>
            </div>    
                
            <?php for($i=0;$i<8;$i++)
            {
                $attuale=$i + 1;
            ?>
                <div class="row">
                    <div class="col s12"><?php echo generate_textarea("TestoDomanda".$attuale, "Testo Domanda ".$attuale, $domandeaperte["TestoDomanda".$attuale], $mode); ?></div>
                </div>
            <?php 
            } 
            ?>
            
            <div class="row">
                <div class="col s12">
                    <?= generate_textinput('NumeroPagineBianche', 'Numero pagine bianche', $domandeaperte["NumeroPagineBianche"], $mode)?>
                </div>
            </div>
            
            <div class="row">
                <div class="col s12">
                    <?=  generate_textarea('TestoLibero1', 'Testo libero 1', $domandeaperte["TestoLibero1"], $mode)?>
                </div>
            </div>
            
            <div class="row">
                <div class="col s12">
                    <?=  generate_textarea('TestoLibero2', 'Testo libero 2', $domandeaperte["TestoLibero2"], $mode)?>
                </div>
            </div>
            
            <div class="row">
                <div class="col s12">
                    <?=  generate_textarea('NoteEsame', 'Note esame', $domandeaperte["NoteEsame"], $mode)?>
                </div>
            </div>
            
        </div>
        

        
    </div>
</div>