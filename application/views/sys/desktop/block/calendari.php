
<?php
$calendari=$data['calendari'];
?>
<link rel="stylesheet" href="<?php echo base_url("/assets/fullcalendar/fullcalendar.css") ?>" />
<script type="text/javascript" src="<?php echo base_url('/assets/js/moment.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/jquery.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/materialize.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/jquery-ui.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/fullcalendar/fullcalendar.js') ?>"></script>

<script type="text/javascript">
$(document).ready(function(){
    
    <?php
    $counter=0;
    foreach ($calendari as $key => $classe) {
    ?>  
            ajax_load_block_calendario_classe($('#calendari_<?=$key?>_container'),'<?=$key?>');
            $('#calendari_<?=$key?>_container').data('loaded','true');
                        //$('#calendari_<?=$key?>').trigger('runFullCalendar'); 
                                       
            
    <?php
       /* $counter++;
        if($counter>1)
        {
            break;
        }*/
    }
    ?>
            
    });    
$('#content_calendario').scroll(function(){
    $('.title').css({
        'margin-left': 5 + $(this).scrollLeft()
    });
    $('.pianificazione').css({
        'margin-left': $(this).scrollLeft()
    });
    var pTop = $('#content_calendario').scrollTop();
    //console.info(pTop);
    <?php
    
    foreach ($calendari as $key => $classe) {
    ?>
            //console.info("calendario <?=$key?>:"+$('#calendari_<?=$key?>').position().top);
            if(pTop>$('#calendari_<?=$key?>_container').offset().top)
            {
                if(!$('#calendari_<?=$key?>_container').data('loaded'))
                {
                    console.info(pTop);
                    $('#calendari_<?=$key?>_container').data('loaded','true');
                    ajax_load_block_calendario_classe($('#calendari_<?=$key?>_container'),'<?=$key?>');
                }
            }
    <?php
    }
     
    ?>
});

</script>

<div id="block_calendari" class="block" style="width: 100%;height: calc(100% - 50px);overflow: scroll;">    
    <?php
    foreach ($calendari as $key => $classe) {
    ?>
    
    <div id="block_<?=$key?>" class="classe blocco card" style="width: 6000px;;margin-bottom: 20px;"  >
            <!--<button onclick="ajax_load_block_calendario_classe(this,'<?=$key?>')">test</button>-->
            <div id="calendari_<?=$key?>_container" class="container" style="height: 600px;width: 6000px;float: left;" data-loaded="false">
             <?php
                //echo $classe['block'];
                ?>
            </div>
            <div class="clearboth"></div>
        </div>
    <?php
    }
    ?>
    </div>

</div>
