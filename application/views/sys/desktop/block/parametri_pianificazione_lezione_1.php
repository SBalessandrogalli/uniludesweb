<script type="text/javascript" src="<?php echo base_url('/assets/js/materialize.min.js') ?>"></script>
<script type="text/javascript">
    $('#block_calendari').ready(function(){
        $('#parametri_pianificazione_1').find('select').not(".initialized").material_select();
        $('.draggable').draggable({
                revert: true,      // immediately snap back to original position
                revertDuration: 0  //
            });
            
        change_testo_timbro();
    });
</script>
<div id="docente_hidden" class="row" style="display: none" data-cloned_counter="2">
    <div class="col s12">
        <?=  generate_select('select_docente_hidden', 'Docente',$options_docenti, null,'edit')?>
    </div>
</div>
<div id="parametri_pianificazione_1" class="parametri_pianificazione">
                            <div class="title">
                                Dati Lezione
                            </div>
                            <br/>
                            
                            
                            <div class="row">
                                <div class="col s12">
                                    <script type="text/javascript">
                                        $('#dati_lezione_CodiceDocente__0_').change(function(){
                                            docente_changed(this)
                                        })
                                    </script>
                                    <?=  generate_select('dati_lezione[CodiceDocente][1]', 'Docente',$options_docenti, null,'edit')?>
                                </div>
                            </div>
                            <a class="btn-floating green" onclick="add_docente(this)">
                                <i class="material-icons">add</i>
                            </a>
                            <div class="row">
                                <div class="col s12">
                                    <script type="text/javascript">
                                        $('#dati_lezione_CodiceMateria_').change(function(){
                                            materia_changed(this)
                                        })
                                    </script>
                                    <div class="input-field">
                                        <select  id="dati_lezione_CodiceMateria_" name="dati_lezione[CodiceMateria]">
                                            <option value=""></option>
                                            <?php  
                                            foreach ($options_materie as $key => $materia) 
                                            {
                                            ?>
                                                <option  value="<?=$materia['Codice']?>" data-sigla="<?=$materia['Sigla']?>" data-descrizioneita="<?=$materia['DescrizioneITA']?>" data-descrizioneeng="<?=$materia['DescrizioneENG']?>"><?=$materia['DescrizioneITA']." ".$materia['Sigla']?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                        <label for="dati_lezione_CodiceMateria_" >Materia</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12">
                                <?=  generate_select('dati_lezione[CodiceSede]', 'Sede',$options_sedi, null,'edit')?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12">
                                <?=  generate_select('dati_lezione[CodiceAula]', 'Aula',$options_aule, null,'edit')?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12">
                                <?php
                                $gruppi_lingua[]=array("Codice"=>"0","Descrizione"=>"0");
                                $gruppi_lingua[]=array("Codice"=>"1A","Descrizione"=>"1A");
                                $gruppi_lingua[]=array("Codice"=>"1B","Descrizione"=>"1B");
                                $gruppi_lingua[]=array("Codice"=>"2","Descrizione"=>"2");
                                generate_select('dati_lezione[GruppoLingua]', 'Gruppo per inglese',$gruppi_lingua, null,'edit')
                                ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12">
                                <?= generate_textarea('dati_lezione[Note]', 'Note',null,'edit')?>
                                </div>
                            </div>
                            <input type="hidden" name="ArgomentoDescrizione" id="ArgomentoDescrizione">
<div class="row" id='div_timbro'>
                            <div class="col s4">
                                <script type="text/javascript">
                                $('#durata').change(function(){
                                    change_testo_timbro();
                                })
                                </script>
                                <?php  
                                for($x=1;$x<8;$x=$x+0.5)
                                {
                                    $durata['Codice']=$x;
                                    $durata['Descrizione']=$x;
                                    $durate[]=$durata;
                                }
                                generate_select('durata', 'Durata', $durate, 3.5, 'edit');
                                ?>
                            </div>
                            <div class="col s8">
                                <div id="timbro_evento" class="draggable" style="width: 80%;min-height: 100px;background-color: #477EB6;color:white;border-radius: 4px;text-align: center;line-height: 25px;cursor: pointer;margin: auto;" >

                                </div>
                            </div>
                        </div>
</div>