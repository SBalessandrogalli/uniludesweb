<div id="block_domandeaperte_elenco" class="block ">
    <?php
        if(count($recordSource)>0)
        {
        ?>
        <table id='tabellaRecord' class="bordered hoverable" style="">
            <thead>
                <tr>
                    <?php 
                    foreach($recordSource[0] as $key => $value)
                    { 
                        if($key!='Codice')
                        {
                        ?>
                            <th><div style="height: 50px;line-height: 50px;overflow: hidden"><?= $key; ?></div></th>
                        <?php 
                        }
                    } 
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php 
                foreach($recordSource as $key => $record) 
                { 
                   
                ?>
                    <tr onclick='domandeaperte_open(this,<?= $record['Codice']; ?>);'>
                        <?php foreach($record as $chiave=>$valore)
                        { 
                            if($chiave!='Codice')
                            {
                            ?>
                                <td style="height: 50px;">
                                    <div style="max-height: 100px;line-height: 25px;overflow: hidden;max-width: 250px;"><?= $valore;?></div>
                                </td>
                            <?php 
                            }
                        } ?>
                    </tr>
                    <?php 
                } ?>
            </tbody>
        </table>
        <?php
        }
        else
        {
        ?>
        <div class="row">
            <div class="col s12">
                Non risultano esserci formulari di domande aperte
            </div>
        </div>
        
        <?php
        }
        ?>
</div>