<?php
$docenti_counter=count($docenti);
if($docenti_counter==0)
{
    $docenti_counter=1;
}
?>
<script type="text/javascript" src="<?php echo base_url('/assets/js/materialize.min.js') ?>"></script>
<script type="text/javascript">
    $('#block_calendari').ready(function(){
        $('#fields_calendario_generale_registropresenza').find('select').not(".initialized").material_select();
            $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 16 // Creates a dropdown of 15 years to control year
          });
    });
</script>

<div id="docente_hidden" class="row" style="display: none" data-cloned_counter="<?=$docenti_counter?>">
    <div class="col s12">
        <?=  generate_select('select_docente_hidden', 'Docente',$options_docenti, null,'edit','initialized')?>
    </div>
</div>

<div id="fields_calendario_generale_registropresenza" class="fields_calendario_generale_registropresenza">
    <input type="hidden" name="calendario_generale[CodiceTipoCalendario]" value="4">

    <div class="row">
        
    <div class="col s3">
        <div class="input-field">
            <input name="data" id="data" type="date" class="datepicker">
         </div>
    </div>
    <div class="col s3">
        <div class="input-field">
            <select id="ora" name="ora" id="ora" style="margin-top: 5px;">
                <option value="08">08</option>
                <option value="09">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
            </select>
            <label for="ora">Ora</label>
         </div>
    </div>
    <div class="col s3">
        <div class="input-field">
            <select id="minuti" name="minuti" id="minuti" style="margin-top: 5px;">
                <option value="00">00</option>
                <option value="01">01</option>
                <option value="02">02</option>
                <option value="03">03</option>
                <option value="04">04</option>
                <option value="05">05</option>
                <option value="06">06</option>
                <option value="07">07</option>
                <option value="08">08</option>
                <option value="09">09</option>
                <option value="10">10</option>
                <?php
                for($x=11;$x<61;$x++)
                {
                    echo "<option value='$x'>$x</option>";
                }
                ?>
            </select>
            <label for="minuti"> Minuti </label>
         </div>
    </div>

</div>

    <!--TIPO RICHIEDENTE-->
    <div class="row">
        <div class="col s12">
            <?=  generate_select('calendario_generale[CodiceTipoRichiedente]', 'Tipo richiedente',$options_tiporichiedente, null,$mode)?>
        </div>
    </div>
    
    <!--RICHIEDENTE-->
    <div class="row">
        <div class="col s12">
        <?=  generate_textinput('calendario_generale[Richiedente]', 'Richiedente', null,$mode)?>
        </div>
    </div>
    
    <!--CODICE RICHIEDENTE - STUDENTE-->
    <div class="row">
        <div class="col s12">
            <?=  generate_select('calendario_generale[CodiceRichiedente]', 'Studente',$options_studenti, null,$mode)?>
        </div>
    </div>
    
    <!--TIPO RICEVENTE-->
    <div class="row">
        <div class="col s12">
            <?=  generate_select('calendario_generale[CodiceTipoRicevente]', 'Tipo ricevente',$options_tiporicevente, null,$mode)?>
        </div>
    </div>
    
    <!--DOCENTE-->
    <div class="row">
        <div class="col s12">
            <?=  generate_select('calendario_generale[CodiceRicevente]', 'Ricevente',$options_docenti, null,$mode)?>
        </div>
    </div>


    

   
    <!--NOTE-->
    <div class="row">
        <script type="text/javascript">
            $('#calendario_generale_Note_').change(function(){
                change_testo_timbro(this)
            })
        </script>
        <div class="col s12">
            Note <textarea id="calendario_generale_Note_" name="calendario_generale[Note]"><?=element('Note', $calendario_generale)?></textarea>
            <!--<?= generate_textarea('calendario_generale[Note]', 'Note',element('Note', $calendario_generale),$mode)?>-->
        </div>
    </div>
    
    <!--LUOGO-->
    <div class="row">
        <div class="col s12">
        <?=  generate_textinput('calendario_generale[Luogo]', 'Luogo', element('Luogo', $calendario_generale),$mode)?>
        </div>
    </div>

</div>