<?php
$materie=$data['materie'];
$materia_selected=$data['materia_selected'];
?>
<script type="text/javascript">
    $('#block_calendari').ready(function(){
        $('select').not(".initialized").material_select();
    });
</script>
<div id="select_materie_block" class="block input-field" style="width: 100%;">
    <select id='materia' name="materia" onchange="materia_changed(this)" style="width: 100%;">
        <option></option>
        <?php
        foreach ($materie as $key => $materia) {
            $selected="";
            if($materia['Codice']==$materia_selected)
            {
                $selected="selected";
            }
        ?>
        <option <?=$selected?> value="<?=$materia['Codice']?>" data-sigla="<?=$materia['Sigla']?>" data-descrizioneita="<?=$materia['DescrizioneITA']?>" data-descrizioneeng="<?=$materia['DescrizioneENG']?>"><?=$materia['DescrizioneITA']?></option>
        <?php
        }
        ?>
    </select>
    <label for="materia">Materia</label>
</div>    
    
