<?php
?>
<script type="text/javascript" src="<?php echo base_url('/assets/js/materialize.min.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
         $('ul.tabs').tabs();
         $('select').not(".initialized").material_select();
         setTimeout(function(){
             $('.tabs').find('.active').click();
         },500);
         $( ".connectedSortable" ).sortable({
                      connectWith: ".connectedSortable",
                      update: function( event, ui ) {
                         
                      }
                    }).disableSelection();
    });
</script>
<div id="block_esame" class="block" style="background-color: white;height: 100%;width: 100%;overflow-y: scroll;">
    <form id="form_save" style="display: none">
    </form>
    <div class="fixed-action-btn" style="top: -10px; right: 10px;position: absolute">
        <?php
        if($mode=='view')
        {
        ?>
        <div class="fixed-action-btn" title="Modifica" style="top: 0px; right: 10px;right: 65px;position: absolute">
            <a class="btn-floating red" onclick="esame_edit(this,'<?=$codice_calendario_generale?>')">
                  <i class="large material-icons">edit</i>
            </a>
        </div>
        <div class="fixed-action-btn" title="Elimina"  style="top: 0px; position: absolute">
            <a class="btn-floating red" onclick="esame_delete(this,<?= $codice_calendario_generale; ?>);">
                  <i class="large material-icons">delete</i>
            </a>
        </div>
        <?php
        }
        ?>
        <?php
        if($mode=='edit')
        {
        ?>
            <a class="btn-floating green" onclick="esame_save(this,'<?=$codice_calendario_generale?>')">
              <i class="large material-icons">save</i>
            </a>
        <?php
        }
        ?>
    </div>
    <div class="row header_lezione">
        
        
        
    </div>
    <div id="tabs" class="row " style="margin-bottom: 10px;">
        <div class="col s12 card" style="margin-top: 0px;">
            <ul class="tabs">
              <li class="tab col s3"><a class="active" href="#dati_esame">Dati esame</a></li>  
            </ul>
        </div>
        <div id="dati_esame" class="col s12" style="">
            <div class="row">
                <div class="col s12">
                    <?=  generate_select('dati_esame[CodiceDocente1]', 'Docente 1',$options_docenti, $calendario_generale['CodiceDocente1'],'edit')?>
                </div>
            </div>
            <div class="row">
            <div class="col s12">
                <?=  generate_select('dati_esame[CodiceDocente2]', 'Docente 2',$options_docenti, $calendario_generale['CodiceDocente2'],'edit')?>
            </div>
            </div>
            
            <div class="row">
                <div class="col s12">
                <?=  generate_select('dati_lezione[CodiceMateria]', 'Materia',$options_materie, $calendario_generale['CodiceMateria'],$mode)?>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                <?=  generate_select('dati_esame[CodiceSede]', 'Sede',$options_sedi, $calendario_generale['CodiceSede'],'edit')?>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                <?=  generate_select('dati_esame[CodiceAula]', 'Aula',$options_aule, $calendario_generale['CodiceAula'],'edit')?>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                <?php
                $gruppi_lingua[]=array("Codice"=>"0","Descrizione"=>"0");
                $gruppi_lingua[]=array("Codice"=>"1A","Descrizione"=>"1A");
                $gruppi_lingua[]=array("Codice"=>"1B","Descrizione"=>"1B");
                $gruppi_lingua[]=array("Codice"=>"2","Descrizione"=>"2");
                generate_select('dati_esame[GruppoLingua]', 'Gruppo per inglese',$gruppi_lingua, $calendario_generale['GruppoLingua'],'edit')
                ?>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                <?= generate_textarea('dati_esame[Note]', 'Note',$calendario_generale['Note'],'edit')?>
                </div>
            </div>
        </div>
        
        
    </div>
</div>    
    

