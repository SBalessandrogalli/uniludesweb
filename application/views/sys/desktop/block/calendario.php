<?php
$meseCorrente = "06";
$idClasse=$data['codice_classe'];
$minTime='08:00:00';
$maxTime='20:00:00';

$PrimoGiornoMese = "$anno-$mese-01";
$classe=$data['classe'];

$calendari=$data['calendari'];
?>
<script type="text/javascript" src="<?php echo base_url('/assets/fullcalendar/lang-all.js') ?>"></script>
<script>
    
	function runFullCalendar(el) {
                <?php
                for ($x = 1; $x <= 5; $x++) 
                {
                 $num_days=($x-1)*7;   
                ?>
                $('#cal_week<?=$x?>_<?=$idClasse?>').show();
                $('#cal_week<?=$x?>_<?=$idClasse?>').fullCalendar({
                    year: 2010,
                    month: 4,
                    editable:true,
                    droppable: true,
                    header: false,
                    allDaySlot: false,
                    defaultDate: '<?php echo date("Y-m-d",strtotime($PrimoGiornoMese. " + $num_days days")); ?>',
                    defaultView: 'agendaWeek',
                    lang: 'it',
                    axisFormat: 'HH:mm',
                    minTime:'<?php echo $minTime; ?>',
                    maxTime:'<?php echo $maxTime; ?>',
                    events:
                    [
                        <?php foreach($calendari as $calendario){
                            $nomecalendario=$calendario['nomecalendario'];
                            $coloreEvento = $calendario['coloreEventi'];
                            foreach($calendario['eventi'] as $evento){ ?>
                            {
                                title: '<?php echo str_replace(array("'", "\n", "\t", "\r"), " ", $evento['titolo']); ?>',
                                start: '<?php echo $evento['data'].' '.$evento['orainizio']; ?>',
                                end: '<?php echo $evento['data'].' '.$evento['orafine']; ?>',
                                backgroundColor: '<?php echo $coloreEvento; ?>',
                                editable:true,
                                <?php
                                if($calendario['tiporender']=='background')
                                {
                                ?>
                                rendering: 'background',
                                <?php
                                }
                                ?>
                                Codice: '<?php echo $evento['codice']?>',
                                CodiceTipoCalendario: '<?php echo $evento['CodiceTipoCalendario']?>',
                                CodiceRegistroPresenze: '<?php echo $evento['CodiceRegistroPresenze']?>',
                            },
                            <?php } ?>
                        <?php } ?>
                    ],
                eventReceive: function(event)
                    {
                        //console.info(event)
                        //var startDate=event.start;
                        //var NuovoOraInizio=startDate.format("HH:mm:ss");
                        ajax_set_evento(this['el'],event);
                        
                    },
                eventClick: function(calEvent, jsEvent, view)
                    {
                        //set_sede(this,calEvent.Codice);
                        if(calEvent.CodiceTipoCalendario==1)
                        {
                            lezione_open_popup(this,calEvent.Codice);
                        }
                        if(calEvent.CodiceTipoCalendario==2)
                        {
                            esame_open_popup(this,calEvent.Codice);
                        }
                        if(calEvent.CodiceTipoCalendario==3)
                        {
                            evento_open_popup(this,calEvent.Codice);
                        }
                    },
                eventDrop: function(event,delta,revertFunc)
                {
                    ajax_change_evento(this['el'],event,delta,revertFunc);
                },
                eventResize:function(event,delta,revertFunc)
                {
                    ajax_change_evento(this['el'],event,delta,revertFunc);
                }
                    
		});
                <?php
                }
                ?>
	}
        $('#calendari_<?=$idClasse?>').bind('runFullCalendar', runFullCalendar);
        
        $('#calendari_<?=$idClasse?>').ready(function(){
            runFullCalendar();
            $('.draggable').draggable({
                revert: true,      // immediately snap back to original position
                revertDuration: 0  //
            });
            var titolo=$('#timbro_evento').html();
            change_testo_timbro();
            //$('#timbro_evento').data('event', {title:titolo,duration:"03:00"});
        })
        
</script>
<!--<button onclick="$(this).next().show();">Test</button>-->

<div id="calendari_<?=$idClasse?>" class="calendario" style="width: 6000px;height: 100%; border: 0px; padding: 0px; margin: 0px;border-top: 1px solid #bcbcbc;" data-idclasse="<?=$idClasse?>">
    
    
    
    <!-- NEL SECONDO DIV MOSTRO VERAMENTE I CALENDARI -->
    <div class="calendario_classe" style="height:100%; margin: 0px;position: absolute;z-index: 0;  ">
        <div style="width: 5000px;height: 200px;">
            <div id="week1_<?=$idClasse?>" style="width:900px;float: left; margin-right:10px;">
                <div style="float: left"><?=$classe['CognomePrimoStudente']." ".$classe['Descrizione']?></div>
                <div style="float: right;margin-right: 20px;"><?=" Ore materia: $totale_ore_assegnate / $totale_ore_previste"?></div>
                <div id='cal_week1_<?=$idClasse?>' style="width: 100%;"></div>
            </div>    
            <div id="week2_<?=$idClasse?>" style="width:900px;float: left; margin-right:10px;">
                <div style="float: left"><?=$classe['CognomePrimoStudente']." ".$classe['Descrizione']?></div>
                <div style="float: right;margin-right: 20px;"><?=" Ore materia: $totale_ore_assegnate / $totale_ore_previste"?></div>
                <div id='cal_week2_<?=$idClasse?>' style="width: 100%;"></div>
            </div>
            <div id="week3_<?=$idClasse?>" style="width:900px;float: left; margin-right:10px;">
                <div style="float: left"><?=$classe['CognomePrimoStudente']." ".$classe['Descrizione']?></div>
                <div style="float: right;margin-right: 20px;"><?=" Ore materia: $totale_ore_assegnate / $totale_ore_previste"?></div>
                <div id='cal_week3_<?=$idClasse?>' style="width: 100%;"></div>
            </div>
            <div id="week4_<?=$idClasse?>" style="width:900px;float: left; margin-right:10px;">
                <div style="float: left"><?=$classe['CognomePrimoStudente']." ".$classe['Descrizione']?></div>
                <div style="float: right;margin-right: 20px;"><?=" Ore materia: $totale_ore_assegnate / $totale_ore_previste"?></div>
                <div id='cal_week4_<?=$idClasse?>' style="width: 100%;"></div>
            </div>
            <div id="week5_<?=$idClasse?>" style="width:900px;float: left; margin-right:10px;">
                <div style="float: left"><?=$classe['CognomePrimoStudente']." ".$classe['Descrizione']?></div>
                <div style="float: right;margin-right: 20px;"><?=" Ore materia: $totale_ore_assegnate / $totale_ore_previste"?></div>
                <div id='cal_week5_<?=$idClasse?>' style="width: 100%;"></div>
            </div>
            <div class="clearboth"></div>
        </div>
    </div>
    <div class="clearboth"></div>
</div>