<?php
$mode=$data['mode'];
$argomenti_lezione=$data['argomenti_lezione'];
$argomenti_mancanti_progettodidattica=$data['argomenti_mancanti_progettodidattica'];
?>
<script type="text/javascript" src="<?php echo base_url('/assets/js/materialize.min.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
         $('ul.tabs').tabs();
         $('select').not(".initialized").material_select();
         
         setTimeout(function(){
             $('.tabs').find('.active').click();
         },500);
         $( ".connectedSortable" ).sortable({
                      connectWith: ".connectedSortable",
                      update: function( event, ui ) {
                         
                      }
                    }).disableSelection();
    });
</script>
<div id="block_lezione" class="block" style="background-color: white;height: 100%;width: 100%;overflow-y: scroll">
    <form id="form_save" style="display: none">
    </form>
    <div class="fixed-action-btn" style="top: -10px; right: 10px;position: absolute">
        <?php
        if($mode=='view')
        {
        ?>
        <div class="fixed-action-btn" title="Modifica" style="top: 0px; right: 10px;right: 65px;position: absolute">
            <a class="btn-floating red" onclick="lezione_edit(this,'<?=$codice_calendario_generale?>')">
                  <i class="large material-icons">edit</i>
            </a>
        </div>
        <div class="fixed-action-btn" title="Elimina"  style="top: 0px; position: absolute">
            <a class="btn-floating red" onclick="lezione_delete(this,<?= $codice_calendario_generale; ?>);">
                  <i class="large material-icons">delete</i>
            </a>
        </div>
        <?php
        }
        ?>
        <?php
        if($mode=='edit')
        {
        ?>
            <a class="btn-floating green" onclick="lezione_save(this,'<?=$codice_calendario_generale?>')">
              <i class="large material-icons">save</i>
            </a>
        <?php
        }
        ?>

    </div>
    <div class="row header_lezione">
        <div class="col s4">
            <div class="row">
                <div class="col s12">
                    <label>Codice reg presenze:</label> <div style="display: inline-block"><?=$codice_registropresenze?></div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <label>Codice cal generale:</label> <div style="display: inline-block"><?=$codice_calendario_generale?></div>
                </div>
            </div>
        </div>
        <div class="col s4">
            <div class="row">
                <div class="col s12">
                    <label>Data:</label> <div style="display: inline-block"><?=date('d/m/Y',  strtotime(str_replace('/', '-', $registropresenze['Data'])))?></div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <label>Ora inzio:</label> <div style="display: inline-block"><?=date('H:i',  strtotime($registropresenze['OraPrevistaInizioLezione1']))?></div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <label>Ora fine:</label> <?=date('H:i',  strtotime($registropresenze['OraPrevistaFineLezione1']))?>
                </div>
            </div>
        </div>
        
    </div>
    <div id="tabs" class="row " style="margin-bottom: 10px;">
        <div class="col s12 card" style="margin-top: 0px;">
            <ul class="tabs">
              <li class="tab col s3"><a class="" href="#dati_lezione">Dati lezione</a></li>  
              <li class="tab col s3"><a class="" href="#argomenti">Argomenti</a></li>
              <li class="tab col s3"><a href="#presenze">Presenze</a></li>
            </ul>
        </div>
        <div id="dati_lezione" class="col s12" style="">
            <?=$fields_calendario_generale_registropresenze?>
        </div>
        <div id="argomenti" class="col s12" style="">
            <div class="row">
                <?php
                if($mode=='view')
                {
                ?>
                <div class="col s6 offset-s3">
                    <h5>Argomenti lezione</h5>
                    <?php
                    foreach ($argomenti_lezione as $key => $argomento_lezione) 
                    {
                    ?>
                        <div class="card badge argomento_assegnato ">
                        <?=$argomento_lezione['Argomento']?>
                        </div>
                    <?php
                    }
                    ?>
                </div>
                <?php
                }
                ?>
                <?php
                if($mode=='edit')
                {
                ?>
                <div class="col s6" style="">
                    <h5>Argomenti da assegnare</h5>
                    <div class="connectedSortable scroll-y" style="height: 50%;">
                        <?php
                        foreach ($argomenti_mancanti_progettodidattica as $key => $argomento_materia) 
                        {
                        ?>
                            <div class="card badge argomento_da_assegnare ">
                                <div class="fixed-action-btn" style="top: -12px; right: 5px;position: absolute;">
                                    <a class="btn-floating red add_argomento" onclick="add_argomento_previsto(this)" style="display: none;">
                                        <i class="large material-icons">send</i>
                                    </a>
                                    <div class="remove_argomento" style="display: none;">
                                        <a class="btn-floating black" onclick="remove_argomento_previsto(this)" style="display: none;">
                                            <i class="large material-icons">clear</i>
                                        </a>
                                    </div>
                                </div>
                                <div class="testo_argomento_previsto">
                                    <?=$argomento_materia['Argomento']?>
                                </div>
                                <input type="hidden" name="argomento_previsto[]" value="<?=$argomento_materia['Codice']?>">
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="argomento_da_assegnare">
                        <div style="width: calc(100% - 50px);float: left">
                            Argomento extra:<textarea id="argomento_extra" name="argomento_extra"></textarea>
                            <!--<?= generate_textarea('argomento_extra', 'Argomento extra:',null,null)?>-->
                        </div>
                        <div style="float: left;margin-top: 40px;">
                            <a class="btn-floating red btn_add_argomento_extra" onclick="add_argomento_extra(this)">
                                <i class="large material-icons">send</i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col s6" style="">
                    <h5>Argomenti trattati</h5>
                    <div id="lista_argomenti_lezione" class="connectedSortable" style="height: 100%;">
                        <?php
                        foreach ($argomenti_lezione as $key => $argomento_lezione) 
                        {
                        ?>
                            <div class="card badge argomento_assegnato ">
                                <div class="testo_argomento_lezione">
                                    <?=$argomento_lezione['Argomento']?>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                        <div id="argomento_extra_hidden" class="card badge argomento_assegnato " style="display: none;">
                                <div class="fixed-action-btn" style="top: -12px; right: 5px;position: absolute;">
                                    <a class="btn-floating black" onclick="remove_argomento_extra(this)" style="display: none;">
                                        <i class="large material-icons">clear</i>
                                    </a>
                                </div>
                                <div class="testo_argomento_lezione">
                                </div>
                            <input type="hidden" name="argomento_extra[]" value="">
                        </div>
                        <input type="hidden" name="argomento_previsto[]" value="">
                    </div>
                </div>
                <?php
                }
                ?>
            </div>
        </div>
        <div id="presenze" class="col s12" style="">
            <?php
            foreach($studenti as $key => $value){
                $colore1="";
                $colore2="";
                if($value['CodiceTipoAssenza']==1)
                {        
                    $opacity=0.2;
                    $colore1='#dddddd';
                    $colore2='#dddddd';
                }
                if($value['CodiceTipoAssenza']==3)
                {                           
                    $colore1='#80FF80';
                    $colore2='#80FF80';
                    $opacity=1.0;
                }
                if($value['CodiceTipoAssenza']==2)
                {                           
                    $colore1='#FFB266';
                    $colore2='#80FF80';
                    $opacity=1.0;
                }
                if($value['CodiceTipoAssenza']==5)
                {                           
                    $colore1='#80FF80';
                    $colore2='#FFB266';
                    $opacity=1.0;
                }
                    
        ?>
        <div style="border: 1px solid grey; margin-left: 5px; margin-right: 0px; margin-top: 5px; margin-bottom: 0px; padding:0px; width: 100px; height: 160px; float: left;">
            <div style="width: 100%; height: 70%; "><img src="<?= $value['Path_Foto']; ?>" style="opacity: <?=$opacity; ?>; margin: auto; height: 100%; width: 100%; display: block;" /></div>
            <div style=" height: 30%; width: 100%; overflow: hidden;font-size: 14px;position: relative">
                <div style="position: absolute;top: 0px;left: 0px;height: 100%;width: 100%;padding: 2px;">
                    <b><i><span style="font-size: 12px;"><?=$value['Cognome']?></span></i></b><br/>
                    <b><i><?=$value['Nome']; ?></i></b>
                </div>
                <div style="width: 50%;height: 100%;background-color: <?=$colore1;?>;float: left">
                    
                </div>
                <div style="width: 50%;height: 100%;background-color: <?=$colore2;?>;float: right">
                    
                </div>
            </div>
        </div>
        <?php } ?>
        </div>
    </div>
</div>    
    