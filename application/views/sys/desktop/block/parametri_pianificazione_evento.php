<script type="text/javascript" src="<?php echo base_url('/assets/js/materialize.min.js') ?>"></script>
<script type="text/javascript">
    $('#block_calendari').ready(function(){
        $('#durata').material_select();
        $('.draggable').draggable({
                revert: true      // immediately snap back to original position
            });
        change_testo_timbro();
    });
</script>
<div id="parametri_pianificazione_evento" class="parametri_pianificazione">
    <div class="title">
        Dati Evento
    </div>
    <br/>
    <div id="block_fields_calendario_generale_registropresenza_container" class="container" style="height:calc(100% - 50px);overflow-y: scroll;width: 100%;" data-contesto='pianificazione' >
        <?=$fields_calendario_generale_registropresenza?>
    </div>  
    <div class="row" id='div_timbro'>
                            <div class="col s4">
                                <script type="text/javascript">
                                $('#durata').change(function(){
                                    change_testo_timbro();
                                })
                                </script>
                                <?php  
                                for($x=1;$x<8;$x=$x+0.5)
                                {
                                    $durata['Codice']=$x;
                                    $durata['Descrizione']=$x;
                                    $durate[]=$durata;
                                }
                                generate_select('durata', 'Durata', $durate, 3.5, 'edit');
                                ?>
                            </div>
                            <div class="col s8">
                                <div id="timbro_evento" class="draggable" style="width: 80%;min-height: 100px;background-color: green;color:white;border-radius: 4px;text-align: center;line-height: 25px;cursor: pointer;margin: auto;" >

                                </div>
                            </div>
                        </div>
</div>