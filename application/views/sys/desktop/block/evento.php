<?php
?>
<script type="text/javascript" src="<?php echo base_url('/assets/js/materialize.min.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
         $('ul.tabs').tabs();
         $('select').not(".initialized").material_select();
         setTimeout(function(){
             $('.tabs').find('.active').click();
         },500);
         $( ".connectedSortable" ).sortable({
                      connectWith: ".connectedSortable",
                      update: function( event, ui ) {
                         
                      }
                    }).disableSelection();
    });
</script>
<div id="block_lezione" class="block" style="background-color: white;height: 100%;width: 100%">
    <form id="form_save" style="display: none">
    </form>
    <div class="fixed-action-btn" style="top: -10px; right: 10px;position: absolute">
        <?php
        if($mode=='view')
        {
        ?>
        <div class="fixed-action-btn" title="Modifica" style="top: 0px; right: 10px;right: 65px;position: absolute">
            <a class="btn-floating red" onclick="evento_edit(this,'<?=$codice_calendario_generale?>')">
                  <i class="large material-icons">edit</i>
            </a>
        </div>
        <div class="fixed-action-btn" title="Elimina"  style="top: 0px; position: absolute">
            <a class="btn-floating red" onclick="evento_delete(this,<?= $codice_calendario_generale; ?>);">
                  <i class="large material-icons">delete</i>
            </a>
        </div>
        <?php
        }
        ?>
        <?php
        if($mode=='edit')
        {
        ?>
            <a class="btn-floating green" onclick="evento_save(this,'<?=$codice_calendario_generale?>')">
              <i class="large material-icons">save</i>
            </a>
        <?php
        }
        ?>
    </div>
    <div class="row header_lezione">
        
        
        
    </div>
    <div id="tabs" class="row " style="margin-bottom: 10px;">
        <div class="col s12 card" style="margin-top: 0px;">
            <ul class="tabs">
              <li class="tab col s3"><a class="active" href="#dati_evento">Dati evento</a></li>  
            </ul>
        </div>
        <div id="dati_evento" class="col s12" style="">
            <?=$fields_calendario_generale_registropresenze?>
        </div>
        
        
    </div>
</div>    
    

