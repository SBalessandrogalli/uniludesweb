<?php
$settore=$data['settore'];
$recordid_azienda=$data['recordid_azienda'];
$recordid_progetto=$data['recordid_progetto'];
$segnalatore=$data['segnalatore']
?>
<div id='segnalazioni' style="margin-top: 10%;" >
    <div style="width: 50%;margin: auto;">
        <h1 style="color: #467bbd">Segnalazione</h1>
        <br/><br/>
        <form id='form_segnalazione'>
            <input type="hidden" name="settore" value="<?=$settore?>">
            <input type="hidden" name="recordid_azienda" value="<?=$recordid_azienda?>">
            <input type="hidden" name="recordid_progetto" value="<?=$recordid_progetto?>">
            <input type="hidden" name="segnalatore" value="<?=$segnalatore?>">
            <label for="tipo">Tipo segnalazione</label>
            <select name='tipo' id='tipo'>
                <option value="modifica">Modifica</option>
                <option value="errore">Errore</option>
                <option value="nuovafunzionalita">Nuova funzionalità</option>
            </select>
            <br/><br/>
            <label for="priorita">Priorità</label>
            <select name='priorita' id='priorita'>
                <option value="bassa">Bassa</option>
                <option value="media">Media</option>
                <option value="alta">Alta</option>
            </select>
            <br/><br/>
            <textarea name='testo' id='testo'></textarea>
            <br/>
            <br/>
            <div class="btn_scritta" onclick="invia_segnalazione(this)">Invia segnalazione</div>
        </form>
    </div>
</div>
