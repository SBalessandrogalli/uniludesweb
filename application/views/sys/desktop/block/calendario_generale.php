<script type="text/javascript" src="<?php echo base_url('/assets/js/moment.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/jquery.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/jquery-ui.js') ?>"></script>
<?php

?>
<script type="text/javascript" src="<?php echo base_url('/assets/fullcalendar/fullcalendar.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/materialize.min.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('select').not(".initialized").material_select();
                $('#calendario_docente').fullCalendar({
                    defaultDate: '<?=$default_date?>',
                    eventLimit: true, // for all non-agenda views
                    views: {
                        month: {
                            eventLimit: 8
                        }
                    },
                    events: function( start, end, timezone, callback ) {
                        var year=end.year();
                        var month=end.month();
                        var url  = controller_url+'/get_eventi_calendario_generale/' + year + '/' + month;
                            $.ajax({
                                url: url,
                                dataType: 'json',
                                type: 'POST',
                                data: $('#form_filtri_calendari').serialize(),
                                success: function( response ) {
                                    console.info(response);
                                    callback(response);
                                }
                            })
                    },
                    dayRender: function (date, cell) {

                    },
                    eventClick: function(calEvent, jsEvent, view)
                    {
                        if(calEvent.CodiceTipoCalendario==1)
                        {
                            lezione_open_popup(this,calEvent.codice);
                        }
                        else
                        {
                            alert(calEvent.title);
                        }
                    },
                    dayClick: function(date, jsEvent, view) {
                        //alert('Clicked on: ' + date.format());
                        //$('#calendario_docente').fullCalendar( 'refetchEvents' );
                        
                        
                    }
                    
                    
		});

        

    });
    
</script>
<div id='calendario_docente' style="width:100%;"></div>