<div id="block_multiplechoice_domanda" class="block card" style="position: relative">
    <div class="title">
    </div>
    <div class="fixed-action-btn" style="top: -10px; right: 10px;position: absolute">
        <?php
        if($mode=='view')
        {
        ?>
            <a class="btn-floating red" onclick="multiplechoice_domanda_edit(this,<?=$CodiceSessioneInserimento?>,'<?=$codice_domanda?>')">
              <i class="large material-icons">edit</i>
            </a>
            <a class="btn-floating red" onclick="multiplechoice_domanda_delete(this,<?= $codice_domanda; ?>);">
                  <i class="large material-icons">delete</i>
            </a>
        <?php
        }
        ?>
        <?php
        if($mode=='edit')
        {
        ?>
            <a class="btn-floating green" onclick="multiplechoice_domanda_save(this,<?=$CodiceSessioneInserimento?>,'<?=$codice_domanda?>')">
              <i class="large material-icons">save</i>
            </a>
        <?php
        }
        ?>
    </div>
    <form id="form_multiplechoice_domanda">
        <input type="hidden" id="CodiceSessione" name="CodiceSessioneInserimento" value="<?=$CodiceSessioneInserimento?>">
        <div class="row">
            <div class="col s12">
                <?=  generate_textarea('TestoDomanda', 'Domanda',$multiplechoice_domanda['TestoDomanda'],$mode)?>
            </div>
        </div>

        <div class="row">
            <?php
            if(($multiplechoice_domanda['Risposta1']!='')||($mode=='edit'))
            {
            ?>
                <div class="col s10">
                    <?=  generate_textarea('Risposta1', 'Risposta 1',$multiplechoice_domanda['Risposta1'],$mode)?>
                </div>
                <div class="col s2">
                    <?=  generate_checkbox("Esatta1", '',$multiplechoice_domanda['Esatta1'],$mode)?>
                </div>
            <?php
            }
            ?>
        </div>
        <div class="row">
            <?php
            if(($multiplechoice_domanda['Risposta2']!='')||($mode=='edit'))
            {
            ?>
            <div class="col s10">
                <?=  generate_textarea('Risposta2', 'Risposta 2',$multiplechoice_domanda['Risposta2'],$mode)?>
            </div>
            <div class="col s2">
                <br/>
                <?=  generate_checkbox("Esatta2", '',$multiplechoice_domanda['Esatta2'],$mode)?>
            </div>
            <?php
            }
            ?>
        </div>
        <div class="row">
            <?php
            if(($multiplechoice_domanda['Risposta3']!='')||($mode=='edit'))
            {
            ?>
            <div class="col s10">
                <?=  generate_textarea('Risposta3', 'Risposta 3',$multiplechoice_domanda['Risposta3'],$mode)?>
            </div>
            <div class="col s2">
                <br/>
                <?=  generate_checkbox("Esatta3", '',$multiplechoice_domanda['Esatta3'],$mode)?>
            </div>
            <?php
            }
            ?>
        </div>
        <div class="row">
            <?php
            if(($multiplechoice_domanda['Risposta4']!='')||($mode=='edit'))
            {
            ?>
            <div class="col s10">
                <?=  generate_textarea('Risposta4', 'Risposta 4',$multiplechoice_domanda['Risposta4'],$mode)?>
            </div>
            <div class="col s2">
                <br/>
                <?=  generate_checkbox("Esatta4", '',$multiplechoice_domanda['Esatta4'],$mode)?>
            </div>
            <?php
            }
            ?>
        </div>
    </form>
</div>