<?php
$docenti=$data['docenti'];
$docente_selected=$data['docente_selected'];
?>
<script type="text/javascript">
    $('#block_calendari').ready(function(){
        $('select').not(".initialized").material_select();
    });
</script>
<div id="select_docenti_block" class="block input-field" style="width: 100%;">
    <select id='docente' name="docente" onchange="docente_changed(this)" style="width: 100%;">
        <option></option>
        <?php
        foreach ($docenti as $key => $docente) {
            $selected="";
            if($docente['Codice']==$docente_selected)
            {
                $selected="selected";
            }
        ?>
        <option <?=$selected?> value="<?=$docente['Codice']?>"><?=$docente['Cognome']." ".$docente['Nome']?></option>
        <?php
        }
        ?>

    </select>
    <label for="docente">Docente</label>
</div>    
    
