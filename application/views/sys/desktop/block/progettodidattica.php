<?php
$mode=$data['mode'];
$codice_progettodidattica=$data['codice_progettodidattica'];
$progettodidattica=$data['progettodidattica'];
$progettodidattica_lezioni=$data['progettodidattica_lezioni'];
$argomenti_previsti=$data['argomenti_previsti'];
$argomenti_extra=$data['argomenti_extra'];
$options=$data['options'];
$studenti=$data['studenti'];
?>
<script type="text/javascript">
    $(document).ready(function(){
         $('ul.tabs').tabs();
         $('select').not(".initialized").material_select();
         setTimeout(function(){
             $('.tabs').find('.active').click();
         },500);
         $('.datepicker').pickadate({
            selectMonths: true,
            selectYears: 15 
          });
    });
</script>
<div id="progettodidattica_<?=$codice_progettodidattica?>" class="block progettodidattica" style="position: relative">
    <form id="form_save" style="display: none">
    </form>
    <div class="fixed-action-btn" style="top: -10px; right: 10px;position: absolute">
        <?php
        if($mode=='view')
        {
        ?>
        <a class="btn-floating red" onclick="progettodidattica_edit(this,'<?=$codice_progettodidattica?>')">
              <i class="large material-icons">edit</i>
            </a>
        <?php
        }
        ?>
        <?php
        if($mode=='edit')
        {
        ?>
            <a class="btn-floating green" onclick="progettodidattica_save(this,'<?=$codice_progettodidattica?>')">
              <i class="large material-icons">save</i>
            </a>
        <?php
        }
        ?>
    </div>
    <div id="tabs" class="row ">
        <div class="col s12 card" style="margin-bottom: 0px;margin-top: 0px;">
            <ul class="tabs">
              <li class="tab col s3"><a class="active" href="#informazioni">Informazioni</a></li>
              <li class="tab col s3"><a href="#lezioni">Lezioni</a></li>
              <li class="tab col s3"><a href="#programma">Programma</a></li>
              <li class="tab col s3"><a href="#conclusione">Conclusione</a></li>
              <li class="tab col s3"><a href="#ammissioni">Ammissioni</a></li>
            </ul>
        </div>
        <!-- TAB INFORMAZIONI -->
        <div id="informazioni" style="padding-top: 80px;">
            <div class="row">
                <div class="col s6">
                    <!--TEMP non viene visualizzato questo valore perchè non c'era nell'array del progettodidattica. devo recuperarlo esplicitamente nel controller-->
                    <?=generate_select('CodiceFacolta','Facoltà',$options['CodiceFacolta'],null,'view')?>
                </div>
                <div class="col s6">
                    <?=generate_select('CodiceAnnoAccademico','Anno Accademico',$options['CodiceAnnoAccademico'],$progettodidattica['CodiceAnnoAccademico'],'view')?>
                </div>
            </div>
            <div class="row">
                <div class="col s6 ">
                    <!--TEMP non viene visualizzato questo valore perchè non c'era nell'array del progettodidattica. devo recuperarlo esplicitamente nel controller-->
                    <?=generate_select('CodiceCorso','Corso',$options['CodiceCorso'],null,'view')?>
                </div>
                <div class="col s6">
                    <!--TEMP non viene visualizzato questo valore perchè non c'era nell'array del progettodidattica. devo recuperarlo esplicitamente nel controller-->
                    <?=generate_select('CodiceAnnoDiCorso','Anno Di Corso',$options['CodiceAnnoDiCorso'],null,'view')?>
                </div>
            </div>
            <div class="row">
                <div class="col s6">
                    <?=generate_select('CodiceMateria','Materia',$options['CodiceMateria'],$progettodidattica['CodiceMateria'],'view')?>
                </div>
                <div class="col s6">
                    <?=generate_select('CodiceClasse','Classe',$options['CodiceClasse'],$progettodidattica['CodiceClasse'],'view')?>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <?=generate_textinput('TempiRealizzazioneOffertaFormativa',"Tempi di Realizzazione dell'Offerta Formativa",$progettodidattica['TempiRealizzazioneOffertaFormativa'],$mode)?>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <?=generate_textarea('PresentazioneCorso','Presentazione del Corso',$progettodidattica['PresentazioneCorso'],$mode)?>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <?=generate_textarea('ObiettivoGenerale','Obiettivo Generale',$progettodidattica['ObiettivoGenerale'],$mode)?>
                </div>
            </div>
            <div class="row">
                <div class="col s6">
                    <?=generate_select('CodiceLivelloTassonomiaBloomPrefissato','Livello Tassonomia Bloom Prefissato',$options['CodiceLivelloTassonomiaBloomPrefissato'],$progettodidattica['CodiceLivelloTassonomiaBloomPrefissato'],$mode)?>
                </div>
                <div class="col s6">
                    <?=generate_select('CodiceMetodologiaDidatticaOperativa','Metodologia Didattica Operativa',$options['CodiceMetodologiaDidatticaOperativa'],$progettodidattica['CodiceMetodologiaDidatticaOperativa'],$mode)?>
                </div>    
            </div>
            <!--<div class="row">
                <div class="col s6">
                    <div class="input-field">
                        <select id="monitoraggiointermedio" name="monitoraggiointermedio" id="mese" style="margin-top: 5px;">
                            <option value=""></option>
                            <option value="Previsto">Previsto</option>
                            <option value="Non Previsto">Non Previsto</option>
                        </select>
                        <label for="monitoraggiointermedio" >Monitoraggio intermedio</label>
                    </div>
                </div>
                <div class="col s6">
                    <div class="input-field">
                        <select id="verificafinale" name="monitoraggiointermedio" id="mese" style="margin-top: 5px;">
                            <option value=""></option>
                            <option value="Previsto">Previsto</option>
                            <option value="Non Previsto">Non Previsto</option>
                        </select>
                        <label for="verificafinale" >Verifica Finale</label>
                    </div>
                </div>
            </div>-->
            <div class="row">
                <div class="col s6">
                       <?=generate_textinput('SpecificaSeMonitoraggioPrevisto',"Specificare Metodo Previsto",$progettodidattica['SpecificaSeMonitoraggioPrevisto'],$mode)?>
                </div>
                <div class="col s6">
                    <?=generate_textinput('SpecificaSeVerificaPrevista',"Specificare Tipologia Prevista",$progettodidattica['SpecificaSeVerificaPrevista'],$mode)?>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <?=generate_textarea('TestiConsigliati','Testi Consigliati',$progettodidattica['TestiConsigliati'],$mode)?>
                </div>
            </div>
        </div>
        <!-- TAB LEZIONI -->
        <div id="lezioni" style="">
            <div>
                <br/>
                <table id='tabellaRecord' class="bordered hoverable">
                    <thead>
                        <tr>
                            <?php
                            if(count($progettodidattica_lezioni)>0)
                            {
                                foreach ($progettodidattica_lezioni[0] as $key => $value) 
                                {
                                    if($key!='CodiceRegistroPresenze')
                                    {
                                ?>
                                    <th><div style="height: 50px;line-height: 50px;overflow: hidden"><?=$key?></div></th>
                                <?php
                                    }
                                }
                            ?>
                            
                            <?php
                            }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($progettodidattica_lezioni as $key => $progettodidattica_lezione) 
                        {
                        ?>
                        <tr onclick="lezione_open_popup(this,'<?=$progettodidattica_lezione['CodiceCalendarioGenerale']?>')">
                            <?php
                            foreach ($progettodidattica_lezione as $key => $value) 
                            {
                                if($key!='CodiceRegistroPresenze')
                                {
                            ?>
                                <td><div style="height: 50px;line-height: 50px;overflow: hidden"><?=$value?></div></td>
                            <?php  
                                }
                            }
                            ?>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
             </div>
            <div id="modal1" class="modal  container lezione_container" style="">
                
            </div>
        </div>
        <div id="programma" style="height: 90%;padding-top: 80px;">
            <div class="row">
                <div class="col s6">
                    <h6>Argomenti previsti per la materia</h6>
                    <?php
                    foreach ($argomenti_previsti as $key => $argomento) 
                    {
                    ?>
                        <div class="card badge argomento_assegnato ">
                            <div style="float: left">
                                <?php
                                    if($argomento['Svolto']==true)
                                    {
                                        $values['svolto']='TRUE';
                                        generate_checkbox('svolto', '', $values['svolto'],'edit');
                                    }
                                    else
                                    {
                                        $values['svolto']='FALSE';
                                        generate_checkbox('svolto', '', $values['svolto'],'edit');
                                    }
                                    ?>
                            </div>
                         
                            <div style="float: left">
                                 <?=$argomento['Argomento']?>
                            </div>
                            <div class="clearboth"></div>
                       
                        </div>
                    <?php
                    }
                    ?>
                </div>
                <div class="col s6">
                    <h6>Argomenti extra</h6>
                    <?php
                    foreach ($argomenti_extra as $key => $argomento) 
                    {
                    ?>
                        <div class="card badge argomento_assegnato ">
                        <?=$argomento['Argomento']?>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <div id="conclusione" style="padding-top: 80px;">
            <div class="row">
                <div class="col s6">
                    <?= generate_checkbox('ObiettivoGenerale', 'Obiettivo Generale Raggiunto',$progettodidattica['ObiettivoGenerale'],$mode); ?>
                </div>
                <div class="col s6">
                    <?= generate_select('CodiceLivelloTassonomiaBloomRaggiunto', 'Codice Livello Tassonomia Bloom Raggiunto', $options['CodiceLivelloTassonomiaBloomRaggiunto'], $progettodidattica['CodiceLivelloTassonomiaBloomRaggiunto'], $mode); ?>
                </div>
            </div>
            
            <div class="row">
                <div class="col s6"><?php echo generate_select('LivelloConclusione1', 'La classe ha seguito con un atteggiamento serio e professionalità la lezione', $options['LivelloConclusione1'], $progettodidattica['LivelloConclusione1'], $mode); ?></div>
                <div class="col s6"><?php echo generate_select('LivelloConclusione2', 'La classe ha interagito in modo positivo con il docente sugli argomenti oggetto di specifica trattazione (domande, considerazioni, dibattito):', $options['LivelloConclusione2'], $progettodidattica['LivelloConclusione2'], $mode); ?></div>
            </div> 
            
            <div class="row">
                <div class="col s6"><?php echo generate_select('LivelloConclusione3', 'La classe dimostra di avere acquisito le conoscenze e i principi secondo gli obiettivi specifici prefissati nel Progetto della Disciplina:', $options['LivelloConclusione3'], $progettodidattica['LivelloConclusione3'], $mode); ?></div>
                <div class="col s6"><?php echo generate_select('LivelloConclusione4', 'La classe dimostra di aver aumentato le conoscenze scientifiche nella disciplina insegnata:', $options['LivelloConclusione4'], $progettodidattica['LivelloConclusione4'], $mode); ?></div>
            </div>
            
            <div class="row">
                <div class="col s6"><?php echo generate_select('LivelloConclusione5', 'La classe ha superato con successo eventuali prove predisposte dal docente (domande campione, test, esercitazioni eccetera):', $options['LivelloConclusione5'], $progettodidattica['LivelloConclusione5'], $mode); ?></div>
                <div class="col s6"><?php echo generate_textinput('TotalePuntiLivelli', 'Totale Punti Ottenuti',$progettodidattica['TotalePuntiLivelli'], $mode); ?></div>
            </div>
            
            <div class="row">
                <div class="col s12" align="center">
                    <b>LEGENDA</b><br>
                    SI = 2 PUNTI&nbsp;SCARSAMENTE = 1 PUNTO&nbsp;NO = 0 PUNTI&nbsp;NON VALUTABILE = NON CONTEGGIATO
                </div>
            </div>
            
            <div class="row">
                <div class="col s12"><?php echo generate_textarea('Osservazioni', 'Osservazioni', $progettodidattica['Osservazioni'],$mode); ?></div>
            </div>
        </div>
        <div id="ammissioni" style="padding-top: 80px;height: 100%;">
            <div style="height: calc(100%);overflow-y: scroll">
                <table id='tabellaRecord' class="bordered" style="">
                    <thead>
                        <tr>
                            <th>Studente</th><th>Assenza materia</th><th>Assenza generale</th><th>Ammesso</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($studenti as $key => $studente) {
                            $background_color="";
                            if($studente['PercentualeAssenzaGenerale']>15)
                            {
                               $background_color="#FF6A60"; 
                            }
                            else
                            {
                                if($studente['Ammesso']===true)
                                {
                                    $background_color="#80FF80";
                                }
                                if($studente['Ammesso']===false)
                                {
                                    $background_color="#FFB266";
                                }
                            }
                            ?>
                            <tr style="background-color:<?=$background_color?>">
                                <td>
                                    <div style="margin-left: 5px; margin-right: 0px; margin-top: 5px; margin-bottom: 0px; padding:0px; width: 200px; height: 80px; float: left;">
                                        <div style="width: 50px; height: 50px;margin-top: 14px;float: left ">
                                            <img src="<?= $studente['Path_Foto']; ?>" style=" margin: auto; height: 100%; width: 100%; display: block;" />
                                        </div>
                                        <div style="width: 140px;margin-left: 10px; overflow: hidden;font-size: 14px;margin-top: 30px;float: left">
                                                <b><i><span style="font-size: 12px;"><?= $studente['Cognome'] ?> <?= $studente['Nome']; ?></span></i></b>
                                        </div>
                                        <div class="clearboth"></div>
                                    </div>
                                </td>
                                <td>
                                    <?=$studente['PercentualeAssenzaMateria']?>
                                </td>
                                <td>
                                    <?=$studente['PercentualeAssenzaGenerale']?>
                                </td>
                                <td>
                                    <?php
                                    $display='';
                                    $value=$studente['Ammesso'];
                                    if($studente['PercentualeAssenzaGenerale']>15)
                                    {
                                       //$display='display: none;';
                                        $value=false;
                                    }
                                    ?>
                                    <div style="<?=$display?>">
                                        <?= generate_checkbox('ammissioni[' . $studente['Codice'].']', '', $value, $mode) ?>
                                    </div>

                                </td>
                            </tr>

                        <?php } ?>
                    </tbody>
                </table>
            </div>  
        </div>
    </div>
</div>