<script type="text/javascript" src="<?php echo base_url('/assets/fullcalendar/fullcalendar.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/materialize.min.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        <?php
        foreach ($calendario_mesi as $key => $calendario_mese) {
        ?>
                $('#cal_<?=$calendario_mese['mese']."-".$calendario_mese['anno']?>').fullCalendar({
                    header: {
                        left:   'title',
                        center: '',
                        right:  ''
                    },
                    defaultDate: '<?=$calendario_mese['anno']?>-<?=$calendario_mese['mese']?>-01',
                    events:
                    [
                            <?php
                            foreach ($calendario_mese['giorni'] as $key => $giorno) {
                            ?>
                                {
                                    start: '<?=$giorno['start']?>',
                                    end: '<?=$giorno['end']?>',
                                    backgroundColor: '<?=$giorno['backgroundColor']?>',
                                    allDay: true,
                                    rendering: 'background',
                                },
                            <?php
                            }
                            ?>
                    ],
                    dayRender: function (date, cell) {
                        var today = new Date();
                        if (date.date() == 14) {
                            //cell.css("background-color", "red");
                        }
                    },
                    dayClick: function(date, jsEvent, view) {
                        //alert('Clicked on: ' + date.format());
                        ajax_set_periodo(this,date.format(),jsEvent);
                        
                    }
		});
        <?php
        }
        ?>
    
    });
    
</script>
<div id="block_calendario_periodi">
        <?php
        foreach ($calendario_mesi as $key => $calendario_mese) {
        ?>
            <div id='cal_<?=$calendario_mese['mese']."-".$calendario_mese['anno']?>' style="width:calc(50% - 20px); float: left; margin: 10px;background-color: white;"></div>
        <?php
            if(($calendario_mese['mese']=='02')||($calendario_mese['mese']=='04')||($calendario_mese['mese']=='06')||($calendario_mese['mese']=='08')||($calendario_mese['mese']=='10')||($calendario_mese['mese']=='12'))
            {
            ?>
            <div class="clearboth"></div>
            <?php
            }
        }
        ?>

</div>  