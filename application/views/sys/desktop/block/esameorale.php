<?php
$counter=0;
for($i=1;$i<=5;$i=$i+1)
{
    $opzioni_voto[$counter]['Codice']=$i;
    $opzioni_voto[$counter]['Descrizione']=$i;
    $counter++;
}



?>
<script type="text/javascript">
    $('#block_esameorale').ready(function(){
        $('select').not(".initialized").material_select();
    });
</script>
<div class="block block_esameorale" style="position: relative;height: 100%;">
    
    <div class="title">
        Esame orale <?=$codice?><br/>
        
        
    </div>
    <div style="height: calc(100% - 100px);overflow-y: scroll">
        <?php
        if(count($studenti)==0)
        {
            echo "Non risultano studenti per questo esame";
        }
        else
        {
        ?>
        <form id="form_studenti_esameorale">
            <table id='tabellaRecord' class="bordered" style="">
                <thead>
                    <tr>
                        <th>Studente</th><th style="width: 100px;text-align: center">Voto</th><th style="text-align: center">Bocciato</th><th style="text-align: center">Assente</th><th>Note</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                foreach($studenti as $key => $value)
                {
                    $CodiceStudente=$value['CodiceStudente'];
                    ?>
                    <tr>
                        <td>
                            <div style="margin-left: 5px; margin-right: 0px; margin-top: 5px; margin-bottom: 0px; padding:0px; width: 150px; height: 80px; float: left;">
                                <div style="width: 50px; height: 50px; "><img src="<?= $value['Path_Foto']; ?>" style=" margin: auto; height: 100%; width: 100%; display: block;" /></div>
                                <div style=" height: 36px;; width: 100%; overflow: hidden;font-size: 14px;position: relative">
                                    <div style="position: absolute;top: 0px;left: 0px;height: 100%;width: 100%;padding: 2px;">
                                        <b><i><span style="font-size: 12px;"><?=$value['Cognome']?> <?=$value['Nome']; ?></span></i></b>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td style="width: 100px;text-align: center">
                            <?php
                            if(!array_key_exists($CodiceStudente, $voti_registrati))
                            {
                                generate_select('studenti['.$CodiceStudente.'][voto]', '', $opzioni_voto,null,'edit');
                                ?>
                            <script type="text/javascript">
                            $('#studenti_<?=$CodiceStudente?>__voto_').change(function(){
                                var val=$(this).val();
                                if(val=='1')
                                {
                                    $('#studenti_<?=$CodiceStudente?>__bocciato_').prop('checked', true);
                                }
                                else
                                {
                                    $('#studenti_<?=$CodiceStudente?>__bocciato_').prop('checked', false);
                                }
                            })
                            </script>
                            <?php
                            }
                            else
                            {
                                echo $voti_registrati[$CodiceStudente]['EsameFinaleVoto'];
                            }
                            ?>
                        </td>
                        <td style="text-align: center">
                            <?php
                            if(!array_key_exists($CodiceStudente, $voti_registrati))
                            {
                                generate_checkbox('studenti['.$CodiceStudente.'][bocciato]', '',null,'edit');
                            }
                            else
                            {
                                if($voti_registrati[$CodiceStudente]['EsameBocciato1Voto']=='1')
                                {
                            ?>
                                <input name='test' type="checkbox" value="TRUE" checked="true">
                                <label for="test" class="active"></label>
                            <?php
                                }
                            }
                            ?>
                        </td>
                        <td style="text-align: center">
                            <?php
                            if(!array_key_exists($CodiceStudente, $voti_registrati))
                            {
                                generate_checkbox('studenti['.$CodiceStudente.'][assente]', '',null,'edit');
                            }
                            else
                            {
                                if($voti_registrati[$CodiceStudente]['EsameBocciato1Voto']=='A')
                                {
                            ?>
                                <input name='test' type="checkbox" value="TRUE" checked="true">
                                <label for="test" class="active"></label>
                            <?php
                                }
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if(!array_key_exists($CodiceStudente, $voti_registrati))
                            {
                                generate_textarea('studenti['.$CodiceStudente.'][note]', '', null, 'edit');
                            }
                            else
                            {
                                if($voti_registrati[$CodiceStudente]['Note']!='null')
                                    echo $voti_registrati[$CodiceStudente]['Note'];
                            }
                            ?>
                        </td>
                    </tr>

            <?php } ?>
                </tbody>
            </table>
        </form>
        <?php
        }
        ?>
    </div>  
    <?php
        if(count($studenti)>0)
        {
            ?>
        <div class="" style="bottom: 5px; right: 10px;position: absolute">
            <button class="btn waves-effect waves-light" onclick="registra_voti_esameorale(this,'<?=$codice?>')" type="button" name="action">Registra voti
                <i class="material-icons right">save</i>
            </button>
    </div>
          <?php
        }
    ?>
    


</div>