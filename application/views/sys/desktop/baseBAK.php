<?php
$firstname='test';
$lastname='test';
$description='test';
$userid=1;
?>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />   
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url('/assets/js/jquery.js') ?>"></script>
        
        <script type="text/javascript" src="<?php echo base_url('/assets/js/moment.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/js/materialize.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/js/materializeInit.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/js/jquery-ui.js') ?>"></script>
        
        
        <link rel="stylesheet" href="<?php echo base_url("/assets/css/sys/desktop/jquery-ui.css") ?>" />
        <link rel="stylesheet" href="<?php echo base_url("/assets/css/sys/desktop/materialize.min.css") ?>" />
        <link rel="stylesheet" href="<?php echo base_url("/assets/js/TableTools/css/dataTables.tableTools.css") ?>" />
        <link rel="stylesheet" href="<?php echo base_url("/assets/css/sys/desktop/font-awesome/css/font-awesome.min.css") ?>" >
        <link rel="stylesheet" href="<?php echo base_url("/assets/js/Jcrop/jquery.Jcrop.css") ?>" />
        <link rel="stylesheet" href="<?php echo base_url("/assets/js/chosen/chosen.min.css") ?>" />
        <link rel="stylesheet" href="<?php echo base_url("/assets/fullcalendar/fullcalendar.css") ?>" />
        <link rel="stylesheet" href="<?php echo base_url("/assets/css/sys/desktop/commonstyle.css") ?>?v=<?=time();?>" />
        <link rel="stylesheet" href="<?php echo base_url("/assets/css/sys/desktop/theme/default/customstyle.css") ?>?v=<?=time();?>" />

        
        <style type="text/css">
            body{
                font-size: 12px !important;
            }
            .ui-widget, .custom-table {
                font-size: 12px !important;
            }
        </style>
        <script type="text/javascript" src="<?php echo base_url('/assets/js/jquery.bpopup.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/js/formatter/lib/jquery.formatter.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/js/jquery.dataTables.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/js/dataTables.date-eu.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/js/TableTools/js/dataTables.tableTools.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/js/Jcrop/Jquery.Jcrop.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/js/chosen/chosen.jquery.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/js/jquery.togglepanel.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/js/jquery.scrollTo-1.4.3.1.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/js/Chart.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/fullcalendar/fullcalendar.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/fullcalendar/lang-all.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/fullcalendar/lib/moment.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/js/JDocWebScript.js') ?>?v=<?=time();?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/js/UniludesWebScript.js') ?>?v=<?=time();?>"></script>
        <script type="text/javascript">
var controller_url="<?php echo site_url('sys_viewcontroller/'); ?>/"; 
var assets_url="<?php echo base_url('assets/'); ?>/";
var lastval="";

  
var screen_width;
var screen_height;
var viewport_height;
var viewport_width;
var scheda_dati_ricerca_container_width;
var scheda_dati_inserimento_container_width;
var scheda_record_container_width;
var scheda_riepilgo_width;
var scheda_risultati_allargata_width;
var scheda_risultati_compatta_width; 
var scheda_container_visualizzatore;
var bPopup=[];
var bPopup_segnalazione;
var prestampa_popup=null;
var current_content='home';

$(document).ready(function(){
    screen_width=screen.width;
    screen_height=screen.height;
    window_width=$(window).width();
    window_height=$(window).height();
    
    content_container_height=window_height-60;
    scheda_dati_ricerca_container_width=screen_width*0.32;
    scheda_dati_inserimento_container_width=screen_width*0.32;
    scheda_record_container_width=screen_width*0.47;
    scheda_riepilgo_width=screen_width*0.16;
    scheda_risultati_allargata_width=screen_width*0.52;
    scheda_risultati_compatta_width=screen_width*0.48;
    scheda_container_visualizzatore=screen_width-scheda_dati_inserimento_container_width-320;

    
    
    $( ".menu_list" ).menu();
    /*$('select').chosen({
        placeholder_text_single:"test"
    });*/
    
    $( ".menu_list_button" ).hover(
            function() {
              $(this).find('.menu_list').show();
            }, function() {
              $(this).find('.menu_list').hide();
            }
          );
        $('#scheda_record_container_hidden').width(scheda_record_container_width);
        $('.tooltip').tooltip();
})

$(document).keydown(function(e) {
var element = e.target.nodeName.toLowerCase();
if (e.keyCode === 8) {
if (element != 'input' && element != 'textarea') {
    
        return false;
    }
}
});



</script>
        <title>Jdoc Web</title>
        <LINK REL="SHORTCUT ICON" HREF="<?php echo base_url("assets/images/JDocWeb_icon_128.png"); ?>" >
        <link rel="stylesheet" href="<?php echo base_url("assets/css/sys/desktop/theme/default/popup.css");?>" >
    </head>

    <body id="ricerca" class="ui-widget" style="overflow: hidden">

    <div class="wrapper scheda" data-popuplvl="0" >
                
    <div class="header" id="menu" style="position: relative;z-index: 10; border-bottom: 1px solid #BCBCBC;height: 60px;" data-visible="true" >
    <div id="" style="display: inline-block; float: left;" >
            <img id="logo_header" src="<?php echo base_url("/assets/images/logo_JDoc.png") ?>"></img>
    </div>
    <div id="menu_pulsanti" style="display: inline-block;float: left;margin-top: 8px; margin-left: 10px;" >
                        <!-- HOME -->
<!-- HOME -->
    <!--<div class="menu_divisore divisore"></div>
    <div class="menu_button"  id="menu_home_icon" onclick="clickMenu(this, 'ajax_load_content_home/desktop')" ></div>      
    <div class="menu_divisore divisore"></div>-->
    <div id="btn_home" class="btn_fa fa fa-home topmenu_btn tooltip" title="Home" onclick="clickMenu(this, 'ajax_load_content_home/desktop')"></div>
    
<!-- CALENDARIO -->
<div id="btn_calendario" class="btn_fa fa fa-calendar-o menu_list_button topmenu_btn tooltip " title="Calendario" onclick="clickMenu(this, '')">  </div>
    <div id="submenu_calendario" class="submenu " style="float: left;display: none;">
        
        <!-- DEFINIZIONE LEZIONI -->
        <div id="inserimento_submenu_btn" class="btn_fa btn_fa_scritta fa fa-calendar submenu_btn" onclick="clickMenu(this, 'ajax_load_content_calendari')"  >&nbsp;Lezioni</div>
        <!-- DEFINIZIONE PERIODI -->
        <div id="inserimento_submenu_btn" class="btn_fa btn_fa_scritta fa fa-calendar submenu_btn" onclick="clickMenu(this, 'ajax_load_content_festivita_tirocini')"  >&nbsp;Periodi</div>
        <!-- DEFINIZIONE INDISPONIBILITA' -->
        <div id="inserimento_submenu_btn" class="btn_fa btn_fa_scritta fa fa-calendar submenu_btn" onclick="clickMenu(this, 'ajax_load_content_disponibilita_docenti')"  >&nbsp;Disponibilità</div>
        
        <div class="clearboth"></div>
    </div>

<!-- PRESENZE -->
<div id="btn_presenze" class="btn_fa fa fa-book menu_list_button topmenu_btn tooltip " title="Presenze" onclick="clickMenu(this, 'ajax_load_content_presenze_lezione')">  </div>

<!-- DOCENTI -->
<div id="btn_docenti" class="btn_fa fa fa-graduation-cap menu_list_button topmenu_btn tooltip" title="Progetto docenti" onclick="clickMenu(this, 'ajax_load_content_didattica')">  </div>
    <div id="submenu_calendario" class="submenu " style="float: left;display: none;">
        <!-- DIDATTICA -->
        <div id="didattica_submenu_btn" class="btn_fa fa fa-search menu_list_button submenu_btn " onclick="clickMenu(this, 'ajax_load_content_didattica')"> </div>

        <!-- DOMANDE APERTE -->
        <div id="domandeaperte_submenu_btn" class="btn_fa fa fa-pencil menu_list_button submenu_btn" onclick="clickMenu(this, 'ajax_load_content_domandeaperte')"> </div>
        
        <!-- DOMANDE MULTIPLECHOICE -->
        <div id="domandemultiplechoice_submenu_btn" class="btn_fa fa fa-pencil menu_list_button submenu_btn" onclick="clickMenu(this, 'ajax_load_content_domandemultiplechoice')"  > </div>
        <div class="clearboth"></div>
    </div>


<!-- IMPOSTAZIONI -->
<div id="btn_impostazioni" class="btn_fa fa fa-cog tooltip topmenu_btn" title="Impostazioni"  onclick="clickMenu(this,'ajax_load_content_impostazioni_preferenze/desktop')"  ></div> 

<!-- LOGOUT 
    <div class="menu_button"  id="menu_logout_icon" > 
        <a href="<?php echo site_url('sys_viewcontroller/logout'); ?>" style="height: 100%;width: 100%;display: block"></a>
    </div> 
    <div class="menu_divisore divisore"></div>-->
        
    </div>
        <div class="contentmenu" style="float: left;margin-top: 28px;margin-left: 10px;display: none;">
            <div id="navigatore" style="float: left">
                <div class="btn_scritta nav" id="nav_ricerca" data-position="0" data-target_id="nav_ricerca" onclick="$('#content_ricerca').scrollTo($('#scheda_dati_ricerca_container'),500);"  >Filtri</div>
                <div class="btn_scritta nav" id="nav_risultati" data-position="0" data-target_id="nav_risultati" onclick="$('#content_ricerca').scrollTo($('#scheda_risultati'),500);" >Risultati</div>
                <div class="btn_scritta nav" id="nav_scheda_hidden" data-position="0" onclick="move_scrollbar(this)" style="display: none;">Risultati</div>
            </div>

            <div class="clearboth"></div>
        </div> 
        
        <div style="float: right">
            <div class="tooltip btn_fa fa fa-question" title="Istruzioni" style="float: none;font-size: 14px !important;"  onclick="ajax_load_block_manuale()"></div><br/>
            <div class="tooltip btn_fa fa fa-bolt" title="Segnalazione" style="float: none;font-size: 14px !important;margin-top: 2px"  onclick="ajax_load_block_segnalazioni()"></div><br/>
            <div class="tooltip btn_fa fa fa-sign-out" title="Esci" style="float: none;font-size: 14px !important;margin-top: 2px"  onclick="signout()"></div>
        </div>
        <div id="" style="display: inline-block; float: right;height: 60px;margin-right: 5px; cursor: pointer;" onclick="" >
            <div id="menu_name" style="height: 30px;font-weight: bold;text-align: center"><?=$firstname?> <?=$lastname?></div>
            <div id="menu_description" style="text-align: center"><?=$description?></div>
        </div>
        <div id="" style="display: inline-block; float: right;" >
            <?php
            if(file_exists("../JDocServer/avatar/$userid.jpg"))
            {
                $avatar_url=domain_url()."/JDocServer/avatar/$userid.jpg";
            }
            else
            {
                $avatar_url=  base_url('/assets/images/anon.png');
            }
            ?>
            <img id="avatar" src="<?=$avatar_url?>"></img>
        </div>
        
        
        <div style="clear: both;">
                        
    </div>
                    
                </div>
                <div id="scheda_record_container_hidden" class="scheda_container scheda_record_container scheda_record_container_hidden"  style="float: left;display: none">
           
            
                </div>
                <div id="manuale_container" class="popup"></div>
                <div id="segnalazioni_container" class="popup"></div>
                
                <div id="content_container"class="content_container" >
                    <a class="waves-effect waves-light btn"><i class="material-icons left">cloud</i>button</a>
                    <div class="input-field">
    <select id="materializeselect">
      <option value="" disabled selected>Choose your option</option>
      <option value="1">Option 1</option>
      <option value="2">Option 2</option>
      <option value="3">Option 3</option>
    </select>
    <label for="materializeselect">Materialize Select</label>
  </div>
                <?=$data['content']?>
                    


	<div id='calendar'></div>
                </div>
                                    </div>


                                    </body>
                                    </html>

