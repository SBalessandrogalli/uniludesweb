<?php
$funzione=$data['funzione'];
$scheda_container=$data['scheda_container'];
$tableid=$data['tableid'];
$query=$data['query'];
$saved_views=$data['saved_views'];
$default_viewid=$data['default_viewid'];
$userid=$data['userid'];
?>
<script type="text/javascript">
$( "#sched_dati_ricerca" ).ready(function(){
 $('#tabs_scheda_dati_ricerca').tabs();
});

</script>

<div id="scheda_dati_ricerca" class="scheda scheda_dati_ricerca" data-tableid="<?=$data['tableid']?>" data-recordid="<?=$data['recordid']?>" data-funzione="<?=$funzione?>" data-scheda_container="<?=$scheda_container?>" data-schedaid="scheda_dati_ricerca" style="overflow: none">

    <div id="menu_scheda_campi" class="menu_mid menu_top ui-widget-header">
       <div style="float: left;margin-left: 10px;color: black;">Ricerca <?=$tableid?></div>
            <div class="fa fa-refresh tooltip" style="float: right;margin-right: 5px;;" title="azzera i parametri di ricerca"  onclick="reload_fields(this,'<?=$tableid?>','<?=$funzione?>');"></div> 
            <div id="autosearch" style="float: right;line-height: 20px;font-weight: normal;margin-right: 20px;display: none;" >
                Auto:
                <input type="radio" id="autosearchTrue" name="autosearch" checked="checked"  /><label for="autosearchTrue" style="width: 30px;">On</label>
                <input type="radio" id="autosearchFalse" name="autosearch" /><label for="autosearchFalse" style="width: 30px;">Off</label>
            </div>

        <div class="clearboth"></div>
    </div>
    <div class="schedabody schedabody_with_menu_bottom">
        <div id="tabs_scheda_dati_ricerca" style="padding: 0px !important;border: 0px !important">    
        <ul>
            <li style="width: 30%;"><a style="display: block;width: 100%" href="#dati_ricerca">Filtri</a></li>
            <?php
            if($userid==1)
            {
            ?>
            <li style="width: 30%;"><a style="display: block;width: 100%" href="#block_riepilogo">Gestione</a></li>
            <?php
            }
            ?>
        </ul>
        <div id="dati_ricerca" class="block_dati_labels_container" style="float: left">
            <div id="saved_view" class="fieldscontainer">
                <div class="fieldcontainer fieldcontainer_edit first" >
                    <div class="fieldlabel fieldlabel_edit">Ricerche salvate</div>
                    <div class="fieldContent">
                        <div class="fieldValueContainer fieldValueContainer_edit">
                                <select id="saved_view_select" class="field fieldInput select" onchange="view_changed(this,'<?=$tableid?>')">
                                    <option value=""></option>
                                    <?php
                                    foreach ($saved_views as $key => $saved_view) {
                                        $selected='';
                                        if($saved_view['id']==$default_viewid)
                                        {
                                            $selected='selected';
                                        }
                                        ?>
                                    <option value="<?=$saved_view['id']?>" <?=$selected?>><?=$saved_view['name']?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                        </div>
                    </div>
                    <div class="clearboth"></div>
                </div>
                <div class="clearboth"></div>
            </div>
            <?php
            echo $data['block']['block_dati_labels'];
            ?>
        </div>
        <div id="block_riepilogo" class="blocco" style="height: 100%;width: 100%;display: none;float: left;">
            <h3 onclick="show_query_riepilogo(this)"> Ricerche salvate</h3>
            
                
                <div class="btn_scritta" style="float: left;" onclick="save_view(this,'<?=$tableid?>')">Salva ricerca</div>
                <div class="btn_scritta" style="float: left" onclick="set_default_view(this)">Imposta Default</div>
                <div class="clearboth"></div>
            
            <div id="riepilogo" class="riepilogo" style=" overflow-y: scroll; height: 100%;">
                Parametri attuali:
                <form id="form_riepilogo" class="form_riepilogo" method="post" action="<?php echo site_url('sys_viewcontroller/'); ?>/esporta_xls" >
                    <input type="hidden" id="view_name" name="view_name">
                    <input type="hidden" id="exportid" name="exportid">
                    <input type="hidden" id="tableid" name="tableid" value="<?=$tableid?>">
                    <textarea id="query" class="query_riepilogo" name="query" style="width: 100%;height: 200px;overflow: scroll;display: none;">
                        <?=$query?>
                    </textarea>
                    <input id="file_allegato_hidden" class="file_allegato" type="text" style="display: none" value="" >
                </form> 
            </div>

        </div>
    </div>
    <div class="clearboth"></div>
    
    </div>
    <div class=" menu_big menu_bottom">
        
        
        <div id="btnCerca" class="btn_scritta" onclick="refresh_risultati_ricerca()" style="width: 100px;">Cerca</div>
        <?php
        if($tableid=='CANDID')
        {
        ?>
            <div id="btnCerca" class="btn_scritta" onclick="ajax_load_block_risultati_ricerca_non_validati(this, '<?=$data['tableid']?>')" style="width: 200px;">Cerca non validati</div>
        <?php
        }
        ?>
        <div id="btnMostraTutti" class="btn_scritta" onclick="reload_fields(this,'<?=$tableid?>')" style="width: 100px;">Mostra tutti</div>
        <div class="clearboth"></div>
    </div>
    
</div>
    
