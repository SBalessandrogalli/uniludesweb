<?php
$firstname='';
$lastname='';
$description='';
if(array_key_exists('docente', $data))
{
    $docente=$data['docente'];
    $firstname=  ucwords(strtolower($docente['Nome']));
    $lastname=  ucwords(strtolower($docente['Cognome']));
    $description=$docente['Titolo']." <br/> ".$docente['Professione'];
}
?>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />   
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url('/assets/js/jquery.js') ?>"></script>
        

        <script type="text/javascript" src="<?php echo base_url('/assets/js/moment.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/fullcalendar/fullcalendar.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/fullcalendar/lang-all.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/js/jquery-ui.js') ?>"></script>
        <link rel="stylesheet" href="<?php echo base_url("/assets/css/sys/desktop/jquery-ui.css") ?>" />
        <script type="text/javascript">
            //necessario perchè per la funzione tooltip c'è conflitto tra jquery-ui e materialize. quindi la copio in una funzione con nome diverso che non fa conflitto
            $.fn.jqui_tooltip = $.fn.tooltip;
        </script>
        <script type="text/javascript" src="<?php echo base_url('/assets/js/materialize.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/js/materializeInit.js') ?>"></script>
        
        
        
        <link rel="stylesheet" href="<?php echo base_url("/assets/css/sys/desktop/materialize.min.css") ?>?v=<?=time();?>" />
        <link rel="stylesheet" href="<?php echo base_url("/assets/js/TableTools/css/dataTables.tableTools.css") ?>" />
        <link rel="stylesheet" href="<?php echo base_url("/assets/css/sys/desktop/font-awesome/css/font-awesome.min.css") ?>" >
        <link rel="stylesheet" href="<?php echo base_url("/assets/js/Jcrop/jquery.Jcrop.css") ?>" />
        <link rel="stylesheet" href="<?php echo base_url("/assets/js/chosen/chosen.min.css") ?>" />
        <link rel="stylesheet" href="<?php echo base_url("/assets/fullcalendar/fullcalendar.css") ?>" />
        <link rel="stylesheet" href="<?php echo base_url("/assets/css/sys/desktop/commonstyle.css") ?>?v=<?=time();?>" />
        <link rel="stylesheet" href="<?php echo base_url("/assets/css/sys/desktop/theme/default/customstyle.css") ?>?v=<?=time();?>" />


        
        <script type="text/javascript" src="<?php echo base_url('/assets/js/JDocWebScript.js') ?>?v=<?=time();?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/js/UniludesWebScript.js') ?>?v=<?=time();?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/assets/js/SyncScript.js') ?>?v=<?=time();?>"></script>
        <script type="text/javascript">
var sync_controller_url="<?php echo site_url('sync_controller/'); ?>/"; 
var controller_url="<?php echo site_url('sys_viewcontroller/'); ?>/"; 
var assets_url="<?php echo base_url('assets/'); ?>/";
var lastval="";

  
var screen_width;
var screen_height;
var viewport_height;
var viewport_width;
var scheda_dati_ricerca_container_width;
var scheda_dati_inserimento_container_width;
var scheda_record_container_width;
var scheda_riepilgo_width;
var scheda_risultati_allargata_width;
var scheda_risultati_compatta_width; 
var scheda_container_visualizzatore;
var bPopup=[];
var bPopup_segnalazione;
var prestampa_popup=null;
var current_content='home';
var refresh=false;

$(document).ready(function(){
    <?php
if(($userid!=1)&&($userid!=102)&&($userid!=57)&&($userid!=44)&&($userid!=18)&&($userid!=233)&&($userid!=234))
{
?>
        $('#btn_docenti').click();
        <?php
}
        ?>

    screen_width=screen.width;
    screen_height=screen.height;
    window_width=$(window).width();
    window_height=$(window).height();
    
    content_container_height=window_height-60;
    scheda_dati_ricerca_container_width=screen_width*0.32;
    scheda_dati_inserimento_container_width=screen_width*0.32;
    scheda_record_container_width=screen_width*0.47;
    scheda_riepilgo_width=screen_width*0.16;
    scheda_risultati_allargata_width=screen_width*0.52;
    scheda_risultati_compatta_width=screen_width*0.48;
    scheda_container_visualizzatore=screen_width-scheda_dati_inserimento_container_width-320;

    
    
    $( ".menu_list" ).menu();
    /*$('select').chosen({
        placeholder_text_single:"test"
    });*/
    
    $( ".menu_list_button" ).hover(
            function() {
              $(this).find('.menu_list').show();
            }, function() {
              $(this).find('.menu_list').hide();
            }
          );
        $('#scheda_record_container_hidden').width(scheda_record_container_width);
        $('.tooltip').jqui_tooltip();
        
        
        
})

$(document).keydown(function(e) {
var element = e.target.nodeName.toLowerCase();
if (e.keyCode === 8) {
if (element != 'input' && element != 'textarea') {
    
        return false;
    }
}
});


function superuser_selecteduserid_login(el)
{
    var serialized_data=[];
    serialized_data.push({name: 'selecteduserid', value: $('#selecteduserid').val()});
    $.ajax( {
        type: "POST",
        url: controller_url+'superuser_selecteduserid_login/',
        data: serialized_data,
        success: function( response ) {
            console.info(response);
            window.location.href = controller_url+"view_home";
        },
        error:function(){
            alert('errore');
        }
    } ); 
}
        
</script>
 
        <title>UniludesWeb</title>
        <LINK REL="SHORTCUT ICON" HREF="<?php echo base_url("assets/images/JDocWeb_icon_128.png"); ?>" >
        <link rel="stylesheet" href="<?php echo base_url("assets/css/sys/desktop/theme/default/popup.css");?>" >
    </head>

    <body id="ricerca" class="ui-widget" style="overflow: hidden">
    <div id="stampa" style="min-height: 100%;width: 100%;position: absolute;top: 0px;left: 0px;z-index: 100;background-color: white;display: none;">
    </div>
    <div class="wrapper scheda" data-popuplvl="0" >
                
    <div class="header" id="menu" style="position: relative;z-index: 10; border-bottom: 1px solid #BCBCBC;height: 60px;overflow: hidden" data-visible="true" >
    
    <div id="menu_pulsanti" style="display: inline-block;float: left; margin-left: 10px;" >
                        <!-- HOME -->
<!-- HOME -->
    <!--<div class="menu_divisore divisore"></div>
    <div class="menu_button"  id="menu_home_icon" onclick="clickMenu(this, 'ajax_load_content_home/desktop')" ></div>      
    <div class="menu_divisore divisore"></div>-->
    
    <div id="btn_home" class="btn_fa fa fa-home topmenu_btn tooltip" title="Home" data-position="bottom" data-delay="0" data-tooltip="Home" onclick="clickMenu(this, 'ajax_load_content_home/desktop')"></div>
    

<!-- CALENDARIO -->
<?php
if(($userid==1)||($userid==102)||($userid==57)||($userid==44)||($userid==18)||($userid==233)||($userid==234)||($userid==248)||($userid==253))
{
?>
<div id="btn_calendario" class="btn_fa fa fa-calendar menu_list_button topmenu_btn tooltip " title="Calendario" data-position="bottom" data-delay="0" data-tooltip="Calendario" onclick="clickMenu(this, '')">  </div>
   
    <div id="submenu_calendario" class="submenu " style="float: left;display: none;">
        <div id="inserimento_submenu_btn" class="btn_fa btn_fa_scritta fa fa-calendar submenu_btn" ></div>
        <!-- CALENDARIO GENERALE -->
        <a class="waves-effect  btn-flat" style="height: auto;padding: 0px;float: left;"><div id="inserimento_submenu_btn" class="submenu_btn" onclick="clickMenu(this, 'ajax_load_content_calendario_generale')"  >Calendario generale</div></a>
        <!-- DEFINIZIONE LEZIONI -->
        <a class="waves-effect  btn-flat" style="height: auto;padding: 0px;float: left;"><div id="inserimento_submenu_btn" class="submenu_btn" onclick="clickMenu(this, 'ajax_load_content_calendari')"  >Pianificazione</div></a>
        <!-- DEFINIZIONE PERIODI -->
        <a class="waves-effect  btn-flat" style="height: auto;padding: 0px;float: left;"><div id="inserimento_submenu_btn" class="submenu_btn" onclick="clickMenu(this, 'ajax_load_content_festivita_tirocini')"  >Periodi</div></a>
        <!-- DEFINIZIONE INDISPONIBILITA' -->
        <!--<a class="waves-effect  btn-flat" style="height: auto;padding: 0px;float: left;"><div id="inserimento_submenu_btn" class="submenu_btn" onclick="clickMenu(this, 'ajax_load_content_disponibilita_docenti')"  >Disponibilità</div></a>-->
        
        <div class="clearboth"></div>
    </div>
<?php
}
?>

<!-- PRESENZE -->
<div id="btn_presenze" class="btn_fa fa fa-book menu_list_button topmenu_btn tooltip " title="Presenze" data-position="bottom" data-delay="0" data-tooltip="Presenze" onclick="clickMenu(this, 'ajax_load_content_presenze_lezione')">  </div>


<!-- DOCENTI -->
<div id="btn_docenti" class="btn_fa fa fa-graduation-cap menu_list_button topmenu_btn tooltip" title="Progetto Docenti" data-position="bottom" data-delay="0" data-tooltip="Progetto Docenti" onclick="clickMenu(this, '')">  </div>
    <div id="submenu_calendario" class="submenu " style="float: left;display: none;">
        <div id="inserimento_submenu_btn" class="btn_fa btn_fa_scritta fa fa-graduation-cap submenu_btn" ></div>
        <!-- CALENDARIO -->
        
            <a class="waves-effect  btn-flat" style="height: auto;padding: 0px;float: left;"><div id="inserimento_submenu_btn" class="submenu_btn" onclick="clickMenu(this, 'ajax_load_content_docenti_calendario')"  >Calendario</div></a>
        <!-- PRESENZE -->
        <a class="waves-effect  btn-flat" style="height: auto;padding: 0px;float: left;"><div id="inserimento_submenu_btn" class="submenu_btn" onclick="clickMenu(this, 'ajax_load_content_docenti_lezioni')"  >Lezioni</div></a>
        
        <!-- DIDATTICA -->
        <a class="waves-effect  btn-flat" style="height: auto;padding: 0px;float: left;"><div id="inserimento_submenu_btn" class="submenu_btn" onclick="clickMenu(this, 'ajax_load_content_docenti_didattica')"  >Didattica</div></a>

        <!-- DOMANDE APERTE -->
        <a class="waves-effect  btn-flat" style="height: auto;padding: 0px;float: left;"><div id="inserimento_submenu_btn" class="submenu_btn" onclick="clickMenu(this, 'ajax_load_content_docenti_domandeaperte')"  >Domande Aperte</div></a>
        
        <!-- DOMANDE MULTIPLECHOICE -->
        <a class="waves-effect  btn-flat" style="height: auto;padding: 0px;float: left;"><div id="inserimento_submenu_btn" class="submenu_btn" onclick="clickMenu(this, 'ajax_load_content_docenti_multiplechoice')"  >Multiple Choice</div></a>
        
        <!-- ESAMI ORALI -->
        <a class="waves-effect  btn-flat" style="height: auto;padding: 0px;float: left;"><div id="inserimento_submenu_btn" class="submenu_btn" onclick="clickMenu(this, 'ajax_load_content_esamiorali')"  >Registrazione voti</div></a>
        
        <!-- RIEPILOGO ARGOMENTI -->
        <a class="waves-effect  btn-flat" style="height: auto;padding: 0px;float: left;"><div id="inserimento_submenu_btn" class="submenu_btn" onclick="clickMenu(this, 'ajax_load_content_argomenti')"  >Argomenti</div></a>
        
        
        <div class="clearboth"></div>
    </div>


<!-- IMPOSTAZIONI -->
<?php
if($userid==1)
{
?>
<div id="btn_impostazioni" class="btn_fa fa fa-cog tooltip topmenu_btn" title="Impostazioni" data-position="bottom" data-delay="0" data-tooltip="Impostazioni"  onclick="clickMenu(this,'ajax_load_content_impostazioni/desktop')"  ></div> 
<?php
}
?>
<!-- LOGOUT 
    <div class="menu_button"  id="menu_logout_icon" > 
        <a href="<?php echo site_url('sys_viewcontroller/logout'); ?>" style="height: 100%;width: 100%;display: block"></a>
    </div> 
    <div class="menu_divisore divisore"></div>-->
        
    </div>
        <div class="contentmenu" style="float: left;margin-top: 28px;margin-left: 10px;display: none;">
            <div id="navigatore" style="float: left">
                <div class="btn_scritta nav" id="nav_ricerca" data-position="0" data-target_id="nav_ricerca" onclick="$('#content_ricerca').scrollTo($('#scheda_dati_ricerca_container'),500);"  >Filtri</div>
                <div class="btn_scritta nav" id="nav_risultati" data-position="0" data-target_id="nav_risultati" onclick="$('#content_ricerca').scrollTo($('#scheda_risultati'),500);" >Risultati</div>
                <div class="btn_scritta nav" id="nav_scheda_hidden" data-position="0" onclick="move_scrollbar(this)" style="display: none;">Risultati</div>
            </div>

            <div class="clearboth"></div>
        </div> 
        
        <div style="float: right">
            <div class="tooltip btn_fa fa fa-question" title="Istruzioni" data-position="left" data-delay="0" data-tooltip="Istruzioni" style="float: none;font-size: 14px !important;"  onclick="ajax_load_block_manuale()"></div><br/>
            <div class="tooltip btn_fa fa fa-bolt" title="Segnalazione" data-position="left" data-delay="0" data-tooltip="Test presenze" style="float: none;font-size: 14px !important;margin-top: 2px"  onclick="clickMenu(this, 'ajax_test_presenze')"></div><br/>
            <div class="tooltip btn_fa fa fa-sign-out" title="Esci" data-position="left" data-delay="0" data-tooltip="Esci" style="float: none;font-size: 14px !important;margin-top: 2px"  onclick="signout()"></div>
        </div>
        <div id="" style="display: inline-block; float: right;height: 60px;margin-right: 5px; cursor: pointer;" onclick="" data-userid="<?=$userid?>" >
            <div id="menu_name" style="font-size: 12px;font-weight: bold;text-align: center"><?=$firstname?> <?=$lastname?></div>
            <div id="menu_description" style="font-size: 12px;text-align: center"><?=$description?></div>
        </div>
        <div id="" style="display: inline-block; float: right;" >
            <?php
            if(file_exists("../JDocServer/avatar/1.jpg"))
            {
                $avatar_url=domain_url()."/JDocServer/avatar/1.jpg";
            }
            else
            {
                $avatar_url=  base_url('/assets/images/anon.png');
            }
            ?>
            <img id="avatar" src="<?=$avatar_url?>"></img>
        </div>
        <?php
        if($superuser)
        {
        ?>
            <div style="float: right">
            <select id="selecteduserid" style="display: block !important" onchange="superuser_selecteduserid_login(this)">
                <option value=""> Cambia utente</option>
                <?php
                        foreach ($users as $key => $user) {
                         ?>
                        <option value="<?=$user['Codice']?>"><?=$user['Nome']?></option>
                         <?php
                        }
                ?>
                
            </select>
        </div>
        <?php
        }
        ?>
        
        
        <div style="clear: both;">
                        
    </div>
                    
                </div>
                <div id="scheda_record_container_hidden" class="scheda_container scheda_record_container scheda_record_container_hidden"  style="float: left;display: none">
           
            
                </div>
                <div id="manuale_container" class="popup"></div>
                <div id="segnalazioni_container" class="popup"></div>
                
                <div id="content_container"class="content_container" >
                
                    <?=$data['content']?>
                    


	<div id='calendar'></div>
                </div>
                                    </div>


                                    </body>
                                    </html>

