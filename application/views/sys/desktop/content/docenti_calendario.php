<script type="text/javascript" src="<?php echo base_url('/assets/js/moment.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/jquery.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/jquery-ui.js') ?>"></script>
<?php
$today=date('Y-m-d');
?>
<script type="text/javascript" src="<?php echo base_url('/assets/fullcalendar/fullcalendar.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/materialize.min.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
                $('#calendario_docente').fullCalendar({
                    defaultDate: '<?=$today?>',
                    events: function( start, end, timezone, callback ) {
            var year=end.year();
            var month=end.month();
            var url  = controller_url+'/get_eventi_docente/' + year + '/' + month;
                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: 'POST',
                    success: function( response ) {
                        callback(response);
                    }
                })
        },
                    dayRender: function (date, cell) {

                    },
                    eventClick: function(calEvent, jsEvent, view)
                    {
                        if(calEvent.CodiceTipoCalendario==1)
                        {
                            alert(calEvent.title);
                            //lezione_open_popup(this,calEvent.codiceRegistroPresenze);
                        }
                        else
                        {
                            alert(calEvent.title);
                        }
                        
                        
                    },
                    dayClick: function(date, jsEvent, view) {
                        //alert('Clicked on: ' + date.format());
                        //$('#calendario_docente').fullCalendar( 'refetchEvents' );
                        
                        
                    }
                    
                    
		});

        

    });
    
</script>
<div id="modal1" class="modal  container lezione_container" style="">
                
            </div>
<div id="content_docenti_calendario" class="content" style="">
        <div class="row">
            <div class="col s7">
                <div class="card" style="padding: 20px;">
                    <div id='calendario_docente' style="width:100%;"></div>
                </div>
            </div>
            <div class="col s3">
                <div class="card" style="padding: 10px;">
                    <div class="row">
                        <button class="btn waves-effect waves-light" onclick="stampa_riepilogo_lezioni_docente(this)">Stampa riepilogo lezioni
                            <i class="material-icons right">print</i>
                        </button>
                    </div>    
                    <form id="stampa_riepilogo_lezioni_docente">
                        <div class="row">
                        <?=  generate_checkbox('01', 'Gennaio',null,'edit')?>
                    </div>
                    <div class="row">
                        <?=  generate_checkbox('02', 'Febbraio',null,'edit')?>
                    </div>
                    <div class="row">
                        <?=  generate_checkbox('03', 'Marzo',null,'edit')?>
                    </div>
                    <div class="row">
                        <?=  generate_checkbox('04', 'Aprile',null,'edit')?>
                    </div>
                    <div class="row">
                        <?=  generate_checkbox('05', 'Maggio',null,'edit')?>
                    </div>
                    <div class="row">
                        <?=  generate_checkbox('06', 'Giugno',null,'edit')?>
                    </div>
                    <div class="row">
                        <?=  generate_checkbox('07', 'Luglio',null,'edit')?>
                    </div>
                    <div class="row">
                        <?=  generate_checkbox('08', 'Agosto',null,'edit')?>
                    </div>
                    <div class="row">
                        <?=  generate_checkbox('09', 'Settembre',null,'edit')?>
                    </div>
                    <div class="row">
                        <?=  generate_checkbox('10', 'Ottobre',null,'edit')?>
                    </div>
                    <div class="row">
                        <?=  generate_checkbox('11', 'Novembre',null,'edit')?>
                    </div>
                    <div class="row">
                        <?=  generate_checkbox('12', 'Dicembre',null,'edit')?>
                    </div>
                    <br/>
                    </form>
                    
                    
                </div>
            </div>
        </div>
</div>


