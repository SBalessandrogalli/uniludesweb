<link rel="stylesheet" href="<?php echo base_url("/assets/fullcalendar/fullcalendar.css") ?>" />
<script type="text/javascript" src="<?php echo base_url('/assets/js/moment.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/jquery.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/jquery-ui.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/fullcalendar/fullcalendar.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        <?php
        for ($i = 1; $i <= 12; $i++) {
        ?>
                $('#cal_<?=$i?>').fullCalendar({
                    defaultDate: '2015-<?=str_pad($i, 2, '0', STR_PAD_LEFT)?>-01',
                    dayRender: function (date, cell) {
                        var today = new Date();
                        if (date.date() == 14) {
                            //cell.css("background-color", "red");
                        }
                    },
                    eventClick: function(calEvent, jsEvent, view)
                    {
                        apri_scheda_record(this,calEvent.tableid,calEvent.recordid,'right','standard_dati','risultati_ricerca');
                    },
                    dayClick: function(date, jsEvent, view) {
                        //alert('Clicked on: ' + date.format());
                        ajax_set_indisponibilita(this,date.format());
                        
                    }
                    
                    
		});
        <?php
        }
        ?>
        

    });
    
</script>
<div class="content" style="overflow-y: scroll">
    <div style="border: 1px solid #bcbcbc;padding: 25px; float: left;height: 100%;width: calc(20% - 54px);">
        <form id='form_eventi'>
           
            <div style="margin-left: 25px;margin-top: 20px;">
                Docente: <br/>
                <?=$data['block']['select_docenti']?>
            </div><br/>
            <input type="radio" name="indisponibilita" value="intera" checked>Giornata intera<br/>
            <input type="radio" name="indisponibilita" value="mezza">Mezza giornata
            
            <input id='data_evento' type="hidden" name="data_evento" value="">
            <div class="clearboth"></div>
        </form>

    </div>   
    <div style="width: 80%;float: left;margin: auto;">
        <?php
        for ($i = 1; $i <= 12; $i++) {
        ?>
            <div id='cal_<?=$i?>' style="width:45%; float: left; margin-left: 2%;"></div>
        <?php
            if(($i==2)||($i==4)||($i==6)||($i==8)||($i==10)||($i==12))
            {
            ?>
            <div class="clearboth"></div>
            <?php
            }
        }
        ?>

    </div>    
</div>


