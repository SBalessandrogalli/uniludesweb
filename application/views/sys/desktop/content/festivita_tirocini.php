<link rel="stylesheet" href="<?php echo base_url("/assets/fullcalendar/fullcalendar.css") ?>" />
<script type="text/javascript" src="<?php echo base_url('/assets/js/moment.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/jquery.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/jquery-ui.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/fullcalendar/fullcalendar.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/materialize.min.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('select').not(".initialized").material_select();
    });
</script>
<div class="content" >
    <div class="card" style="border: 1px solid #bcbcbc;padding: 25px; float: left;height: 100%;width: 360px;">
        <form id='form_periodi'>
            <div>
                <script type="text/javascript">
                $('#CodiceAnnoAccademico').change(function(){
                    var value=$(this).val();
                    $.ajax({
                        url: controller_url+'ajax_get_classi'+'/'+value,
                        dataType:'json',
                        success:function(data){
                            $('#CodiceClasse').empty().html(' ');
                            $('#CodiceClasse').append(
                                $("<option></option>")
                                  .attr("value",'')
                                  .text('')
                              );
                            var options=data;
                            $.each(options,function(i,option){
                                $('#CodiceClasse').append(
                                $("<option></option>")
                                  .attr("value",option.Codice)
                                  .text(option.Descrizione)
                              );
                            })
                            $('#CodiceClasse').material_select();
                            $('#CodiceClasse').closest('.input-field').children('span.caret').remove();
                        },
                        error:function(){
                            alert('errore');
                        }
                    });
                })
                $('#CodiceAnnoAccademico').change();
                </script>
                    
                <?=  generate_select('CodiceAnnoAccademico', 'Anno accademico', $annoaccademico_options, 7, 'edit')?>
            </div>
            <div>
                <script type="text/javascript">
                $('#CodiceClasse').change(function(){
                    load_block_calendario_periodi();
                })
                </script>
                <?=  generate_select('CodiceClasse', 'Classe', $data['classi'], null, 'edit')?>
            </div>

            <div class="tipo_evento" style="margin-left: 25px;margin-top: 20px;border: 1px solid lightblue;padding: 2px;text-align: center;cursor: pointer;" data-color="blue" onclick="$('.tipo_evento').removeClass('checked');$('.tipo_evento').css('border-width','1px');$(this).addClass('checked');$(this).css('border-width','3px');$('#CodiceTipoCalendario').val('9')">
                Vacanza
            </div>
            <div class="tipo_evento" style="margin-left: 25px;margin-top: 20px;border: 1px solid green;padding: 2px;text-align: center;cursor: pointer;" data-color="green" onclick="$('.tipo_evento').removeClass('checked');$('.tipo_evento').css('border-width','1px');$(this).addClass('checked');$(this).css('border-width','3px');$('#CodiceTipoCalendario').val('10')">
                Tirocinio
            </div>
            <div class="tipo_evento" style="margin-left: 25px;margin-top: 20px;border: 1px solid red;padding: 2px;text-align: center;cursor: pointer;" data-color="red" onclick="$('.tipo_evento').removeClass('checked');$('.tipo_evento').css('border-width','1px');$(this).addClass('checked');$(this).css('border-width','3px');$('#CodiceTipoCalendario').val('11')">
                Esami
            </div>
            <div class="tipo_evento" style="margin-left: 25px;margin-top: 20px;border: 1px solid lightsteelblue;padding: 2px;text-align: center;cursor: pointer;" data-color="lightsteelblue" onclick="$('.tipo_evento').removeClass('checked');$('.tipo_evento').css('border-width','1px');$(this).addClass('checked');$(this).css('border-width','3px');$('#CodiceTipoCalendario').val('12')">
                Inizio anno accademico
            </div>
            <br/>
            <br/>
            <br/>
            <div class="tipo_evento" style="margin-left: 25px;margin-top: 20px;border: 1px solid black;padding: 2px;text-align: center;cursor: pointer;" data-color="white" onclick="$('.tipo_evento').removeClass('checked');$('.tipo_evento').css('border-width','1px');$(this).addClass('checked');$(this).css('border-width','3px');$('#CodiceTipoCalendario').val('0')">
                Annulla
            </div>

            <input id='Giorno' type="hidden" name="Giorno" value="">
            <input id='CodiceTipoCalendario' type="hidden" name="CodiceTipoCalendario" value="">
            <div class="clearboth"></div>
        </form>

    </div>  
    <div id="calendario_periodi_container" class="card" style="height: 100%;width: calc(100% - 400px);float: left;overflow-y: scroll">
        
    </div>
      
</div>


