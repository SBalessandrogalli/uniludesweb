<?php
?>
<script type="text/javascript">
    $(document).ready(function(){
         
    });
</script>
<div id="content_docenti_esamiorali" class="content" style="background-color: #F5F5F5">
    <div class="card" style="width: calc(50% - 20px);height: calc(100% - 20px);float: left;padding: 0px;">
        <div class="title">
            Elenco esami orali
        </div>
        <div id="esamiorali_elenco_container" class="container block_container" style="width: 100%;overflow: scroll;height:calc(100% - 50px)">
        <?=$data['block']['esamiorali_elenco']?>
        </div>
    </div>
    <div class="card container esameorale_container" style="float: left;height: calc(100% - 20px);width: calc(50% - 20px);">
        <?=$data['block']['esameorale']?>
    </div>
</div>