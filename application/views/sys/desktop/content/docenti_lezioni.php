<?php
$recordSource=$data['recordSource'];
?>
<script type="text/javascript">
    $(document).ready(function(){
        /*$('#tabellaRecord').dataTable({
                "bJQueryUI": true,
                "bFilter": false,
                "scrollY": "100",
                "scrollCollapse": false,
                
            });*/
        
        var url=controller_url + "/ajax_load_progetto_didattica";
        $.ajax
        ({
            url: url,
            dataType:'json',
            success:function(data)
            {
                $.each(data['varie'],function(index,value){
                    $('#CognomeDocente').val(value['Cognome']);
                    $('#NomeDocente').val(value['Nome']);
                    $('#Laurea1').append("<option value='" + value['Laurea 1'] + "'>" + value['Laurea 1'] + "</option>");
                    $('#Specializzazione1').append("<option value='" + value['Specializzazione1'] +"'>" + value['Specializzazione1'] + "</option>");
                    $('#MateriaInsegnataUniludes').append("<option value='" + value['Materia Insegnata Uniludes'] + "'>" + value['Materia Insegnata Uniludes'] + "</option>");
                    $('#inquadramento').val(value['Inquadramento']);
                });
                
                $.each(data['QualificheUniversitarie'],function(index,value){
                    $('#qualificaUniversitaria').append("<option value='" + value['Codice'] + "'>" + value['Descrizione'] + "</option>");
                });
            },
            error:function(){alert("ERRORE LOAD PROGETTO DIDATTICA");}
        });    
    });
</script>
<div id="content_docenti_didattica" class="content" style="background-color: #F5F5F5">
    <div class="card scroll" style="width: calc(50% - 20px);height: calc(100% - 20px);float: left;padding: 0px;">
        <?php
        if(count($recordSource)>0)
        {
        ?>
        <table id='tabellaRecord' class="bordered hoverable" style="">
            <thead>
                <tr>
                    <?php 
                    foreach($recordSource[0] as $key => $value)
                    { 
                        if($key!='CodiceRegistroPresenze')
                        {
                        ?>
                            <th><div style="height: 50px;line-height: 50px;overflow: hidden"><?= $key; ?></div></th>
                        <?php 
                        }
                    } 
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php 
                foreach($recordSource as $key => $record) 
                { 
                   
                ?>
                    <tr onclick='lezione_open(this,<?= $record['CodiceCalendarioGenerale']; ?>);'>
                        <?php foreach($record as $chiave=>$valore)
                        { 
                            if($chiave!='CodiceRegistroPresenze')
                            {
                                if($chiave!='Compilato')
                                {
                            ?>
                                <td style="height: 50px;">
                                    <div style="max-height: 100px;line-height: 25px;overflow: hidden;max-width: 250px;"><?= $valore;?></div>
                                </td>
                            <?php 
                                }
                                if($chiave=='Compilato')
                                {
                                    if($valore)
                                    {
                                    ?>
                                    <td style="height: 50px;">
                                        <div style="max-height: 100px;line-height: 25px;overflow: hidden;max-width: 250px;color:green">Compilato</div>
                                    </td>
                                    
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                    <td style="height: 50px;">
                                        <div style="max-height: 100px;line-height: 25px;overflow: hidden;max-width: 250px;color:red">Non compilato</div>
                                    </td>
                                    <?php
                                    }
                                ?>
                                
                                <?php
                                }
                            }
                        } ?>
                    </tr>
                    <?php 
                } ?>
            </tbody>
        </table>
        <?php
        }
        else
        {
        ?>
        <div class="row">
            <div class="col s12">
                Non risultano lezioni al momento
            </div>
        </div>
        
        <?php
        }
        ?>
    </div>
    <div class="card container lezione_container" style="float: left;height: calc(100% - 20px);width: calc(50% - 20px);">
        <?=$data['block']['lezione']?>
    </div>
</div>