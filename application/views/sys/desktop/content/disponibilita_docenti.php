<link rel="stylesheet" href="<?php echo base_url("/assets/fullcalendar/fullcalendar.css") ?>" />
<script type="text/javascript" src="<?php echo base_url('/assets/js/moment.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/jquery.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/jquery-ui.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/fullcalendar/fullcalendar.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/materialize.min.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('select').not(".initialized").material_select();
        <?php
        for ($i = 1; $i <= 12; $i++) {
        ?>
                $('#cal_<?=$i?>').fullCalendar({
                    header: {
                        left:   'title',
                        center: '',
                        right:  ''
                    },
                    defaultDate: '2015-<?=str_pad($i, 2, '0', STR_PAD_LEFT)?>-01',
                    dayRender: function (date, cell) {
                        var today = new Date();
                        if (date.date() == 14) {
                            //cell.css("background-color", "red");
                        }
                    },
                    eventClick: function(calEvent, jsEvent, view)
                    {
                        apri_scheda_record(this,calEvent.tableid,calEvent.recordid,'right','standard_dati','risultati_ricerca');
                    },
                    dayClick: function(date, jsEvent, view) {
                        //alert('Clicked on: ' + date.format());
                        ajax_set_periodo(this,date.format());
                        
                    }
		});
        <?php
        }
        ?>
    });
    
</script>
<div class="content" style="overflow-y: scroll">
    <div class="card" style="border: 1px solid #bcbcbc;padding: 25px; float: left;height: 100%;width: 300px;">
        <form id='form_eventi'>
            <div style="margin-left: 25px;margin-top: 20px;">
                <?=$block['select_docenti']?>
            </div><br/>

            <p>
                <input name="indisponibilita" type="radio" id="indisponibilita_intera" />
                <label for="indisponibilita_intera">Giornata intera</label>
            </p>
            <p>
                <input name="indisponibilita" type="radio" id="indisponibilita_mattina" />
                <label for="indisponibilita_mattina">Mattina</label>
            </p>
            <p>
                <input name="indisponibilita" type="radio" id="indisponibilita_pomeriggio" />
                <label for="indisponibilita_pomeriggio">Pomeriggio</label>
            </p>
            <input id='data_evento' type="hidden" name="data_evento" value="">
            <input id='codice_tipocalendario' type="hidden" name="codice_tipocalendario" value="">
            <div class="clearboth"></div>
        </form>

    </div>   
    <div class="card" style="width: calc(100% - 360px);float: left;">
        <?php
        for ($i = 1; $i <= 12; $i++) {
        ?>
            <div id='cal_<?=$i?>' style="width:calc(50% - 20px); float: left; margin: 10px;background-color: white;"></div>
        <?php
            if(($i==2)||($i==4)||($i==6)||($i==8)||($i==10)||($i==12))
            {
            ?>
            <div class="clearboth"></div>
            <?php
            }
        }
        ?>

    </div>    
</div>


