<?php
$recordSource=$data['recordSource'];
?>
<script type="text/javascript">
    $(document).ready(function(){
         
    });
</script>
<div id="content_docenti_multiplechoice" class="content" style="background-color: #F5F5F5">
    <div id="modal1" class="modal container multiplechoice_domanda_container" style="">
                
    </div>
    <div class="card" style="width: calc(50% - 20px);height: calc(100% - 20px);float: left;padding: 0px;">
        <div class="title">
            Elenco formulari multiplechoice
        </div>
        <div class="fixed-action-btn" style="top: -10px; right: 10px;position: absolute">
        <a class="btn-floating red" onclick="multiplechoice_insert(this);">
              <i class="large material-icons">add</i>
        </a>
        </div>
        <div id="multiplechoice_elenco_container" class="container block_container" style="width: 100%;overflow: scroll;height:calc(100% - 50px)">
        <?=$data['block']['multiplechoice_elenco']?>
        </div>
    </div>
    <div class="card container multiplechoice_container" style="float: left;height: calc(100% - 20px);width: calc(50% - 20px);">
        <?=$data['block']['multiplechoice']?>
    </div>
</div>