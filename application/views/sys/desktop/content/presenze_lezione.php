<script type="text/javascript">
    $(document).ready(function() {
        refresh=true;
        setTimeout(function(){
            if(refresh)
            {
                load_content('ajax_load_content_presenze_lezione');
            }
        },10000);
    });
</script>
<?php
    if(count($registri)>0)
    {
    ?>
<div style='overflow-y: scroll; width: 100%; height: 100%;'>
<?php
    foreach ($registri as $key => $data) {


        $RegistroPresenze=$data['RegistroPresenze'];
        if($RegistroPresenze['DocenteArrivato']){
            $ListaStudenti=$data['ListaStudenti'];
            $facolta=$data['facolta'];
            $corso=$data['corso'];
            $classe=$data['classe'];
            $materia=$data['materia'];
            $StudentiEffettivi=$data['StudentiEffettivi'];
            $attesa_timbrature=$data['attesa_timbrature'];
?>

    <?php if(isset($ListaStudenti)){
        $NumStudentiTeorici=count($ListaStudenti);
        if($NumStudentiTeorici>=0)
        {
    ?>
    <div style="width: 100%;margin-bottom: 10px;">
        <div style="width: 50%;float: left">
            <div><b>Facoltà: </b><?=$facolta[0]['Descrizione'];?></div>
            <div ><b>Corso:</b> <?= $corso[0]['Descrizione']; ?></div>
        </div>
        <div style="width: 50%;float: left">
            <div ><b>Classe: </b><?= $classe[0]['Descrizione']; ?></div>
            <div><b>Materia: </b><?= $materia[0]['DescrizioneITA']; ?></div>
        </div>
        <div class="clearboth"></div>
    </div>
    <div style="margin-bottom: 10px;">
        <div style='width: 30%; float: left;'>
            <br/>
            <b><i>Studenti Previsti <?= $NumStudentiTeorici; ?></i></b><br />
            <b><i>Studenti Effettivi <?= $StudentiEffettivi; ?></i></b>
        </div>
        <?php
        if($attesa_timbrature)
        {
        ?>
            <div style='width: 20%; float: left;'>
                <br/>
                <div class="blink">In attesa delle timbrature <br/>di uscita degli studenti</div>
            </div>
        <?php
        }
        ?>
        <div style='width: 50%; float: left;'>
            <b><i>Legenda timbrature studenti:</i></b><br/>
            <div align='center' style='width: 30%; background-color: #FF1818; float: left; border: 1px solid black;height: 50px;line-height: 50px;color: white;'><b> Mancata timbratura di uscita</b></div>
            <div align='center' style='width: 30%; background-color: #FFB266; float: left; border: 1px solid black;height: 50px;'><b>Ritardo</b><br/><b> Uscita anticipata</b></div>
            <div align='center' style='width: 30%; background-color: #80FF80; float: left; border: 1px solid black;height: 50px;line-height: 50px;'><b>Puntuale</b></div>
        </div>
        <div class="clearboth"></div>
    </div>    
    <hr><br>
    <div style="width: 100%;">
        <?php
            foreach($ListaStudenti as $key => $value){
                $opacity=0.2;
                $colore1="";
                $colore2="";
                if($value['codice_tipoassenza']==9)
                {                           
                    $colore1='#80FF80';
                    $colore2='#80FF80';
                    $opacity=1.0;
                }
                if($value['codice_tipoassenza']==3)
                {                           
                    $colore1='#80FF80';
                    $colore2='#80FF80';
                    $opacity=1.0;
                }
                if($value['codice_tipoassenza']==2)
                {                           
                    $colore1='#FFB266';
                    $colore2='#80FF80';
                    $opacity=1.0;
                }
                if($value['codice_tipoassenza']==5)
                {                           
                    $colore1='#80FF80';
                    $colore2='#FFB266';
                    $opacity=1.0;
                }
                if($value['codice_tipoassenza']==8)
                {                           
                    $colore1='#FF1818';
                    $colore2='#FF1818';
                    $opacity=1.0;
                }
                    
        ?>
        <div style="border: 1px solid grey; margin-left: 5px; margin-right: 0px; margin-top: 5px; margin-bottom: 0px; padding:0px; width: 100px; height: 160px; float: left;">
            <div style="width: 100%; height: 70%; "><img src="<?= $value['Path_Foto']; ?>" style="opacity: <?=$opacity; ?>; margin: auto; height: 100%; width: 100%; display: block;" /></div>
            <div style=" height: 30%; width: 100%; overflow: hidden;font-size: 14px;position: relative">
                <div style="position: absolute;top: 0px;left: 0px;height: 100%;width: 100%;padding: 2px;">
                    <b><i><span style="font-size: 12px;"><?=$value['Cognome']?></span></i></b><br/>
                    <b><i><?=$value['Nome']; ?></i></b>
                </div>
                <div style="width: 50%;height: 100%;background-color: <?=$colore1;?>;float: left">
                    
                </div>
                <div style="width: 50%;height: 100%;background-color: <?=$colore2;?>;float: right">
                    
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
    <?php }
        else
            echo "Non ci sono studenti";
    }
        else
            echo "Non ci sono studenti";
    ?>
<?php 
        } else { 
?>

    <div style="width: 100%; background-color: orange; border: 1px solid black; margin-top: 200px;"><h1 align="center">È NECESSARIO TIMBRARE PRIMA DI VEDERE LA LISTA STUDENTI</h1></div>
    <?php }
    
    }
?>
</div>
<?php
    } else { ?>
<div style="width: 100%; background-color: orange; border: 1px solid black; margin-top: 200px;"><h1 align="center">NON SONO PREVISTI EVENTI IN QUESTO ORARIO</h1></div>
    <?php } ?>

