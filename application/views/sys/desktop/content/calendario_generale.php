<script type="text/javascript" src="<?php echo base_url('/assets/js/moment.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/jquery.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/jquery-ui.js') ?>"></script>
<?php
$today=date('Y-m-d');
?>
<script type="text/javascript" src="<?php echo base_url('/assets/fullcalendar/fullcalendar.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/materialize.min.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('select').not(".initialized").material_select();
        load_block_calendario_generale();
    });
</script>
<div id="modal1" class="modal  container lezione_container" style="">
                
            </div>
<div id="content_docenti_calendario" class="content" style="overflow: scroll;position: relative">
    <div class="fixed-action-btn" style="top: -10px; left: 10px;position: absolute;height: 50px;width: 50px;">
        <a class="btn-floating waves-effect waves-light red" onclick="$('#filtri_calendari').toggle('slide');">
              <i class="large material-icons">search</i>
        </a>
    </div>
    
    <div class="fixed-action-btn horizontal" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large red">
      <i class="large material-icons">mode_edit</i>
    </a>
        <ul style="font-size: 12px;">
        <!--<li><a class="btn-floating waves-effect waves-light blue" onclick="lezione_open_popup(this,null)">Lez</a></li>
        <li><a class="btn-floating waves-effect waves-light red">Esam</a></li>
        <li><a class="btn-floating waves-effect waves-light green">Even</a></li>-->
        <li><a class="btn-floating waves-effect waves-light " onclick="appuntamento_open_popup(this,null)">App</a></li>
        <li><a class="btn-floating waves-effect waves-light " onclick="dipendente_open_popup(this,null)">Dip</a></li>
    </ul>
  </div>
        
    <div id="filtri_calendari" class="card" style="float: left;display: none;padding-left: 50px;overflow: visible;width: 98%">
    <form id="form_filtri_calendari">
        <h5>Filtri</h5>
        <div class="row">
            <div class="col s6">
                <div class="row">
                    <div class="col s6">
                        <div class="row">
                            <div class="col s6">
                                <div class="input-field" style="width: 100px;">
                                    <select id="anno" name="anno" id="mese" style="margin-top: 5px;">
                                        <option value="2009">2009</option>
                                        <option value="2010">2010</option>
                                        <option value="2011">2011</option>
                                        <option value="2012">2012</option>
                                        <option value="2013">2013</option>
                                        <option value="2014">2014</option>
                                        <option value="2015">2015</option>
                                        <option value="2016" selected>2016</option>
                                        <option value="2015">2017</option>
                                    </select>
                                    <label for="anno" >Anno</label>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col s12">
                                <div class="input-field">
                                    <select id="mese" name="mese" id="mese" style="margin-top: 5px;">
                                        <option value="01">Gennaio</option>
                                        <option value="02">Febbraio</option>
                                        <option value="03">Marzo</option>
                                        <option value="04">Aprile</option>
                                        <option value="05">Maggio</option>
                                        <option value="06">Giugno</option>
                                        <option value="07">Luglio</option>
                                        <option value="08">Agosto</option>
                                        <option value="09">Settembre</option>
                                        <option value="10" selected>Ottobre</option>
                                        <option value="11">Novembre</option>
                                        <option value="12">Dicembre</option>
                                    </select>
                                    <label for="mese">Mese</label>
                                 </div>
                            </div>
                        </div>
                    </div>
                    <div class="col s6">
                        <div class="row">
                            <div class="col s12">
                                <div class="input-field">
                                    <select id='filtri_annoaccademico' name="annoaccademico" onchange="annoaccademico_changed(this)" style="margin-top: 5px;">
                                        <option></option>
                                        <?php
                                        foreach ($anniaccademici as $key => $annoaccademico) {
                                        ?>
                                        <option value="<?=$annoaccademico['Codice']?>"><?=$annoaccademico['Descrizione']?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                    <label for="filtri_annoaccademico">Anno accademico</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <b>Classi</b>:<br/>
                                <div id="checklist_classi_container" >

                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="col s4">
                <div class="row">
                    <div class="col s12">
                        <?php  
                        echo generate_select("docente", "Docente",$options_docenti, null,'edit');
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <?php  
                        $options_tipo_calendario[]=array("Codice"=>"1","Descrizione"=>"Lezione");
                        $options_tipo_calendario[]=array("Codice"=>"2","Descrizione"=>"Esame");
                        echo generate_select("CodiceTipoCalendario", "Tipo calendario",$options_tipo_calendario, null,'edit');
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s3">
                <div class="btn waves-effect waves-light" onclick="load_block_calendario_generale_filtrato();$('#filtri_calendari').toggle('slide');">Filtra</div>
                <br/><br/>
            </div>
        </div>
    </form>
</div>
        <div class="row">
            <div class="col s12">
                <div id="block_calendario_generale_container" class="card" style="padding: 20px;">
                    
                </div>
            </div>
        </div>
</div>


