<?php
$recordSource=$data['recordSource'];
?>
<script type="text/javascript">
    $(document).ready(function(){
         
    });
</script>
<div id="content_docenti_domandeaperte" class="content" style="background-color: #F5F5F5">
    <div class="card" style="width: calc(50% - 20px);height: calc(100% - 20px);float: left;padding: 0px;">
        <div class="title">
            Elenco formulari domande aperte
        </div>
        <div class="fixed-action-btn" style="top: -10px; right: 10px;position: absolute">
        <a class="btn-floating red" onclick="domandeaperte_insert(this);">
              <i class="large material-icons">add</i>
        </a>
        </div>
        <div id="domandeaperte_elenco_container" class="container block_container" style="width: 100%;overflow: scroll;height:calc(100% - 50px)">
        <?=$data['block']['domandeaperte_elenco']?>
        </div>
    </div>
    <div class="card container domandeaperte_container" style="float: left;height: calc(100% - 20px);width: calc(50% - 20px);">
        <?=$data['block']['domandeaperte']?>
    </div>
</div>