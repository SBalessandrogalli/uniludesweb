<?php
$recordSource=$data['recordSource'];
?>
<script type="text/javascript" src="<?php echo base_url('/assets/js/materialize.min.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        
    });
</script>
<div id="content_argomenti" class="content" style="background-color: #F5F5F5">
    <div class="card scroll" style="width: 90%;height: calc(100% - 20px);float: left;padding: 0px;">
        <?php
        if(count($recordSource)>0)
        {
        ?>
        <table id='tabellaRecord' class="bordered hoverable" style="">
            <thead>
                <tr>
                    <?php 
                    foreach($recordSource[0] as $key => $value)
                    { 
                        if($key!='Codice')
                        {
                        ?>
                            <th><div style="height: 50px;line-height: 50px;overflow: hidden"><?= $key; ?></div></th>
                        <?php 
                        }
                    } 
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php 
                foreach($recordSource as $key => $value) 
                { 
                   
                ?>
                    <tr >
                        <?php foreach($value as $chiave=>$valore)
                        { 
                            if($chiave!='Codice')
                            {
                            ?>
                                <td style="height: 50px;">
                                    <div style="max-height: 100px;line-height: 25px;overflow: hidden;max-width: 250px;"><?= $valore;?></div>
                                </td>
                            <?php 
                            }
                        } ?>
                    </tr>
                    <?php 
                } ?>
            </tbody>
        </table>
        <?php
        }
        else
        {
        ?>
        <div class="row">
            <div class="col s12">
                Non risultano argomenti
            </div>
        </div>
        
        <?php
        }
        ?>
    </div>
</div>