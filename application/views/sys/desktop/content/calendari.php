<?php
$block_calendari=$data['block']['calendari'];
$block_select_docenti=$data['block']['select_docenti'];
$block_select_materie=$data['block']['select_materie'];
?>
<script type="text/javascript" src="<?php echo base_url('/assets/js/materialize.min.js') ?>"></script>
<script type="text/javascript">
    $('#block_calendari').ready(function(){
        $('select').not(".initialized").material_select();
    });
    $('#tipoevento').change(function(){
            //$('.parametri_pianificazione').hide();
            var tipoevento=$(this).val();
             //$('#parametri_pianificazione_'+tipoevento).show();
             //ajax_load_block('fields_calendario_generale_registropresenza',null,$('#parametri_pianificazione_container'));
             $('#modal1').html("");
             if(tipoevento==1)
             {
                 ajax_load_block('parametri_pianificazione_lezione',null,$('#parametri_pianificazione_container'));
             }
             if(tipoevento==2)
             {
                 ajax_load_block('parametri_pianificazione_esame',null,$('#parametri_pianificazione_container'));
             }
             if(tipoevento==3)
             {
                 ajax_load_block('parametri_pianificazione_evento',null,$('#parametri_pianificazione_container'));
             }
             
        })
    $('#content_calendari').find('#materia').change(function(){
        //pianificazione_materia_changed(this);
    })

</script>
<div id="content_calendari" class="content" style="background-color: #F5F5F5;position: relative">
    <div id="modal1" class="modal  container lezione_container" style="">
                
            </div>
    <div class="fixed-action-btn" style="top: -10px; left: 10px;position: absolute;height: 50px;width: 50px;">
        <a class="btn-floating waves-effect waves-light red" onclick="$('#block_calendari_container').hide();$('#filtri_calendari').toggle('slide');">
              <i class="large material-icons">search</i>
        </a>
    </div>
    <form id="form_pianificazione" class="form_pianificazione">
        <div id="filtri_calendari" class="card" style="float: left;height: 95%;width: calc(40%);padding-top: 40px;">
            <div class="row" style="height: calc(100% - 100px)">
                
                            <div class="col s12">
                                <div class="row">
                <div class="col s12">
                        <?php
                        $anni[]=array("Codice"=>"2015","Descrizione"=>"2015");
                        $anni[]=array("Codice"=>"2016","Descrizione"=>"2016");
                        $anni[]=array("Codice"=>"2017","Descrizione"=>"2017");
                        $anni[]=array("Codice"=>"2018","Descrizione"=>"2018");
                        $anni[]=array("Codice"=>"2019","Descrizione"=>"2019");
                        $anni[]=array("Codice"=>"2020","Descrizione"=>"2020");
                        $anni[]=array("Codice"=>"2021","Descrizione"=>"2021");
                        $anni[]=array("Codice"=>"2022","Descrizione"=>"2022");
                        $anni[]=array("Codice"=>"2023","Descrizione"=>"2023");
                        ?>
                        <div class="row">
                            <script type="text/javascript">
                                $('#anno').change(function(){
                                    annoaccademico_changed(this);
                                })
                                $('#anno').change();
                            </script>
                            <?php
                            $current_year=date("Y");
                            generate_select('anno', 'Anno', $anni, $current_year, 'edit');
                            ?>
                        </div>
                        
                </div>
            </div>
                                <div class="row">
                <div class="col s12">
                        <?php
                        $mesi[]=array("Codice"=>"01","Descrizione"=>"Gennaio");
                        $mesi[]=array("Codice"=>"02","Descrizione"=>"Febbraio");
                        $mesi[]=array("Codice"=>"03","Descrizione"=>"Marzo");
                        $mesi[]=array("Codice"=>"04","Descrizione"=>"Aprile");
                        $mesi[]=array("Codice"=>"05","Descrizione"=>"Maggio");
                        $mesi[]=array("Codice"=>"06","Descrizione"=>"Giugno");
                        $mesi[]=array("Codice"=>"07","Descrizione"=>"Luglio");
                        $mesi[]=array("Codice"=>"08","Descrizione"=>"Agosto");
                        $mesi[]=array("Codice"=>"09","Descrizione"=>"Settembre");
                        $mesi[]=array("Codice"=>"10","Descrizione"=>"Ottobre");
                        $mesi[]=array("Codice"=>"11","Descrizione"=>"Novembre");
                        $mesi[]=array("Codice"=>"12","Descrizione"=>"Dicembre");
                        //TODO mettere dinamico il mese corrente
                        
                        ?>
                        <div class="row">
                            <script type="text/javascript">
                                $('#mese').change(function(){
                                    annoaccademico_changed(this);
                                })
                                $('#mese').change();
                            </script>
                            <?php
                            $current_month=date("m");
                            generate_select('mese', 'Mese', $mesi, $current_month, 'edit');
                            ?>
                        </div>
                </div>
            </div>
                                <div class="row">
                                    <div class="col s12">
                                        <!--<div class="">
                                            <?=  generate_select('facolta', 'Facoltà', $facolta_options,null,'edit')?>
                                        </div>    
                                        <div class="row">
                                            <?=  generate_select('corso', 'Corso', $corso_options,null,'edit')?>
                                        </div>-->
                                        <!--<div class="row">
                                            <script type="text/javascript">
                                                $('#annoaccademico').change(function(){
                                                    annoaccademico_changed(this);
                                                })
                                                $('#annoaccademico').change();
                                            </script>
                                            <?=  generate_select('annoaccademico', 'Anno accademico', $data['anniaccademici'],7,'edit')?>
                                        </div>-->
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s12">
                                        <label for="checklist_classi_container">Classi</label>
                                        <div id="checklist_classi_container" style="height: 70%;overflow-y: scroll;">

                                        </div>
                                    </div>
                                </div>
                                    
                            </div>


                </div>
                <div class="row">
                    <div class="col s3">
                        <div id="btn_inizia" class="btn waves-effect waves-light" onclick="load_block_calendari(this);" data-iniziato=false>Inizia pianificazione</div>
                        <br/><br/>
                    </div>
                </div>


        </div>

        <div class="pianificazione card" style="width: 400px; height:95%;  float: left;padding-top: 40px;">
                        
                        <div  class="row">
                            <div class="col s12">
                                <div id="select_tipoevento_container">
                                    <?php
                                    $tipieventi[]=array("Codice"=>"1","Descrizione"=>"Lezione");
                                    $tipieventi[]=array("Codice"=>"2","Descrizione"=>"Esame");
                                    $tipieventi[]=array("Codice"=>"3","Descrizione"=>"Evento");
                                    generate_select('tipoevento', 'Tipo evento', $tipieventi,'lezione','edit');
                                    ?>
                                </div>
                            </div>
                        </div>

            <div id="parametri_pianificazione_container" style="height: calc( 100% - 230px);">
            </div>

                        





                        
            
                        <input type="hidden" name="start" id="start" class="start">
                        <input type="hidden" name="end" id="end" class="end">
                        <input type="hidden" name="calendario_generale[CodiceClasse]" id="CodiceClasse" class="classe" value="">
                        

        </div>
            
    </form>
    <div id="block_calendari_container" class="" style="height: 100%;width: calc(100% - 450px);float: left;">


    </div>
</div>
