<script type="text/javascript" src="<?php echo base_url('/assets/js/moment.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/jquery.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/jquery-ui.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/fullcalendar/fullcalendar.js') ?>"></script>

<div class="row">
    <div class="col s4">
        <h5>Riepilogo lezioni del docente: Galli</h5>
    </div>
</div>

<div class="row">
    <div class="col s12">
        <table id='tabellaRecord' class="bordered striped ">
            <thead>
                <tr>
                    <th><div style="height: 50px;line-height: 50px;overflow: hidden">Data</div></th>
                    <th><div style="height: 50px;line-height: 50px;overflow: hidden">Ora inizio</div></th>
                    <th><div style="height: 50px;line-height: 50px;overflow: hidden">Ora fine</div></th>
                    <th><div style="height: 50px;line-height: 50px;overflow: hidden">Descrizione</div></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($lezioni as $key => $lezione) 
                {
                ?>
                <tr>
                    <td style="height: 50px;"><?=$lezione['Giorno']?></td>
                    <td style="height: 50px;"><?=$lezione['OraInizio']?></td>
                    <td style="height: 50px;"><?=$lezione['OraFine']?></td>
                    <td style="height: 50px;"><?=$lezione['ArgomentoDescrizione']?></td>
                </tr>
                <?php
                }
                ?>
                
            </tbody>
        </table>
    </div>
</div>
