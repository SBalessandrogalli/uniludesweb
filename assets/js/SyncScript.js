function get_new_remote_records()
{
    $.ajax({
        
                url: sync_controller_url+'get_new_remote_records',
                type: "url",
                success:function(data){
                    alert('nuovi record scaricati');
                    $('#modal1').html(data);
                    $('#modal1').openModal();
                },
                error:function(){
                    alert('errore');
                }
            });
}

function get_local_logquery()
{
    $.ajax({
        
                url: sync_controller_url+'get_local_logquery',
                type: "url",
                success:function(data){
                    $('#modal1').html(data);
                    $('#modal1').openModal();
                },
                error:function(){
                    alert('errore');
                }
            });
}

function reset_logquery()
{
    $.ajax({
        
                url: sync_controller_url+'reset_logquery',
                type: "url",
                success:function(data){
                    alert('logquery resettato');
                },
                error:function(){
                    alert('errore');
                }
            });
}

function reset_recordstatus()
{
    $.ajax({
        
                url: sync_controller_url+'reset_record_status',
                type: "url",
                success:function(data){
                    alert('record status resettati');
                },
                error:function(){
                    alert('errore');
                }
            });
}

function upload_sync_tables()
{
    $.ajax({
        
                url: sync_controller_url+'upload_sync_tables',
                type: "url",
                success:function(data){
                    alert('tabelle da sincronizzare caricate');
                    $('#modal1').html(data);
                    $('#modal1').openModal();
                },
                error:function(){
                    alert('errore');
                }
            });
}

function download_sync_tables()
{
    $.ajax({
        
                url: sync_controller_url+'download_sync_tables',
                type: "url",
                success:function(data){
                    alert('tabelle da sincronizzare scaricate');
                    $('#modal1').html(data);
                    $('#modal1').openModal();
                },
                error:function(){
                    alert('errore');
                }
            });
}

function sync_tables()
{
    $.ajax({
        
                url: sync_controller_url+'sync_tables',
                type: "url",
                success:function(data){
                    alert('tabelle sincronizzate');
                    $('#modal1').html(data);
                    $('#modal1').openModal();
                },
                error:function(){
                    alert('errore');
                }
            });
}