function login(el)
{
    $.ajax({
        
                url: controller_url+'login',
                type: "POST",
                data: $('#login_form').serialize(),
                success:function(data){
                    location.reload();
                                    
                },
                error:function(){
                    alert('errore');
                }
            });
}

function ajax_load_block(block_name,arg,target)
{
    $.ajax({
        
                url: controller_url+'ajax_load_block/'+block_name,
                type: "POST",
                data: arg,
                success:function(data){
                    $(target).html(data);
                },
                error:function(){
                    alert('error load block '+block_name);
                }
            });
}

function clickMenu(el,funzione){
    refresh=false;
    $('.ui-tooltip').hide();
    $('.submenu_btn').removeClass('selected');
    $(el).addClass('selected');
    $('.topmenu_btn').each(function(i){
        if($(el).attr('id')=='btn_home')
        {
            $('.submenu').hide();
            $(this).show('slide');
        }
        else
        {
            if($(this).attr('id')!='btn_home')
            {
                $(this).hide('slide');
            }
        }
    });
    setTimeout(function(){
        var next=$(el).next();
        if($(next).hasClass('submenu'))
        {
            $(next).show("slide",function(){
            load_content(funzione);
            },500);
        }
        else
        {
            load_content(funzione);
        }
    },500);
    //load_content(funzione)
    //$('#content_container').fadeOut();
    ;
    
    
}

function signout()
{
    $.ajax({
        
                url: controller_url+'logout',
                type: "html",
                success:function(data){
                    location.reload();
                },
                error:function(){
                    alert('errore');
                }
            });
    //var url= controller_url+'logout';
    //window.location.replace(url);
}

function load_content(funzione)
{
    if(funzione!='')
    {
     $.ajax({
        
                url: controller_url+funzione,
                type: "POST",
                data: $('#form_filtri_calendari').serialize(),
                success:function(data){
                    if(data.replace(/\s+/, "")!='logout')
                    {
                        $('#content_container').css('display', 'none');
                        $('#content_container').html('');
                        $('#content_container').html(data);
                        $('#content_container').css('display', 'block'); 
                    }
                    else
                    {
                        signout();
                    }
                                    
                },
                error:function(){
                    alert('errore');
                }
            });
        }
}

function ajax_load_block_calendario_classe(el,codice_classe)
{
    $.ajax({
        
                url: controller_url+'ajax_load_block_calendario/'+codice_classe,
                type: "POST",
                data: $('#form_pianificazione').serialize(),
                success:function(data){
                    //$('#content_container').css('display', 'none')
                    var container=$(el).closest('.blocco').find('.container');
                    $(container).html('');
                    $(container).html(data);
                },
                error:function(){
                    alert('errore');
                }
            });
}

function annoaccademico_changed(el)
{
    var value=0;
    var form=$(el).closest('form');
    $.ajax({
        
        url: controller_url+'ajax_load_block_checklist_classi'+'/'+value,
        type: "POST",
        data: $(form).serialize(),
        success:function(data){
            $(form).find('#checklist_classi_container').html(data);
        },
        error:function(){
            alert('errore');
        }
    });

}

function docente_changed(el)
{
    var container=$(el).closest('.container');
    if($(container).data('contesto')=='pianificazione')
    {
        change_testo_timbro();
        if($('#btn_inizia').data('iniziato'))
        {
            load_block_calendari(el);
        } 
    }
    
    /*var value=$(el).val();
    var form=$(el).closest('form');
    $.ajax({
        
        url: controller_url+'ajax_load_block_select_materie'+'/'+value,
        dataType:'html',
        success:function(data){
            console.info(data);
            //$(form).find('#select_materie_container').html(data);
            change_testo_timbro();
            if($('#btn_inizia').data('iniziato'))
            {
                load_block_calendari(el);
            }
            
        },
        error:function(){
            alert('errore');
        }
    });*/
}

function materia_changed(el)
{
    var container=$(el).closest('.container');
    if($(container).data('contesto')=='pianificazione')
    {
        change_testo_timbro();
        if($('#btn_inizia').data('iniziato'))
        {
            load_block_calendari(el);
        }
    }
}


function change_testo_timbro()
{
    testo_timbro='';
    var content=$('#timbro_evento').closest('.content');
    var tipoevento=$('#tipoevento').val();
    var docenti='';
    if((tipoevento==1)||(tipoevento==2))
    {
        //var docente1=$('#dati_lezione_CodiceDocente1_ option:selected').text();
        $('[id*="docenti"]').each(function(i) {
                var docente=$(this).find('option:selected').text();
                docenti=docenti+' '+docente;
            }
        )
        var selected_materia=$("#calendario_generale_CodiceMateria_ option:selected");
        var descrizioneita=$(selected_materia).data('descrizioneita');
        var descrizioneeng=$(selected_materia).data('descrizioneeng');
        var sigla=$(selected_materia).data('sigla');
        var testo_timbro=descrizioneita+' D:'+docenti;
        if(descrizioneita!=descrizioneeng)
        {
            var argomentodescrizione=descrizioneeng+' '+sigla+' '+descrizioneita+' D:'+docenti;
        }
        var argomentodescrizione=descrizioneeng+' '+sigla+' D:'+docenti;
        $('#calendario_generale_ArgomentoDescrizione_').val(argomentodescrizione);
    }
    /*if(tipoevento==2)
    {
        var docente1=$('#dati_esame_CodiceDocente1_ option:selected').text();
        var selected_materia=$("#dati_esame_CodiceMateria_ option:selected");
        var descrizioneita=$(selected_materia).data('descrizioneita');
        var descrizioneeng=$(selected_materia).data('descrizioneeng');
        var sigla=$(selected_materia).data('sigla');
        var testo_timbro=descrizioneita+' D:'+docente1;
        var argomentodescrizione=descrizioneeng+' '+sigla+'-'+descrizioneita+' D:'+docente1;
        $('#ArgomentoDescrizione').val(argomentodescrizione);
    } */  
    if(tipoevento==3)
    {
        var testo_timbro=$('#calendario_generale_Note_').val();
    }
    
    $('#timbro_evento').html(testo_timbro);
    var durata=$('#durata').val();
    var ore=parseInt(durata);
    var minuti_decimale=durata-ore;
    var minuti=60*minuti_decimale;
    $('#timbro_evento').data('event', {title:testo_timbro,duration:ore+":"+minuti,backgroundColor:$('#timbro_evento').css('background-color')});
}

function pianificazione_ore_changed(el)
{
    /*var calendario=$(el).closest('.calendario');
    var docente=$(calendario).find('#docente option:selected').text();
    var materia=$(calendario).find('#materia option:selected').text();
    var ore=$('#pianificazione_ore').val();
    var testo_timbro=materia+"<br/>"+docente+"<br/>"+ore+" ore";
    $('#timbro_evento').html(testo_timbro);*/
    //$('#timbro_evento').data('event','{"title":"'+materia+'"}');
}


function load_block_calendari(el)
{
    
    $.ajax({
                url: controller_url+"ajax_load_block_calendari",
                type: "POST",
                data: $('#form_pianificazione').serialize(),
                success:function(data){
                    
                        $('#filtri_calendari').hide(500);
                        $('#block_calendari_container').css('display', 'none');
                        $('#block_calendari_container').html('');
                        $('#block_calendari_container').html(data);
                        $('#block_calendari_container').css('display', 'block'); 
                        $('#block_calendari_container').show();
                        $('#btn_inizia').data('iniziato',true);

                                    
                },
                error:function(){
                    alert('errore');
                }
            });
}

function load_block_calendario_generale(el)
{
    
    $.ajax({
                url: controller_url+"ajax_load_block_calendario_generale",
                type: "POST",
                data: $('#form_filtri_calendari').serialize(),
                success:function(data){
                        $('#block_calendario_generale_container').html(data);
                },
                error:function(){
                    alert('errore');
                }
            });
}

function load_block_calendario_generale_filtrato(el)
{
    
    $.ajax({
                url: controller_url+"ajax_load_block_calendario_generale_filtrato",
                type: "POST",
                data: $('#form_filtri_calendari').serialize(),
                success:function(data){
                        $('#block_calendario_generale_container').html(data);
                },
                error:function(){
                    alert('errore');
                }
            });
}

function ajax_set_evento(el,event)
{
    var calendario=$(el).closest('.calendario');
    var idclasse=$(calendario).data('idclasse');
    var form_pianificazione=$('#form_pianificazione');
    $(form_pianificazione).find('.start').val(event.start);
    $(form_pianificazione).find('.end').val(event.end);
    $(form_pianificazione).find('#CodiceClasse').val(idclasse);
    $.ajax({
        
                url: controller_url+'ajax_pianificazione_calendario',
                type: "POST",
                data: $(form_pianificazione).serialize(),
                success:function(data){
                    var color=$('.checked').data('color');
                    $(el).css('background-color', color);
                },
                error:function(){
                    alert('errore');
                }
            });
}

function ajax_change_evento(el,event,delta,revertFunc)
{
    var Codice=event.Codice;
    var CodiceRegistroPresenze=event.CodiceRegistroPresenze;
    var NuovoData=event.start.format("YYYY-MM-DD");
    var NuovoOraInizio=event.start.format("HH:mm:ss");
    var NuovoOraFine = event.end.format("HH:mm:ss");
    
    var url=controller_url+"/ajax_change_calendario";
    $.ajax({
       url: url,
       type: 'post',
       data: "Codice=" + Codice + "&CodiceRegistroPresenze=" + CodiceRegistroPresenze + "&Giorno=" + NuovoData + "&OraInizio=" + NuovoOraInizio + "&OraFine=" + NuovoOraFine,
       dataType:'text',
       success:function()
       {
          //alert("aggiornamento eseguito"); 
       },
       error:function()
       {
           revertFunc();
       }
    }); 
}

function ajax_set_periodo(el,data,jsEvent)
{
    console.info(jsEvent);
    $('#Giorno').val(data);
    var tr=$(el).closest('tr');
    var counter=0;
    $(tr).find('td').each(function(i){
        
        if($(this).is($(el)))
        {
            var fcrow=$(el).closest('.fc-row');
            $(fcrow).find('.fc-bgevent-skeleton').each(function(i){
                var td=$(this).find('td:first');
                if(!$(td).hasClass('fc-bgevent'))
                {
                    if($(td).prop("colspan")==counter)
                    {
                        $(td).closest('.fc-bgevent-skeleton').remove();
                    }
                }
                else
                {
                    if(counter==0)
                    {
                        $(td).closest('.fc-bgevent-skeleton').remove();
                    }
                }
            });
            
        }
        counter++;
        
    });
    $.ajax({
        
                url: controller_url+'ajax_set_periodo',
                type: "POST",
                data: $('#form_periodi').serialize(),
                success:function(data){
                    var color=$('.checked').data('color');
                    $(el).css('background-color', color);
                },
                error:function(){
                    alert('errore');
                }
            });
}

function ajax_set_indisponibilita(el,data)
{
    $('#data_evento').val(data);
    $.ajax({
        
                url: controller_url+'ajax_set_indisponibilita',
                type: "POST",
                data: $('#form_eventi').serialize(),
                success:function(data){
                    var indisponibilita=$("input[type='radio'][name='indisponibilita']:checked").val();
                    if(indisponibilita=='mezza')
                    {
                        $(el).css('background-color', 'orange');
                    }
                    if(indisponibilita=='intera')
                    {
                        $(el).css('background-color', 'red');
                    }
                },
                error:function(){
                    alert('errore');
                }
            });
}

/**
 * 
 * @param {type} el
 * @returns {undefined}
 * @author Alessandro Galli
 * 
 * Ricarica il progetto passandolo in modalità di modifica
 */
function progettodidattica_open(el,codice_progettodidattica)
{
    var container=$('.progettodidattica_container');
    $.ajax({
        url: controller_url+'ajax_load_block_progettodidattica/'+codice_progettodidattica+'/view',
        type: "html",
        success:function(data){
            $(container).html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

function domandeaperte_open(el,codice_domandaaperta)
{
    var container=$('.domandeaperte_container');
    //$(container).html("<b>PROVOLA</b>");
    $.ajax({
        url: controller_url+'ajax_load_block_docenti_domande_aperte/'+ codice_domandaaperta + '/view',
        type: "html",
        success:function(data){
            $(container).html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

function esameorale_open(el,codice_esameorale)
{
    var container=$('.esameorale_container');
    //$(container).html("<b>PROVOLA</b>");
    $.ajax({
        url: controller_url+'ajax_load_block_esameorale/'+ codice_esameorale + '/view',
        type: "html",
        success:function(data){
            $(container).html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

function domandeaperte_insert(el)
{
    var container=$('.domandeaperte_container');
    //$(container).html("<b>PROVOLA</b>");
    $.ajax({
        url: controller_url+'ajax_load_block_docenti_domande_aperte/null/edit',
        type: "html",
        success:function(data){
            $(container).html(data);
            domandeaperte_elenco_refresh();
        },
        error:function(){
            alert('errore');
        }
    });
}

function domandeaperte_edit(el,codice_domandeaperte)
{
    var container=$(el).closest('.container');
    $.ajax({
        url: controller_url+'ajax_load_block_docenti_domande_aperte/' + codice_domandeaperte + '/edit',
        type: "html",
        success:function(data){
            $(container).html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

function domandeaperte_delete(el,codice_domandeaperte)
{
    var r = confirm("Procedere con l'eliminazione del formulario?");
    if (r == true) {
        var container=$(el).closest('.container');
        $.ajax({
            url: controller_url+'ajax_domandeaperte_delete/' + codice_domandeaperte,
            type: "html",
            success:function(data){
                $(container).html('');
                domandeaperte_elenco_refresh();
            },
            error:function(){
                alert('errore');
            }
        });
    }
    
}

/**
 * @author Alessandro Galli
 * salva le moifiche alle domande aperte
 */
function domandeaperte_save(el,codice_domandeaperte)
{
    var container=$(el).closest('.container');
    var domandeaperte=$(el).closest('.domandeaperte');
    var form_save=$('#form_save');
    $(form_save).html();
    $(domandeaperte).find('.input-field').each(function(i) {
        if(!$(this).prop('disabled'))
        {
           $(form_save).append(this); 
        }
        
    });
    $.ajax({
        url: controller_url+'ajax_domandeaperte_save/'+codice_domandeaperte,
        type: "POST",
        data: $('#form_save').serialize(),
        success:function(data){
            $(container).html('');
            $(container).html(data);
            domandeaperte_elenco_refresh();
        },
        error:function(){
            alert('errore');
        }
    });
}

function domandeaperte_duplica(el,codice_domandeaperte)
{
    var container=$(el).closest('.container');
    $.ajax({
        url: controller_url+'ajax_domandeaperte_duplica/' + codice_domandeaperte,
        type: "html",
        success:function(data){
            $(container).html(data);
            domandeaperte_elenco_refresh();
        },
        error:function(){
            alert('errore');
        }
    });
}

function domandeaperte_elenco_refresh()
{
    var container=$('#domandeaperte_elenco_container');
    $.ajax({
        url: controller_url+'ajax_load_block_domandeaperte_elenco',
        type: "html",
        success:function(data){
            $(container).html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

function multiplechoice_open(el,codice_multiplechoice)
{
    var container=$('.multiplechoice_container');
    $.ajax({
        url: controller_url+'ajax_load_block_multiplechoice/'+ codice_multiplechoice + '/view',
        type: "html",
        success:function(data){
            $(container).html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

function multiplechoice_insert(el)
{
    var container=$('.multiplechoice_container');
    //$(container).html("<b>PROVOLA</b>");
    $.ajax({
        url: controller_url+'ajax_load_block_multiplechoice/null/edit',
        type: "html",
        success:function(data){
            $(container).html(data);
        },
        error:function(){ 
            alert('errore');
        }
    });
}

function multiplechoice_edit(el,codice_multiplechoice)
{
    var container=$(el).closest('.container');
    $.ajax({
        url: controller_url+'ajax_load_block_multiplechoice/' + codice_multiplechoice + '/edit',
        type: "html",
        success:function(data){
            $(container).html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

function multiplechoice_save(el,codice_multiplechoice)
{
    var container=$(el).closest('.container');
    var multiplechoice=$(el).closest('.multiplechoice');
    var form_save=$('#form_save');
    $(form_save).html();
    $(multiplechoice).find('.input-field').each(function(i) {
        if(!$(this).prop('disabled'))
        {
           $(form_save).append(this); 
        }
        
    });
    $.ajax({
        url: controller_url+'ajax_multiplechoice_save/'+codice_multiplechoice,
        type: "POST",
        data: $('#form_save').serialize(),
        success:function(data){
            $(container).html('');
            $(container).html(data);
            multiplechoice_elenco_refresh();
        },
        error:function(){
            alert('errore');
        }
    });
}

function multiplechoice_delete(el,codice_multiplechoice)
{
    var r = confirm("Procedere con l'eliminazione del formulario?");
    if (r == true) {
        var container=$(el).closest('.container');
        $.ajax({
            url: controller_url+'ajax_multiplechoice_delete/' + codice_multiplechoice,
            type: "html",
            success:function(data){
                $(container).html('');
                multiplechoice_elenco_refresh();
            },
            error:function(){
                alert('errore');
            }
        });
    }
    
}

function multiplechoice_domanda_delete(el,codice_multiplechoice_domanda)
{
    var r = confirm("Procedere con l'eliminazione del formulario?");
    if (r == true) {
        var container=$(el).closest('.container');
        $.ajax({
            url: controller_url+'ajax_multiplechoice_domanda_delete/' + codice_multiplechoice_domanda,
            type: "html",
            success:function(data){
                multiplechoice_domande_elenco_refresh();
                $('#modal1').closeModal();
            },
            error:function(){
                alert('errore');
            }
        });
    }
    
}

function multiplechoice_duplica(el,codice_multiplechoice)
{
    var container=$(el).closest('.container');
    $.ajax({
        url: controller_url+'ajax_multiplechoice_duplica/' + codice_multiplechoice,
        type: "html",
        success:function(data){
            $(container).html(data);
            multiplechoice_elenco_refresh();
        },
        error:function(){
            alert('errore');
        }
    });
}

function multiplechoice_domande_elenco_refresh()
{
    var container=$('#block_multiplechoice_elencodomande').closest('.container');
    var codiceSessione=$('#block_multiplechoice_elencodomande').data('codice');
    $.ajax({
        url: controller_url+'ajax_load_block_multiplechoice_domande_elenco/'+codiceSessione,
        type: "html",
        success:function(data){
            $(container).html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

function multiplechoice_elenco_refresh()
{
    var container=$('#multiplechoice_elenco_container');
    $.ajax({
        url: controller_url+'ajax_load_block_multiplechoice_elenco',
        type: "html",
        success:function(data){
            $(container).html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

/*function multiplechoice_domanda(codice_domanda)
{
    var container=$('#block_multiplechoice_domanda').closest('.container');
    $.ajax({
        url: controller_url+'ajax_load_block_multiplechoice_domanda/'+codice_domanda,
        type: "html",
        success:function(data){
            $(container).html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}*/
/**
 * 
 * @param {type} el
 * @returns {undefined}
 * @author Alessandro Galli
 * 
 * Ricarica il progetto passandolo in modalità di modifica
 */
function progettodidattica_edit(el,codice_progettodidattica)
{
    var container=$(el).closest('.container');
    $.ajax({
        url: controller_url+'ajax_load_block_progettodidattica/'+codice_progettodidattica+'/edit',
        type: "html",
        success:function(data){
            $(container).html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

/**
 * 
 * @param {type} el
 * @returns {undefined}
 * @author Alessandro Galli
 * 
 * Ricarica il progetto passandolo in modalità di modifica
 */
function lezione_edit(el,codice_lezione)
{
    var container=$(el).closest('.container');
    $.ajax({
        url: controller_url+'ajax_load_block_lezione/'+codice_lezione+'/edit',
        type: "html",
        success:function(data){
            $(container).html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

function evento_edit(el,codice_evento)
{
    var container=$(el).closest('.container');
    $.ajax({
        url: controller_url+'ajax_load_block_evento/'+codice_evento+'/edit',
        type: "html",
        success:function(data){
            $(container).html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

function esame_edit(el,codice_esame)
{
    var container=$(el).closest('.container');
    $.ajax({
        url: controller_url+'ajax_load_block_esame/'+codice_esame+'/edit',
        type: "html",
        success:function(data){
            $(container).html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

/**
 * 
 * @param {type} el
 * @returns {undefined}
 * @author Alessandro Galli
 * 
 * Salva le modifiche apportate al progetto del docente
 */
function lezione_save(el,codice_calendario_generale)
{
    $('.btn_add_argomento_extra').click();
    var container=$(el).closest('.container');
    var form_save= $(container).find('#form_save');
   $('#lista_argomenti_lezione').find('input').each(function(i){
      $(form_save).append(this);
   })
   $(container).find('#dati_lezione').find('input,select,textarea').each(function(i){
      $(form_save).append(this); 
   })
    $.ajax({
        url: controller_url+'ajax_lezione_save/'+codice_calendario_generale,
        type: "POST",
        data: $(form_save).serialize(),
        success:function(data){
            $(container).html('');
            $(container).html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

function esame_save(el,codice_calendario_generale)
{
    $('.btn_add_argomento_extra').click();
    var container=$(el).closest('.container');
    var form_save= $(container).find('#form_save');
   $(container).find('#dati_esame').find('input,select,textarea').each(function(i){
      $(form_save).append(this); 
   })
    $.ajax({
        url: controller_url+'ajax_esame_save/'+codice_calendario_generale,
        type: "POST",
        data: $(form_save).serialize(),
        success:function(data){
            $(container).html('');
            $(container).html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

function evento_save(el,codice_calendario_generale)
{
    var container=$(el).closest('.container');
    var form_save= $(container).find('#form_save');
   $(container).find('#dati_evento').find('input,select,textarea').each(function(i){
      $(form_save).append(this); 
   })
    $.ajax({
        url: controller_url+'ajax_evento_save/'+codice_calendario_generale,
        type: "POST",
        data: $(form_save).serialize(),
        success:function(data){
            $(container).html('');
            $(container).html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

function lezione_delete(el,CodiceRegistroPresenze)
{
     var r = confirm("Procedere con l'eliminazione della lezione?");
    if (r == true) {
        var container=$(el).closest('.container');
        $.ajax({
            url: controller_url+'ajax_lezione_delete/' + CodiceRegistroPresenze,
            type: "html",
            success:function(data){
                $(container).html('');
                $('#modal1').closeModal();
                $('#btn_inizia').click();
            },
            error:function(){
                alert('errore');
            }
        });
    }
}

function esame_delete(el,CodiceCalendarioGenerale)
{
     var r = confirm("Procedere con l'eliminazione della lezione?");
    if (r == true) {
        var container=$(el).closest('.container');
        $.ajax({
            url: controller_url+'ajax_esame_delete/' + CodiceCalendarioGenerale,
            type: "html",
            success:function(data){
                $(container).html('');
                $('#modal1').closeModal();
                $('#btn_inizia').click();
            },
            error:function(){
                alert('errore');
            }
        });
    }
}

function evento_delete(el,CodiceCalendarioGenerale)
{
     var r = confirm("Procedere con l'eliminazione della lezione?");
    if (r == true) {
        var container=$(el).closest('.container');
        $.ajax({
            url: controller_url+'ajax_evento_delete/' + CodiceCalendarioGenerale,
            type: "html",
            success:function(data){
                $(container).html('');
                $('#modal1').closeModal();
                $('#btn_inizia').click();
            },
            error:function(){
                alert('errore');
            }
        });
    }
}

function multiplechoice_domanda_insert(el,CodiceSessione)
{
    $('#modal1').openModal();
    $.ajax({
        url: controller_url+'ajax_load_block_multiplechoice_domanda/'+CodiceSessione+'/null/edit',
        type: "html",
        success:function(data){
            $('#modal1').html(data);
             //$('#CodiceSessione').val(CodiceSessione);
             //multiplechoice_elencodomande_refresh();
        },
        error:function(){
            alert('errore');
        }
    });
}

/**
 * 
 * @param {type} el
 * @returns {undefined}
 * @author Alessandro Galli
 * 
 * Ricarica la domanda di un formulario passandolo in modalità di modifica
 */
function multiplechoice_domanda_edit(el,codice_sessione,codice_domanda)
{
    var container=$(el).closest('.container');
    $.ajax({
        url: controller_url+'ajax_load_block_multiplechoice_domanda/'+codice_sessione+'/'+codice_domanda+'/edit',
        type: "html",
        success:function(data){
            $(container).html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

/**
 * 
 * @param {type} el
 * @returns {undefined}
 * @author Alessandro Galli
 * 
 * Salva le modifiche apportate a una domanda di un formulario multiplechoice
 */
function multiplechoice_domanda_save(el,codice_sessione,codice_domanda)
{
    var container=$(el).closest('.container');
    var form= $(container).find('#form_multiplechoice_domanda');
   $('#lista_argomenti_lezione').find('input').each(function(i){
      $(form).append(this);
   })
    $.ajax({
        url: controller_url+'ajax_multiplechoice_domanda_save/'+codice_sessione+'/'+codice_domanda,
        type: "POST",
        data: $(form).serialize(),
        success:function(data){
            $(container).html('');
            $(container).html(data);
            multiplechoice_domande_elenco_refresh();
            $('#modal1').closeModal();
        },
        error:function(){
            alert('errore');
        }
    });
}

function multiplechoice_domanda_open_popup(el,codice_sessione,codice_domanda)
{
    $('#modal1').openModal();

    $.ajax({
        url: controller_url+'ajax_load_block_multiplechoice_domanda/'+codice_sessione+'/'+codice_domanda+'/view',
        type: "html",
        success:function(data){
            $('#modal1').html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

/**
 * 
 * @param {type} el
 * @returns {undefined}
 * @author Alessandro Galli
 * 
 * Salva le modifiche apportate al progetto del docente
 */
function progettodidattica_save(el,codice_progettodidattica)
{
    var container=$(el).closest('.container');
    var progettodidattica=$(el).closest('.progettodidattica');
    var form_save=$('#form_save');
    $(form_save).html();
    $(progettodidattica).find('.input-field').each(function(i) {
        if(!$(this).prop('disabled'))
        {
           $(form_save).append(this); 
        }
        
    });
    $.ajax({
        url: controller_url+'ajax_progettodidattica_save/'+codice_progettodidattica,
        type: "POST",
        data: $('#form_save').serialize(),
        success:function(data){
            $(container).html('');
            $(container).html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}





function lezione_open(el,codice_registropresenze)
{
    var container=$('.lezione_container');
    $.ajax({
        url: controller_url+'ajax_load_block_lezione/'+codice_registropresenze+'/view',
        type: "html",
        success:function(data){
            $(container).html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

/**
 * 
 * @param {type} el
 * @param {type} codice_lezione
 * @returns {undefined}
 * @author Alessandro Galli
 * 
 * Apre in popup la scheda di una lezione
 */
function lezione_open_popup(el,codice_calendario_generale)
{
    $('#modal1').openModal();
    $.ajax({
        url: controller_url+'ajax_load_block_lezione/'+codice_calendario_generale+'/view',
        type: "html",
        success:function(data){
            $('#modal1').html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

function appuntamento_open_popup(el,codice_calendario_generale)
{
    $('#modal1').openModal();
    $.ajax({
        url: controller_url+'ajax_load_block_appuntamento/',
        type: "html",
        success:function(data){
            $('#modal1').html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

function esame_open_popup(el,codice_registropresenze)
{
    $('#modal1').openModal();
    $.ajax({
        url: controller_url+'ajax_load_block_esame/'+codice_registropresenze+'/view',
        type: "html",
        success:function(data){
            $('#modal1').html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

function evento_open_popup(el,codice)
{
    $('#modal1').openModal();
    $.ajax({
        url: controller_url+'ajax_load_block_evento/'+codice+'/view',
        type: "html",
        success:function(data){
            $('#modal1').html(data);
        },
        error:function(){
            alert('errore');
        }
    });
}

function set_sede(el,codice)
{
    alert(codice);
    var sede = prompt("Sede?");

    alert(sede);
   $.ajax({
        url: controller_url+'ajax_set_sede/'+codice+'/'+sede,
        type: "html",
        success:function(data){
            alert('ok');
        },
        error:function(){
            alert('errore');
        }
    });
}



function stampa_riepilogo_lezioni_docente(el)
    { 
        var url=controller_url + '/ajax_stampa_riepilogo_lezioni_docente/';
        $.ajax( {
            type: "POST",
            url: url,
            data: $('#stampa_riepilogo_lezioni_docente').serialize(),
            success: function( response ) {
                $('#stampa').html(response);
                setTimeout(function(){
                    $('#stampa').show();
                   window.print(); 
                   $('#stampa').hide();
                },1000);
                
            },
            error:function(){
                alert('errore');
            }
        } ); 
    }
    
function add_argomento_previsto(el)
{
    var argomento=$(el).closest('.argomento_da_assegnare');
    $(argomento).removeClass('argomento_da_assegnare');
    $(argomento).addClass('argomento_assegnato');
    $(argomento).find('.add_argomento').remove();
    $(argomento).find('.remove_argomento').show();
    $('#lista_argomenti_lezione').append(argomento);
}

function add_argomento_extra(el)
{
    var testo_argomento=$(el).closest('.argomento_da_assegnare').find('textarea').val();
    var argomento_extra_cloned=$('#argomento_extra_hidden').clone();
    $(argomento_extra_cloned).attr('id','');
    $('#lista_argomenti_lezione').append(argomento_extra_cloned);
    $(argomento_extra_cloned).find('.testo_argomento_lezione').html(testo_argomento);
    $(argomento_extra_cloned).find('input').val(testo_argomento);
    $(argomento_extra_cloned).show();
    $(el).closest('.argomento_da_assegnare').find('textarea').val('');
}

function remove_argomento_previsto(el)
{
    $(el).closest('.argomento_assegnato').remove();
}

function remove_argomento_extra(el)
{
    $(el).closest('.argomento_assegnato').remove();
}

function load_block_calendario_periodi()
{
    var arg=$('#form_periodi').serialize();
    ajax_load_block('calendario_periodi',arg,$('#calendario_periodi_container'));
}

function registra_voti_esameorale(el,CodiceRegistroPresenze)
{
    var container=$(el).closest('.container');
    var r = confirm("Procedere con la registrazione dei voti? Non potranno più essere modificati");
    if (r == true) {
        var block=$(el).closest('.block');
        var form_studenti_esameorale=$(block).find('#form_studenti_esameorale');
        $.ajax({
                    url: controller_url+'ajax_registra_voti_esameorale/'+CodiceRegistroPresenze,
                    type: "POST",
                    data: $(form_studenti_esameorale).serialize(),
                    success:function(data){
                        $(container).html('');
                        $(container).html(data);
                    },
                    error:function(){
                        alert('error load block '+block_name);
                    }
                });
            }
}

function add_docente(el)
{
    var cloned_counter=$('#docente_hidden').data('cloned_counter');
    cloned_counter++;
    var cloned_docente=$('#docente_hidden').clone();
    $(el).before(cloned_docente);
    $(cloned_docente).attr('id','');
    var cloned_select=$(cloned_docente).find('select');
    var cloned_label=$(cloned_docente).find('label');
    $(cloned_select).attr('id','docenti_'+cloned_counter+'_');
    $(cloned_select).attr('name','docenti['+cloned_counter+']');
    $(cloned_label).attr('for','docenti_'+cloned_counter+'_');
    $(cloned_docente).show();
    $(cloned_select).material_select();
    $(cloned_select).change(function(){
        docente_changed(this)
    })
    
    $('#docente_hidden').data('cloned_counter',cloned_counter);
}