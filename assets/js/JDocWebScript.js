//BASE
//
//MENU
function show_menu(){
    if($('#menu').data('visible')){ 
        hide_menu();
    }
    else
    {
        $('.btn_showmenu').css('background-image','url('+upimage+')');
        $('#menu').show(500,function(i){
            $('#menu').data('visible',true)
        });
    }

}

function CambioAnnoDiCorso(elemento)
{
    var i=1;
    while(i<=5)
    {
        $('#checkAnno' + i).prop('checked', false);
        i++;
    }
    
    var codiceAnnoDiCorso=$(elemento).val();
    $('#checkAnno' + codiceAnnoDiCorso).prop('checked',true);
}

function SelezionaMaterieCorsoProgettoDidattica(elemento)
{
    var codiceCorso = $(elemento).val();
    var url = controller_url + "/ajax_load_block_materie_corso/" + codiceCorso;
    
    $('#materiaIntestazione').empty();
    $('#materia').empty();
    $('#disciplina').val('');
    
    $.ajax({
        url: url,
        dataType: 'json',
        success:function(data)
        {
            $('#materia').append("<option value='null'></option>");
            $.each(data,function(index,value){
               $('#materia').append("<option value='" + value['Codice'] + "'>" + value['materia'] + "</option>"); 
            });
            
            //visto che la prima richiesta è andata a buon fine ne faccio partire subito un altra che si occupa
            //di recuperare le materie nell'intestazione
            url=controller_url + '/ajax_get_materie_intestazioni/' + codiceCorso;
            
            $.ajax({
                url: url,
                dataType: 'json',
                success:function(data){
                    $('#materiaIntestazione').append("<option value='null'></option>");
                    $.each(data,function(index,value)
                    {
                        $('#materiaIntestazione').append("<option value='" + value['Codice'] + "'>" + value['materia'] + "</option>");
                    });
                },
                error:function(){
                    alert("ERRORE AJAX MATERIA INTESTAZIONE")
                }
            });
        },
        error:function()
        {
            alert("ERRORE AJAX CARICAMENTO MATERIE CORSO")
        }
    });
}

function CambioMateriaSelezionata(elemento)
{
    var codiceMateria = $(elemento).val();
    $('#disciplina').val($("#materia option[value='" + codiceMateria + "']").text());
}

function CambioFacoltaProgettoDidattica(elemento)
{
    var codiceFacolta = $(elemento).val();
    var url = controller_url + "/ajax_load_block_corso_facolta/" + codiceFacolta;
    
    $('#corso').empty();
    $('#materia').empty();
    $('#disciplina').val('');
    
    $.ajax({
        url: url,
        dataType: 'json',
        success:function(data)
        {
            $('#corso').append("<option value='null'></option>");
            $.each(data,function(index,value){
              $('#corso').append("<option value='" + value['Codice'] + "'>" + value['Descrizione'] + "</option>");  
            });
        },
        error:function(){ alert("ERRORE AJAX CAMBIO FACOLTA' PROGETTO DIDATTICA"); }
    });
}

function CaricaProgettoDidattica(CodiceDidattica)
{
    var url= controller_url + '/ajax_load_progetto_didattica';
    $.ajax({
        url:url,
        dataType: 'json',
        success:function(data)
        {
            var recordSource = data['recordSource'];
            $.each(recordSource,function(index,value){
                if(value['Codice']==CodiceDidattica){
                    $('#destinatari').val(value['Destinatari']);
                    $('#NumeroStudenti').val(value['NumeroStudenti']);
                    $('#TempiRealizzazioneOffertaFormativa').val(value['TempiRealizzazioneOffertaFormativa']);
                    $('#PresentazioneCorso').val(value['PresentazioneCorso']);
                }
            });
        },
        error:function(){ alert("ERRORE ajax_load_progetto_didattica"); }
    });
    
    url=controller_url + '/ajax_load_progetto_didattica_parte1/' + CodiceDidattica;
    $.ajax({
        url: url,
        dataType:'json',
        success:function(data)
        {
            //prima di inserire i dati devo pulire il contenuto
            $('#facolta').empty();
            $('#corso').empty();
            $('#materia').empty();
            $('#disciplina').val('');
            $('#annoaccademico').empty();
            $('#annodicorso').empty();
            
            var facolta = data['Facolta'];
            $.each(facolta,function(index,value){
                $('#facolta').append("<option value='" + value['Codice'] + "'>" + value['Descrizione'] + "</option>");
            });
            
            var AnniAccademici = data['AnniAccademici'];
            $.each(AnniAccademici,function(index,value){
                $('#annoaccademico').append("<option value='" + value['Codice'] + "'>" + value['Descrizione'] + "</option>");
            });
            
            var AnniDiCorso = data['AnniDiCorso'];
            $.each(AnniDiCorso,function(index,value){
                $('#annodicorso').append("<option value='" + value['Codice'] + "'>" + value['Descrizione'] + "</option>");
            });
        },
        error:function(){ alert("ERRORE AJAX LOAD PROGETTO DIDATTICA PARTE ");}
    });
}

function ShowHideCalendar(elemento)
{
    var calendario=$(elemento).data("nomecalendario");
    if($(elemento).is(':checked'))
        $("#" + calendario).show();
    else
        $("#" + calendario).hide();
}

function hide_menu(){
    $('#pulsantino').attr('src',downimage);
    $('#menu').hide(500);
    $('#menu').data('visible',false)
}



//CUSTOM work&work
function invio_mail_modulo(el){
    var scheda_corrente=$(el).closest('.scheda');
    var scheda_corrente_container=$(scheda_corrente).closest('.scheda_container');
    var cloned_scheda_record_container=$('#scheda_record_container_hidden').clone();
    popuplvl_new=1;
    var popuplvl_corrente=$(scheda_corrente).data('popuplvl');
    var popuplvl_new=popuplvl_corrente+1;
    var url=controller_url+'invio_mail_modulo';
    $.ajax({
        url: url,
        dataType:'html',
        success:function(data){              
            //carico la scheda nel relativo container
            cloned_scheda_record_container.html("");
            cloned_scheda_record_container.html(data);
                $('.wrapper').append(cloned_scheda_record_container);
                $(cloned_scheda_record_container).addClass('popup');
                bPopup[popuplvl_new] =$(cloned_scheda_record_container).bPopup({
                    onClose: function() { 
                        $(cloned_scheda_record_container).remove();
                    }
                });
            cloned_scheda_record_container.show();
        },
        error:function(){
            alert('errore');
            $('#dettaglio_record').html('fallimento');
        }
    });
}


function ajax_load_content(el,funzione)
{
    $.ajax({
        
                url: controller_url+funzione,
                dataType:'html',
                success:function(data){
                    $('#content_container').css('display', 'none')
                    $('#content_container').html('');
                    $('#content_container').html(data);
                    $('#content_container').fadeIn(2000);

                },
                error:function(){
                    alert('errore');
                }
            });
}

//RICERCA
//


function labelclick(el,scroll){
    if(typeof scroll==='undefined')
    {
        scroll=true;
    }
    console.info('fun labelclick');
    var tables_labelcontainer=$(el).next();
    var block_dati_labels_container=$(el).closest('.block_dati_labels_container');
    
    var opened=$(el).data('opened');
    $(el).toggleClass("ui-state-active");
    if(opened)
    {
       $(el).data('opened',false);
       $(tables_labelcontainer).toggle( "blind", 500,function(){} );
       console.info('opened false');
    }
    else
    {
        $(el).data('opened',true);
        console.info('opened true');
        
        var loaded=$(el).data('loaded');
        if(loaded)
        {
            $(tables_labelcontainer).toggle( "blind", 500,function(){} );
            if(scroll)
            {
                $(block_dati_labels_container).scrollTo(el,500);
            }
            //tempodemo$(tables_labelcontainer).find('.first').first().focus();
        }
        else
        {
            tablesloading=true;
            
            $(tables_labelcontainer).toggle( "blind", 500,function(){} );
            load_tables_labelcontainer(tables_labelcontainer,'null',scroll);
            if((typeof(block_dati_labels_container) !== 'undefined')||(typeof(el) !== 'undefined'))
            {
                waittablesloading(block_dati_labels_container,el,scroll);
            }
            $(el).data('loaded','true');
        }
        
        
        
        

        
         //$(tables_labelcontainer).find('.first').first().focus();
    }
}

function waittablesloading(block_dati_labels_container,el,scroll){
    if(typeof scroll==='undefined')
    {
        scroll=true;
    }
    if(!tablesloading)
    {
        if(scroll)
        {
            $(block_dati_labels_container).scrollTo(el,500);
        }
        waittablesloading_counter=0;
        return;
    }
    else{
        setTimeout(function(){
            waittablesloading_counter=waittablesloading_counter+1;
            console.info("waitsaving_counter:"+waittablesloading_counter)
            if(waittablesloading_counter>10)
            {
                if(scroll)
                {
                    $(block_dati_labels_container).scrollTo(el,500);
                }
                tablesloading=false;
                waittablesloading_counter=0;
                return;
            }
            else
            {
                waittablesloading(block_dati_labels_container,el,scroll);
            }
        },500);
    }
}

//APERTURA SCHEDA LABEL
function labelclick2(el){
        console.info('fun labelclick2');
        var label=$(el).prev();
     /*   var block_dati_labels_container=$(label).closest('.block_dati_labels_container');
    var opened=$(label).data('opened');
    if(opened)
    {
       $(label).data('opened',false); 
       console.info('opened false');
    }
    else
    {
        $(label).data('opened',true);
        console.info('opened true');
        $(block_dati_labels_container).scrollTo(label,500);
    }*/
        
        var loaded=$(label).data('loaded');
        if(!loaded)
        {
           load_tables_labelcontainer(el,'null');
        }
        else
        {
            //tempdemo$(el).find('.first').first().focus();
        }
}

//FIELDS_TABLE
//cambio selezione in una lookup table
function select_changed(el) {
            var selectedoption=$('#'+$(el).attr('id')+' option:selected');
            var selectedvalue=selectedoption.attr('data-linkvalue');
            var selectedlink=selectedoption.attr('data-link');
            var selectedlinkfield=selectedoption.attr('data-linkfield');
            var table_container=$(el).closest('.table_container');
            if(selectedlink=='true')
                {
                    var finded_select=table_container.find('[data-linkedfield_select*="'+selectedlinkfield+'"]');
                    $(finded_select).prop('selectedIndex', 0);    
                    var next_finded=finded_select.next();
                    if(next_finded.attr("id")!=finded_select.attr("id"))
                    {
                        var cloned_select=finded_select.clone();
                        cloned_select.css("display","none");
                        cloned_select.attr("data-linkedfield_select","");
                        cloned_select.attr("name","");
                        finded_select.after(cloned_select);
                        var next_finded=cloned_select;
                    }
                   // var cloned_select=$(next_finded).clone();
                    //cloned_select.attr("data-linkedfield_select",selectedlinkfield);
                    $(finded_select).html($(next_finded).html());
                    finded_select.find("option").each(function(i) {
                        //$(this).show();
                        /*if ($(this).parent("span").length) {
                            $(this).unwrap();
                        }*/
                       var templinkedvalue=$(this).attr('data-linkedvalue');

                               if(templinkedvalue!==undefined)
                               {
                               if((templinkedvalue.toUpperCase()!=selectedvalue.toUpperCase())&&($(this).attr('class')!='emptyOption'))
                                    {
                                       $(this).remove();
                                       //$(this).hide();
                                       //$(this).wrap('<span/>');
                                    }
                                }
                               
                           
                    });
                }
            $(el).removeClass("notselected");
            $(el).addClass("selected");
            //field_blurred(el);
        }
        
function field_blurred(el)
{
    console.info('field_blurred('+el+')');
    var block_dati_labels=$(el).closest('.block_dati_labels');
    var funzione=$(block_dati_labels).data('funzione');
    var mastertableid=$(block_dati_labels).data('tableid');
    var masterrecordid=$(block_dati_labels).data('recordid');
    var scheda_container=$(block_dati_labels).data('scheda_container');
    var fieldValueContainer=$(el).closest(".fieldValueContainer");
    var lastval= $(el).data('lastval');
    $(fieldValueContainer).removeClass('fieldValueContainerFocused');
    var val=$(el).val();
    if((val!=lastval)&&(val!='loading')&&(!$(el).hasClass('fieldLookup'))&&(!$(el).hasClass('fieldUtente')))
    {
        //lastval=$(el).attr('id')+':'+$(el).val();
        $(el).data('lastval',val);
        field_changed(el);
    }
    if((scheda_container=='scheda_dati_inserimento')&&(masterrecordid!=null))
    {
        timeoutID = setTimeout(function () {
            var focused = $( document.activeElement );
            var blurred_container=$(el).closest('.table_container');
            var focused_container=$(focused).closest('.table_container');
            if($(blurred_container).attr('id')!=$(focused_container).attr('id'))
            {
                var table_container=$(el).closest('.table_container');
                var tableid=$(table_container).data('tableid');
                var recordid=$(table_container).data('recordid');
                salva_modifiche_record(el,tableid,recordid);

            }
        }, 50);
    }
}

function field_changed(field_input)
{
    console.info('funzione:field_changed('+field_input+')');
    $('#btnMostraTutti').html('Annull Filtri');
    if($(field_input).attr('id')!='saved_view_select')
    {
        $('#saved_view_select').val('');
    }
    var block_dati_labels=$(field_input).closest('.block_dati_labels');
    var table_block=$(field_input).closest('.table_block');
    var funzione=$(table_block).data('funzione');
    var mastertableid=$(block_dati_labels).data('tableid');
    var masterrecordid=$(block_dati_labels).data('recordid');
    var scheda_container=$(block_dati_labels).data('scheda_container');
    var placeholder=$(field_input).attr("placeholder");
    var tables_container=$(field_input).closest(".tables_container");
    var fieldcontainer=$(field_input).closest(".fieldcontainer");
    var fieldValue1=$(fieldcontainer).find('.fieldvalue1');
    if(($(field_input).hasClass('fieldValue0'))&&($(field_input).hasClass('fieldRange'))&&($(field_input).val()!='')&&($(fieldValue1).val()==''))
    {
        $(fieldValue1).val($(field_input).val());
    }
    if(funzione=='ricerca')
    {
        change_field_name(fieldcontainer,'search');
        //custom Work&Work
        if((placeholder=="parlato")||(placeholder=="letto")||(placeholder=="scritto"))
        {
            var value=$(field_input).val();
            if(value=='O')
            {
                or_field_onclick(field_input,true,'M');
            }
            if(value=='B')
            {
                or_field_onclick(field_input,true,'O');
                or_field_onclick(field_input,true,'M');
            }
            if(value=='S')
            {
                or_field_onclick(field_input,true,'B');
                or_field_onclick(field_input,true,'O');
                or_field_onclick(field_input,true,'M');
            }
            
            //var prev_option=$(field_input).find(":selected").prev();
            /*while(prev_option.size()>0)
            {
                autoOr($(field_input).closest(".fieldcontainer").find(".oppure"),prev_option.val());
                prev_option=prev_option.prev();
            }*/
        }
        var tables_container_cloned=tables_container.clone();
        tables_container_cloned.find("button").remove();
        /*tables_container_cloned.find('[name*="value"]').each(function(i)
        {
            var valore=$(block_dati_labels).find("#"+$(this).attr("id")).val();
            $(this).val(valore);
            var fieldscontainer=$(this).closest(".fieldscontainer");
            var fieldcontainer=$(this).closest(".fieldcontainer");
            var field_param=$(fieldcontainer).find('.field_param');
            var fieldvalueContainer=$(this).closest(".fieldValueContainer");
            if(($(this).val()=='')&&($(field_param).val()==''))
            {   
                $(fieldcontainer).remove();
            }
        });*/
        tables_container_cloned.find('.fieldcontainer').each(function(i)
        {
            var fieldValue0=$(this).find('.fieldValue0');
            var fieldValue1=$(this).find('.fieldValue1');
            var valore0=$(block_dati_labels).find("#"+$(fieldValue0).attr("id")).val();
            var valore1=$(block_dati_labels).find("#"+$(fieldValue1).attr("id")).val();
            $(fieldValue0).val(valore0);
            $(fieldValue1).val(valore1);
            //var fieldscontainer=$(this).closest(".fieldscontainer");
            //var fieldcontainer=$(this).closest(".fieldcontainer");
            var field_param=$(this).find('.field_param');
            //var fieldvalueContainer=$(this).closest(".fieldValueContainer");
            if(($(fieldValue0).val()=='')&&($(fieldValue1).val()=='')&&($(field_param).val()==''))
            {   
                $(this).remove();
            }
        });
        tables_container_cloned.prepend("<h3>"+tables_container_cloned.attr("data-labelName")+"</h3>");
        var scheda_dati_ricerca=$(field_input).closest('.scheda_dati_ricerca');
        var form_riepilogo=$(scheda_dati_ricerca).find('#form_riepilogo');
        var finded_container=$(form_riepilogo).find("#"+tables_container.attr("id"));
        if(finded_container.size()>0)
        {
            finded_container.replaceWith(tables_container_cloned);
        }
        else
        {

            $(form_riepilogo).append(tables_container_cloned);                    
        }
        update_query(field_input,mastertableid);
    }
    if(funzione=='inserimento')
    {
        change_field_name(fieldcontainer,'insert');
    }
    if(funzione=='modifica')
    {
        change_field_name(fieldcontainer,'update');
    }
}

function change_field_name(fieldcontainer,fun)
{
    var fieldscontainer=$(fieldcontainer).closest('.fieldscontainer');
    $(fieldscontainer).find('.fieldInput').each(function(i){
        var original_name=$(this).attr('name');
        var new_name=original_name.replace('[null]', '['+fun+']');
        $(this).attr('name',new_name);
    })
}
/*
function autoOr(el,value){
        
        var fieldcontainer=$(el).closest(".fieldcontainer");
        var fieldcontainer_cloned=fieldcontainer.clone(true,true);
        $(fieldcontainer_cloned).addClass('fieldcontainer_cloned');
        var fieldscontainer=fieldcontainer.closest(".fieldscontainer");
        var counter=fieldscontainer.attr("data-counter");
        var counter_modified=parseInt(counter)+1;
        fieldscontainer.attr("data-counter",counter_modified);
        var index_original=fieldcontainer.attr("data-index");
        var index_modified="f_"+counter_modified;
        fieldcontainer_cloned.attr("data-index",index_modified);
        id_original=$(fieldcontainer).attr("id");
        id_modified=id_original.replace(index_original,index_modified);
        fieldcontainer_cloned.attr("id",id_modified);
        var name_original;
        var name_modified;
        var id_original;
        var id_modified;
        fieldcontainer_cloned.find('[id*="'+index_original+'"]').each(function(i) {
            id_original=$(this).attr("id");
            id_modified=id_original.replace(index_original,index_modified);
            $(this).attr("id",id_modified);
            if($(this).is('[name]'))
            {
                name_original=$(this).attr("name");
                name_modified=name_original.replace(index_original,index_modified);
                $(this).attr("name",name_modified);
            }
        });
        var field_cloned=$(fieldcontainer_cloned).find('.field');
        $(field_cloned).val(value);
        $(field_cloned).show();
        var field_param_cloned=$(fieldcontainer_cloned).find('.field_param');
        $(field_param_cloned).val("");
        var field_layer_cloned=$(fieldcontainer_cloned).find('.field_layer');
        $(field_layer_cloned).hide();
        $(field_param_cloned).val('or');
        $(fieldcontainer_cloned).find('.or_layer').show();
        fieldcontainer_cloned.attr('data-cloned', 'true');
        $(fieldcontainer_cloned).hide();
        $(fieldcontainer).after(fieldcontainer_cloned);
     }
    */
    
function clear_field_click(el)
{
    var value_container=$(el).parent();
    var field=$(value_container).find('.field');
    var label=$(value_container).find('.label');
    var clear_field=$(value_container).find('.clear_field');
    $(field).val('');
    $(label).hide();
    $(clear_field).hide();
    $(field).width($(field).width()+$(label).width());
    field_changed(el);
    
}
//RISULTATI RICERCA
function TableRowSelected(table,nRow,aData,tableid,contesto){
    var dom_row=nRow;
    var data_row = aData;
    var recordid=data_row[0];
    //CUSTOM WORK&WORK
    var navigator_field=data_row[2];
    if(tableid=='CANDID')
        {
            var navigator_index=Lookup_RealColumnIndex(table, 'Cognome')
            navigator_field=data_row[navigator_index];
        }
    if(tableid=='AZIEND')
        {
            var navigator_index=Lookup_RealColumnIndex(table, 'Ragione Sociale')
            navigator_field=data_row[navigator_index];
        }
    if(contesto=='risultati_ricerca')
    {
        record_click(nRow,recordid,tableid,navigator_field);
    }
    if(contesto=='records_linkedtable')
    {
        ajax_load_fields_record_linkedtable(dom_row,tableid,'scheda',recordid,data_row.length);
    }
    //table.fnAdjustColumnSizing();
   
}

function TableRowSelectedBAK(table,nodes,tableid,contesto){
    var dom_row=nodes[0];
    var data_row = table.fnGetData( nodes[0] );
    var recordid=data_row[0];
    //CUSTOM WORK&WORK
    var navigator_field=data_row[2];
    if(tableid=='CANDID')
        {
            var navigator_index=Lookup_RealColumnIndex(table, 'Cognome')
            navigator_field=data_row[navigator_index];
        }
    if(tableid=='AZIEND')
        {
            var navigator_index=Lookup_RealColumnIndex(table, 'Ragione Sociale')
            navigator_field=data_row[navigator_index];
        }
    if(contesto=='risultati_ricerca')
    {
        record_click(nodes[0],recordid,tableid,navigator_field);
    }
    if(contesto=='records_linkedtable')
    {
        ajax_load_fields_record_linkedtable(dom_row,tableid,'scheda',recordid,data_row.length);
    }
    //table.fnAdjustColumnSizing();
   
}
function apri_linkedtable(el,recordid_,tableid,navigatorField){
    set_pinned();
    var tr=$(el).closest('tr');
    var prev_tr=$(tr).prev();
    var array_td=$(prev_tr).find('td');
    var td=$(array_td)[0];
    var navigatorField=$(td).html();
    record_click(el,recordid_,tableid,navigatorField);
    set_pinned();
}
function apri_mastertable(el,recordid_,tableid,navigatorField){
    set_pinned();
    record_click(el,recordid_,tableid,navigatorField);
    set_pinned();
}

var global_tableid;

function record_click(el,recordid,tableid,navigatorField,table) {
            global_tableid=tableid;
            
            apri_scheda_record(el,tableid,recordid,'right','standard_dati','risultati_ricerca');
            /*if((ultimascheda=="")||(pinned==true))
            {
                pinned=false;
                //genera navigatore
                
                var scroll_aggiuntivo=scheda_record_container_width*(schedaid-1);
                var newsposition=scheda_dati_ricerca_container_width*1+scheda_risultati_compatta_width*1+scroll_aggiuntivo;
                cloned_nav_button.attr("data-position", newsposition);
                lastnav=cloned_nav_button;
                $('#nav_risultati').after(cloned_nav_button);
                cloned_nav_button.show();
            }
            else
            {
                var container_ultimascheda=$('#'+ultimascheda);
                $(container_ultimascheda).remove();
                var newwidth=$('#ricerca_subcontainer').width()-scheda_record_container_width*2;
                $('#ricerca_subcontainer').width(newwidth);  
                var content=$('#'+ultimascheda);
                nuovoid_scheda=$(content).attr('id');
                var cloned_nav_button=$('#nav_scheda_hidden').clone();
                cloned_nav_button.attr("id", "nav_"+nuovoid_scheda);
                cloned_nav_button.attr("data-target_id", nuovoid_scheda);
                cloned_nav_button.html("");
                cloned_nav_button.html(navigatorField);
                lastnav.replaceWith(cloned_nav_button);
                lastnav=cloned_nav_button;
                cloned_nav_button.show();
                content.html("");
                content.html(data);
            }*/
            
                    
           
        
}

//APRI SCHEDA RECORD
        
function apri_scheda_record(el,tableid,recordid,target,layout,origine,funzione)
{
    if (typeof funzione === 'undefined') { funzione = 'scheda'; }
    var scheda_corrente=$(el).closest('.scheda');
    var scheda_corrente_container=$(scheda_corrente).closest('.scheda_container');
    var cloned_scheda_record_container=$('#scheda_record_container_hidden').clone();
    if(origine=='risultati_ricerca')
    {
        var origine_id=$(el).closest('.scheda_container').attr('id');
        $(cloned_scheda_record_container).data('origine',origine);
        $(cloned_scheda_record_container).data('origine_id',origine_id);
    }
    if(origine=='linked_table')
    {
        var origine_id=$(el).closest('.tables_container').attr('id');
        $(cloned_scheda_record_container).data('origine',origine);
        $(cloned_scheda_record_container).data('origine_id',origine_id); 
        
    }
    popuplvl_new=1;
    if(target=='popup')
    {
        var popuplvl_corrente=$(scheda_corrente).data('popuplvl');
        var popuplvl_new=popuplvl_corrente+1;
    }
    var url=controller_url+'ajax_load_block_scheda_record/'+tableid+'/'+recordid+'/'+layout+'/'+target+'/'+popuplvl_new+'/'+funzione;
    $.ajax({
        url: url,
        dataType:'html',
        success:function(data){              
            
            
            
            //carico la scheda nel relativo container
            cloned_scheda_record_container.html("");
            cloned_scheda_record_container.html(data);
            var cloned_scheda=$(cloned_scheda_record_container).find('.scheda');
            
            //gestisco l'id della nuova scheda
            //schedaid=schedaid+1;
            var id_cloned_scheda=$(cloned_scheda).attr('id');
            var nuovoid_scheda_container='scheda_record_container_'+id_cloned_scheda;
            cloned_scheda_record_container.attr('id', nuovoid_scheda_container);
            
            var cloned_nav_button=$('#nav_scheda_hidden').clone();
            cloned_nav_button.attr("id", "nav_"+nuovoid_scheda_container);
            cloned_nav_button.attr("data-target_id", nuovoid_scheda_container);
            cloned_nav_button.html("");
            var navigatorField=$(cloned_scheda).data('navigatorfield');
            cloned_nav_button.html(tableid+': '+navigatorField);
            
            
            
            
            
            if(target=='left')
            {
                $(scheda_corrente_container).before(cloned_scheda_record_container);
            }
            if(target=='right')
            {
                //allargo il div del content in modo da farci stare la nuova scheda
                var newwidth=$('#ricerca_subcontainer').width()+scheda_record_container_width+50;
                $('#ricerca_subcontainer').width(newwidth);  
                $('#content_ricerca').scrollTo($(scheda_corrente_container),500);
                
                    var scheda_successiva_container=$(scheda_corrente_container).next();
                    if($(scheda_successiva_container).attr('id')!='scheda_record_container_hidden')
                    {
                        var pinned=$(scheda_successiva_container).find('.scheda').data('pinned');
                        if(pinned)
                        {
                            $('#nav_risultati').after(cloned_nav_button);
                            
                        }  
                        else
                        {
                            $(lastnav).after(cloned_nav_button);
                           $(scheda_successiva_container).remove();
                           $(lastnav).remove(); 
                        }
                    }
                
                $(scheda_corrente_container).after(cloned_scheda_record_container);

                
            }
            if(target=='popup')
            {
                //var popuplvl_corrente=$(scheda_corrente).data('popuplvl');
                //var popuplvl_new=popuplvl_corrente+1;
                
                //$(cloned_scheda).data('popuplvl',popuplvl_new);
                $('.wrapper').append(cloned_scheda_record_container);
                $(cloned_scheda_record_container).addClass('popup');
                bPopup[popuplvl_new] =$(cloned_scheda_record_container).bPopup({
                    onClose: function() { 
                        $(cloned_scheda_record_container).remove();
                    }
                });
                $(lastnav).after(cloned_nav_button);
                $(lastnav).remove();
                
            }
            if(target=='self')
            {
                $(scheda_corrente_container).replaceWith(cloned_scheda_record_container);
                $(lastnav).after(cloned_nav_button);
                $(lastnav).remove();
            }
            
            $(cloned_nav_button).show();
            lastnav=cloned_nav_button;
            cloned_scheda_record_container.show();
            
        },
        error:function(){
            alert('errore');
            $('#dettaglio_record').html('fallimento');
        }
    });
}

//RICERCA
function update_risultati(el,archivio)
    {
        var block_container=$(el).closest('.block_container');
        $('#dvLoading_risultati').css('display', 'block');
        $('#scheda_risultati').animate({
                opacity: 1
                    }, 1000);
        var url=controller_url+'ajax_load_block_risultati_ricerca/'+archivio;
        $.ajax( {
            type: "POST",
            url: url,
            data: $(block_container).find('#form_riepilogo').serialize(),
            success: function( response ) {
                $('#risultati_ricerca').html('');
                $('#risultati_ricerca').append(response);
                $('#dvLoading_risultati').fadeOut(500);

            },
            error:function(){
                alert('errore');
            }
        } ); 
    }
    
function update_query(el,tableid)
    {
        var scheda_dati_ricerca=$(el).closest('.scheda_dati_ricerca');
        var funzione=$(scheda_dati_ricerca).data('funzione');
        var url=controller_url+'ajax_load_query/'+tableid+'/'+funzione;
        var form_riepilogo=$(scheda_dati_ricerca).find('.form_riepilogo');
        $.ajax( {
            type: "POST",
            url: url,
            data: $(form_riepilogo).serialize(),
            success: function( response ) {
                $(scheda_dati_ricerca).find('#query').val(response);
                if($(scheda_dati_ricerca).find('#autosearchTrue').is(':checked'))
                {   
                    //ajax_load_block_risultati_ricerca(el, tableid);
                    refresh_risultati_ricerca();
                }
                
            },
            error:function(){
                alert('errore');
            }
        } ); 
    }
   
//custom Work&Work
function ajax_load_block_risultati_ricerca_non_validati(el,tableid)
{
    var scheda_dati_ricerca=$(el).closest('.scheda_dati_ricerca');
    var sql="SELECT user_candid.recordid_,user_candid.recordstatus_,user_candid.id,disp.statodisp,disp.wwws,coll.validato,user_candid.pflash,user_candid.cognome,user_candid.nome,'temp' as qualifica,coll.giudizio,user_candid.consulente,user_candid.datanasc FROM user_candid LEFT JOIN    (SELECT    MAX(recordid_) as recordid_max,recordidcandid_ FROM      user_canddisponibilita GROUP BY  recordidcandid_) as disp_max ON (user_candid.recordid_ = disp_max.recordidcandid_) LEFT JOIN user_canddisponibilita as disp ON (disp.recordid_ = disp_max.recordid_max) LEFT JOIN    (SELECT    MAX(recordid_) as recordid_max,recordidcandid_ FROM      user_candcolloquio GROUP BY  recordidcandid_) as coll_max ON (user_candid.recordid_ = coll_max.recordidcandid_)LEFT JOIN user_candcolloquio as coll ON (coll.recordid_ = coll_max.recordid_max)WHERE user_candid.recordstatus_='new'";
    
    $(scheda_dati_ricerca).find('#query').val(sql);
    ajax_load_block_risultati_ricerca(el,tableid);
}

function ajax_load_block_risultati_ricerca(el,tableid)
{
    //var block_container=$(el).closest('.block_container');
    var scheda_dati_ricerca=$('#scheda_dati_ricerca');
        $('#dvLoading_risultati').css('display', 'block');
        $('#scheda_risultati').animate({
                opacity: 1
                    }, 1000); 
        var url=controller_url+'ajax_load_block_risultati_ricerca/'+tableid;
        $.ajax( {
            type: "POST",
            url: url,
            data: $(scheda_dati_ricerca).find('#form_riepilogo').serialize(),
            success: function( response ) {
                $('#risultati_ricerca').html('');
                $('#risultati_ricerca').append(response);
                $('#dvLoading_risultati').fadeOut(500);

            },
            error:function(){
                alert('errore');
            }
        } ); 
}

//SCHEDA
//AGGIORNAMENTO SCHEDA DOPO MODIFICHE
function update_scheda(el,recordid_,tableid) {
            var url=controller_url+'ajax_load_block_scheda_record/'+tableid+'/'+recordid_;
            $.ajax({
                url: url,
                dataType:'html',
                success:function(data){

                            $(el).html(data);
                },
                error:function(){
                    alert('errore');
                }
            });

                  
}






//MODIFICA
function modifica_record(el,tableid,recordid){
        var block_container=$(el).closest('.block_container');
        $(block_container).find('#campi_fissi').hide(1000);
        var tabs=$(block_container).find('#tabs');
        var active_tab=$(tabs).tabs("option","active");
        var url=controller_url+'ajax_load_block_modifica_record/'+tableid+'/'+recordid;
        $.ajax({
                    url : url,
                    success : function (response) {
                        $(block_container).html(response);
                        var tabs=$(block_container).find('#tabs');
                        console.info(tabs);
                        $(tabs).tabs("option","active",active_tab);
                    },
                    error : function () {
                        alert("Errore");
                    }
                });
        
    }
   
// chiamata da "salva modifiche" della scheda record
function salva_modifiche_record(el,tableid,recordid)
    {
        console.info("salva_modifiche_record "+el+" "+tableid+" "+recordid);
        //var recordid=$(scheda_record).data('recordid');
        //var tableid=$(scheda_record).data('tableid');
        var page=$(el).closest('.page');
        var block_dati_labels=$(el).closest('.block_dati_labels');
        var master_recordid=$(block_dati_labels).data('recordid');
        var master_tableid=$(block_dati_labels).data('tableid');
        var scheda_container=$(block_dati_labels).data('scheda_container');
        var tables_container=$(el).closest(".tables_container");
        var funzione=$(tables_container).data('funzione');
        //var tables_container_cloned=tables_container.clone();
        var table_container=$(el).closest('.table_container');
        var table_container_cloned=$(table_container).clone();
        var form_dati_labels=$(block_dati_labels).find('.form_dati_labels');
        $(form_dati_labels).html('');
        var compiled=false;
        if((master_recordid==null)&&(scheda_container=='scheda_dati_inserimento'))
        {
            $(block_dati_labels).find('.tables_container').each(function(){
                var tables_container=this;
                $(form_dati_labels).append(tables_container);
            })
            var block_allegati=$(page).find('.block_allegati');
            var block_lista_files=$(block_allegati).find('.block_lista_files');
            var files_container=$(block_lista_files).find('.files_container');
            $(form_dati_labels).append(files_container);
            var serialized=$(form_dati_labels).serialize();
            compiled=true;
            
        }
        else
        {
            $(form_dati_labels).html(table_container_cloned);
            $(table_container).find(".field").each(function(i){
                var field_value=$(this).val();
                if(field_value!='')
                {
                    compiled=true;
                }
            });
            var serialized=$(table_container).find("select,textarea,input").serialize();
        }
        
        if(compiled)
        {
        var url=controller_url+'ajax_salva_modifiche_record/'+master_tableid+'/'+master_recordid;
        $.ajax( {
            type: "POST",
            url: url,
            data: serialized,
            success: function( response ) {
                if(master_recordid==null)
                {
                    master_recordid=response.replace(";","");
                    if(scheda_container=='scheda_dati_inserimento')
                    {
                        ajax_load_block_dati_labels(tableid,master_recordid,'modifica',scheda_container,block_dati_labels);
                        ajax_load_block_allegati(tableid,master_recordid,'modifica',block_allegati);
                    }
                    if(scheda_container=='scheda_record')
                    {
                        record_click(null,master_recordid,master_tableid,null);  
                    }
                     
                }
                else
                {
                    var scheda_record=$(el).closest('.scheda_record');
                    $(scheda_record).find('#form_scheda_record').html("");
                    load_tables_labelcontainer(tables_container,'refresh');
                }
            },
            error:function(){
                alert('errore'); 
            }
        } ); 
        }
        else
        {
           
        }
    }

function salva_record(el,param)
    {
        console.info("salva_record ");
        var block_scheda_container=$(el).closest('.scheda_container');
        var block_dati_labels=$(block_scheda_container).find('.block_dati_labels');
        var tipo_scheda_container=$(block_dati_labels).data('scheda_container');
        var origine=$(block_scheda_container).data('origine');
        var origine_id=$(block_scheda_container).data('origine_id');
        if(origine=='linked_table')
        {
            var tables_container=$('#'+origine_id);
            var origine_block_dati_labels=$(tables_container).closest('.block_dati_labels');
            var origine_recordid=$(origine_block_dati_labels).data('recordid');
            var origine_tableid=$(origine_block_dati_labels).data('tableid');
            var master_origine_field_value=$(block_dati_labels).find('#records_linkedmaster_field_'+origine_tableid);
            $(master_origine_field_value).val(origine_recordid);
        }
        var labels=$(block_dati_labels).find('.labels');
        var form_dati_labels=$(block_dati_labels).find('.form_dati_labels');
        var recordid=$(block_dati_labels).data('recordid');
        var tableid=$(block_dati_labels).data('tableid')

            $(labels).find('.tables_container').each(function(index){
                $(form_dati_labels).append(this);
            })
            var block_allegati=$(block_scheda_container).find('.block_allegati');
            var files_container=$(block_allegati).find('.files_container');
            $(files_container).find('.file_container').each(function(i){
                var input_fileorigine=$(this).find('.fileorigine');
                if(($(input_fileorigine).val()=='coda')||($(input_fileorigine).val()=='autobatch'))
                    {
                        $(this).find('input').each(function(i){
                        var original_name=$(this).attr('name');
                        var new_name=original_name.replace('[null]', '[insert]');
                        var new_name=new_name.replace('[update]', '[insert]');
                        $(this).attr('name',new_name);
                        })
                    }
                $(form_dati_labels).append(this);
            });
            if(tableid=='CONTRA')
            {
                 $(form_dati_labels).append($('#contra_check'));
            }
            var url=controller_url+'ajax_salva_modifiche_record/'+tableid+'/'+recordid;
            $.ajax( {
                type: "POST",
                url: url,
                data: $(form_dati_labels).serialize(),
                success: function( response ) {
                        recordid=response.replace(";","");
                        if(tipo_scheda_container=='scheda_record')
                        {
                            var scheda_origine=$(el).closest('.scheda');
                            var popuplvl=$(scheda_origine).data('popuplvl');
                            var origine_target=$(scheda_origine).data('target');
                            if(param=='nuovo')
                            {
                                //record_click(null,'null',tableid,null);
                                if(origine_target=='popup')
                                {
                                    //apri_scheda_record(el,tableid,'null','popup','allargata',origine);
                                    ajax_load_block_dati_labels(tableid,'null','inserimento',tipo_scheda_container,block_dati_labels);
                                    var scheda_container_visualizzatore=$(block_scheda_container).find('.scheda_container_visualizzatore');
                                    $(scheda_container_visualizzatore).html('');
                                    var campi_fissi=$(block_scheda_container).find('#campi_fissi');
                                    $(campi_fissi).remove();
                                }

                                if(origine_target=='right')
                                {
                                    var risultati_ricerca=$('#risultati_ricerca');
                                    var risultati_ricerca_btn_plus_right=$(risultati_ricerca).find('.btn_plus_right');
                                    apri_scheda_record(risultati_ricerca_btn_plus_right,tableid,'null','right','standard_dati',origine);
                                }
                                //apri_scheda_record(el,tableid,'null','popup','allargata');
                            }
                                //record_click(null,recordid,tableid,null); 

                            if(param=='continua')
                            {
                                if(origine_target=='popup')
                                {
                                    apri_scheda_record(el,tableid,recordid,'popup','allargata',origine,'modifica');
                                    try {
                                    bPopup[popuplvl].close();
                                    }
                                    catch (e)
                                    {
                                        console.info(e);
                                    }
                                }

                                if(origine_target=='right')
                                {
                                    var risultati_ricerca=$('#risultati_ricerca');
                                    var risultati_ricerca_btn_plus_right=$(risultati_ricerca).find('.btn_plus_right');
                                    apri_scheda_record(risultati_ricerca_btn_plus_right,tableid,recordid,'right','standard_dati',origine);  
                                }
                            }

                            if(param=='chiudi')
                            {
                                if(origine=='risultati_ricerca')
                                {
                                    var risultati_ricerca=$('#risultati_ricerca');
                                    var risultati_ricerca_btn_plus_right=$(risultati_ricerca).find('.btn_plus_right');
                                    apri_scheda_record(risultati_ricerca_btn_plus_right,tableid,recordid,'right','standard_dati',origine);  
                                }
                                chiudi_scheda(el);
                                /*try {
                                bPopup[popuplvl].close();
                                }
                                catch (e)
                                {
                                    console.info(e);
                                }*/

                            }

                            if(origine=='risultati_ricerca')
                            {
                                ajax_load_block_risultati_ricerca(el,tableid);

                            }
                            if(origine=='linked_table')
                            {
                                var tables_container=$('#'+origine_id);
                                load_tables_labelcontainer(tables_container,'refresh');
                            }
                        }

                },
                error:function(){
                    alert('errore'); 
                }
            } ); 


    }

function salva_modifiche_allegati(el,tableid,recordid)
{
    console.info('funzione:salva_modifiche_allegati')
    //var scheda_allegati=$(el).closest('.scheda_allegati');
    var scheda=$(el).closest('.scheda');
    var schedaid=$(scheda).data('schedaid');
    
        var block_lista_files=$(el).closest(".block_lista_files");
        var form_riepilogo=$(block_lista_files).find('.form_riepilogo');
        if(schedaid!='scheda_code')
        {
            $(form_riepilogo).find('.file_container').each(function(i){
                var input_fileorigine=$(this).find('.fileorigine');

                    if(($(input_fileorigine).val()=='coda')||($(input_fileorigine).val()=='autobatch'))
                    {
                        $(this).find('input').each(function(i){
                        var original_name=$(this).attr('name');
                        var new_name=original_name.replace('[null]', '[insert]');
                        var new_name=new_name.replace('[update]', '[insert]');
                        $(this).attr('name',new_name);
                        })
                    }

            });
        }
        var url=controller_url+'ajax_salva_modifiche_allegati/'+tableid+'/'+recordid;
        $.ajax( {
            type: "POST",
            url: url,
            data: $(form_riepilogo).serialize(),
            success: function( response ) {
                //alert('allegati salvati');
                var target=scheda;
                /*var funzione=$(scheda_allegati).data('funzione');
                if(funzione=='modifica')
                {
                    funzione='modifica';
                }*/
                
                ajax_load_block_allegati(tableid,recordid,'inserimento',target);
                //update_scheda(scheda_container, recordid, tableid);
            },
            error:function(){
                alert('errore'); 
            }
        } ); 
}


function ajax_load_block_dati_labels(tableid,recordid,funzione,scheda_container,target)
{
    var url=controller_url+'ajax_load_block_dati_labels/'+tableid+'/'+recordid+'/'+funzione+'/'+scheda_container+'/null';
        $.ajax( {
            type: "html",
            url: url,
            success: function( response ) {
                $(target).replaceWith(response);
            },
            error:function(){
                alert('errore'); 
            }
        } ); 
}

function ajax_load_block_allegati(tableid,recordid,funzione,target)
{
    var url=controller_url+'ajax_load_block_allegati/'+tableid+'/'+recordid+'/'+funzione;
        $.ajax( {
            type: "html",
            url: url,
            success: function( response ) {
                $(target).replaceWith(response);
            },
            error:function(){
                alert('errore'); 
            }
        } ); 
}
//elimina record
function elimina_record(el,tableid,recordid){
        var risultati_ricerca=$('#risultati_ricerca');
        var dataTables_wrapper=$(risultati_ricerca).find('.dataTables_wrapper');
        var datatable_id=$(dataTables_wrapper).attr('id');
        datatable_id=datatable_id.replace("_wrapper",""); 
        var datatable=$('#'+datatable_id).dataTable();
        var confirmation=confirm('Sicuro di voler eliminare questo record?')
        if(confirmation)
            {
                var scheda_record=$(el).closest('.scheda_record');
                var url=controller_url+'ajax_elimina_record/'+tableid+'/'+recordid;
                $.ajax({
                            url : url,
                            success : function (response) {
                                container=$(el).closest('.scheda_record');
                                var id=$(container).attr('id');
                                if(id==ultimascheda)
                                    {
                                      ultimascheda="";
                                    }
                                    $('#nav_'+id).remove();
                                $(scheda_record).remove();
                                datatable.api().row('.selected').remove().draw( false );

                            },
                            error : function () {
                                alert("Errore");
                            }
                        });
            }
        
    }
 
 //elimina record linkato
function elimina_linked_record(el,tableid,recordid){
        var confirmation=confirm('Sicuro di voler eliminare questo record?')
        if(confirmation)
            {
                var url=controller_url+'ajax_elimina_record/'+tableid+'/'+recordid;
                $.ajax({
                            url : url,
                            success : function (response) {
                                alert('record eliminato');
                                var tables_container=$(el).closest('.tables_container');
                                var fields_record_linkedtable=$(el).closest('.fields_record_linkedtable');
                                var tr=$(fields_record_linkedtable).prev();
                                $(fields_record_linkedtable).remove();
                                $(tr).remove();
                            },
                            error : function () {
                                alert("Errore");
                            }
                        });
            }
        
    }
    
    
function valida_record(el,tableid,recordid)
{
    var url=controller_url+'ajax_valida_record/'+tableid+'/'+recordid;
    $.ajax({
            url : url,
            success : function (response) {
                alert('record validato');
            },
            error : function () {
                alert("Errore");
            }
        });
}

function valida_tutto_record(el,tableid,recordid)
{
    var url=controller_url+'ajax_valida_tutto_record/'+tableid+'/'+recordid;
    $.ajax({
            url : url,
            success : function (response) {
                alert('record validato');
            },
            error : function () {
                alert("Errore");
            }
        });
}

//FIELDS
//MOSTRARE IL RIEPILOGO
function show_riepilogo(el)
{
    var scheda_dati_ricerca_container=$(el).closest('.scheda_dati_ricerca_container');
    var block_dati_labels_container=$(scheda_dati_ricerca_container).find('.block_dati_labels_container');
    var block_dati_labels_container_width=$(block_dati_labels_container).width();
    var block_riepilogo=$(scheda_dati_ricerca_container).find('#block_riepilogo');
    if($(block_riepilogo).is(':hidden'))
    {
        $(scheda_dati_ricerca_container).animate({width:$(scheda_dati_ricerca_container).width()*2},1000);
        $(block_dati_labels_container).width(block_dati_labels_container_width);
        $(block_riepilogo).width(block_dati_labels_container_width);
        $(block_riepilogo).show(1000);
        $(el).removeClass('btn_right');
        $(el).addClass('btn_left');
    }
    else
    {
        $(block_riepilogo).hide(1000);
        $(scheda_dati_ricerca_container).animate({width:$(scheda_dati_ricerca_container).width()*0.5},1000);
        $(el).removeClass('btn_left');
        $(el).addClass('btn_right');
    }
}

//RICARICA LA SCHEDA DEI CAMPI
/*function reload_fields(el,tableid,funzione)
{
    var scheda=$(el).closest('.scheda');
    var block_dati_labels=$(scheda).find('.block_dati_labels');
    var url=controller_url+'ajax_load_block_dati_labels/'+tableid+'/null/'+funzione+'/null/null';
    $.ajax( {
            dataType:'html',
            url: url,
            success: function( response ) {
                $(block_dati_labels).html(response);

            },
            error:function(){
                alert('errore'); 
            }
        } ); 
}*/
function reload_fields(el,tableid,funzione)
{
    var scheda_dati_ricerca_container=$(el).closest('.scheda_dati_ricerca_container');
    var url=controller_url+'ajax_load_scheda_dati_ricerca/'+tableid+'/false';
    $.ajax( {
            dataType:'html',
            url: url,
            success: function( response ) {
                $(scheda_dati_ricerca_container).html(response);
                refresh_risultati_ricerca();
            },
            error:function(){
                alert('errore'); 
            }
        } ); 
}

function panel_activate(event, ui)
{
    var header=ui['newHeader'];
    var newPanel=ui['newPanel'];
    var block_container=$(header).closest('.block_container');
    var fields_container=$(block_container).find('.fields_container');
    var first=$(newPanel).find('.first');
    $(fields_container).scrollTo(header,500);
    //$(first).focus();
}

function linked_table_add(el)
{
  
  //$(tables_container).data('funzione','inserimento');
  $(el).hide();
  add_table(el,'add');  
}

// PARAMETRI TABELLA. AND
function table_param_onclick(el,param){
        add_table(el,param);
    }
    
 //CARICARE CONTENUTO LABEL
 function load_tables_labelcontainer(tables_container,table_param,scroll)
 {
     if(typeof scroll==='undefined')
    {
        scroll=true;
    }
    var block_dati_labels=$(tables_container).closest('.block_dati_labels');
    var block_dati_labels_container=$(block_dati_labels).closest('.block_dati_labels_container');
    var scheda_container=$(block_dati_labels).data('scheda_container');
    var tableid=$(tables_container).data('linkedtableid');
    var type=$(tables_container).data('type');
    if(type=='master')
    {
        var label=$(tables_container).data('labelname'); 
    }
    else
    {
       label='null'; 
    }
    
    var mastertableid=$(tables_container).data('mastertableid');
    var funzione=$(tables_container).data('funzione');
    var recordid=$(tables_container).data('recordid');
    var table_index=$(tables_container).data('table_index');
    var new_table_index=table_index+1;
    $(tables_container).data('table_index',new_table_index);
    
    var link=controller_url+'ajax_load_block_tables_labelcontainer/'+tableid+'/'+label+'/'+new_table_index+'/'+table_param+'/'+type+'/'+mastertableid+'/'+funzione+'/'+recordid+'/'+scheda_container;
        $.ajax({
            url: link,
            dataType:'html',
            success:function(data){
                        $(tables_container).html("");
                        $(tables_container).html(data);
                        tablesloading=false;
                        //apply();
                        if(((funzione=='inserimento')&&(type!='master'))||(funzione!='inserimento'))
                        {
                            var label=$(tables_container).prev();
                            $(label).data('loaded','true');
                            if(scroll)
                            {
                                $(block_dati_labels_container).scrollTo(label); 
                            }
                            //tempdemo $(tables_container).find('.first').first().focus();
                        }
                        
                            
            },
            error:function(){
                alert('errore');
            }
            });
 }   
 //AGGIUNGERE UNA TABELLA
 function add_table(el,table_param)
 {
    var tables_container=$(el).closest(".tables_container");
    var block_dati_labels=$(tables_container).closest('.block_dati_labels');
    var scheda_container=$(block_dati_labels).data('scheda_container');
    var tableid=$(tables_container).data('linkedtableid');
    var label=$(tables_container).data('labelName');
    var type=$(tables_container).data('type');
    var mastertableid=$(tables_container).data('mastertableid');
    var test=$(tables_container).data('funzione');
    var funzione=$(block_dati_labels).data('funzione');
    if(table_param=='add')
    {
        funzione='inserimento';
    }
    var recordid='null';
    var table_index=$(tables_container).data('table_index');
    var new_table_index=table_index+1;
    $(tables_container).data('table_index',new_table_index);
    var link=controller_url+'ajax_load_block_table/'+tableid+'/'+label+'/'+new_table_index+'/'+table_param+'/'+type+'/'+mastertableid+'/'+funzione+'/'+recordid+'/'+scheda_container;
        $.ajax({
            url: link,
            dataType:'html',
            success:function(data){
                var block_dati_labels_container=$(el).closest('.block_dati_labels_container');
                if(table_param!='null')
                    {
                        //$(block_dati_labels_container).scrollTo(el,500, {offset: {top:-100} });
                        $(tables_container).find('.first').removeClass('first');
                        $(tables_container).find('.last').removeClass('last');
                        $(tables_container).find('.lastbutton').removeClass('lastbutton');
                        var menu_small=$(el).closest('.menu_small');
                        $(data).insertBefore(menu_small);
                        apply();
                        var first=$(tables_container).find('.first');
                        var lastbutton=$(tables_container).find('.lastbutton');
                    }
                    else
                    {
                        //$(block_dati_labels_container).scrollTo(el,500, {offset: {top:-100} });
                        $(el).insertBefore(data);
                        apply();
                        var first=$(tables_container).find('.first');
                    }
            },
            error:function(){
                alert('errore');
            }
            });
 }
 
 //AGGIUNGERE UNA TABELLA
 function linkedmaster_table_add(el)
 {
    var tables_container=$(el).closest(".tables_container");
    var block_dati_labels=$(tables_container).closest('.block_dati_labels');
    var scheda_container=$(block_dati_labels).data('scheda_container');
    var tableid=$(tables_container).data('linkedtableid');
    var label=$(tables_container).data('labelName');
    var type=$(tables_container).data('type');
    var mastertableid=$(tables_container).data('mastertableid');
    var funzione=$(block_dati_labels).data('funzione');
    var recordid='null';
    var table_index=$(tables_container).data('table_index');
    var new_table_index=table_index+1;
    $(tables_container).data('table_index',new_table_index);
    var link=controller_url+'ajax_load_block_table/'+tableid+'/'+label+'/'+new_table_index+'/'+table_param+'/'+type+'/'+mastertableid+'/'+funzione+'/'+recordid+'/'+scheda_container;
        $.ajax({
            url: link,
            dataType:'html',
            success:function(data){
                var block_dati_labels_container=$(el).closest('.block_dati_labels_container');
                if(table_param!='null')
                    {
                        //$(block_dati_labels_container).scrollTo(el,500, {offset: {top:-100} });
                        $(tables_container).find('.first').removeClass('first');
                        $(tables_container).find('.last').removeClass('last');
                        $(tables_container).find('.lastbutton').removeClass('lastbutton');
                        $(data).insertBefore(el);
                        apply();
                        var first=$(tables_container).find('.first');
                        var lastbutton=$(tables_container).find('.lastbutton');
                    }
                    else
                    {
                        //$(block_dati_labels_container).scrollTo(el,500, {offset: {top:-100} });
                        $(el).insertBefore(data);
                        apply();
                        var first=$(tables_container).find('.first');
                    }
            },
            error:function(){
                alert('errore');
            }
            });
 }
 
    function last_tab(el){
        var tables_container=$(el).closest('.tables_container');
        var next_label=$(tables_container).next();
        //$('#testfocus').focus();

        //labelclick2(next_label);
        $(next_label).click();
    }   
  function delete_field_onclick(el){
         var field_container=$(el).closest(".fieldcontainer");
         $('#form_riepilogo').find('#'+field_container.attr('id')).remove();
         field_container.remove();
         field_changed(el);
         
    }
    
    function delete_table_onclick(el){
         var table_container=$(el).closest(".tablecontainer");
         var block_container=$(el).closest('.block_container');
         var mastertableid=block_container.data('tableid');
         $('#form_riepilogo').find('#'+table_container.attr('id')).remove();
         table_container.remove();
         update_query(el, mastertableid);
    }
    
    
//RISULTATI RICERCA
//ORDINAMENTO PER LA STAMPA
function set_order(el,key){
   var scheda=$(el).closest('.scheda');
   var btn_stampa_elenco_container= $(scheda).find('#btn_stampa_elenco_container');
   if(typeof btn_stampa_elenco_container!=='undefined')
   {
    var old_ascdesc=$(btn_stampa_elenco_container).data('orderascdesc');
    var old_key=$(btn_stampa_elenco_container).data('orderkey');
    if(old_ascdesc=='desc'){
        $(btn_stampa_elenco_container).data('orderascdesc', 'asc');
    }
    else
        {
            $(btn_stampa_elenco_container).data('orderascdesc', 'desc');
        }
    $(btn_stampa_elenco_container).data('orderkey', key);
   }
}




//GESTIONE CODE
//
//CREA CODA
function crea_coda(el)
{
   var nome_coda=$('#nome_coda').val(); 
   var scheda_code_container=$('.scheda_code_container');
   var url=controller_url+'ajax_crea_coda/'+nome_coda;
    $.ajax( {
            type: "html",
            url: url,
            success: function( response ) {
                alert('coda creata');
                ajax_load_block_code(response,scheda_code_container);
            },
            error:function(){
                alert('errore'); 
            }
        } ); 
}

function importacoda(el)
    {
        var nome_coda=$('#nome_coda').val();
         var url=controller_url+'importacoda/'+nome_coda;
        $.ajax({
            url: url,
            success:function(data){
                    alert("CODA CREATA CORRETTAMENTE!");
                    var content=$(el).closest('.content');
                    var scheda_code_container=$(content).find('.scheda_code_container');
                    ajax_load_block_code(data,scheda_code_container);
            },
            error:function(){
                alert("errore");
            }
        });
    }
    
function load_coda(el,funzione)
 {
     var id=$(el).val();
     var scheda=$(el).closest('.scheda');
     var url=controller_url+'ajax_load_block_lista_files/'+funzione+'/coda/'+id;
     $.ajax({
                url: url,
                dataType:'html',
                success:function(data){
                    
                    $(scheda).find('#files_coda_container').html(data);
                   

                },
                error:function(){
                    console.info('fallimento');
                    $('#files_coda').html('fallimento');
                }
            });
            
 }
 
 function load_autobatch(el,funzione)
 {
     var id=$(el).val();
     var scheda=$(el).closest('.scheda');
     var url=controller_url+'ajax_load_block_lista_files/'+funzione+'/autobatch/'+id;
     $.ajax({
                url: url,
                dataType:'html',
                success:function(data){
                    
                    $(scheda).find('#files_coda_container').html(data);
                   

                },
                error:function(){
                    alert('errore');
                }
            });
            
 }
//caricamento blocco code
function ajax_load_block_code(coda_precaricataid,target)
{
    var url=controller_url+'ajax_load_block_code/gestione_code/'+coda_precaricataid;
    $.ajax( {
            type: "url",
            url: url,
            success: function( response ) {
                $(target).html(response);
            },
            error:function(){
                alert('errore'); 
            }
        } ); 
}
//SALVA CODA
function salva_modifiche_coda(el)
{
    var scheda_code=$(el).closest('.scheda_code');
    var select_lista_code=$(scheda_code).find('.select_lista_code');
    var codaid=$(select_lista_code).val();
    var block_lista_files=$(scheda_code).find('.block_lista_files');
        $(block_lista_files).find('.file_container').each(function(i){

                var input_fileorigine=$(this).find('.fileorigine');
                if(($(input_fileorigine).val()=='coda')||($(input_fileorigine).val()=='autobatch'))
                    {
                        $(this).find('input').each(function(i){
                        var original_name=$(this).attr('name');
                        var new_name=original_name.replace('[null]', '[insert]');
                        var new_name=new_name.replace('[update]', '[insert]');
                        $(this).attr('name',new_name);
                        })
                    }
        });
    var url=controller_url+'ajax_salva_modifiche_coda/'+codaid;
    $.ajax( {
            type: "POST",
            url: url,
            data: $(block_lista_files).find('.form_riepilogo').serialize(),
            success: function( response ) {
                alert('coda salvata');

            },
            error:function(){
                alert('errore'); 
            }
        } ); 
}


function salva_modifiche_lista_files(el)
{
     var block_container=$(el).closest('.block');
     var originefiles=$(block_container).find('#originefiles').val();
       /* $(block_container).find('.file_container').each(function(i){

                var input_fileorigine=$(this).find('.fileorigine');
                if($(input_fileorigine).val()=='coda')
                    {
                        $(this).find('input').each(function(i){
                        var original_name=$(this).attr('name');
                        var new_name=original_name.replace('[null]', '[insert]');
                        var new_name=new_name.replace('[update]', '[insert]');
                        $(this).attr('name',new_name);
                        })
                    }
        $(block_container).find('.form_riepilogo').append(this);
        });*/
    var block_lista_code=$(el).closest('#block_lista_code');
    var nome_coda=$(block_lista_code).find('#select_lista_code').val();
    var url="";
    if(originefiles=='coda')
    {
       url=controller_url+'ajax_salva_modifiche_coda/'+nome_coda; 
    }
    if(originefiles=='allegati')
    {
        url=controller_url+'ajax_salva_modifiche_allegati/'+nome_coda;
    }
    $.ajax( {
            type: "POST",
            url: url,
            data: $(block_container).find('#form_riepilogo').serialize(),
            success: function( response ) {
                alert('Modifiche apportate correttamente')

            },
            error:function(){
                alert('errore'); 
            }
        } );
}



function Lookup_RealColumnIndex(table,needle){
    for(var i = 0, m = null; i < table.fnSettings().aoColumns.length; ++i) {
        var title=table.fnSettings().aoColumns[i].sTitle
        if(title != needle)
            continue;
        return(i);
        break;
    }
}  

function custom_risultati(table,nRow, aData, iDataIndex)
{
    var recordstatus_index=Lookup_RealColumnIndex(table, 'recordstatus_') ;
    if(aData[recordstatus_index]=='new')
    {
       $('td', nRow).css({
                         "background-color": "rgb(129, 52, 47)",
                         "color":"white",
                       }); 
    }
    //custom About-x
    var importoProposto_index=Lookup_RealColumnIndex(table, 'Importo proposto') ;
    if(typeof(importoProposto_index) !== 'undefined')
    {
        var column_index=importoProposto_index-2;
        $('td:eq('+column_index+')', nRow).css({
                        "text-align":"right"
                      }); 
    }
    
    var importoConcluso_index=Lookup_RealColumnIndex(table, 'Importo concluso') ;
    if(typeof(importoConcluso_index) !== 'undefined')
    {
        var column_index=importoConcluso_index-2;
        $('td:eq('+column_index+')', nRow).css({
                        "text-align":"right"
                      }); 
    }
    
    var importo_index=Lookup_RealColumnIndex(table, 'Importo') ;
    if(typeof(importo_index) !== 'undefined')
    {
        var column_index=importo_index-2;
        $('td:eq('+column_index+')', nRow).css({
                        "text-align":"right"
                      }); 
    }
    
    var dafatturare_index=Lookup_RealColumnIndex(table, 'Da fatturare') ;
    if(typeof(dafatturare_index) !== 'undefined')
    {
        var column_index=dafatturare_index-2;
        $('td:eq('+column_index+')', nRow).css({
                        "text-align":"right"
                      }); 
    }
}
function custom_risultati_CANDID(table,nRow, aData, iDataIndex)
{

        var Dis_index=Lookup_RealColumnIndex(table, 'Dis') ;
        var Dis_index2=0;
        var wwws_index=Lookup_RealColumnIndex(table, 'wwws') ;
        var valid_index=Lookup_RealColumnIndex(table, 'Valid') ;
        var valid_index2=0;
        var pflash_index=Lookup_RealColumnIndex(table, 'pflash') ;
        var recordstatus_index=Lookup_RealColumnIndex(table, 'recordstatus_') ;
   if(aData[recordstatus_index]=='new')
   {
      $('td:eq(0)', nRow).css({
                        "background-color": "red",
                        "font-weight": "bold",
                        "color":"white",
                        "text-align":"center"
                      }); 
   }
   if(aData[Dis_index]!==null)
       {
            if ( aData[Dis_index].toUpperCase()=='D' )
            {
                Dis_index2=Dis_index-2;
                $('td:eq('+Dis_index2+')', nRow).css({
                        "background-color": "#92D050",
                        "font-weight": "bold",
                        "color":"black",
                        "text-align":"center"
                      });
              $('td:eq('+Dis_index2+')', nRow).html( 'D' );
            }
            if ( aData[Dis_index].toUpperCase()=='A' )
            {
                Dis_index2=Dis_index-2;
                $('td:eq('+Dis_index2+')', nRow).css({
                        "background-color": "#AAA580",
                        "font-weight": "bold",
                        "color":"black",
                        "text-align":"center"
                      });
              $('td:eq('+Dis_index2+')', nRow).html( 'A' );
            }
            if ( aData[Dis_index].toUpperCase()=='N' )
            {
                Dis_index2=Dis_index-2;
                $('td:eq('+Dis_index2+')', nRow).css({
                        "background-color": "#7F7F7F",
                        "font-weight": "bold",
                        "color":"black",
                        "text-align":"center"
                      });
              $('td:eq('+Dis_index2+')', nRow).html( 'N' );
            }
            if ( aData[Dis_index].toUpperCase()=='W' )
            {
                Dis_index2=Dis_index-2;
                $('td:eq('+Dis_index2+')', nRow).css({
                        "background-color": "#C00000",
                        "font-weight": "bold",
                        "color":"black",
                        "text-align":"center"
                      });
              if(aData[wwws_index]!=null)
                  {
                        if(aData[wwws_index].toUpperCase()=='WW')
                           $('td:eq('+Dis_index2+')', nRow).html( 'WW' );
                       if(aData[wwws_index].toUpperCase()=='WS')
                           $('td:eq('+Dis_index2+')', nRow).html( 'WS' );
                  }
            }
       }
       if(aData[valid_index]!==null)
       {
           if(aData[valid_index].toUpperCase()=='SI')
            {
                valid_index2=valid_index-3;
                if(aData[pflash_index]!=null)
                {
                    if(aData[pflash_index].toUpperCase()=='SI')
                    {
                        $('td:eq('+valid_index2+')', nRow).html( "<img src='"+assets_url+"images/custom/WW/dossier.png'></img>" );
                    }
                    else
                    {
                        $('td:eq('+valid_index2+')', nRow).html( 'V' );
                    }
                }
                else
                {
                    $('td:eq('+valid_index2+')', nRow).html( 'V' );
                }
            }
            else
            {
                valid_index2=valid_index-3;
                $('td:eq('+valid_index2+')', nRow).html( '-' );
            }
       }
}

function custom_risultati_Immobili(table,nRow, aData, iDataIndex)
{

        var Dis_index2=1;
            if(iDataIndex==0)
            {
              $('td:eq('+Dis_index2+')', nRow).html( '<img src="http://localhost:8888/jdocwebtest/Immobile0.jpg" style="height:100px;width:100px;"></img>' );
          }
          if(iDataIndex==1)
            {
              $('td:eq('+Dis_index2+')', nRow).html( '<img src="http://localhost:8888/jdocwebtest/Immobile1.jpg" style="height:100px;width:100px;"></img>' );
          }
          if(iDataIndex==2)
            {
              $('td:eq('+Dis_index2+')', nRow).html( '<img src="http://localhost:8888/jdocwebtest/Immobile2.jpg" style="height:100px;width:100px;"></img>' );
          }
          console.info(aData);
          if(aData[6]=='disponibile')
          {
              $('td:eq(5)', nRow).css({
                        "color":"green"
                      });
          }
          
          if(aData[6]=='non disponibile')
          {
              $('td:eq(5)', nRow).css({
                        "color":"red"
                      });
          }
}
 
function custom_risultati_Documenti(table,nRow, aData, iDataIndex)
{

        var Dis_index2=1;
            if(aData[0]=='00000000000000000000000000000002')
            {
              $('td:eq('+Dis_index2+')', nRow).html( '<img src="http://localhost:8888/JDocServer/archivi/documenti/000/00000002_thumbnail.jpg" style="height:100px;width:100px;"></img>' );
          }
          if(aData[0]=='00000000000000000000000000000003')
            {
              $('td:eq('+Dis_index2+')', nRow).html( '<img src="http://localhost:8888/JDocServer/archivi/documenti/000/00000004_thumbnail.jpg" style="height:100px;width:100px;"></img>' );
          }
          if(aData[0]=='00000000000000000000000000000001')
            {
              $('td:eq('+Dis_index2+')', nRow).html( '<img src="http://localhost:8888/JDocServer/archivi/documenti/000/00000003_thumbnail.jpg" style="height:100px;width:100px;"></img>' );
          }
          if(aData[0]=='00000000000000000000000000000004')
            {
              $('td:eq('+Dis_index2+')', nRow).html( '<img src="http://localhost:8888/JDocServer/archivi/documenti/000/00000005_thumbnail.jpg" style="height:100px;width:100px;"></img>' );
          }
          if(aData[0]=='00000000000000000000000000000005')
            {
              $('td:eq('+Dis_index2+')', nRow).html( '<img src="http://localhost:8888/JDocServer/archivi/documenti/000/00000006_thumbnail.jpg" style="height:100px;width:100px;"></img>' );
          }
          
} 

//custom about
function custom_risultati_aziende(table,nRow, aData, iDataIndex,today)
{
        var dacontattare_index=Lookup_RealColumnIndex(table, 'Da Contattare') ;
        var dacontattare_index2=dacontattare_index-3;
        if(aData[dacontattare_index]=='no')
        {
            $('td:eq('+dacontattare_index2+')', nRow).css({
                      "color":"green"
                    });
        }
        if(aData[dacontattare_index]=='si')
        {
            $('td:eq('+dacontattare_index2+')', nRow).css({
                      "color":"red"
                    });
        }
}
 
 //ALLEGATI VISUALIZZATORE
 //APERTURA ALLEGATO
 function apri_allegato(el,recordid,path_,filename_,extension_) {

            var pathcompleta;
            var allegati=$(el).closest('.allegati');
            var connected_sortable=$(el).closest('.connectedSortable');
            $(connected_sortable).find('.allegato_selected').each(function(i){
                $(this).removeClass('allegato_selected');
            });
            
            $(el).addClass('allegato_selected');
  
        
            filename_=filename_+'.'+extension_;
            var baseurl=controller_url+'ajax_load_block_visualizzatore';
            var url=baseurl+'/'+path_+'/'+filename_+'/'+extension_+'/'+recordid;
            
            
            $.ajax({
                url: url,
                dataType:'html',
                success:function(data){
                    var scheda_container_visualizzatore=$(allegati).find('.scheda_container_visualizzatore');
                    $(scheda_container_visualizzatore).show();
                    $(scheda_container_visualizzatore).html(data);
                    //$(el).closest('.file_container').find('.originalname').toggle();
                    //$(el).closest('.file_container').find('.file_container_menu').toggle();
                    
                    
                },
                error:function(){
                    $('#visualizzatore').html('fallimento');
                }
            });
        }
        
  //STAMPA ALLEGATO
  function stampa_allegato(el,percorso)
  {
      PDFtoPrint=document.getElementById('PDFtoPrint'); 
      PDFtoPrint.contentWindow.print();
  }
  //LISTA FILES - SORTABLE
  
  function update_connectedSortable(event,ui)
  {
      
       var connectedSortable=$(ui.item).closest('.connectedSortable');
       console.info(connectedSortable);
       //console.info($(ui.item));
       //$(ui.item).find('.originalname').toggle();
       //$(ui.item).find('.file_container_menu').toggle();
       update_order_lista_files(connectedSortable);
  }
  function update_order_lista_files(connectedSortable)
  {
      console.info('funzione:update_order_lista_files');
      var index=0;
      $(connectedSortable).find('.file_container').each(function(i){
          var input_fileid=$(this).find('.fileid');
          var input_fileparam=$(this).find('.fileparam');
          var input_fileindex=$(this).find('.fileindex');
          if($(input_fileparam).val()!='delete')
              {
                $(input_fileindex).val(index);
                index=index+1; 
              }
      });
      var scheda=$(connectedSortable).closest('.scheda');
      var funzione=$(scheda).data('funzione');
      var tableid=$(scheda).data('tableid');
      var recordid=$(scheda).data('recordid');
      var schedaid=$(scheda).data('schedaid');
      /*if((funzione=='inserimento')&&(schedaid='scheda_allegati')&&(recordid!=null))
      {
          salva_modifiche_allegati(connectedSortable,tableid,recordid);
      }*/
      var scheda_record=$(connectedSortable).closest('.scheda_record')
    var recordid=$(scheda_record).data('recordid');
    if((recordid!='null')&&(recordid!=null))
    {
       salva_modifiche_allegati(connectedSortable,tableid,recordid); 
    }
      
  }
  
  function allega_file(el)
  {
      var allegati=$(el).closest('.allegati');
      var file_container=$(el).closest('.file_container');
      $(file_container).find('.left_icon').hide();
      //$(file_container).find('.right_icon').show();
      var scheda_allegati=$(allegati).find('.scheda_allegati');
      var files_container=$(scheda_allegati).find('#files_container');
      $(files_container).append(file_container);
      update_order_lista_files(files_container);
      
  }
  
  function allega_tutti_file(el)
  {
      var allegati=$(el).closest('.allegati');
      var scheda_allegati=$(allegati).find('.scheda_allegati');
      var files_container=$(scheda_allegati).find('#files_container');
      var connectedSortable=$(el).closest('.connectedSortable');
      $(connectedSortable).find('.file_container').each(function(i){
          $(this).find('.left_icon').hide();
        // $(this).find('.right_icon').show();
         $(files_container).append(this);
      });
      update_order_lista_files(files_container);
  }
  
  function UploadFile(el)
    {

            //$('#upload_target').html(''); //resetto il contenitore della lista caricata con ajax
            var scheda_record=$(el).closest('.scheda_record');
            var popuplvl=$(scheda_record).data('popuplvl');
            var url=controller_url+'uploadfile/'+popuplvl;
            var form=$(el).closest('#form_upload');
            var form_upload_loading=$(form).next();
            $(form).hide();
            $(form_upload_loading).show();
            
 
    $.ajax( {
      url: url,
      type: 'POST',
      data: new FormData($(form)[0]),
      processData: false,
      contentType: false,
      success: function( response ) {
                $(form).show();
                $(form_upload_loading).hide();
                update_sys_batch_temp(form);
            },
            error:function(){
                alert('errore'); 
            }
    } );

            /*
        $.ajax( {
            type: "POST",
            url: url,
            data: $('#form_upload_'+uploadid).serialize(),
            success: function( response ) {
                updatelista(uploadid);

            },
            error:function(){
                alert('errore'); 
            }
        } ); */
            //setTimeout('updatelista()',1500);
        
    }
  
function updatelista(uploadid)
    {
       // if (primoupdate==false){
       var file_toupload=$('#file_toupload_'+uploadid);
        $('#file_toupload_'+uploadid).val(''); //resetto il valore dell'input type file
        var url=controller_url+'ajax_load_block_lista_files/inserimento/coda/sys_batch_temp/';
             $.ajax({
                url: url,
                dataType:'html',
                success:function(data){
                    var files_sys_batch_creata_container_=$('#files_sys_batch_creata_container_'+uploadid);
                    $('#files_sys_batch_creata_container_'+uploadid).html(data);
                    //var allega_tutti=$('#files_sys_batch_creata_container').find('.allega_tutti_button')[0];    
                   // allega_tutti_file(allega_tutti);
                   caricaInLista($('#files_sys_batch_creata_container_'+uploadid));
                },
                error:function(){
                    alert('fallimento');
                    $('#files_sys_batch_creata_container_'+uploadid).html('fallimento');
                }
            });
       //}
      // primoupdate=false; //metto a false... questa procedura mi consente di evitare di caricare il contenuto della sys_temp_batch
    }
  function update_sys_batch_temp(form)
    {
       // if (primoupdate==false){
        $(form).find('#file_toupload').val(''); //resetto il valore dell'input type file
        var scheda_record=$(form).closest('.scheda_record');
        var popuplvl=$(scheda_record).data('popuplvl');
        var block_upload_files=$(form).closest('.block_upload_files');
        var url=controller_url+'ajax_load_block_sys_batch_temp/'+popuplvl;
             $.ajax({
                url: url,
                dataType:'html',
                success:function(data){
                    var files_sys_batch_temp=$(block_upload_files).find('#files_sys_batch_temp');
                    $(files_sys_batch_temp).html(data);
                   caricaInLista(files_sys_batch_temp);
                },
                error:function(){
                    alert('fallimento');
                }
            });
       //}
      // primoupdate=false; //metto a false... questa procedura mi consente di evitare di caricare il contenuto della sys_temp_batch
    }
    
  function caricaInLista(el)
  {
     var scheda=$(el).closest('.scheda');
     var files_container=$(scheda).find('#files_container');
     $(files_container).find('.file_container').each(function(i){
         $(this).remove();
     });
     $(el).find('.file_container').each(function(i){
         $(this).find('input').each(function(i){
                        var original_name=$(this).attr('name');
                        var new_name=original_name.replace('[null]', '[insert]');
                        var new_name=new_name.replace('[update]', '[insert]');
                        $(this).attr('name',new_name);
                        })
          //$(this).find('.left_icon').hide();
         //$(this).find('.right_icon').show();
          $(files_container).append(this);
      });
      update_order_lista_files(files_container); 
      $( ".connectedSortable" ).sortable({
                      connectWith: ".connectedSortable",
                      update: function( event, ui ) {
                          update_connectedSortable(event, ui);
                      }
                    }).disableSelection();
  }
  
  function nonallegare_file(el)
  {
      var file_container=$(el).closest('.file_container');
      $(file_container).remove();
  }
  
  function elimina_file(el)
  {
      var confirmation=confirm('Sicuro di voler eliminare questo allegato?')
    if(confirmation)
    {
        var file_container=$(el).closest('.file_container');
        $(file_container).find('.fileparam').val('delete');
        $(file_container).find('input').each(function(i){
            var original_name=$(this).attr('name');
            var new_name=original_name.replace('[null]', '[delete]');
            var new_name=new_name.replace('[update]', '[delete]');
            $(this).attr('name',new_name);
        })
        var connectedSortable=$(el).closest('.connectedSortable');
        //$(file_container).css('opacity', '0.5');
        $(file_container).hide();
        $(file_container).data('delete','true');
        update_order_lista_files(connectedSortable);
    }
        
     /* var file_container=$(el).closest('.file_container');
      if($(file_container).data('delete'))
        {
            $(file_container).find('.fileparam').val('');
            $(file_container).find('input').each(function(i){
                var original_name=$(this).attr('name');
                var new_name=original_name.replace('[delete]', '[update]');
                $(this).attr('name',new_name);
            })
            var connectedSortable=$(el).closest('.connectedSortable');
            $(file_container).css('opacity', '1');
            update_order_lista_files(connectedSortable);
        }
        else
        {
            $(file_container).find('.fileparam').val('delete');
            $(file_container).find('input').each(function(i){
                var original_name=$(this).attr('name');
                var new_name=original_name.replace('[null]', '[delete]');
                var new_name=new_name.replace('[update]', '[delete]');
                $(this).attr('name',new_name);
            })
            var connectedSortable=$(el).closest('.connectedSortable');
            //$(file_container).css('opacity', '0.5');
            $(file_container).hide();
            $(file_container).data('delete','true');
            update_order_lista_files(connectedSortable);
        }
      */
                    
      
  }
  
  
  
  
  
  //INSERIMENTO
function inserisci(el,param)
{
    var block_scheda_container=$(el).closest('.scheda_container');
    var block_dati_labels=$(block_scheda_container).find('.block_dati_labels');
    var tipo_scheda_container=$(block_dati_labels).data('scheda_container');
    var origine=$(block_scheda_container).data('origine');
    var origine_id=$(block_scheda_container).data('origine_id');
    if(origine=='linked_table')
    {
        var tables_container=$('#'+origine_id);
        var origine_block_dati_labels=$(tables_container).closest('.block_dati_labels');
        var origine_recordid=$(origine_block_dati_labels).data('recordid');
        var origine_tableid=$(origine_block_dati_labels).data('tableid');
        var master_origine_field_value=$(block_dati_labels).find('#records_linkedmaster_field_'+origine_tableid);
        $(master_origine_field_value).val(origine_recordid);
    }
    var labels=$(block_dati_labels).find('.labels');
    var form_dati_labels=$(block_dati_labels).find('.form_dati_labels');
    var recordid=$(block_dati_labels).data('recordid');
    var tableid=$(block_dati_labels).data('tableid')
    if(recordid==null)
    {
        $(labels).find('.tables_container').each(function(index){
            $(form_dati_labels).append(this);
        })
        var block_allegati=$(block_scheda_container).find('.block_allegati');
        var files_container=$(block_allegati).find('.files_container');
        $(files_container).find('.file_container').each(function(i){
            var input_fileorigine=$(this).find('.fileorigine');
            if(($(input_fileorigine).val()=='coda')||($(input_fileorigine).val()=='autobatch'))
                {
                    $(this).find('input').each(function(i){
                    var original_name=$(this).attr('name');
                    var new_name=original_name.replace('[null]', '[insert]');
                    var new_name=new_name.replace('[update]', '[insert]');
                    $(this).attr('name',new_name);
                    })
                }
            $(form_dati_labels).append(this);
        });
        //$(form_dati_labels).append(files_container);
        //custom Work&Work contra
        if(tableid=='CONTRA')
        {
             $(form_dati_labels).append($('#contra_check'));
        }
        var url=controller_url+'ajax_salva_modifiche_record/'+tableid+'/null';
        $.ajax( {
            type: "POST",
            url: url,
            data: $(form_dati_labels).serialize(),
            success: function( response ) {
                    recordid=response.replace(";","");
                   /* if(tipo_scheda_container=='scheda_dati_inserimento')
                    {
                        
                        if(param=='nuovo')
                        {
                            ajax_load_block_dati_labels(tableid,'null','inserimento',tipo_scheda_container,block_dati_labels);
                            //ajax_load_block_allegati(tableid,'null','inserimento',block_allegati);
                            var scheda_allegati_container=$('#scheda_allegati_container');
                            var files_container=$(scheda_allegati_container).find('#files_container');
                            var scheda_container_visualizzatore=$('.scheda_container_visualizzatore');
                            $(scheda_container_visualizzatore).html('');
                            //$(files_container).find('.file_container').remove();
                            //ajax_load_content(el,'ajax_load_content_inserimento/'+tableid+'/desktop')
                        }
                        else
                        {
                            ajax_load_block_dati_labels(tableid,recordid,'inserimento',tipo_scheda_container,block_dati_labels);
                            ajax_load_block_allegati(tableid,recordid,'inserimento',block_allegati);
                            if(tableid=='CANDID')
                            {
                                $('#btnCaricaOnline').show();
                            }
                        }
                        //$('#btnNuovo').show();
                    }
                    */
                    if(tipo_scheda_container=='scheda_record')
                    {
                        var scheda_origine=$(el).closest('.scheda');
                        var popuplvl=$(scheda_origine).data('popuplvl');
                        var origine_target=$(scheda_origine).data('target');
                        
                        
                        if(param=='nuovo')
                        {
                            //record_click(null,'null',tableid,null);
                            if(origine_target=='popup')
                            {
                                //apri_scheda_record(el,tableid,'null','popup','allargata',origine);
                                ajax_load_block_dati_labels(tableid,'null','inserimento',tipo_scheda_container,block_dati_labels);
                                var scheda_container_visualizzatore=$(block_scheda_container).find('.scheda_container_visualizzatore');
                                $(scheda_container_visualizzatore).html('');
                            }
                            
                            if(origine_target=='right')
                            {
                                var risultati_ricerca=$('#risultati_ricerca');
                                var risultati_ricerca_btn_plus_right=$(risultati_ricerca).find('.btn_plus_right');
                                apri_scheda_record(risultati_ricerca_btn_plus_right,tableid,'null','right','standard_dati',origine);
                            }
                            //apri_scheda_record(el,tableid,'null','popup','allargata');
                        }
                            //record_click(null,recordid,tableid,null); 

                        if(param=='continua')
                        {
                            if(origine_target=='popup')
                            {
                                apri_scheda_record(el,tableid,recordid,'popup','allargata',origine,'modifica');
                                try {
                                bPopup[popuplvl].close();
                                }
                                catch (e)
                                {
                                    console.info(e);
                                }
                            }
                            
                            if(origine_target=='right')
                            {
                                var risultati_ricerca=$('#risultati_ricerca');
                                var risultati_ricerca_btn_plus_right=$(risultati_ricerca).find('.btn_plus_right');
                                apri_scheda_record(risultati_ricerca_btn_plus_right,tableid,recordid,'right','standard_dati',origine);  
                            }
                        }
                        
                        if(param=='chiudi')
                        {
                            if(origine=='risultati_ricerca')
                            {
                                var risultati_ricerca=$('#risultati_ricerca');
                                var risultati_ricerca_btn_plus_right=$(risultati_ricerca).find('.btn_plus_right');
                                apri_scheda_record(risultati_ricerca_btn_plus_right,tableid,recordid,'right','standard_dati',origine);  
                            }
                            try {
                            bPopup[popuplvl].close();
                            }
                            catch (e)
                            {
                                console.info(e);
                            }
                            
                        }
                        
                        if(origine=='risultati_ricerca')
                        {
                            ajax_load_block_risultati_ricerca(el,tableid);
                                                          
                        }
                        if(origine=='linked_table')
                        {
                            var tables_container=$('#'+origine_id);
                            load_tables_labelcontainer(tables_container,'refresh');
                        }
                    }
                    
            },
            error:function(){
                alert('errore'); 
            }
        } ); 
    }
    else
    {
        if(param=='nuovo')
        {
            ajax_load_content(el,'ajax_load_content_inserimento/'+tableid+'/desktop')
        }
    }
}





//VISUALIZZATORE
//ALLARGA ALLEGATO
function allarga_allegato(el)
{
    var scheda_container=$(el).closest('.scheda_container');
    var allargato=$(el).data('allargato');
    if(allargato==false)
    {
        
       
       var campi_fissi=$(scheda_container).find('#campi_fissi');
       campi_fissi.hide(500);
       var lista_allegati=$(scheda_container).find('#lista_allegati');
       lista_allegati.hide(500);
       var dati_e_allegati=$(scheda_container).find('#dati_e_allegati');
       dati_e_allegati.hide();

       
        var visualizzatore_content_clone=$(scheda_container).find('#visualizzatore_content').clone();
        $(visualizzatore_content_clone).find('#btn_allarga_allegato').data('allargato','true');
        $(visualizzatore_content_clone).attr('id', 'visualizzatore_content_clone');
        $(scheda_container).append(visualizzatore_content_clone);
    }
    else
    {
        var visualizzatore_content_clone=$(scheda_container).find('#visualizzatore_content_clone');
        $(visualizzatore_content_clone).remove(); 
       var campi_fissi=$(scheda_container).find('#campi_fissi');
       campi_fissi.show(500);
       var lista_allegati=$(scheda_container).find('#lista_allegati');
       lista_allegati.show(500);
       var dati_e_allegati=$(scheda_container).find('#dati_e_allegati');
       dati_e_allegati.show();

       $(el).data('allargato','true');
        
    }
 
}



//SCHEDA RECORD FISSI
//salva foto record
function salva_foto_record(el)
{
    alert('salva');
}


//SCHEDE SALVATE
function cambio_archivio_schede_salvate(el)
    {
        var tableid=$(el).val();
        var url=controller_url+'ajax_cambio_archivio_schede_salvate/'+tableid;
        $.ajax( {
            dataType:'html',
            url: url,
            success: function( response ) {
                $('#query').html(response);

                var url=controller_url+'ajax_load_block_risultati_ricerca/'+tableid;
                $.ajax( {
                    type: "POST",
                    url: url,
                    success: function( response ) {
                        $('#target').html('');
                        $('#target').append(response);

                    },
                    error:function(){
                        alert('errore');
                    }
                } ); 
                
                
            },
            error:function(){
                alert('errore');
            }
        } ); 
    }
    
    
    


function ajax_load_fields_record_linkedtable_modifica(el,linkedtableid,funzione,recordid,numcol)
{
    var url=controller_url+'ajax_load_block_fields_record_linkedtable/'+linkedtableid+'/null/null/'+funzione+'/'+recordid;
    $.ajax({
                url: url,
                dataType:'html',
                success:function(data){
                    if(data!='')
                    {
                        var td_container=$(el).closest('td');
                        $(td_container).html(data);
                    }
                },
                error:function(){
                    alert('errore');
                }
            });
}

function ajax_load_table(el,tableid,label,funzione,recordid)
{
    var block_dati_labels=$(el).closest('.block_dati_labels');
    var scheda_container=$(block_dati_labels).data('scheda_container');
    var url=controller_url+'ajax_load_block_table/'+tableid+'/'+label+'/null/null/null/null/'+funzione+'/'+recordid+'/'+scheda_container;
    $.ajax({
                url: url,
                dataType:'html',
                success:function(data){
                    if(data!='')
                    {
                        var table_container=$(el).closest('.table_container');
                        $(table_container).replaceWith(data);
                    }
                },
                error:function(){
                    alert('errore');
                }
            });
}

function ajax_load_allegati(el,tableid,recordid,funzione)
{
    var url=controller_url+'ajax_load_block_allegati/'+tableid+'/'+recordid+'/'+funzione;
    $.ajax({
                url: url,
                dataType:'html',
                success:function(data){
                    if(data!='')
                    {
                        var block_allegati=$(el).closest('.block_allegati');
                        $(block_allegati).replaceWith(data);
                    }
                },
                error:function(){
                    alert('errore');
                }
            });
}

//RECORDS_LINKEDMASTER


function ajax_load_fields_record_linkedmaster(el,tableid,funzione)
{
    var block_dati_labels=$(el).closest('.block_dati_labels');
    var table_container=$(el).closest('.table_container');
    var fields_record_linkedmaster=$(table_container).find('.fields_record_linkedmaster');
    var scheda_container=$(block_dati_labels).data('scheda_container');
    var recordid=$(el).val();
    var url=controller_url+'ajax_load_block_table/'+tableid+'/null/null/null/null/null/scheda/'+recordid+'/'+scheda_container;
    $.ajax({
                url: url,
                dataType:'html',
                success:function(data){
                    $(fields_record_linkedmaster).html(data);
                },
                error:function(){
                    alert('errore');
                }
            });
}

function ajax_load_fissi_linkedmaster(el,tableid)
{
    var block_dati_labels=$(el).closest('.block_dati_labels');
    var table_container=$(el).closest('.table_container');
    var fissi_record_linkedmaster=$(table_container).find('.fissi_record_linkedmaster');
    var scheda_container=$(block_dati_labels).data('scheda_container');
    var recordid=$(el).val();
    var url=controller_url+'ajax_load_block_fissi/'+tableid+'/'+recordid;
    $.ajax({
                url: url,
                dataType:'html',
                success:function(data){
                    $(fissi_record_linkedmaster).html(data);
                },
                error:function(){
                    alert('errore');
                }
            });
}

function ajax_load_fields_record_linkedtable(el,tableid,funzione,recordid,numcol)
{
    var block_dati_labels=$(el).closest('.block_dati_labels');
    var scheda_container=$(block_dati_labels).data('scheda_container');
    var label='null';
    var table_index='null';
    var table_param='null';
    var type='linked';
    var mastertableid='null';
    var url=controller_url+'ajax_load_block_table/'+tableid+'/'+label+'/'+table_index+'/'+table_param+'/'+type+'/'+mastertableid+'/'+funzione+'/'+recordid+'/'+scheda_container;
    $.ajax({
                url: url,
                dataType:'html',
                success:function(data){
                    //$(el).toggleClass('selected');
                    if(data!='')
                    {
                        if($(el).data('selected'))
                            {
                                //$(el).removeClass('DTTT_selected');
                                var fields_record_linketable=$(el).next();
                                $(fields_record_linketable).remove();
                                $(el).data('selected',false);
                            }
                        else
                            {
                                //$(el).addClass('DTTT_selected');
                                $(el).data('selected',true)
                                $(el).after('<tr><td colspan="'+numcol+'" style="padding:0px;">'+data+'</td></tr>')
                            }
                    }
                },
                error:function(){
                    alert('errore');
                }
            });
}

//STAMPE
//STAMPA CONTRATTI
function stampa_contratti()
{
    var url=controller_url+'/stampa_contratti'
    var urldownload=controller_url+'/download_contratti/'
    $.ajax({
        url: url,
        success:function(data){
            //$('#target').html(data);
            alert('successo');
            //window.location.href = urldownload + data;
        },
        error:function(){
            alert("ERRORE RICHIESTA AJAX");
        }
    });
}

function stampa_vis_contratto(recordid){
    var url=controller_url+'/stampa_vis_contratto/'+recordid;
    var urldownload=controller_url+'/download_profilo/';
            $.ajax({
                url: url,
                dataType:'html',
                success:function(data){
                    window.location.href = urldownload + data;
 
                },
                error:function(){
                    alert('errore');
                }
            });
}

function stampa_vis_profilorischio(recordid){
    var url=controller_url+'/stampa_vis_profilorischio/'+recordid;
    var urldownload=controller_url+'/download_profilo/';
            $.ajax({
                url: url,
                dataType:'html',
                success:function(data){
                    window.location.href = urldownload + data;
 
                },
                error:function(){
                    alert('errore');
                }
            });
}


        
        
  //IMPOSTAZIONI
  //mostra i campi preferiti
    function ShowCampiPreferenza()
    {
        var indirizzo_blocco_preferenze=controller_url+'ajax_load_block_macrogruppo_preferenze/desktop';
        $.ajax({
            url : indirizzo_blocco_preferenze,
            success : function (data) {
                $('#sottogruppo').html('');
                $("#sottogruppo").html(data);
            },
            error : function () {
                alert("ERRORE RICHIESTA AJAX!");
            }
        });
    }
    function ShowCampiPreferiti()
    {
        var indirizzo_blocco_preferenze=controller_url+'ajax_load_block_macrogruppo_preferenze/desktop';
        $.ajax({
            url : indirizzo_blocco_preferenze,
            success : function (data) {
                $('#sottogruppo').html('');
                $("#sottogruppo").html(data);
            },
            error : function () {
                alert("ERRORE RICHIESTA AJAX!");
            }
        });
    }
    
    function ChangeArchives()
    {
        var indirizzo= controller_url + 'ajax_load_block_campi_preferenze/desktop'; // variabile da inizializzare al primo script - mi servirà per la richiesta ajax di carico del blocco campi ricerca
        var chiave = $('.ui-selected').data("chiave");
        //alert(stringaSelezionata.toUpperCase());
        if ((chiave == "keylabel") || (chiave=='keylabel_scheda') || (chiave=='keylabel_inserimento'))
            indirizzo= controller_url + 'ajax_load_block_labels';
        var url;
        if($('#archivio').val()!="null"){
                url=indirizzo + '/' + $('#archivio').val();
            $.ajax( {
                type: "POST",
                url: url,
                success: function(response) {
                    if(chiave!='creazione_campi'){
                        var campipreferenze= $('#campipreferenze');
                        $(campipreferenze).html('');
                        $(campipreferenze).append(response);
                    }
                },
                error: function()
                    {
                        alert("Errore Richiesta Ajax");
                    }
            } ); 
        }
        if ((chiave == "keylabel") || (chiave=='keylabel_scheda') || (chiave=='keylabel_inserimento'))
            LoadSavedPreferencesLabel($('#archivio').val(),chiave);
        else
            LoadSavedPreferences_NewVersion($('#archivio').val());
    }
    
    function ChangeArchivesCampiArchivi()
    {
        var indirizzo= 'sys_viewcontroller/ajax_load_block_campi_archivi_preferenze/desktop'; // variabile da inizializzare al primo script - mi servirà per la richiesta ajax di carico del blocco campi ricerca
        var url;
        //alert("ciao");
        //alert($('#archivio').val());
        if($('#archivio').val()!="null"){
                url=indirizzo + '/' + $('#archivio').val();
           // alert(url);
            $.ajax( {
                type: "POST",
                url: url,
                success: function( response ) {
                    $('#campipreferenze').html('');
                    $('#campipreferenze').append(response);
                },
                error: function()
                    {
                        alert("Errore Richiesta Ajax");
                    }
            } ); 
        }
            LoadSavedPreferences_NewVersion($('#archivio').val());
    }
    
    function ShowLayoutSettings()
    {
        var indirizzo_blocco_layout=controller_url + 'ajax_load_block_macrogruppo_layout/desktop';
        $.ajax({
            url: indirizzo_blocco_layout,
            success : function (data) {
                $('#sottogruppo').html('');
                $("#sottogruppo").html(data);
            },
            error : function () {
                alert("ERRORE RICHIESTA AJAX!");
            }
        });
    }
    
    function ShowSuperuserSettings()
    {
        var processogit= controller_url + 'run_git';
        $.ajax({
           url: processogit,
           success:function(data)
           {
               alert("PROGRAMMA AVVIATO CORRETTAMENTE");
           },
           error:function(data)
           { alert("ERRORE RICHIESTA AJAX");}
        });
        /*var indirizzo_blocco_superuser_settings = controller_url + 'ajax_load_blocco_superuser_settings/';
        $.ajax({
            url: indirizzo_blocco_superuser_settings,
            success:function(data)
            {
                $('#sottogruppo').html('');
                $('#sottogruppo').html(data);
            },
            error:function()
            {
                alert("ERRORE RICHIESTA AJAX");
            }
        });*/
    }
    
    function LoadSavedPreferencesLabel(idarchivio,typeLabels)
    {
        var address=controller_url + 'ajax_load_block_LoadPreferencesLabel/' + idarchivio + '/' + typeLabels;
        $.ajax({
            url : address,
            success : function (data) {
                $("#precaricato").html("<div align='right'><button onclick='" + bottoneSalvataggio + "'><i><b>Salva Preferenze</b></i></button></div><br>" + data);
                $( "#sortable" ).sortable({
                    placeholder: "ui-state-highlight",
                    update: function(event, ui) {
                        newOrderMacroGruppoPreferenze = ""+$(this).sortable('toArray').toString();
                        $('#sortable').children().each(function()
                        {
                            var elementoLI = $(this);
                            $(elementoLI).children().each(function()
                            {
                                var sottoInput = $(this);
                                if($(sottoInput).data('type')=="campoposition")
                                {
                                    var posizione = $(elementoLI).index();
                                    $(sottoInput).val(posizione);
                                }
                            });
                            //console.log("New position: " + $(this).index());  
                        });
                    }
                });
                $( "#sortable" ).disableSelection();
            },
            error : function () {
                alert("ERRORE CARICAMENTO AJAX");
            }
        });
    }
    
    function LoadSavedPreferences_NewVersion(idarchivio)
    {
        //questa funzione è da riguardare perchè usa gli indici
        var address=controller_url + 'ajax_load_block_LoadPreferencesNewVersion' + '/' + idarchivio + '/' + $('.ui-selected').data('chiave');
        //alert(address);
        $.ajax({
            url : address,
            success : function (data) {
                $('#precaricato').html("<div align='right'><button onclick='" + bottoneSalvataggio + "'><i><b>Salva</b></i></button></div><br>" + data);
                $("#sortable").sortable({
                    placeholder: "ui-state-highlight",
                    update: function(event, ui) {
                        //newOrderMacroGruppoPreferenze = ""+$(this).sortable('toArray').toString();
                        //console.log("newOrder: " + newOrderMacroGruppoPreferenze);
                        $('#sortable').children().each(function()
                        {
                            var elementoLI = $(this);
                            $(elementoLI).children().each(function()
                            {
                                //var sottoInput = $(this);
                                var datatype = $(this).data('type');
                                if(datatype=="campoposition")
                                {
                                    var posizione = $(elementoLI).index();
                                    $(this).val(posizione);
                                }
                            });
                            //console.log("New position: " + $(this).index());  
                        });
                    }
                });
                $("#sortable").disableSelection();
            },
            error : function () {
                alert("ERRORE CARICAMENTO AJAX");
            }
        });
    }
    
//IMPOSTAZIONI - MACROGRUPPO PREFERENZE
    //var indirizzo_blocco_campi_tabella="<?php echo site_url('sys_viewcontroller/ajax_load_block_impostazioni_campi_tabella'); ?>";
    var newOrderMacroGruppoPreferenze;
    var bottoneSalvataggio="";
    var svuotaCampiPreferenze = false; //questa variabile serve per fare svuota il div "campipreferenze" (che viene riempito con l'evento changes archives)
    
    function ChangeSelection(typepreference){
    var IndirizzoLoadPreferences= controller_url + 'ajax_load_block_LoadSavedPreferences/';
    var indirizzo_blocco_campi= controller_url + 'ajax_load_block_impostazioni_campi';
    var IndirizzoLoadArchives= controller_url + 'ajax_load_block_archives_list';
   
    svuotaCampiPreferenze = false;//imposto l'ordine di svotare il div campiPreferenze quando faro' click su ChangesArchives
    //scelgo il bottone da mettere nel salva del blocchetto di sinistra
    switch(typepreference)
    {
        case 'campiFissi':
            bottoneSalvataggio='GenericSavePreferences();';
            break;
        case 'campiricerca':
            bottoneSalvataggio='GenericSavePreferences();';
            break;
        case 'campischeda':
            bottoneSalvataggio='GenericSavePreferences();';
            break;
        case 'risultatiricerca':
            bottoneSalvataggio='GenericSavePreferences();';
            break;
        case 'schedanavigatore':
            bottoneSalvataggio='SavePreferencesNavigatoreSchede();';
            break;
        case 'keylabel':
            bottoneSalvataggio='GenericSavePreferences();';
            //indirizzo_blocco_campi=controller_url + 'ajax_load_block_labels/AZIEND';
            break;
        case 'keylabel_scheda':
            bottoneSalvataggio='GenericSavePreferences();';
            break;
        case 'campiInserimento':
            bottoneSalvataggio='GenericSavePreferences();';
            break;
        case 'keylabel_inserimento':
            bottoneSalvataggio='GenericSavePreferences();';
            break;
    }
    $.ajax({
            url : indirizzo_blocco_campi + '/' + 'desktop',
            success : function (data) {
                $('#centrale').html('');//svuoto la parte centrale della pagina impostazioni
                //e la divido in destra e sinistra
                $('#centrale').html("<div id='sinistra' style='width: 49%; height: 100%; float: left; border: 1px solid black; overflow-y: scroll;'></div><div id='destra' style='width: 49%; height: 100%; float: left; border: 1px solid black; overflow-y: scroll;'><div>");
                $('#destra').append("<form id='FormCreazioneCampi' onsubmit='return false'><div id='precaricato' style='width: 100%;'></div></form>");
                $('#sinistra').html('');//sinistra svuoto
                $("#sinistra").html(data);//ci metto l'elenco archivi
                //metto il sortable
                //$('#destra').html("<div align='right'><button onclick='" + bottoneSalvataggio + "'><i><b>Salva Preferenze</b></i></button></div><br><div align='center'><ul id='sortable'><li class='ui-state-default' id='placeholder'><span class='ui-icon ui-icon-arrowthick-2-n-s'></span>Add Element Here</li></ul></div>");
                /*$( "#sortable" ).sortable({
                    placeholder: "ui-state-highlight",
                    update: function(event, ui) {
                        newOrderMacroGruppoPreferenze = ""+$(this).sortable('toArray').toString();
                    }
                });
                $( "#sortable" ).disableSelection();*/
                },
            error : function () {
                alert("ERRORE RICHIESTA AJAX!");
            }
        });
    }
    
    function addElementCampiPreferiti(idelemento){ //questa funzione viene richiamata dalla pagina php lista campi
            if($(".ui-selected").data("chiave")=='schedanavigatore')
                $('#sortable').html('');
            
            var contaLiPresenti = $('#sortable li').length;
            var idSplittato = idelemento.split('-');
            var fieldid=$.trim(idSplittato[0]);
            var tableid=$.trim(idSplittato[1]);
            var description = $('#' + idelemento).text();
            description = $.trim(description);
            
            $('#accordion').find('#'+idelemento).remove();//rimuovo l'elemento nella colonna di sinistra
            $('#sortable').append('<li id="' + idelemento + '" class="ui-state-default">'+description+"</li>");
            //newOrderMacroGruppoPreferenze = ""+$('#sortable').sortable('toArray').toString();//aggiorno l'ordine del contenuti
            //imposto tutte le proprietà dell'elemento appena clonato come il name e tutti i campi hidden
            $('#'+idelemento).append('<input data-type="campodescription" type="text" style="width: 100%; display: none;" name="fields[' + contaLiPresenti + '][description]" value="' + description + '">');
            $('#'+idelemento).append('<input data-type="campofieldid" type="hidden" style="width: 100%;" name="fields[' + contaLiPresenti + '][fieldid]" value="' + fieldid + '">');
            $('#'+idelemento).append('<input data-type="campoidarchivio" type="hidden" style="width: 100%;" name="fields[' + contaLiPresenti + '][idarchivio]" value="' + tableid + '">');
            $('#'+idelemento).append('<input data-type="campoposition" type="hidden" style="width: 100%;" name="fields[' + contaLiPresenti + '][position]" value="' + contaLiPresenti + '">');
            
            if($(".ui-selected").data("chiave")=='schedanavigatore')
            {
                var response=confirm("Confermi questa opzione?");
                if(response==true)
                    GenericSavePreferences();
                if(response==false)
                    $('#sortable').html('');
            }
    }
    
    function SavePreferencesCampiPreferiti()
    {
        var chiave = $('.ui-selected').data("chiave");
        var str;//stringa di appoggio
        var n; // nuova stringa (quella che sar√† passata alla funzione in codeigniter)
        var url=controller_url + 'set_preferences/';
        str=newOrderMacroGruppoPreferenze;
        n=str.replace(/,/g,"___");
        url=url+'/'+ n + '/' + chiave + '/' + idutente;
        //alert(url);
        $.ajax({
                    url : url,
                    success : function (){
                        alert ("IMPOSTAZIONI SETTATE! ");
                    },
                    error : function () {
                        alert("C'√® stato un errore");
                    }
                });
    }
    
    function SavePreferencesCampiRicerca()
    {
        var chiave = $('.ui-selected').data("chiave");
        var str;//stringa di appoggio
        var n; // nuova stringa (quella che sar√† passata alla funzione in codeigniter)
        var url= controller_url + 'set_preferences';
        str=newOrderMacroGruppoPreferenze;
        n=str.replace(/,/g,"___");
        url=url+'/'+ n + '/' + chiave + '/' + idutente;
        $.ajax({
                    url : url,
                    success : function (){
                        alert ("IMPOSTAZIONI SETTATE!");
                    },
                    error : function () {
                        alert("C'√® stato un errore");
                    }
                });
    }
    
    function SavePreferencesCampiScheda()
    {
        var chiave = $('.ui-selected').data("chiave");
        var str;//stringa di appoggio
        var n; // nuova stringa (quella che sar√† passata alla funzione in codeigniter)
        var url=controller_url + 'set_preferences';
        str=newOrderMacroGruppoPreferenze;
        n=str.replace(/,/g,"___");
        url=url+'/'+ n + '/' + chiave + '/' + idutente;
        //alert(url);
        $.ajax({
                    url : url,
                    success : function (){
                        alert ("IMPOSTAZIONI SETTATE!");
                    },
                    error : function () {
                        alert("C'√® stato un errore");
                    }
                });
    }
    function SavePreferencesRisultatiRicerca()
    {
        var dati = $('#FormCreazioneCampi').serialize();
        var chiave = $('.ui-selected').data("chiave");
        var url=controller_url + 'set_preferences';
        /*var str;//stringa di appoggio
        var n; // nuova stringa (quella che sarà passata alla funzione in codeigniter
        str=newOrderMacroGruppoPreferenze;
        n=str.replace(/,/g,"___");*/
        url=url+'/' + chiave + '/' + idutente;
        //alert(url);
        $.ajax({
                    url : url,
                    data: dati,
                    type: 'post',
                    success : function (){
                        alert ("IMPOSTAZIONI SETTATE!");
                    },
                    error : function () {
                        alert("C'è stato un errore");
                    }
                });
    }
    function SavePreferencesNavigatoreSchede()
    {
        var str;//stringa di appoggio
        var n; // nuova stringa (quella che sar√† passata alla funzione in codeigniter
        var url=controller_url + 'set_preferences_TabNavigator';
        str=newOrderMacroGruppoPreferenze;
        //alert(newOrder);
        n=str.replace(/,/g,"___");
        url=url+'/'+ n;
        //alert(url);
        $.ajax({
                    url : url,
                    success : function (){
                        alert ("IMPOSTAZIONI SETTATE!");
                    },
                    error : function () {
                        alert("C'√® stato un errore");
                    }
                });
    }    
    function SavePreferencesKeyLabel()
    {
        var chiave = $('.ui-selected').data("chiave");
        var str;//stringa di appoggio
        var n; // nuova stringa (quella che sar√† passata alla funzione in codeigniter)
        var url=controller_url + 'set_preferences';
        str=newOrderMacroGruppoPreferenze;
        //alert(NewOrder);
        n=str.replace(/,/g,"___");
        url=url+'/'+ n + '/' + chiave + '/' + idutente;
        //alert(url);
        $.ajax({
                    url : url,
                    success : function (){
                        alert ("IMPOSTAZIONI SETTATE!");
                    },
                    error : function () {
                        alert("C'√® stato un errore");
                    }
                });
    }
    function SavePreferencesKeyLabelScheda()
    {
        var chiave = $('.ui-selected').data("chiave");
        var str;//stringa di appoggio 
        var n; // nuova stringa (quella che sar√† passata alla funzione in codeigniter)
        var url=controller_url + 'set_preferences';
        str=newOrderMacroGruppoPreferenze;
        //alert(NewOrder);
        n=str.replace(/,/g,"___");
        url=url+'/'+ n + '/' + chiave + '/' + idutente;
        //alert(url);
        $.ajax({
                    url : url,
                    success : function (){
                        alert ("IMPOSTAZIONI SETTATE!");
                    },
                    error : function () {
                        alert("C'√® stato un errore");
                    }
                });
    }
    function SavePreferencesCampiInserimento()
    {
        var chiave = $('.ui-selected').data("chiave");
        if(chiave=='creazione_campi')
            chiave='campiInserimento';
        var str;//stringa di appoggio 
        var n; // nuova stringa (quella che sarà passata alla funzione in codeigniter)
        var url=controller_url + 'set_preferences';
        str=newOrderMacroGruppoPreferenze;
        //alert(NewOrder);
        n=str.replace(/,/g,"___");
        url=url+'/'+ n + '/' + chiave + '/' + idutente;
        //alert(url);
        $.ajax({
                    url : url,
                    success : function (){
                        alert ("IMPOSTAZIONI SETTATE!");
                    },
                    error : function () {
                        alert("C'è stato un errore");
                    }
                });
    }
    function SavePreferencesKeyLabelInserimento()
    {
        var chiave = $('.ui-selected').data("chiave");
        var str;//stringa di appoggio 
        var n; // nuova stringa (quella che sar√† passata alla funzione in codeigniter)
        var url=controller_url + 'set_preferences';
        str=newOrderMacroGruppoPreferenze;
        //alert(NewOrder);
        n=str.replace(/,/g,"___");
        url=url+'/'+ n + '/' + chiave + '/' + idutente;
        //alert(url);
        $.ajax({
                    url : url,
                    success : function (){
                        alert ("IMPOSTAZIONI SETTATE!");
                    },
                    error : function () {
                        alert("C'√® stato un errore");
                    }
                });
    }
    
    function SaveCampiCollegati()
    {
        var str;//stringa di appoggio
        var n; // nuova stringa (quella che sar√† passata alla funzione in codeigniter)
        var url=controller_url + 'set_preferences_tabelle_collegate/' + $('#archiviomaster').val() + '/' + $('#archiviolinked').val();
        str=newOrderMacroGruppoPreferenze;
        n=str.replace(/,/g,"___");
        url=url+'/'+ n + '/' + idutente;
        $.ajax({
                    url : url,
                    success : function (){
                        alert ("IMPOSTAZIONI SETTATE! ");
                    },
                    error : function () {
                        alert("C'è stato un errore");
                    }
                });
    }
    
        function GenericSavePreferences()
    {
        var dati = $('#FormCreazioneCampi').serialize();
        var chiave = $('.ui-selected').data("chiave");
        var url=controller_url + 'set_preferences';
        /*var str;//stringa di appoggio
        var n; // nuova stringa (quella che sarà passata alla funzione in codeigniter
        str=newOrderMacroGruppoPreferenze;
        n=str.replace(/,/g,"___");*/
        url=url+'/' + chiave + '/' + idutente;
        //alert(url);
        $.ajax({
                    url : url,
                    data: dati,
                    type: 'post',
                    success : function (){
                        alert ("IMPOSTAZIONI SETTATE!");
                    },
                    error : function () {
                        alert("C'è stato un errore");
                    }
                });
    }
    
    function addElementCampiCollegaTabelle(idelemento)
    {
        $('#sortable').find( "#placeholder" ).remove();
        //alert($('#'+idelemento).text());
        $('#sortable').append('<li id="'+$('#'+idelemento).attr('id')+'" class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'+$('#'+idelemento).text()+"</li>");
        $('#accordion').find('#'+idelemento).remove();
        newOrderMacroGruppoPreferenze = ""+$('#sortable').sortable('toArray').toString();//aggiorno l'ordine del contenuti
    }
    
    function ChangeLayoutChoise(choise)
    {
        var indirizzo_settings_layout=controller_url + 'ajax_load_block_settings_layout';
        var indirizzo_precaricamento_layout=controller_url + 'ajax_load_block_precaricamento_layout';
        //RICHIESTA AJAX PER IMPOSTARE LA STRUTTURA
        $.ajax({
                    url : indirizzo_settings_layout + '/' + choise,
                    success : function(data) {
                        $("#centrale").html('');
                        $('#centrale').html('<div id="sinistra" style="width: 49%;height: 100%; float: left; border: 1px solid black; overflow-y: scroll;"></div><div id="destra" style="width: 49%;height: 100%; float: right; border: 1px solid black; overflow-y: scroll;"><div align="center"><h3>ATTUALI<h3></div></div>');
                        $('#sinistra').html(data);
                     },
                    error : function() {
                        alert("ERRORE AJAX SETTINGS LAYOUT");
                    }
                });
        /*RICHIESTA AJAX PER PRE-CARICARE LE IMPOSTAZIONI ATTUALI*/
        $.ajax({
                    url : indirizzo_precaricamento_layout + '/' + choise,
                    success : function(data) {
                        $('#destra').html(data);
                     },
                    error : function() {
                        alert("ERRORE PRECARICAMENTO LAYOUT");
                    }
                });
    }
    
    function SavePreferencesLayoutDashBoard()
    {
        var AddressSalvataggio=controller_url + 'set_preferences_layout';
        var temp='';
        
        if($('#ultimicandidati').is(':checked'))
            temp=temp + 'lasttenrecords_';
        
        if($('#candidatiscadenza').is(':checked'))
            temp=temp + 'candidatiscadenza_';
        
        if($('#compleanni').is(':checked'))
            temp=temp + 'happybirthdays_';
        
        if($('#updatesoftware').is(':checked'))
            temp=temp + 'softwareupdates_';
        
        if($('#candidatiattivi').is(':checked'))
            temp=temp + 'candidatiattivi_';
        
        if($('#aziendeattive').is(':checked'))
            temp=temp + 'aziendeattive_';
        
        var indice=temp.length - 1;
        var stringa=temp.substr(0,indice);
        $.ajax({
            url: AddressSalvataggio + '/' + stringa + '/dashboard',
            success:function(){
                alert('IMPOSTAZIONE SETTATE!');
            },
            error: function(){
                alert("ERRORE AJAX DASHBOARD");
            }
        });
    }
    
    function SavePreferencesLayoutSchede()
    {
        var AddressSalvataggio=controller_url + 'set_preferences_layout';
        var dati=$("input[name=dati]:checked").val();
        var allegati=$("input[name=allegati]:checked").val();
        var appoggio=AddressSalvataggio + '/' + dati + '_' + allegati + '/schede';
        $.ajax({
            url: appoggio,
            success : function(){
               alert('IMPOSTAZIONI SETTATE!');
            },
            error : function(){
                alert('ERRORE AJAX SCHEDE');
            }
        });
    }


function ajax_load_block_esporta_risultati(el,tableid){
    $.ajax({
        
                url: controller_url+'ajax_load_block_esporta_risultati/'+tableid,
                dataType:'html',
                success:function(data){
                    $('#esporta_risultati').html(data);
                    $('#form_esporta').find('#query').val($('#form_riepilogo').find('#query').val()); 
                    $('#esporta_risultati').bPopup();
                },
                error:function(){
                    alert('errore');
                }
            });
}

function SavePreferencesLayoutFont()
{
    var AddressSalvataggio = controller_url + 'set_preferences_layout';
    var dato=$("input[name=fontsize]:checked").val();
    var url=AddressSalvataggio + '/' + dato + '/font';
    $.ajax({
        url: url,
        success:function(){ alert("IMPOSTAZIONI SETTATE!");},
        error:function(){ alert("ERRORE AJAX FONT");}
    });     
}


    
function ajax_load_block_jpgcrop(el,recordid,cartella,nomefile)
{
    cartella=cartella.replace(/\//g,"-");
    var url=controller_url+'ajax_load_block_jpgcrop/'+recordid+'/'+cartella+'/'+nomefile;
    $.ajax({
        
                url: url,
                dataType:'html',
                success:function(data){
                    
                    var scheda_container=$(el).closest('.scheda_container')
                    $(scheda_container).html(data);
                    //$('.block_popup').bPopup();
                },
                error:function(){
                    alert('errore');
                }
            });
}

function ajax_cropImg(el,recordid,cartella)
{
    var form=$(el).closest('form');
    var scheda_record=$(el).closest('.scheda_record');
    var tableid=$(scheda_record).data('tableid');
    //var tableid=global_tableid;
   var url=controller_url+'ajax_cropImg/'+tableid+"/"+recordid;
    $.ajax({
        
                url: url,
                data:$(form).serialize(),
                type: 'post',
                success:function(data){
                    
                    alert('Immagine impostata');
                    update_scheda(scheda_record, recordid, tableid);
                    //var pop=$('.block_popup').bPopup();
                    //pop.close();
                },
                error:function(){
                    alert('errore');
                }
            }); 
}

function LoadCollegamentiTabelle()
{
    var indirizzo_blocco_campi = controller_url + '/ajax_load_block_impostazioni_collega_tabelle';
    $.ajax({
        url : indirizzo_blocco_campi,
        success:function(data){
            $('#centrale').html("<div id='sinistra' style='width: 49%; height: 100%; float: left; border: 1px solid black; overflow-y:scroll;'></div><div id='destra' style='width: 49%; height: 100%; float: left; border: 1px solid black; overflow-y: scroll;'></div>");
            $('#sinistra').html(data);
            $('#destra').html("<div align='right'><button onclick='SaveCampiCollegati();'><i><b>Salva Preferenze</b></i></button></div><br><div align='center'><ul id='sortable'><li class='ui-state-default' id='placeholder'><span class='ui-icon ui-icon-arrowthick-2-n-s'></span>Add Element Here</li></ul></div>");
            $( "#sortable" ).sortable({
                placeholder: "ui-state-highlight",
                update: function(event, ui) {
                    newOrderMacroGruppoPreferenze = ""+$(this).sortable('toArray').toString();
                }
                });
            $( "#sortable" ).disableSelection();
        },
        error:function(data){
            alert(data);
        }
    });
}

function LoadCreazioneCampi()
{
    svuotaCampiPreferenze = true;
    var indirizzo_blocco_campi= controller_url + '/ajax_load_block_impostazioni_campi';
    //carico i tipi di campo
    var indirizzo_tipi_campo = controller_url + '/ajax_load_block_tipi_campi';
    $.ajax({
        url: indirizzo_tipi_campo,
        success:function(data)
        {
            $('#centrale').html("<div id='sinistra' style='width: 49%; height: 100%; float: left; border: 1px solid black; overflow-y:scroll;'></div><div id='destra' style='width: 49%; height: 100%; float: left; border: 1px solid black; overflow-y: scroll;'></div>");
            $('#sinistra').html(data);
            bottoneSalvataggio = "AddNewFieldsTable();";
                //faccio partire la richiesta ajax che carica gli archivi
            $.ajax({
                url: indirizzo_blocco_campi,
                success:function(data)
                {
                    $('#destra').html("<div id='ElencoArchivi' style='width: 100%;'></div>");
                    $('#destra').append("<form id='FormCreazioneCampi' onsubmit='return false'><div id='precaricato' style='width: 100%;'></div></form>");
                    $('#ElencoArchivi').html(data);
                    //creo il form e creo anche il tag ul per metterci dentro i vari <li> clonati
                },
                error:function()
                {
                    alert("ERRORE AJAX CARICAMENTO BLOCCO CAMPI");
                }
            });
        },
        error:function(){
            alert("ERRORE AJAX CARICAMENTO TIPO CAMPI");
        }
    });
}

function LoadCreazioneLabel()
{
    var indirizzo_blocco_creazione_label= controller_url + '/ajax_load_block_creazione_labels';
    $.ajax({
        url: indirizzo_blocco_creazione_label,
        success:function(data){
            $('#centrale').html(data);
        },
        error:function(){
            alert("ERRORE RICHIESTA AJAX");
        }
    });
}

function CreazioneArchivi()
{
    var indirizzo_blocco_creazione_archivi = controller_url + '/ajax_load_block_creazione_campi';
    $.ajax({
        url: indirizzo_blocco_creazione_archivi,
        success:function(data){ $('#centrale').html(data); },
        error:function(){ alert("ERRORE RICHIESTA AJAX!");}
    });
}

    function CreaArchivio()
    {
        var idarchivio = $('#idarchivio').val();
        var descrizione=$('#descrizione').val();
        var chk = $('#chklookupestesa').prop('checked');
        if((idarchivio=='')||(descrizione==''))
            alert("IDARCHIVIO O DESCRIZIONE MANCANTE!");
        else{
            var data="idarchivio=" + idarchivio + "&descrizione=" + descrizione + "&chklookupestesa=" + chk;
            $.ajax({
                url:controller_url + "/ajax_create_archive",
                data: data,
                type: "post",
                success:function(){
                    alert("ARCHIVIO CREATO CORRETTAMENTE");
                    //---CREAZIONE DELLA LABEL---//
                    $.ajax({
                        url: controller_url + 'create_new_label',
                        type: 'post',
                        data: "idarchivio=" + idarchivio + "&textlabel=Dati",
                        success:function()
                        {
                            //alert("CREAZIONE NUOVA LABEL AVVENUTA CORRETTAMENTE");
                        },
                        error:function(){
                            alert("ERRORE SALVATAGGIO LABEL");
                        }
                     });
                     
                     $.ajax({
                        url: controller_url + 'create_new_label',
                        type: 'post',
                        data: "idarchivio=" + idarchivio + "&textlabel=Office",
                        success:function()
                        {
                            //alert("CREAZIONE NUOVA LABEL AVVENUTA CORRETTAMENTE");
                        },
                        error:function(){
                            alert("ERRORE SALVATAGGIO LABEL");
                        }
                     });
                     //---FINE CREAZIONE DELLA LABEL---//
                },
                error:function(){ alert("ERRORE AJAX SALVA DATI!"); }
            });
        }
        //alert('IDARCHIVIO: ' + idarchivio + "\nDESCRIZIONE: " + descrizione + "\nCHECKBOX: " + chk);
    }

function AddNewFieldsTable()
{
    if($('#archivio').val()=="null")
        alert("SCEGLIERE PRIMA L'ARCHIVIO");
    else{
        var dati=$("#FormCreazioneCampi").serialize();
        if(dati!=""){
            $.ajax({
                url: controller_url + '/save_creazione_campi',
                data: dati,
                type: 'post',
                success:function(){
                    alert("SALVATAGGIO AVVENUTO CORRETTAMENTE");
                    ChangeArchives();
                },
                error:function(){
                    alert("ERRORE INSERIMENTO AJAX");
                    //SavePreferencesCampiInserimento();
                }
            });
        }
        else
            SavePreferencesCampiInserimento();
    }
}

function AggiungiInput(elemento)
{
    
    var numcampo = $(elemento).data('numcampo');
    var numoption = $(elemento).data('numoption');
    var campoimpost=$(elemento).closest('.campoimpost');
    var numfield=$(campoimpost).data('numfield');
    var DivParent = $(elemento).parents();
    var liContainer = DivParent[0];
    var CampoID = $('<input type="text" value="" placeholder="id" style="border: 1px solid;">');
    //$(liContainer).append('Valore Option:<br>' + CampoID + '<br>');
    $(liContainer).append(CampoID);
    $(CampoID).attr('name','fields[' + numfield + '][options][' + numoption + '][id]');
    var CampoDescription = $('<input type="text" value="" placeholder="description" style="border: 1px solid black;">');
    //$(liContainer).append(CampoDescription + '<br>');
    $(liContainer).append(CampoDescription);
    $(CampoDescription).attr('name','fields[' + numfield + '][options][' + numoption + '][description]');
    numoption++;
    $(elemento).data('numoption',numoption);//adesso incremento il numoption anche nell'html
    //$('#CampiAggiunti').append("<input type='text' style='border: 1px solid black;'><br>");
    indiceCategoria++;
}

     

function ClonaCategoriaBAK(elemento)
    {
        var elementoCreato = $(elemento).clone();
        $(elementoCreato).attr('onclick','').unbind('click');//tolgo l'evento onclick dall'elemento clonato
        
        //abilito la visione dei degli element per la tabella sys_lookup_table
        $(elementoCreato).children().children().each(function(){
            $(this).css('display','inline');
            $(this).attr('name','nuovocampo[' + ContatoreElementiCreati + '][categorie][' + indiceCategoria + '][' + $(this).attr('placeholder') + ']');
        });
        
        //questo mi serve per rendere visibile il bottone +
        $(elementoCreato).children().each(function(){
            $(this).css('display','inline');
            if($(this).attr('type')=='button')
                $(this).attr('onclick','AggiungiInput(this,' + indiceCategoria + ');');
            $(this).data('numcampo',"'" + ContatoreElementiCreati + "'");
        });
        $('#NuoviCampi').append(elementoCreato);
        indiceCategoria++;
        ContatoreElementiCreati++;
    }

    function ClonaCategoria(elemento)
    {
        if($('#archivio').val()=='null')
            alert("SELEZIONARE PRIMA L'ARCHIVIO!");
        else{
            var IndiceNuovoCampo = $('#sortable').children().length;//dato che gli indici partono da zero, in pratica è il numero di elementi presenti
            var elementoCreato = $(elemento).clone(); //clono l'elemento e lo metto in elementoCreato
            $(elementoCreato).attr('onclick','').unbind('click');//tolgo l'evento onclick dall'elemento clonato
            $(elementoCreato).attr('id','tipocampo' + IndiceNuovoCampo);//questo è l'id del nuovo <li> clonato
            $(elementoCreato).data('numfield',IndiceNuovoCampo);

            //abilito la visione dei degli element per la tabella sys_lookup_table
            $(elementoCreato).children().children().each(function(){
                $(this).css('display','inline');
                $(this).attr('name','fields[' + IndiceNuovoCampo + '][' + $(this).attr('placeholder') + ']');
                if($(this).attr('placeholder')=='idarchivio')
                    $(this).val($('#archivio').val());
                if($(this).attr('placeholder')=='campoposition')
                    $(this).val(IndiceNuovoCampo);
            });

            //questo mi serve per rendere visibile il bottone +
            $(elementoCreato).children().each(function(){
                $(this).css('display','inline');
                if($(this).attr('type')=='button')
                    $(this).attr('onclick','AggiungiInput(this,' + IndiceNuovoCampo + ');');
                $(this).data('numcampo',"'" + IndiceNuovoCampo + "'");
            });

            $('#sortable').append(elementoCreato);
            
            $(elementoCreato).children().children().each(function(){
                var sottoElemento = $(this);
                var placeholder=$(this).attr('placeholder');
                if(placeholder=='label')
                {
                    //faccio partire la richiesta ajax che carica le label
                    var url= controller_url + '/get_label_for_option/' + $('#archivio').val();
                    $.ajax({
                        url: url,
                        dataType: 'json',
                        success:function(data)
                        {
                            var Options = "";
                            $.each(data,function(index,value){
                                Options=Options + "<option value='" + value +"'>" + value + "</option>";
                            });
                            $(sottoElemento).html(Options);
                        },
                        error:function()
                        {
                            alert("ERRORE CARICAMENTO OPTION LABEL");
                        }
                    });
                }
            });
            
            ContatoreElementiCreati++;
        }
    }
    
    function ClonaElemento(elemento)
    {
        if($('#archivio').val()=='null')
            alert("SELEZIONARE PRIMA L'ARCHIVIO!");
        else
        {
            var IndiceNuovoCampo = $('#sortable').children().length;//dato che gli indici partono da zero, in pratica è il numero di elementi presenti
            var elementoCreato = $(elemento).clone(); //clono l'elemento e lo metto in elementoCreato
            $(elementoCreato).attr('onclick','').unbind('click');//tolgo l'evento onclick dall'elemento clonato
            $(elementoCreato).attr('id','tipocampo' + IndiceNuovoCampo);//questo è l'id del nuovo <li> clonato
            $(elementoCreato).children().each(function(){
                $(this).css('display','inline');
                $(this).attr('name','fields[' + IndiceNuovoCampo + '][' + $(this).attr('placeholder') + ']');
                if($(this).attr('placeholder')=='idarchivio')
                    $(this).val($('#archivio').val());
                if($(this).attr('placeholder')=='campoposition')
                    $(this).val(IndiceNuovoCampo);
            });
            $('#sortable').append(elementoCreato);

            $(elementoCreato).children().each(function(){
                var sottoElemento = $(this);
                var placeholder=$(this).attr('placeholder');
                if(placeholder=='label')
                {
                    //faccio partire la richiesta ajax che carica le label
                    var url= controller_url + '/get_label_for_option/' + $('#archivio').val();
                    $.ajax({
                        url: url,
                        dataType: 'json',
                        success:function(data)
                        {
                            var Options = "";
                            $.each(data,function(index,value){
                                //alert(value);
                                Options=Options + "<option value='" + value +"'>" + value + "</option>";
                            });
                            $(sottoElemento).html(Options);
                        },
                        error:function()
                        {
                            alert("ERRORE CARICAMENTO OPTION LABEL");
                        }
                    });
                }
            });
            ContatoreElementiCreati++;
        }
    }
    
    
    function file_container_menu_toggle(el)
    {
       $(el).find('.originalname').toggle();
       $(el).find('.file_container_menu').toggle();
       
    }
    
    function apri_blocco_code(el)
    {
        var scheda_allegati=$(el).closest('.scheda_allegati');
        var scheda_container=$(scheda_allegati).parent().closest('.scheda');
        var scheda_container_id=$(scheda_container).data('schedaid');
        if(scheda_container_id=='scheda_inserimento')
        {
            var scheda_code_container=$(scheda_container).find('.scheda_code_container');
            $(scheda_code_container).toggle();   
        }
        if(scheda_container_id=='scheda_record')
        {
            var visualizzatore=$(scheda_container).find('#visualizzatore');
            var scheda_code_container=$(scheda_container).find('.scheda_code_container');
            $(scheda_code_container).toggle(); 
            if($(scheda_code_container).is(":visible"))
            {
                $(visualizzatore).width('calc(100% - 300px)');
            }
            else
            {
                $(visualizzatore).width('calc(100% - 155px)');
            }
        }
    }
    function apri_blocco_autobatch(el)
    {
        var scheda_allegati=$(el).closest('.scheda_allegati');
        var scheda_container=$(scheda_allegati).parent().closest('.scheda');
        var scheda_container_id=$(scheda_container).data('schedaid');
        if(scheda_container_id=='scheda_inserimento')
        {
            var scheda_autobatch_container=$(scheda_container).find('.scheda_autobatch_container');
            $(scheda_autobatch_container).toggle();   
        }
        if(scheda_container_id=='scheda_record')
        {
            var visualizzatore=$(scheda_container).find('#visualizzatore');
            
            var scheda_autobatch_container=$(scheda_container).find('.scheda_autobatch_container');
            $(scheda_autobatch_container).toggle();  
            if($(scheda_autobatch_container).is(":visible"))
            {
                //$(visualizzatore).width('calc(100% - 300px)');
                $(visualizzatore).width($(visualizzatore).width()-150);
            }
            else
            {
               // $(visualizzatore).width('calc(100% - 155px)');
               $(visualizzatore).width($(visualizzatore).width()+150);
            }
        }
    }
    
    /*function notnull_field_onclick(el){
        var fieldcontainer=$(el).closest(".fieldcontainer");
        param_field_onclick(el,fieldcontainer,'notnull');
        $(fieldcontainer).find('.qualsiasi_layer').show();
    }
    
    function null_field_onclick(el){
        var fieldcontainer=$(el).closest(".fieldcontainer");
        param_field_onclick(el,fieldcontainer,'null');
        $(fieldcontainer).find('.nessuno_layer').show();
    }
    
    function currentuser_field_onclick(el){
        param_field_onclick(el,fieldcontainer,'currentuser');
    }
    */
    function param_field_onclick(el,param)
    {
        var fieldcontainer=$(el).closest(".fieldcontainer");
        var field_param=$(fieldcontainer).find('.field_param');
        var field_layer=$(fieldcontainer).find('.field_layer');
        var param_layer=$(fieldcontainer).find('.param_layer');
        $(field_layer).hide();
        var field=$(fieldcontainer).find('.field');
        $(field_param).val(param);
        if(param=='null')
        {
          $(param_layer).html('Nessun valore');  
        }
        if(param=='notnull')
        {
            $(param_layer).html('Almeno un valore');
        }
        if(param=='currentuser')
        {
            $(param_layer).html('Utente corrente');
        }
        if(param=='today')
        {
            $(param_layer).html('Oggi');
        }
        $(param_layer).show();
        $(field).hide();
        field_changed(field);
    }
    
    function not_field_onclick(el){
        var fieldcontainer=$(el).closest(".fieldcontainer");
        var field_param=$(fieldcontainer).find('.field_param');
        var field_layer=$(fieldcontainer).find('.field_layer');
        var field_not_layer=$(fieldcontainer).find('.field_not_layer');
        $(field_layer).hide();
        var field=$(fieldcontainer).find('.field');
        var value=$(field).val();
        $(field_not_layer).show();
        $(field).show();
        $(field_param).val('not');
        if(value!="")
        {
            field_changed(field);
        }
    }
    
    function or_field_onclick(el,autoOr,autoValue)
    {
        if (typeof autoOr === 'undefined') { autoOr = false; }
        if (typeof autoValue === 'undefined') { autoValue = ""; }
        var fieldcontainer=$(el).closest(".fieldcontainer");
        var field=$(fieldcontainer).find('.field');
        var fieldtypeid=$(fieldcontainer).data('fieldtypeid');
        if(fieldtypeid=='Utente')
        {
            jQuery(field).autocomplete("destroy");
            jQuery(field).removeData('autocomplete');
        }
        var fieldcontainer_cloned=fieldcontainer.clone(true,true);
        var field_cloned=$(fieldcontainer_cloned).find('.field');
        if(fieldtypeid=='Utente')
        {
            set_autocomplete(field);
            set_autocomplete(field_cloned);
        }
        $(field_cloned).val("");
        $(field_cloned).show();
        $(fieldcontainer_cloned).addClass('fieldcontainer_cloned');
        var fieldscontainer=fieldcontainer.closest(".fieldscontainer");
        var counter=fieldscontainer.attr("data-counter");
        var counter_modified=parseInt(counter)+1;
        fieldscontainer.attr("data-counter",counter_modified);
        var index_original=fieldcontainer.attr("data-index");
        var index_modified="f_"+counter_modified;
        fieldcontainer_cloned.attr("data-index",index_modified);
        id_original=$(fieldcontainer).attr("id");
        id_modified=id_original.replace(index_original,index_modified);
        fieldcontainer_cloned.attr("id",id_modified);
        var name_original;
        var name_modified;
        var id_original;
        var id_modified;
        fieldcontainer_cloned.find('[id*="'+index_original+'"]').each(function(i) {
            id_original=$(this).attr("id");
            id_modified=id_original.replace(index_original,index_modified);
            $(this).attr("id",id_modified);
            if($(this).is('[name]'))
            {
                name_original=$(this).attr("name");
                name_modified=name_original.replace(index_original,index_modified);
                $(this).attr("name",name_modified);
            }
        });
        //var field_cloned=$(fieldcontainer_cloned).find('.field');
        
        var field_operator_cloned=$(fieldcontainer_cloned).find('.field_operator');
        $(field_operator_cloned).val("");
        var field_layer_cloned=$(fieldcontainer_cloned).find('.field_layer');
        $(field_layer_cloned).hide();
        $(field_operator_cloned).val('or');
        $(fieldcontainer_cloned).find('.or_layer').show();
        fieldcontainer_cloned.attr('data-cloned', 'true');
        if(autoOr)
        {
            $(field_cloned).val(autoValue);
            $(fieldcontainer_cloned).css('display','none');
        }
        $(fieldcontainer).after(fieldcontainer_cloned);
        $(fieldcontainer_cloned).find('.field').blur(function() { 
            field_blurred($(this));
        });
    }
    
    function set_autocomplete(el){
        $.ajax({
            url: controller_url +'/ajax_get_users',
            dataType:'JSON',
            success:function(data){
                var fieldcontainer=$(el).closest(".fieldcontainer");
                var fieldLayer=$(fieldcontainer).find('.fieldLayer');
                var fieldValue0=$(fieldcontainer).find('.fieldValue0');
                $(fieldLayer).autocomplete({
                    minLength: 0,
                    source: data,
                    appendTo: fieldcontainer,
                    position: {  collision: "flip"  },
                    select: function( event, ui ) {
                        //var el_value=$('#<?= $fieldid . "-value-0" ?>');
                        $(fieldValue0).val(ui.item.value);
                        //var el_layer=$('#<?= $fieldid . "-layer" ?>');
                        $(fieldLayer).val(ui.item.label);
                        field_changed($(fieldValue0));
                        return false;
                    }
                  })
                  .autocomplete( "instance" )._renderItem = function( ul, item ) {
                          var li=$( "<li>" );
                          $(li).append( "<a></a>");
                          var a=$(li).find('a');
                          if((item.icon!='')&&(item.icon!=null))
                          {
                              $(a).append("<img class='autocomplete_icon_small' src='"+item.icon+"'></img>");
                          }
                          $(a).append(item.label);
                          if((item.desc!='')&&(item.desc!=null))
                          {
                              $(a).append('<br>');
                              $(a).append(item.desc);
                          }
                          $(li).appendTo( ul );
                          return li;
                          };
            }
        });
        
    }
    function multi_field_onclick(el){
        var fieldcontainer=$(el).closest(".fieldcontainer");
        var field=$(fieldcontainer).find('.field');
        var fieldtypeid=$(fieldcontainer).data('fieldtypeid');
        if(fieldtypeid=='Utente')
        {
            jQuery(field).autocomplete("destroy");
            jQuery(field).removeData('autocomplete');
        }
        var fieldcontainer_cloned=fieldcontainer.clone(true,true);
        var field_cloned=$(fieldcontainer_cloned).find('.field');
        if(fieldtypeid=='Utente')
        {
            set_autocomplete(field);
            set_autocomplete(field_cloned);
        }
        $(field_cloned).val("");
        $(field_cloned).show();
        //$(fieldcontainer_cloned).addClass('fieldcontainer_cloned');
        var fieldscontainer=fieldcontainer.closest(".fieldscontainer");
        var counter=fieldscontainer.attr("data-counter");
        var counter_modified=parseInt(counter)+1;
        fieldscontainer.attr("data-counter",counter_modified);
        var index_original=fieldcontainer.attr("data-index");
        var index_modified="f_"+counter_modified;
        fieldcontainer_cloned.attr("data-index",index_modified);
        id_original=$(fieldcontainer).attr("id");
        id_modified=id_original.replace(index_original,index_modified);
        fieldcontainer_cloned.attr("id",id_modified);
        var name_original;
        var name_modified;
        var id_original;
        var id_modified;
        fieldcontainer_cloned.find('[id*="'+index_original+'"]').each(function(i) {
            id_original=$(this).attr("id");
            id_modified=id_original.replace(index_original,index_modified);
            $(this).attr("id",id_modified);
            if($(this).is('[name]'))
            {
                name_original=$(this).attr("name");
                name_modified=name_original.replace(index_original,index_modified);
                $(this).attr("name",name_modified);
            }
        });
        
        
        var field_param_cloned=$(fieldcontainer_cloned).find('.field_param');
        $(field_param_cloned).val("");
        var field_layer_cloned=$(fieldcontainer_cloned).find('.field_layer');
        $(field_layer_cloned).hide();
        $(field_param_cloned).val('multi');
        fieldcontainer_cloned.attr('data-cloned', 'true');
        $(fieldcontainer_cloned).css('margin-top','-5px');
        $(fieldcontainer).after(fieldcontainer_cloned);
        $(fieldcontainer_cloned).find('.field').blur(function() { 
            field_blurred($(this));
        });
    }
    
    function range_field_onclick(el){
        var fieldcontainer=$(el).closest(".fieldcontainer");
        var fieldcontainer_cloned=fieldcontainer.clone(true,true);
        $(fieldcontainer_cloned).addClass('fieldcontainer_cloned');
        var fieldscontainer=fieldcontainer.closest(".fieldscontainer");
        var counter=fieldscontainer.attr("data-counter");
        var counter_modified=parseInt(counter)+1;
        fieldscontainer.attr("data-counter",counter_modified);
        var index_original=fieldcontainer.attr("data-index");
        var index_modified="f_"+counter_modified;
        fieldcontainer_cloned.attr("data-index",index_modified);
        id_original=$(fieldcontainer).attr("id");
        id_modified=id_original.replace(index_original,index_modified);
        fieldcontainer_cloned.attr("id",id_modified);
        var name_original;
        var name_modified;
        var id_original;
        var id_modified;
        fieldcontainer_cloned.find('[id*="'+index_original+'"]').each(function(i) {
            id_original=$(this).attr("id");
            id_modified=id_original.replace(index_original,index_modified);
            $(this).attr("id",id_modified);
            if($(this).is('[name]'))
            {
                name_original=$(this).attr("name");
                name_modified=name_original.replace(index_original,index_modified);
                $(this).attr("name",name_modified);
            }
        });
        var field_param=$(fieldcontainer).find('.field_param');
        var field_layer=$(fieldcontainer_cloned).find('.field_layer');
        $(field_layer).hide();
        $(field_param).val('from');
        $(fieldcontainer).find('.from_layer').show();
        var field_cloned=$(fieldcontainer_cloned).find('.field');
        $(field_cloned).val("");
        $(field_cloned).show();
        var field_param_cloned=$(fieldcontainer_cloned).find('.field_param');
        var field_layer_cloned=$(fieldcontainer_cloned).find('.field_layer');
        $(field_layer_cloned).hide();
        $(field_param_cloned).val('to');
        $(fieldcontainer_cloned).find('.to_layer').show();
        fieldcontainer_cloned.attr('data-cloned', 'true');
        $(fieldcontainer).after(fieldcontainer_cloned);
        $(fieldcontainer_cloned).find('.field').blur(function() { 
            field_blurred($(this));
        });
    }

  function show_query_riepilogo(el)
  {
      var scheda_dati_ricerca=$(el).closest('.scheda_dati_ricerca');
      var query_riepilogo=$(scheda_dati_ricerca).find('.query_riepilogo');
      $(query_riepilogo).toggle();
  }

function show_fieldMenu(el)
{
    var fieldContainer=$(el).closest('.fieldcontainer');
    var fieldMenu=$(fieldContainer).find('.fieldMenu');
    $(fieldMenu).toggle();
}

var NumeroUltimaOptionsCategoriaPreloaded = 0;

function InsertNewOption(numeroElencoOptions,lookuptableid)
{
    var NuovoNumOption = NumeroUltimaOptionsCategoriaPreloaded + 1;
    var indexName = $('#ElencoOptions' + numeroElencoOptions).data('indexname');
    var InputFieldid="<input type='text' name='fields[" + indexName + "][options][" + NuovoNumOption + "][itemcode]' placeholder='fieldid'>";
    var InputDescription = "<input type='text' name='fields[" + indexName + "][options][" + NuovoNumOption + "][itemdesc]' placeholder='description'>";
    var InputInsertOrUpdate = "<input type='hidden' name='fields[" + indexName + "][options][" + NuovoNumOption + "][insertorupdate]' value='insert'>";
    var InputLookuptableID = "<input type='hidden' name='fields[" + indexName + "][options][" + NuovoNumOption + "][lookuptableid]' value='" + lookuptableid + "'>";
    $('#ElencoOptions' + numeroElencoOptions).append(InputFieldid);
    $('#ElencoOptions' + numeroElencoOptions).append(InputDescription);
    $('#ElencoOptions' + numeroElencoOptions).append(InputInsertOrUpdate);
    $('#ElencoOptions' + numeroElencoOptions).append(InputLookuptableID);
    NumeroUltimaOptionsCategoriaPreloaded++;
}

function DeleteOption(indexname,indiceOptions,bottoneElimina)
{
    var itemcode=$('#fields_' + indexname + '__options__' + indiceOptions + '__itemcode_').val();
    var lookuptableid=$('#fields_' + indexname + '__options__' + indiceOptions + '__lookuptableid_').val();
    var url=controller_url + "/deleteOption";
    $.ajax({
        url: url,
        data: "itemcode=" + itemcode + "&lookuptableid=" + lookuptableid,
        type: 'post',
        success:function(){
            $('#fields_' + indexname + '__options__' + indiceOptions + '__itemcode_').remove();
            $('#fields_' + indexname + '__options__' + indiceOptions + '__itemdesc_').remove();
            $('#fields_' + indexname + '__options__' + indiceOptions + '__lookuptableid_').remove();
            $('#fields_' + indexname + '__options__' + indiceOptions + '__insertorupdate_').remove();
            $(bottoneElimina).remove();
            //alert("OPTION ELIMINATA CORETTAMENTE");
        },
        error:function(){alert("ERRORE DELETE OPTION");}
    });
}

function MostraOptions(elemento)
{
    var lookuptableid = $(elemento).data("lookuptableid");
    var lengthOptions = 0;
    var numciclo = $(elemento).data('numciclo');
    
    $(elemento).children().each(function(){
       var sottoElemento = $(this);
       var dataType = $(sottoElemento).data('type');
       if(dataType=="ElencoOptions"){
           var children = $(sottoElemento).children();
           lengthOptions = $(children).length;
       }
    });
    if((lookuptableid!="")&&(lookuptableid!=null))
    {
        if($(".butAddOption").length==0)
        {
            $(elemento).append("<button class='butAddOption' onclick='InsertNewOption(" + numciclo + ",\"" + lookuptableid + "\");' >+</button>");
        }
        $.ajax(
        {
            url: controller_url + "/ajax_get_options_lookuptableid/" + lookuptableid,
            dataType: 'json',
            success:function(data)
            {
                var indiceOptions = 0;
                $.each(data,function(index,value){
                    $(elemento).children().each(function(){
                        if(($(this).data("type")=="ElencoOptions") && (lengthOptions==0))
                        {
                            var indexName=$(this).data("indexname");
                            var eventoBottone = "DeleteOption(" + indexName + "," + indiceOptions + ",this);";
                            $(this).append("<input type='hidden' id='fields_" + indexName + "__options__" + indiceOptions +"__itemcode_' name='fields[" + indexName + "][options][" + indiceOptions +"][itemcode]' style='width: 100%;' value='" + value['itemcode'] + "'>");
                            $(this).append("<input type='text' id='fields_" + indexName + "__options__" + indiceOptions +"__itemdesc_' name='fields[" + indexName + "][options][" + indiceOptions +"][itemdesc]' style='width: 95%;' value='" + value['itemdesc'] + "'>");
                            $(this).append("<button onclick='" + eventoBottone + "' style='width: 5%;'>-</button>");
                            $(this).append("<input type='hidden' id='fields_" + indexName + "__options__" + indiceOptions +"__lookuptableid_' name='fields[" + indexName + "][options][" + indiceOptions +"][lookuptableid]' style='width: 100%;' value='" + lookuptableid +"'>");
                            $(this).append("<input type='hidden' id='fields_" + indexName + "__options__" + indiceOptions +"__insertorupdate_' name='fields[" + indexName + "][options][" + indiceOptions +"][insertorupdate]' style='width: 100%;' value='update'>");
                            NumeroUltimaOptionsCategoriaPreloaded = indiceOptions;
                        }
                    });
                    indiceOptions++;
                });
            },
            error:function()
            {
                alert("ERRORE AJAX MOSTRA OPTIONS!");
            }
        });
    }
}

function stampa_selezionati(el,tableid,recordid)
{
    MergedPDFtoPrint=document.getElementById('MergedPDFtoPrint');
    MergedPDFtoPrint.src='';
    var scheda_allegati=$(el).closest('.scheda');
    var form_riepilogo=$(scheda_allegati).find('.form_riepilogo');
    var counter=0;
    $(form_riepilogo).find('.file_checkbox').each(function(i){
        if($(this).prop('checked'))
            {
                counter++;
            }
    })
    if(counter>0)
    {
    var url=controller_url+'ajax_stampa_selezionati/'+tableid+'/'+recordid;
        $.ajax( {
            type: "POST",
            url: url,
            data: $(form_riepilogo).serialize(),
            success: function( response ) {
                $('#stampa_selezionati_popup').bPopup();
                
                MergedPDFtoPrint.src=response;
            },
            error:function(){
                alert('errore stampa selezionati');
            }
        } ); 
    }
    else
    {
        alert('Nessun allegato selezionato');
    }
}

  function conferma_stampa_selezionati(el)
  {
      MergedPDFtoPrint=document.getElementById('MergedPDFtoPrint'); 
      MergedPDFtoPrint.contentWindow.print();
  }

    function seleziona_tutti(el)
    {
        var files_container=$(el).closest('.files_container');
        $(files_container).find('.file_container').each(function(i){
            var checkbox=$(this).find('.file_checkbox');
            if($(el).prop('checked'))
            {
               $(checkbox).prop('checked', true); 
            }
            else
            {
                $(checkbox).prop('checked', false); 
            }
            
            
        })
    }
    
    function EliminaCampo(tableid,fieldid,lookuptableid,elementoLI,elementoBottone)
    {
        $.ajax({
            url: controller_url + '/elimina_campo/' + $('.ui-selected').data("chiave"),
            data: 'tableid=' + tableid + '&fieldid=' + fieldid + '&lookuptableid=' + lookuptableid,
            type: 'post',
            success:function(){
                $(elementoBottone).remove();
                $('#' + elementoLI).remove();
                alert("Campo Eliminato Correttamente");
            },
            error:function(){ alert("Errore ajax elimina campo"); }
        });
    }
    
    function svuota_campo(el,funzione)
    {
        var fieldcontainer=$(el).closest('.fieldcontainer');
        var field=$(fieldcontainer).find('.field');
        var field_param=$(fieldcontainer).find('.field_param');
        var field_layer=$(fieldcontainer).find('.field_layer');
        $(field_param).val("");
        $(field_layer).hide();
        $(field).val("");
        $(field).show();
        //var fieldcontainerid=$(fieldcontainer).attr('id');
        //var scheda_dati_ricerca_container=$(el).closest('.scheda_dati_ricerca_container');
        //var block_riepilogo=$(scheda_dati_ricerca_container).find('#block_riepilogo');
        //var finded=$(block_riepilogo).find('#'+fieldcontainerid);
        //$(finded).remove();
        field_changed(field);
    }
    
    
    function add_lookuptable_item(el,el_field_id,lookuptableid,fieldid,tableid,code)
    {
        var itemdesc=prompt("Inserisci valore");
        if((itemdesc!=null)&&(itemdesc!=''))
        {
         $.ajax({
                url: controller_url + '/ajax_add_lookuptable_item/' + lookuptableid + '/' + itemdesc,
                dataType:'html',
                success:function(data){ 
                    if(data!='null')
                    {
                        alert('Inserito correttamente');
                        var fieldcontainer=$(el).closest('.fieldcontainer');
                        var fieldvalue0=$(fieldcontainer).find('.fieldvalue0');
                        el_field_id=$(fieldvalue0).attr('id');
                        ajax_get_lookuptable_items(el,el_field_id,lookuptableid,fieldid,tableid,data);
                        field_changed(fieldvalue0);
                    }
                    else
                    {
                        alert('Valore già esistente');
                    }
                },
                error:function(){
                    
                    alert('error');
                }
            });
        }
    }

    function ajax_get_lookuptable_items(el,el_field_id,lookuptableid,fieldid,tableid,code)
    {
        $.ajax({
                url: controller_url + '/ajax_get_lookuptable/' + lookuptableid + '/' + fieldid + '/' + tableid,
                dataType:'JSON',
                success:function(data){  
                    var fieldValue= $('#'+el_field_id);
                    $(fieldValue).html('');
                    $(fieldValue).append('<option  id="" class="emptyOption" value="" > &#9660;</option>');
                    var last_val;
                    var selected='';
                    var style=''
                    $.each(data, function(key, val){ 
                        selected='';
                        if(val.itemcode==code)
                        {
                            selected='selected="selected"';
                        }
                        $(fieldValue).append('<option ' + selected + ' value="' + val.itemcode + '" data-link="'+val.link+'" data-linkfield="'+val.linkfield+'" data-linkvalue="'+val.linkvalue+'" data-linkedfield="'+val.linkedfield+'" data-linkedvalue="'+val.linkedvalue+'">' + val.itemdesc + '</option>');
                        last_val=val;   
                        if(selected!='')
                            {
                                $(fieldValue).css('color', 'black');
                            }
                    })
                    $(fieldValue).attr("data-linkedfield_select",last_val.linkedfield);
                    //var table_container=$(fieldValue).closest('.table_container');
                    //var first=$(table_container).find('.first');
                    //$(first).focus();
                    //$(fieldValue).combobox();
                },
                error:function(){
                    $('#'+el_field_id).html('<option id="-1">none available</option>');
                }
            });
    }
    
    function loadToJDocOnlineCV(el,tableid,recordid)
    {
        var input_allfields=$(el).find('#allfields');
        //var remote_url='http://localhost:8822/jdoconlinecv/index.php/sys_viewcontroller/ajax_set_allfields';
        var remote_url='http://workandwork.com/OnlineCV/index.php/sys_viewcontroller/ajax_set_allfields';
        var url=controller_url + '/ajax_get_allfields/' + tableid + '/' + recordid;
        $.ajax( {
            type: "html",
            url: url,
            success: function( response ) {
                var obj = JSON.parse(response);
                var mail='';
                if("candmail" in obj)
                    if("0" in obj["candmail"])
                        if("fields" in obj["candmail"][0])
                            if("indirizzo" in obj["candmail"][0]["fields"])
                                mail=obj["candmail"][0]['fields']['indirizzo']
                $(input_allfields).val(response);
                $.ajax( {
                    type: "POST",
                    url: remote_url,
                    data: $(input_allfields).serialize(),
                    success: function( response ) {
                        //var obj = JSON.parse(response);
                        $('#helper_popup').find('#response').html(response);
                        $('#helper_popup').find('#jdoconline_url').attr('href','http://workandwork.com/OnlineCV/index.php/sys_viewcontroller/view_onlineform/'+tableid+'/'+recordid);
                        $('#helper_popup').find('#jdoconline_url').html('http://workandwork.com/OnlineCV/index.php/sys_viewcontroller/view_onlineform/'+tableid+'/'+recordid);
                        //$('#helper_popup').find('#jdoconline_url').attr('href','http://localhost:8822/jdoconlinecv/index.php/sys_viewcontroller/view_onlineform/'+tableid+'/'+recordid);
                        //$('#helper_popup').find('#jdoconline_url').html('http://localhost:8822/jdoconlinecv/index.php/sys_viewcontroller/view_onlineform/'+tableid+'/'+recordid);
                        $( "#btn_invia_mail").unbind( "click" );    
                        $('#btn_invia_mail').click(function(){
                                window.open('mailto:'+mail+"?subject=Aggiornamento CV&body="+'http://workandwork.com/OnlineCV/index.php/sys_viewcontroller/view_onlineform/'+tableid+'/'+recordid);
                            })

                        
                        
                        $('#helper_popup').bPopup();
                    },
                    error:function(){
                        alert('errore');
                    }
                } ); 
            },
            error:function(){
                alert('errore');
            }
        } ); 
        
        
    }
    
    function ajax_get_new_records(el,tableid)
    {
        var url=controller_url + '/get_new_records/' + tableid;
        $.ajax( {
            type: "html",
            url: url,
            success: function( response ) {
                alert('Dati scaricati');
            },
            error:function(){
                alert('errore');
            }
        } ); 
    }

    
    function chiudi_scheda(el){
        var scheda=$(el).closest('.scheda');
        var popuplvl=$(scheda).data('popuplvl');
        try 
        {
            bPopup[popuplvl].close();
        }
        catch(err)
        {
            console.info('nopopup');
        }
        var container=$(el).closest('.scheda_container');
        var id=$(container).attr('id');
        if(id==ultimascheda)
        {
          ultimascheda="";
        }
        $('#nav_'+id).remove();
        container.remove();  
    }
    
    function chiudi_nuovo(el)
    {
        chiudi_scheda(el);
        var risultati_ricerca=$('#risultati_ricerca');
        var risultati_ricerca_btn_plus_right=$(risultati_ricerca).find('.btn_plus_right');
        var block_scheda_container=$(el).closest('.scheda_container');
        var block_dati_labels=$(block_scheda_container).find('.block_dati_labels');
        var tableid=$(block_dati_labels).data('tableid')
        apri_scheda_record(risultati_ricerca_btn_plus_right,tableid,'null','popup','allargata','risultati_ricerca');
    }
    
    function set_pinned(el)
    {
        var scheda=$(el).closest('.scheda');
        if($(scheda).data('pinned'))
        {
            $(scheda).data('pinned',false);
            $(el).css("color", "black"); 
        }
        else
        {
            $(scheda).data('pinned',true);
            $(el).css("color", "orange"); 
        }
        
    }
    
    function prestampa_scheda_record_completa(tableid,recordid)
    {
      var url=controller_url + '/ajax_prestampa_scheda_record_completa/' + tableid + '/' + recordid;
        $.ajax( {
            type: "html",
            url: url,
            success: function( response ) {
                $('#prestampa').html(response);
                prestampa_popup=$('#prestampa').bPopup();
                
            },
            error:function(){
                alert('errore');
            }
        } );   
    }
    
    function stampa_scheda_record_completa(el,tableid,recordid)
    { 
        prestampa_popup.close();
        var url=controller_url + '/ajax_stampa_scheda_record_completa/' + tableid + '/' + recordid;
        $.ajax( {
            type: "POST",
            url: url,
            data: $(el).closest('form').serialize(),
            success: function( response ) {
                $('#stampa').html(response);
                setTimeout(function(){
                    $('#stampa').show();
                   window.print(); 
                   $('#stampa').hide();
                },1000);
                
            },
            error:function(){
                alert('errore');
            }
        } ); 
    }
    
    function ajax_load_block_manuale()
    {
        var url=controller_url+'ajax_load_block_manuale';
        $.ajax({
                    url: url,
                    dataType:'html',
                    success:function(data){
                            $('#manuale_container').html(data);
                            $('#manuale_container').bPopup();
                            
                    },
                    error:function(){
                        alert('errore');
                    }
                });
    }
    function ajax_load_block_segnalazioni()
    {
        var url=controller_url+'ajax_load_block_segnalazioni';
        $.ajax({
                    url: url,
                    dataType:'html',
                    success:function(data){
                            $('#segnalazioni_container').html(data);
                            bPopup_segnalazione=$('#segnalazioni_container').bPopup();
                    },
                    error:function(){
                        alert('errore');
                    }
                });
    }
    
    function invia_segnalazione(el)
    {
        var url='http://46.14.191.65:8822/jdocweb/index.php/sys_viewcontroller/ajax_invia_segnalazione';
        //var url=controller_url+'ajax_invia_segnalazione';
        $.ajax( {
            type: "POST",
            url: url,
            data: $(el).closest('#form_segnalazione').serialize(),
            success: function( response ) {
                alert('Segnalazione inviata');
                bPopup_segnalazione.close();
            },
            error:function(){
                alert("E' necessario contattare direttamente l'assistenza");
            }
        } ); 
    }
    
    function save_view(el,tableid){
        var view_name=prompt("Nome ricerca");
        if((view_name!=null)&&(view_name!=''))
        {
            var scheda_dati_ricerca=$(el).closest('.scheda_dati_ricerca');
            var url=controller_url+'ajax_save_view/'+tableid;
            var form_riepilogo=$(scheda_dati_ricerca).find('.form_riepilogo');
            $(form_riepilogo).find('#view_name').val(view_name);
            $.ajax( {
                type: "POST",
                url: url,
                data: $(form_riepilogo).serialize(),
                success: function( response ) {
                    alert('salvata');

                },
                error:function(){
                    alert('errore');
                }
            } ); 
        }
    }
    
    function view_changed(el,tableid)
    {
        var viewid=$(el).val();
        var url=controller_url+'ajax_view_changed/'+tableid+'/'+viewid;
        $.ajax( {
            type: "url",
            url: url,
            success: function( response ) {
                var scheda_dati_ricerca=$(el).closest('.scheda_dati_ricerca');
                var form_riepilogo=$(scheda_dati_ricerca).find('.form_riepilogo');
                $(form_riepilogo).find('#query').val(response); 
                ajax_load_block_risultati_ricerca(el,tableid);
            },
            error:function(){
                alert("Errore");
            }
        } ); 
    }
    
    function save_report(el,tableid)
    {
            var form=$(el).closest('form');
            var url=controller_url+'ajax_save_report/'+tableid;
            $.ajax( {
                type: "POST",
                url: url,
                data: $(form).serialize(),
                success: function( response ) {
                    alert('salvata');

                },
                error:function(){
                    alert('errore');
                }
            } ); 
        
    }
    
    function set_default_view(el)
    {
        var scheda=$(el).closest('.scheda');
        var tableid=$(scheda).data('tableid');
        var saved_view_select=$(scheda).find('#saved_view_select');
        var viewid=$(saved_view_select).val();
        var url=controller_url+'ajax_set_default_view/'+tableid+'/'+viewid;
            $.ajax( {
                type: "url",
                url: url,
                success: function( response ) {
                    if(response=='ok')
                    {
                        alert('salvata');
                    }
                    else
                    {
                        alert('errore: '+response);
                    }

                },
                error:function(){
                    alert('errore');
                }
            } ); 
    }
    
    function refresh_risultati_ricerca(activetab_id)
    {
        var scheda_dati_ricerca=$('#scheda_dati_ricerca');
        var tableid=$(scheda_dati_ricerca).data('tableid');
        if(typeof activetab_id==='undefined')
        {
            activetab_id=$('#tabs_risultati_ricerca').find("[aria-selected='true']").attr('aria-controls')
        }
        var url='';
        if(activetab_id=='risultati_ricerca_datatable')
        {
            url=controller_url+'ajax_load_block_datatable_records/'+tableid+'/risultati_ricerca';
        }  
        if(activetab_id=='risultati_ricerca_report')
        {
            url=controller_url+'ajax_load_block_reports_relativi/'+tableid;
        }   
        if(activetab_id=='risultati_ricerca_calendar')
        {
            url=controller_url+'ajax_load_block_calendar/'+tableid;
        }   
        
       
        
        $.ajax( {
            type: "POST",
            url: url,
            data: $(scheda_dati_ricerca).find('#form_riepilogo').serialize(),
            success: function( response ) {
                console.info(response);
                $('#'+activetab_id).html(response);
            },
            error:function(){
                alert('errore');
            }
        } ); 
        
        
        
    }
    
    
    function esporta_xls(el,exportid)
    {
        $('#form_riepilogo').find('#exportid').val(exportid);
        $('#form_riepilogo').submit();
        
    }
